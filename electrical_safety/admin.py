from django.contrib import admin
from .models import *

admin.site.register(ElSafetyDocument)
admin.site.register(FieldElSafety)
admin.site.register(FieldInDocument)
admin.site.register(ReadyElSafetyDocument)
# Register your models here.
