# coding: utf8
from django.conf.urls import include, url
from .views import *


urlpatterns = [
    url(r'^instructions/$', ElInstructionView.as_view(), name='instructions'),
    url(r'^create_document/(?P<pk>[0-9]+)/$', DocumentCreate.as_view(), name='create_document',),
    url(r'^create_document/chain/(?P<q_pk>[0-9]+)/$', ChainDocumentCreate.as_view(), name='create_in_chain',),
    url(r'^create_document/el/$', CreateDocumentsElView.as_view(), name='create_document_el',),
    url(r'^update_leaders/(?P<pk>[0-9]+)/$', ElectricalLeadersUpdateView.as_view(), name='update_leaders',),
    url(r'^list/$', DocumentList.as_view(), name='list',),
]
