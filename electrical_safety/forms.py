# coding: utf8
from django import forms

from django.forms import BaseModelFormSet, modelformset_factory
from company.models import WorkProfession, EmployeePosition
from django.forms import ModelForm
from company.models import *
from .models import *
from django.core.exceptions import ValidationError, FieldError
from fire_safety.forms import customize_style
from fire_safety.models import Subdivision


class CompanyUpdateElLeadersForm(ModelForm):
    class Meta:
        model = Company
        fields = ['boss']

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company', None)
        super(CompanyUpdateElLeadersForm, self).__init__(*args, **kwargs)
        staff_position_company = StaffPosition.objects.filter(staff__in=Staff.objects.filter(company=self.company))
        self.fields['boss'].queryset = staff_position_company
        customize_style(self)


class Base(ModelForm):

    def __init__(self, *args, **kwargs):
        kwargs.pop('queryset', None)
        form_kwargs = kwargs.pop('form_kwargs', None)
        self.label = form_kwargs.get('label', None)
        self.company = form_kwargs.get('company', None)
        self.position_number = form_kwargs.get('position_number', None)
        self.document = form_kwargs.get('document', None)
        self.field_name = form_kwargs.get('field_name', None)
        self.default = form_kwargs.get('default', None)
        super(Base, self).__init__(*args, **kwargs)
        staff_id = [x.id for x in Staff.objects.filter(company=self.company)
                    if x.employee_positions.all().exists() or x.work_professions.all().exists()]
        mayor_staff = Company.objects.first().staff.first()
        staff_id.append(mayor_staff.id)
        if self.fields.get('employee'):
            self.fields['employee'].queryset = Staff.objects.filter(id__in=staff_id).order_by('surname')
            css_class = self.fields['employee'].widget.attrs.get('class')
            css_class = 'employee {}'.format(css_class) if css_class else 'employee'
            self.fields['employee'].widget.attrs.update({'class': css_class})
        if self.fields.get('staff_profession'):
            self.fields['staff_profession'].queryset = StaffProfession.objects.filter(staff__id__in=staff_id)
            self.fields['staff_profession'].widget.attrs.update({'class': 'professions'})
        if self.fields.get('staff_position'):
            self.fields['staff_position'].queryset = StaffPosition.objects.filter(staff__id__in=staff_id)
            self.fields['staff_position'].widget.attrs.update({'class': 'positions'})
        initial = {}
        try:
            instance = FieldElSafety.objects.get(
                company=self.company, document=self.document, field_name=self.field_name, position_number=self.position_number
            )
        except Exception:
            instance = ''
        if instance:
            self.instance = instance
            for field in self.fields:
                    initial.update({field: getattr(instance, field)})
            [initial.update({field: getattr(instance, field)}) for field in self.fields]
            [initial.update({key: initial.get(key).all()}) for key in initial.keys() if hasattr(initial.get(key), 'all')]
            self.initial = initial
        customize_style(self)

    def save(self, commit=True):
        instance = super(Base, self).save(commit)
        if instance:
            if instance.employee:
                positions = instance.employee.staffposition_set.all()
                professions = instance.employee.staffprofession_set.all()
                if instance.staff_profession not in professions and instance.staff_position not in positions:
                    if instance.employee.employee_positions.all():
                        instance.staff_position = instance.employee.staffposition_set.all().first()
                    else:
                        instance.staff_profession = instance.employee.staffprofession_set.all().first()
        return instance


class BaseForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.label = kwargs.pop('label', None)
        self.company = kwargs.pop('company', None)
        self.position_number = kwargs.pop('position_number', None)
        self.document = kwargs.pop('document', None)
        self.field_name = kwargs.pop('field_name', None)
        self.default = kwargs.get('default', None)
        super(BaseForm, self).__init__(*args, **kwargs)
        staff_id = [x.id for x in Staff.objects.filter(company=self.company)
                    if x.employee_positions.all().exists() or x.work_professions.all().exists()]
        mayor_staff = Company.objects.first().staff.first()
        staff_id.append(mayor_staff.id)
        if self.fields.get('employee'):
            self.fields['employee'].queryset = Staff.objects.filter(id__in=staff_id).order_by('surname')
            css_class = self.fields['employee'].widget.attrs.get('class')
            css_class = 'employee {}'.format(css_class) if css_class else 'employee'
            self.fields['employee'].widget.attrs.update({'class': css_class})
        if self.fields.get('staff_profession'):
            self.fields['staff_profession'].queryset = StaffProfession.objects.filter(staff__id__in=staff_id)
            self.fields['staff_profession'].widget.attrs.update({'class': 'professions'})
        if self.fields.get('staff_position'):
            self.fields['staff_position'].queryset = StaffPosition.objects.filter(staff__id__in=staff_id)
            self.fields['staff_position'].widget.attrs.update({'class': 'positions'})
        customize_style(self)

    def save(self, commit=True):
        instance = super(BaseForm, self).save(commit=True)
        if instance:
            if instance.employee:
                positions = instance.employee.staffposition_set.all()
                professions = instance.employee.staffprofession_set.all()
                if instance.staff_profession not in professions and instance.staff_position not in positions:
                    if instance.employee.employee_positions.all():
                        instance.staff_position = instance.employee.staffposition_set.all().first()
                    else:
                        instance.staff_profession = instance.employee.staffprofession_set.all().first()
        instance.save()
        return instance


class BaseFormSet(BaseModelFormSet):
    def __init__(self, *args, **kwargs):
        self.label = kwargs.get('form_kwargs').get('label')
        self.company = kwargs.get('form_kwargs').get('company')
        self.position_number = kwargs.get('form_kwargs').get('position_number')
        self.document = kwargs.get('form_kwargs').get('document')
        self.field_name = kwargs.get('form_kwargs').get('field_name')
        self.default = kwargs.get('form_kwargs').get('default')
        super(BaseFormSet, self).__init__(*args, **kwargs)
        self.queryset = self.model.objects.filter(
            company=self.company, document=self.document, position_number=self.position_number, field_name=self.field_name
        )
        self.extra = 1 if not self.queryset else 0

    def clean(self):
        super(BaseFormSet, self).clean()
        if self.field_name == 'F9':
            for form in self.forms:
                data = form.cleaned_data.get('employee')
                if not data:
                    raise ValidationError("Укажите сотрудника фирмы", code='invalid')


class F3Form(Base):
    class Meta:
        model = FieldElSafety
        fields = ['paragraph']
        widgets = {
            "paragraph": forms.Textarea(attrs={'rows': 2}),
            }


class F4Form(Base):
    class Meta:
        model = FieldElSafety
        fields = ['employee', 'staff_profession', 'staff_position']
        widgets = {
            'employee': forms.Select(attrs={'class': 'select_employee'}),
        }

    def clean_employee(self):
        data = self.cleaned_data['employee']
        if not data:
            raise ValidationError("Укажите сотрудника фирмы", code='invalid')
        return data


class F41Form(Base):
    class Meta:
        model = FieldElSafety
        fields = ['employee', 'staff_profession', 'staff_position']
        widgets = {
            'employee': forms.Select(attrs={'class': 'select_employee'}),
        }


class F7Form(Base):
    class Meta:
        model = FieldElSafety
        fields = ['date']
        widgets = {
            "date": forms.TextInput(attrs={'class': 'date', 'readonly': "true"}),
            }


class F8Form(Base):
    class Meta:
        model = FieldElSafety
        fields = ['document_number']


class F11Form(Base):
    class Meta:
        model = FieldElSafety
        fields = ['trigger']


class F12Form(Base):
    class Meta:
        model = FieldElSafety
        fields = ['pdf_file']

    # def clean_pdf_file(self):
    #     data = self.cleaned_data['pdf_file']
    #     if not data:
    #         raise ValidationError("Укажите сотрудника фирмы", code='invalid')
    #     return data


# Forms for Formsets
class F6Form(BaseForm):

    class Meta:
        model = FieldElSafety
        fields = ['employee', 'staff_profession', 'staff_position']


class F9Form(BaseForm):
    class Meta:
        model = FieldElSafety
        fields = ['employee', 'staff_profession', 'staff_position']
        widgets = {
            'employee': forms.Select(attrs={'class': 'select_employee'}),
        }

F6Formset = modelformset_factory(FieldElSafety, form=F6Form, formset=BaseFormSet, extra=0, can_delete=True)
F9Formset = modelformset_factory(FieldElSafety, form=F9Form, formset=BaseFormSet, extra=0, can_delete=True)

FORMSETS_PREFIX = ['formset_fio%s' % x for x in range(1, 10)] + ['agreement1']


def all_forms():
    return dict(
        F3=F3Form,
        F4=F4Form,
        F41=F41Form,
        F7=F7Form,
        F8=F8Form,
        F11=F11Form,
        F12=F12Form,
        # formsets
        F6=F6Formset,
        F9=F9Formset,
    )

