# coding: utf8
import shutil

from subprocess import Popen
from django.template import Template, Context
from django.core.exceptions import ObjectDoesNotExist

from .models import *
from zipfile import ZipFile
from mayor.settings import WINWORD_PATH, FINISHED_DOCUMENT, FINISHED_OLD_PATH
from company.models import Staff
from django.core.files import File

APP = 'электробезопасность'
ALPHABET = [chr(x) for x in range(256, 10000) if 'а' <= chr(x) <= 'я' or 'А' <= chr(x) <= 'Я']
# fields and forms for this field
FIELDS_FORMS = {
    'F1': ['F1'], 'F2': ['F2'], 'F3': ['F3'], 'F4': ['F4', 'F41'], 'F5': ['F5'], 'F6': ['F6'], 'F7': ['F7'],
    'F8': ['F8'], 'F9': ['F9'], 'F10': ['F10'], 'F11': ['F11'], 'F12': ['F12'], 'F13': ['F13'], 'F14': ['F14'],
    'F15': ['F15'], 'F16': ['F16'], 'F17': ['F17'], 'F18': ['F18'], 'F19': ['F19'], 'F20': ['F20'], 'F21': ['F21'],
    'F22': ['F22'], 'F23': ['F23']
}


def parsing(document, company, section='overall', preview=False):
    context = {}
    if isinstance(document, ElSafetyDocument):
        for name_model, forms in FIELDS_FORMS.items():
            context.update({name_model.lower(): FieldElSafety.objects.filter(
                Q(company=company) | Q(company=Company.objects.first()), document=document, field_name__in=forms)
            })

    answers = company.question.all()
    [context.update({'q_{}'.format(x.question.code): x.answer}) for x in answers]
    related_context = get_related_context(document, company)
    context.update(related_context)

    people_who_known = get_list_known_order(context, company.boss)

    try:
        context.update({
            'i': 1,
            'legal_address': company.genericinfocompany.address,
            'address': company.address,
            'dev_scroll': company.safety_health,
            'subordination': company.genericinfocompany.subordination,
            'characteristic': company.genericinfocompany.characteristic,

            'company_name': '{} {}'.format(company.genericinfocompany.short_ownership,
                                           company.genericinfocompany.short_title),
            'staff': Staff.objects.filter(company=company).order_by('surname'),
            'staff_profession': StaffProfession.objects.filter(staff__in=Staff.objects.filter(company=company)),
            'staff_position': StaffPosition.objects.filter(staff__in=Staff.objects.filter(company=company)),
            'unique_profession': StaffProfession.objects.filter(
                staff__in=Staff.objects.filter(company=company)).distinct('work_profession', 'rank'),
            'unique_position': StaffPosition.objects.filter(
                staff__in=Staff.objects.filter(company=company)).distinct('derivative_position', 'employee_position', 'category'),
            'company_title': company.genericinfocompany.full_title,
            'company_ownership': company.genericinfocompany.ownership,
            'full_name': company.genericinfocompany.full_title,
            'director': company.boss,
            'person_safety_health': company.safety_health,

            'person_development_program': company.development_program,
            'person_development_provision': company.development_provision,
        })
    except ObjectDoesNotExist:
        pass

    name_document = '{}.docx'.format(document.__str__())
    company_folder_name = ''
    for letter in company.genericinfocompany.short_title:
        if letter in ALPHABET:
            company_folder_name += letter

    folder_for_finished_document = '{0}{1}/{2}'.format(FINISHED_DOCUMENT, company_folder_name, section)
    if not os.path.exists(folder_for_finished_document):
        os.makedirs(folder_for_finished_document)
    document_word = os.path.abspath('{0}{1}/{2}/{3}'.format(
        FINISHED_DOCUMENT, company_folder_name, section, name_document)
    )
    document_pdf = os.path.abspath(document_word.replace('docx', 'pdf'))
    if document.path:
        with ZipFile(document.path, 'r') as template_word:
            with ZipFile(document_word, 'w') as new_document_word:
                for item in template_word.infolist():
                    buffer = template_word.read(item.filename)
                    if item.filename == 'word/document.xml' or item.filename == 'word/header1.xml':
                        with template_word.open(item) as render_file:
                            content = str(render_file.read(), 'utf-8')
                            template = Template(content)
                            new_document_word.writestr(item, template.render(Context(context)).encode())
                    else:
                        new_document_word.writestr(item, buffer)
        if os.path.exists(document_pdf):
            os.remove(document_pdf)

        if os.path.exists(document_word):
            import requests
            import requests.exceptions as exc
            with open(document_word, 'rb') as docx:
                try:
                    res = requests.post(
                        url='http://127.0.0.2:9016/v1/00000000-0000-0000-0000-000000000000/convert',
                        data=docx,
                        headers={'Content-Type': 'application/octet-stream'}
                    )
                    f = open(document_pdf, 'wb')
                    f.write(res.content)
                except exc.ConnectionError:
                    process = Popen('"{}" "{}" /mExportToPDFext /q'.format(WINWORD_PATH, document_word), shell=True)
                    process.wait()
        else:
            return

    if os.path.exists(document_pdf):
        with open(document_pdf, 'rb') as f:
            if isinstance(document, ElSafetyDocument):
                ready = ReadyElSafetyDocument.objects.filter(company=company, el_document=document).first()
                if ready:
                    ready.pdf_file.delete()
                else:
                    ready = ReadyElSafetyDocument.objects.create(company=company, el_document=document)
                ready.pdf_file.save('{}.pdf'.format(document.__str__()), File(f))
    document_word_old_path = '{0}{1}/{2}/'.format(FINISHED_OLD_PATH, company_folder_name, APP)
    # добавление файла в старые папки
    if not os.path.exists(document_word_old_path):
        os.makedirs(document_word_old_path)
    if document.path and os.path.exists(document_word):
        shutil.copy(document_word, document_word_old_path)
    elif os.path.exists(document_pdf):
        shutil.copy(document_pdf, document_word_old_path)
    else:
        return
    return 1


def get_list_known_order(context, director):
    people_who_known = []
    result = {}
    for data in context.values():
        if data:
            for value in data:
                if hasattr(value, 'staff_position') or hasattr(value, 'staff_profession'):
                    position = getattr(value, 'staff_position')
                    if position and hasattr(position, 'staff'):
                        fio_pos = position.staff.surname, position.staff.initials
                    else:
                        fio_pos = ''
                    profession = getattr(value, 'staff_profession')
                    if profession and hasattr(profession, 'staff'):
                        fio_prof = profession.staff.surname, profession.staff.initials
                    else:
                        fio_prof = ''

                    if position and fio_pos not in result and director != position:
                        result.update({fio_pos: position})
                    if profession and fio_prof not in result and director != profession:
                        result.update({fio_prof: profession})
    for person in result.values():
        if person not in people_who_known:
            people_who_known.append(person)
    return people_who_known


def get_related_context(document, company):
    related_context = {}
    fields = document.related_fields.all()
    if fields:
        for field in fields:
            document = field.document
            field_name = field.field_name
            position_number = field.position_number
            related_data = FieldElSafety.objects.filter(
                company=company, document=document, position_number=position_number, field_name=field_name)
            related_context.update(
                {'{}_{}_{}'.format(document.document_code, field_name, position_number).lower(): related_data}
            )
        return related_context
    return related_context


