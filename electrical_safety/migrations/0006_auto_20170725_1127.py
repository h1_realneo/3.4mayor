# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2017-07-25 08:27
from __future__ import unicode_literals

from django.db import migrations, models
import electrical_safety.models


class Migration(migrations.Migration):

    dependencies = [
        ('electrical_safety', '0005_auto_20170622_0039'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fieldelsafety',
            name='pdf_file',
            field=models.FileField(null=True, storage=electrical_safety.models.PDFStorage(base_url='/static/image_documents/', location='D:/projects/3.4mayor\\static/image_documents/'), upload_to=electrical_safety.models.generate_filename),
        ),
    ]
