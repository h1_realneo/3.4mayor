# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2017-06-21 15:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('electrical_safety', '0003_auto_20170621_1501'),
    ]

    operations = [
        migrations.AddField(
            model_name='fieldelsafety',
            name='pdf_file',
            field=models.FileField(null=True, upload_to=''),
        ),
    ]
