from django.apps import AppConfig


class ElectricalSafetyConfig(AppConfig):
    name = 'electrical_safety'
