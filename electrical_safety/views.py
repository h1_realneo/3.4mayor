from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.views.generic import DetailView, FormView, TemplateView, CreateView, ListView, UpdateView
from .forms import *
from .parsing_document import parsing
from django.http.response import HttpResponseRedirect, HttpResponseNotFound
from django.core.urlresolvers import reverse
from django.http import Http404

from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from company.views import get_staff_work
import json
from django.http.response import JsonResponse
from django.views.decorators.http import require_safe
from django.contrib import messages
from django.core.cache import cache
# Create your views here.


def check_generic_info(self, set_message=False):
    if hasattr(self, 'login_url'):
        self.login_url = reverse('company:generic_info_update', kwargs={'pk': self.request.user.genericinfocompany.pk})
    href = self.login_url if hasattr(self, 'login_url') else ''
    if self.request.user.genericinfocompany.full_title:
        return True
    if set_message:
        messages.error(self.request, 'Заполните Общую информацию о вашей фирме <a href="{}">здесь</a>'.format(href))
    return False


def check_position_leaders(self, set_message=False):
    if hasattr(self, 'login_url'):
        self.login_url = reverse('company:update_leaders', kwargs={'pk': self.request.user.pk})
    href = self.login_url if hasattr(self, 'login_url') else ''
    if self.request.user.boss:
        return True
    if set_message:
        messages.error(self.request, 'Укажите людей <a href="{}">здесь</a>'.format(href))
    return False


def check_pass_test(self, set_message=False):
    from .models import FieldInDocument, FieldElSafety
    if hasattr(self, 'login_url'):
        self.login_url = reverse('electrical_safety:create_in_chain', kwargs={'q_pk': 0})
    href = self.login_url if hasattr(self, 'login_url') else ''
    el_documents = ElSafetyDocument.objects.all()
    chain_documents = [x.document for x in FieldInDocument.objects.filter(element_chain=True).distinct('document')]
    chain_documents = [x for x in el_documents if x in chain_documents]
    for document in chain_documents:
        chain_fields = document.fields.filter(element_chain=True)
        for field in chain_fields:
            q = FieldElSafety.objects.filter(
                company=self.request.user,
                document=document,
                field_name=field.field_name,
                position_number=field.position_number
            )
            if not q:
                if set_message:
                    messages.error(self.request, 'Ответьте на тест <a href="{}">здесь</a>'.format(href))
                return False
    return True


@method_decorator(login_required, name='dispatch')
class ElInstructionView(TemplateView):
    template_name = 'electrical_safety/instructions_user.html'

    def get_context_data(self, **kwargs):
        from company.views import check_staffing_table
        context = super(ElInstructionView, self).get_context_data(**kwargs)
        context['staff_check'] = check_staffing_table(self)
        context['generic_info'] = check_generic_info(self)
        context['leaders'] = check_position_leaders(self)
        context['test'] = check_pass_test(self)
        return context


@method_decorator(login_required, name='dispatch')
class DocumentCreate(CreateView):
    template_name = 'electrical_safety/create.html'
    all_forms = all_forms()
    form_kwargs = {}
    object = None
    prefix = None
    document = None
    formsets_prefix = FORMSETS_PREFIX

    def get_context_data(self, **kwargs):
        context = super(DocumentCreate, self).get_context_data(**kwargs)
        context['document'] = ElSafetyDocument.objects.filter(id=int(self.kwargs.get('pk', -1))).first()
        context['staff'] = Staff.objects.filter(company=self.request.user)
        context['staff_work'] = get_staff_work(self.request.user)
        formset = self.formsets_prefix
        context['formset'] = formset
        context['method'] = self.request.method
        context['ready_document'] = ReadyElSafetyDocument.objects.filter(
            company=self.request.user, el_document=context['document']).first()
        return context

    def get_forms(self, render_fields=None, **kwargs):
        render_forms = []
        if render_fields is None:
            return
        fields = [(x.field_name, x.position_number, x.prefix, x.label, x.default) for x in render_fields]
        for field in fields:
            form_name, position, prefix, label, name, default = field[0], field[1], field[2], field[3], field[0], field[4]
            self.prefix = prefix
            self.form_kwargs = dict(
                document=self.document,
                company=Company.objects.first() if default else self.request.user,
                field_name=name,
                position_number=position,
                label=label,
                default=default
            )
            form = self.get_form(form_name)
            render_forms.append(form)
        return render_forms

    def get_form(self, form_name=None):
        if form_name:
            form_class = self.all_forms.get(form_name)
            return form_class(**self.get_form_kwargs())
        return

    def get_form_kwargs(self):
        form_name = self.form_kwargs.get('field_name')
        form = self.all_forms.get(form_name)
        kwargs = dict(prefix=self.prefix, form_kwargs=self.form_kwargs)
        if form and self.request.method == 'POST':
            kwargs.update({'data': self.request.POST, 'files': self.request.FILES})
            if not hasattr(form, 'management_form'):
                x = {}
                x.update(self.form_kwargs)
                x.pop('default')
                instance = FieldElSafety.objects.filter(**x).first()
                kwargs.update({'instance': instance})
        return kwargs

    def get(self, request, *args, **kwargs):
        document = ElSafetyDocument.objects.filter(id=int(kwargs.get('pk', -1))).first()
        fields = document.fields.all().order_by('index_number') if document else kwargs.pop('fields', None)
        if not fields and not document:
            raise Http404
        self.document = fields.first().document if fields else document
        render_forms = self.get_forms(render_fields=fields)
        return self.render_to_response(self.get_context_data(form=render_forms))

    def post(self, request, *args, **kwargs):
        errors_in_forms = False
        company = self.request.user
        document = ElSafetyDocument.objects.filter(id=int(kwargs.get('pk', -1))).first()
        fields = document.fields.all().order_by('index_number') if document else kwargs.pop('fields', None)
        if not fields and not document:
            raise Http404
        self.document = fields.first().document if fields else document
        if document and not fields:
            response = parsing(document=document, company=self.request.user, section=document.section_document, preview=True)
            if response:
                return HttpResponseRedirect(reverse('electrical_safety:list'))
        render_forms = self.get_forms(render_fields=fields)
        if render_forms:
            for form in render_forms:
                if not form.is_valid():
                    errors_in_forms = True
            if not errors_in_forms:
                for form in render_forms:
                    if not hasattr(form, 'management_form'):
                        instance = form.save(commit=False)
                        instance.company = company if not form.default else Company.objects.first()
                        instance.document = self.document
                        instance.position_number = form.position_number
                        instance.field_name = form.field_name
                        instance.label = form.label
                        instance.save()
                        form.save_m2m()
                    else:
                        instances = form.save(commit=False)
                        [obj.delete() for obj in form.deleted_objects]
                        for cls in instances:
                            cls.company = company
                            cls.document = self.document
                            cls.position_number = form.position_number
                            cls.field_name = form.field_name
                            cls.label = form.label
                            cls.save()
            else:
                return self.form_invalid(render_forms)
            if not document:
                success_url = kwargs.get('success_url')
                return HttpResponseRedirect(success_url)
            response = parsing(document=document, company=self.request.user, section=document.section_document, preview=True)
            if response:
                return HttpResponseRedirect(reverse('electrical_safety:list'))
        return HttpResponseRedirect(reverse('work-safety:create-document', kwargs={'pk': document.pk}))


class DocumentList(ListView):
    model = ElSafetyDocument

    def get_context_data(self, **kwargs):
        context = super(DocumentList, self).get_context_data(**kwargs)
        document_ready = []
        documents = self.request.user.electrical_documents.all()
        for document in documents:
            if ReadyElSafetyDocument.objects.filter(el_document=document, company=self.request.user):
                document_ready.append([document, True])
            else:
                document_ready.append([document, False])
        context['documents'] = document_ready
        return context


class ChainDocumentCreate(PermissionRequiredMixin, DocumentCreate):
    template_name = 'electrical_safety/create_in_chain.html'

    def get_context_data(self, **kwargs):
        context = super(ChainDocumentCreate, self).get_context_data(**kwargs)
        document, chain_documents = self.get_document_in_chain()
        context['document'] = document
        questions_remain = len([x for x in chain_documents if not x[1]])
        context['staff'] = Staff.objects.filter(company=self.request.user)
        context['staff_work'] = get_staff_work(self.request.user)
        formset = self.formsets_prefix
        context['formset'] = formset
        context['documents_chain'] = chain_documents
        context['questions_remain'] = questions_remain
        context['q_pk'] = int(self.kwargs.get('q_pk', '0'))+1
        return context

    def get_document_in_chain(self):
        el_documents = ElSafetyDocument.objects.all()
        chain_documents = [x.document for x in FieldInDocument.objects.filter(element_chain=True).distinct('document')]
        chain_documents = [x for x in el_documents if x in chain_documents]
        sort_by_document_code = lambda ot_document: ot_document.document_code
        chain_documents.sort(key=sort_by_document_code)
        return_chain_documents = []
        for document in chain_documents:
            ready = True
            chain_fields = document.fields.filter(element_chain=True)
            for field in chain_fields:
                q = FieldElSafety.objects.filter(
                    company=self.request.user,
                    document=document,
                    field_name=field.field_name,
                    position_number=field.position_number
                )
                if not q:
                    ready = False
                    break
            return_chain_documents.append([document, ready])
        q_pk = self.kwargs.get('q_pk', '0')
        try:
            return chain_documents[int(q_pk)], return_chain_documents
        except IndexError:
            return None, return_chain_documents

    def get(self, request, *args, **kwargs):
        document, documents = self.get_document_in_chain()
        if document is None:
            return HttpResponseRedirect(reverse('electrical_safety:instructions'))
        render_fields = document.fields.filter(element_chain=True).order_by('index_number')
        kwargs.update({'fields': render_fields})
        response = super(ChainDocumentCreate, self).get(request, *args, **kwargs)
        return response

    def post(self, request, *args, **kwargs):
        document, documents = self.get_document_in_chain()
        if document is None:
            return HttpResponseRedirect(reverse('home'))
        render_fields = document.fields.filter(element_chain=True).order_by('index_number')
        success_url = reverse('electrical_safety:instructions')
        q_pk = self.kwargs.get('q_pk', '0')
        for i, doc in enumerate(documents, 0):
            if not doc[1] and int(q_pk) < i:
                success_url = reverse('electrical_safety:create_in_chain', kwargs={'q_pk': i})
                break
        kwargs.update({'fields': render_fields, 'success_url': success_url})
        response = super(ChainDocumentCreate, self).post(request, *args, **kwargs)
        return response

    def has_permission(self):
        if not check_generic_info(self, set_message=True):
            return False
        elif not check_position_leaders(self, set_message=True):
            return False
        return True


class CreateDocumentsElView(ChainDocumentCreate, TemplateView):
    template_name = 'electrical_safety/create_documents_el.html'
    pk = None

    def get(self, request, *args, **kwargs):
        return self.render_to_response({})

    def post(self, request, *args, **kwargs):
        el_documents = ElSafetyDocument.objects.all()
        count_documents = el_documents.count()
        import time
        start = time.time()
        time_processing = None
        num_document = cache.get('{0}_el'.format(self.request.user)) + 1 if cache.get('{0}_el'.format(self.request.user)) else 1
        if num_document < count_documents:
            if num_document == 1:
                cache.set('{}_time_el'.format(self.request.user.id), start, 60*1)
            cache.set('{0}_el'.format(self.request.user), num_document, 60*0.5)
            document = el_documents[num_document - 1]
        elif num_document == count_documents:
            document = el_documents.last()
            time_processing = int(time.time() - cache.get('{}_time_el'.format(self.request.user.id)))
            cache.delete_many(['{}_time_el'.format(self.request.user.id), '{0}_el'.format(self.request.user)])
        else:
            return
        parsing(document=document, company=self.request.user, section=document.section_document)
        percent = round(100*int(num_document)/int(count_documents))
        return JsonResponse({'percent': percent, 'time': time_processing})

    def has_permission(self):
        if not check_pass_test(self, set_message=True):
            return False
        return super(CreateDocumentsElView, self).has_permission()


class ElectricalLeadersUpdateView(UpdateView):
    model = Company
    form_class = CompanyUpdateElLeadersForm
    template_name = 'electrical_safety/update_leaders.html'

    def get_context_data(self, **kwargs):
        context = super(ElectricalLeadersUpdateView, self).get_context_data(**kwargs)
        context['staff_work'] = get_staff_work(self.request.user)
        return context

    def get_form_kwargs(self):
        kwargs = super(ElectricalLeadersUpdateView, self).get_form_kwargs()
        kwargs['company'] = self.request.user
        return kwargs

    def get_success_url(self):
        return reverse('company:question', kwargs={'division': 'work_safety', 'pk': self.request.user.pk})
