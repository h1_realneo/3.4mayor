# coding: utf8
from __future__ import unicode_literals
from django.conf import settings
from company.models import *
from django.core.files.storage import FileSystemStorage
import os

from django.contrib.auth.models import AbstractBaseUser
from django.db import models
from fire_safety.models import Company
from mayor.settings import FINISHED_DOCUMENT


class ElSafetyDocument(models.Model):

    ELECTRICAL = 'electrical'
    SECTIONS_DOC = (
        (ELECTRICAL, 'Электробезопасность'),
    )

    document_code = models.IntegerField(unique=True, null=True)
    related_fields = models.ManyToManyField('FieldInDocument', blank=True, related_name='related_field')
    title = models.CharField(max_length=300, default='первый')
    path = models.FilePathField(blank=True, null=True, max_length=300, path=settings.FILE_PATH_ELECTRICAL_DOCUMENT,
                                recursive=True)
    section_document = models.CharField(max_length=100, choices=SECTIONS_DOC, default=ELECTRICAL, blank=True)
    default_document = models.BooleanField('Стандартно необходимый документ', default=False)

    def __str__(self):
        return "{}".format(self.title)

    class Meta:
        verbose_name = 'Документ электробезопасности'
        verbose_name_plural = 'Документы электобезопасности'


class FieldInDocument(models.Model):
    F3 = 'F3'
    F4 = 'F4'
    F41 = 'F41'
    F6 = 'F6'
    F7 = 'F7'
    F8 = 'F8'
    F12 = 'F12'
    FIELDS = (
        (F3, 'F3 дополнительный параграф'),
        (F4, 'F4 фио'),
        (F41, 'F41 фио или ничего'),
        (F6, 'F6 согласовавшие'),
        (F7, 'F7 дата документа'),
        (F8, 'F8 номер документа'),
        (F12, 'F12 pdf файл'),
    )
    document = models.ForeignKey(ElSafetyDocument, related_name='fields')
    label = models.TextField(max_length=300, blank=True)
    field_name = models.CharField(max_length=100, choices=FIELDS, default=F41)
    prefix = models.CharField(max_length=100, blank=True)
    position_number = models.IntegerField(blank=True, null=True)
    index_number = models.IntegerField('Порядковый номер поля на форме', blank=True, null=True)
    required_field = models.BooleanField('Необходимое поле?', blank=True, default=False)
    element_chain = models.BooleanField('Поле для цепочки вопросов', blank=True, default=False)
    default = models.BooleanField('Поле по умолчанию', blank=True, default=False)

    def __str__(self):
        return '{}, {}, {}, {}'.format(self.document.title, self.field_name, self.prefix, self.label)

    class Meta:
        verbose_name = 'Поле в документе'
        verbose_name_plural = 'Поля в документах'


def generate_filename(self, filename):
    from mayor.settings import FINISHED_DOCUMENT
    alphabet = [chr(x) for x in range(256, 10000) if 'а' <= chr(x) <= 'я' or 'А' <= chr(x) <= 'Я']
    name_document = '{}.pdf'.format(self.document.__str__())
    company_folder_name = ''
    for letter in self.company.genericinfocompany.short_title:
        if letter in alphabet:
            company_folder_name += letter

    folder_for_finished_document = '{0}{1}/{2}'.format(
        FINISHED_DOCUMENT, company_folder_name, self.document.section_document)
    if not os.path.exists(folder_for_finished_document):
        os.makedirs(folder_for_finished_document)
    document_pdf = '{0}/{1}/{2}'.format(
        company_folder_name, self.document.section_document, name_document)
    if os.path.exists(os.path.abspath('{}{}'.format(FINISHED_DOCUMENT, document_pdf))):
        os.remove(os.path.abspath('{}{}'.format(FINISHED_DOCUMENT, document_pdf)))
    return document_pdf


class PDFStorage(FileSystemStorage):
    pass


custom_fs = PDFStorage(location=FINISHED_DOCUMENT, base_url='/static/image_documents/')


class FieldElSafety(models.Model):

    field_name = models.CharField(max_length=255, choices=FieldInDocument.FIELDS, default=FieldInDocument.FIELDS[0])
    company = models.ForeignKey('company.Company', blank=True, null=True)
    document = models.ForeignKey(ElSafetyDocument, blank=True, null=True)
    position_number = models.IntegerField(blank=True, null=True)
    label = models.CharField(max_length=255, blank=True)
    # field's name
    employee = models.ForeignKey('company.Staff', blank=True, null=True, verbose_name='сотрудник')
    date = models.CharField('дата подписи', max_length=255, blank=True)
    document_number = models.CharField('номер документа', max_length=255, blank=True)
    paragraph = models.TextField('дополнительный пункт', max_length=800, blank=True)
    trigger = models.BooleanField('Да Нет', max_length=255, blank=True, default=False)
    pdf_file = models.FileField(upload_to=generate_filename, storage=custom_fs, null=True)
    # if employee have some professions in this field we save one of them that use in this field
    staff_profession = models.ForeignKey(StaffProfession, blank=True, null=True, verbose_name='профессия')
    staff_position = models.ForeignKey(StaffPosition, blank=True, null=True, verbose_name='должность')

    class Meta:
        verbose_name = 'Поле'
        verbose_name_plural = 'Поля'

    def __str__(self):
        return '{} {} {} {}'.format(self.company, self.document, self.field_name, self.position_number)


class ElectricalStorage(FileSystemStorage):
    def get_available_name(self, name, max_length=None):
        name = name.replace('_', ' ')
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name


fs = ElectricalStorage(location=settings.MEDIA_ROOT)


def el_document_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/company_<id>/ot_<section>/document_<id>/<filename>
    return 'electrical/{}/{}/{}/{}'.format(
        instance.company.id, instance.el_document.section_document, instance.el_document.id, filename
    )


class ReadyElSafetyDocument(models.Model):
    company = models.ForeignKey(Company)
    el_document = models.ForeignKey(ElSafetyDocument)
    pdf_file = models.FileField(upload_to=el_document_directory_path, storage=fs, max_length=350)
    word_file = models.FileField(upload_to=el_document_directory_path, blank=True, null=True)

    def __str__(self):
        return '{} {}'.format(self.company, self.el_document)
