$(document).ready(function () {
    var ColorSelectChoice = function (data) {
        var select_class, all_option, selected_option;
        select_class = "." + this['custom_class'];
        $(data).find(select_class).find('select option:selected').each(
            function(index, element){
                $(element).removeAttr('selected')
            }
        );
        if ($(data.context).hasClass('delete-row')) {
            var divs = $('div').filter(function () {
                return this.className == $(data).attr('class');
            });
            all_option = divs.find(select_class).find('select option');
        } else {
            all_option = $(data).parent().find(select_class).find('select option');
        }
        selected_option = $(all_option).filter(':selected');
        all_option.each(function (index, element) {
                var constant = 1;
                selected_option.each(
                    function (ind, el) {
                        if ($(element).val() && $(element).text() == $(el).text()) {
                            $(element).attr({'style': "color: #FF0022;"});
                            constant = 0
                        }
                    }
                );
                if (constant){
                 $(element).removeAttr('style');
                }
            }
        );
        $('.select_employee').selectpicker({
            'actionsBox': 'true',
            'dropupAuto': false,
            'deselectAllText': 'Снять все выделения',
            'selectAllText': 'Выделить все',
            'liveSearch': true,
            'liveSearchPlaceholder': null,
            'liveSearchStyle': 'contains'
        }).selectpicker('refresh');
        $('.subdivision').selectpicker({
            'actionsBox': 'true',
            'deselectAllText': 'Снять все выделения',
            'selectAllText': 'Выделить все',
            'size': 12,
            'dropupAuto': false,
            'liveSearch': true,
            'liveSearchPlaceholder': null,
            'liveSearchStyle': 'contains'
        }).selectpicker('refresh');
        $('.selectpicker').selectpicker({
            'actionsBox': 'true',
            'deselectAllText': 'Снять все выделения',
            'selectAllText': 'Выделить все',
            'size': 12,
            'dropupAuto': false
        }).selectpicker('refresh');

    };
    var added_pos_prof = function () {
        $('.select-position-profession').selectpicker({
            'size': 11,
            'liveSearch': true,
            'liveSearchPlaceholder': null,
            'liveSearchStyle': 'contains',
            'dropupAuto': false
        });
    };
    $(".inline.additional_info").formset({
        prefix: "additional_info",
        addText: 'Добавить',
        deleteText: 'Удалить',
        formCssClass: 'dynamic-additional_info'
    });
    $(".inline.system1").formset({
        prefix: "system1",
        addText: 'Добавить',
        deleteText: 'Удалить',
        formCssClass: 'dynamic-system1'
    });
    $(".inline.agreement1").formset({
        prefix: "agreement1",
        addText: 'Добавить',
        deleteText: 'Удалить',
        formCssClass: 'dynamic-agreement1'
    });
    $(".inline.subdivision1").formset({
        prefix: "subdivision1",
        addText: "Добавить",
        custom_class: 'dev_subdivisions',
        deleteText: "Удалить",
        formCssClass: 'dynamic-subdivision1',
        added: ColorSelectChoice,
        removed: ColorSelectChoice
    });
    $(".inline.room1").formset({
        prefix: "room1",
        addText: 'Добавить',
        deleteText: 'Удалить',
        formCssClass: 'dynamic-room1'
    });
    $(".inline.telephone1").formset({
        prefix: "telephone1",
        addText: 'Добавить',
        deleteText: 'Удалить',
        formCssClass: 'dynamic-telephone1'
    });
    $(".inline.staffposition_set").formset({
        prefix: "staffposition_set",
        addText: 'Добавить',
        deleteText: 'Удалить',
        formCssClass: 'dynamic-staffposition_set',
        added: added_pos_prof
    });
    $(".inline.staffprofession_set").formset({
        prefix: "staffprofession_set",
        addText: 'Добавить',
        deleteText: 'Удалить',
        formCssClass: 'dynamic-staffprofession_set',
        added: added_pos_prof

    });
    $(".inline.workprofession_set").formset({
        prefix: "workprofession_set",
        addText: 'Добавить',
        deleteText: 'Удалить',
        formCssClass: 'dynamic-workprofession_set'
    });
    $(".inline.employeeposition_set").formset({
        prefix: "employeeposition_set",
        addText: 'Добавить',
        deleteText: 'Удалить',
        formCssClass: 'dynamic-workprofession_set'

    });
    $(".inline.staff").formset({
        prefix: "staff",
        addText: 'Добавить',
        deleteText: 'Удалить',
        formCssClass: 'dynamic-staff'
    });
    $(".inline.siz_formset").formset({
        prefix: "siz_formset",
        addText: 'Добавить',
        deleteText: 'Удалить',
        formCssClass: 'dynamic-siz_formset'
    });
    $(".inline.time_briefing_prof").formset({
        prefix: "time_briefing_prof",
        addText: 'Добавить',
        deleteText: 'Удалить',
        formCssClass: 'dynamic-time_briefing_prof',
        custom_class: 'briefing_profession',
        added: ColorSelectChoice,
        removed: ColorSelectChoice
    });
    $(".inline.time_briefing_pos").formset({
        prefix: "time_briefing_pos",
        addText: 'Добавить',
        deleteText: 'Удалить',
        formCssClass: 'dynamic-time_briefing_pos',
        custom_class: 'briefing_position',
        added: ColorSelectChoice,
        removed: ColorSelectChoice

    });
    $(".inline.dev_instruction").formset({
        prefix: "dev_instruction",
        addText: 'Добавить',
        deleteText: 'Удалить',
        formCssClass: 'dynamic-dev_instruction',
        custom_class: 'dev_instructions',
        added: ColorSelectChoice,
        removed: ColorSelectChoice
    });
    $(".inline.view_instructions").formset({
        prefix: "view_instructions",
        addText: 'Добавить',
        deleteText: 'Удалить',
        formCssClass: 'dynamic-view_instructions'
    });
    $(".inline.subdivisions").formset({
        prefix: "subdivisions",
        addText: 'Добавить',
        deleteText: 'Удалить',
        custom_class: 'dev_subdivisions',
        formCssClass: 'dynamic-subdivisions',
        added: ColorSelectChoice,
        removed: ColorSelectChoice
    });
    $(".inline.days").formset({
        prefix: "days",
        addText: 'Добавить',
        deleteText: 'Удалить',
        formCssClass: 'dynamic-days'
    });
    $(".inline.telephone_generic").formset({
        prefix: "telephone_generic",
        addText: 'Добавить',
        deleteText: 'Удалить',
        formCssClass: 'dynamic-telephone_generic'
    });
    $(".inline.staff_surname_formset").formset({
        prefix: "staff_surname_formset",
        addText: 'Добавить',
        deleteText: 'Удалить',
        formCssClass: 'dynamic-staff_surname_formset'
    });
    for (i = 0; i < 5; i++) {
        $(".inline.formset_fio" + i).formset({
            prefix: "formset_fio" + i,
            addText: 'Добавить',
            deleteText: 'Удалить',
            formCssClass: 'dynamic-formset_fio' + i,
            custom_class: 'employee_backlight',
            added: ColorSelectChoice,
            removed: ColorSelectChoice
        });
    }
    $.datepicker.setDefaults($.datepicker.regional['ru']);
    $(".date").datepicker({
        dateFormat: "dd.mm.yy"
    });
});
