$(document).ready(function () {

    $('.bootstrap-select').selectpicker({
        'actionsBox': 'true',
        'deselectAllText': 'Снять все выделения',
        'selectAllText': 'Выделить все',
        'size': 12,
        'dropupAuto': false
    });
    $('.selectpicker').selectpicker({
        'actionsBox': 'true',
        'deselectAllText': 'Снять все выделения',
        'selectAllText': 'Выделить все',
        'size': 12,
        'dropupAuto': false
    });
    $('.select_employee').selectpicker({
        'actionsBox': 'true',
        'dropupAuto': false,
        'deselectAllText': 'Снять все выделения',
        'selectAllText': 'Выделить все',
        'liveSearch': true,
        'liveSearchPlaceholder': null,
        'liveSearchStyle': 'contains'
        });
    $('.select-generic-form').selectpicker({
        'actionsBox': 'true',
        'size': 10,
        'deselectAllText': 'Снять все выделения',
        'selectAllText': 'Выделить все',
        'liveSearch': true,
        'liveSearchPlaceholder': null,
        'liveSearchStyle': 'contains'
    });
    $('.select_download_work_documents').selectpicker({
        'actionsBox': 'true',
        'size': 16,
        'deselectAllText': 'Снять все выделения',
        'selectAllText': 'Выделить все',
        'liveSearch': true,
        'liveSearchPlaceholder': null,
        'liveSearchStyle': 'contains'
    });

    $('.select-kind-activity').selectpicker({
        'actionsBox': 'true',
        'deselectAllText': 'Снять все выделения',
        'selectAllText': 'Выделить все',
        'size': 12,
        'dropupAuto': false,
        'liveSearch': true,
        'liveSearchPlaceholder': 'Введите номер или название вида деятельности',
        'liveSearchStyle': 'contains'
    });
    $('.select-position-profession').selectpicker({
        'size': 12,
        'liveSearch': true,
        'liveSearchPlaceholder': null,
        'liveSearchStyle': 'contains'
    });
    $('.subdivision').selectpicker({
        'actionsBox': 'true',
        'deselectAllText': 'Снять все выделения',
        'selectAllText': 'Выделить все',
        'size': 8,
        'dropupAuto': false,
        'liveSearch': true,
        'liveSearchPlaceholder': null,
        'liveSearchStyle': 'contains'
    });
    $('.attachment').selectpicker({
        'actionsBox': 'true',
        'deselectAllText': 'Снять все выделения',
        'selectAllText': 'Выделить все',
        'size': 'auto',
        'selectedTextFormat':'count > 0',
        'dropupAuto': false,
        'liveSearch': true,
        'liveSearchPlaceholder': null,
        'liveSearchStyle': 'contains'
    });
    $('.selectpicker-rank').selectpicker().on('show.bs.select', function (e) {
        var options = $(this).children();
        var select = $(this);
        options.each(function (index, element) {
            $(element).removeAttr('disabled')
        });
        $.ajax({
            url: "http://127.0.0.1:8080/company/dynamic_rank_select",
            dataType: "json",
            async: false,
            data: {
                work_profession_select: $(this).parent().prevAll('input').val(),
                code: $(this).parent().prevAll('input').last().val()
            },
            success: function (data) {
                var available_ranks = data.json;
                available_ranks.reverse();
                available_ranks.push('0');
                available_ranks.reverse();
                options.each(function (index, element) {
                    if (available_ranks.indexOf(index.toString()) == -1) {

                         $(element).prop('disabled', 'true')
                    }
                });

            }
        });
        select.selectpicker('refresh');
    });
    $('.selectpicker-category').selectpicker().on('show.bs.select', function (e) {
        var options = $(this).children();
        var select = $(this);
        options.each(function (index, element) {
            $(element).removeAttr('disabled')
        });
        $.ajax({
            url: "/company/dynamic_category_select",
            dataType: "json",
            async: false,
            data: {
                position_select: $(this).parent().prevAll('input').val(),
                code: $(this).parent().prevAll('input').last().val()
            },
            success: function (data) {
                var available_ranks = data.json;
                available_ranks.reverse();
                available_ranks.push('0');
                available_ranks.reverse();
                options.each(function (index, element) {
                    if (available_ranks.indexOf(index.toString()) == -1) {
                         $(element).prop('disabled', 'true')
                    }
                });
            }
        });
        select.selectpicker('refresh');
    });
    $('.add-row').click(function () {
        $('.selectpicker').selectpicker('show');
    });

});
