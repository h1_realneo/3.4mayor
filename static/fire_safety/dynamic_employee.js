$(".employee").change(function () {
    var select = this;
    $.ajax({
        url: "/fire/dynamic_employee/",
        dataType: "json",
        async: false,
        data: {
            staff_id: select.value
        },
        select: select,
        success: function (data, textStatus, jqXHR) {
            if (data) {
                if (data.professions) {
                    $(select).parent().parent().nextAll('.staff_profession:first').show('slice');
                    $(select).parent().parent().nextAll('.staff_profession:first').find('select').html(data.professions);
                    $(select).parent().next().show('slice');
                    $(select).parent().next().children().last().html(data.professions)
                }
                else {
                    $(select).parent().parent().nextAll('.staff_profession:first').hide('slice');
                    $(select).parent().parent().nextAll('.staff_profession:first').find('select').empty();
                    $(select).parent().next().hide('slice');
                    $(select).parent().next().children().last().empty()
                }
                if (data.positions) {
                    $(select).parent().parent().nextAll('.staff_position:first').show('slice');
                    $(select).parent().parent().nextAll('.staff_position:first').find('select').html(data.positions);
                    $(select).parent().next().next().show('slice');
                    $(select).parent().next().next().children().last().html(data.positions)
                }
                else {
                    $(select).parent().parent().nextAll('.staff_position:first').hide('slice');
                    $(select).parent().parent().nextAll('.staff_position:first').find('select').empty();
                    $(select).parent().next().next().hide('slice');
                    $(select).parent().next().next().children().last().empty()
                }
            }
            else {
                $(select).prev().prev().empty()
            }
        }
    });
});
