$(".dynamic_category").change(function () {
    var select = this;
    $.ajax({
        url: "/company/dynamic_category_select",
        dataType: "json",
        async: false,
        data: {
            employee_position_id: select.value
        },
        select: select,
        success: function (data, textStatus, jqXHR) {
            if (data){
                $(select).parent().parent().children('.dynamic_category_select').empty();
                $(select).parent().parent().children('.dynamic_category_select').html(data.html)
            }
            else{
                $(select).parent().parent().children('.dynamic_category_select').empty()
            }
        }
    });
});
