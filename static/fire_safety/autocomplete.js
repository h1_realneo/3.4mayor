$(document).on('keydown.autocomplete', ".autocomplete-profession", function () {
    var this_input = this;
    var work_profession_input = $(this_input).prevAll('input');
    $(this).autocomplete({
        select: function (event, ui) {
            $(work_profession_input).prop('value', ui.item.code)
        },
        source: function (request, response) {
            $.ajax({
                url: "/company/dynamic_rank_select",
                dataType: "json",
                data: {
                    work_profession: request.term,
                    code: work_profession_input.attr('value')
                },
                success: function (data) {
                    response($.map(data.json, function (item) {
                        return {
                            label: item.label,
                            value: item.value,
                            code: item.code
                        }
                    }));
                }
            });
        },
        minLength: 1,
        autoFocus: true
    });
});

$(document).on('keydown.autocomplete', ".autocomplete-position", function () {
    var this_input = this;
    var position_input = $(this_input).prevAll('input');
    $(this).autocomplete({
        select: function (event, ui) {
            $(position_input).prop('value', ui.item.code)
        },
        source: function (request, response) {
            $.ajax({
                url: "/company/dynamic_category_select",
                dataType: "json",
                data: {
                    position: request.term,
                    code: position_input.attr('value')
                },
                success: function (data) {
                    response($.map(data.json, function (item) {
                        return {
                            label: item.label,
                            value: item.value,
                            code: item.code
                        }
                    }));
                }
            });
        },
        minLength: 1,
        autoFocus: true
    });
});