$(".dynamic_rank").change(function () {
    var select = this;
    $.ajax({
        url: "/company/dynamic_rank_select",
        dataType: "json",
        async: false,
        data: {
            work_profession_id: select.value
        },
        select: select,
        success: function (data, textStatus, jqXHR) {
            if (data){
                $(select).parent().parent().children('.dynamic_rank_select').empty();
                $(select).parent().parent().children('.dynamic_rank_select').html(data.html)
            }
            else{
                $(select).parent().parent().children('.dynamic_rank_select').empty()
            }
        }
    });
});
