# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2017-02-02 10:35
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('work_safety', '0002_auto_20170202_1334'),
    ]

    operations = [
        migrations.AlterField(
            model_name='siz',
            name='exception_company',
            field=models.ManyToManyField(blank=True, related_name='exception_company', to=settings.AUTH_USER_MODEL),
        ),
    ]
