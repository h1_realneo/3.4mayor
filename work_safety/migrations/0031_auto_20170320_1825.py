# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2017-03-20 15:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('work_safety', '0030_auto_20170316_1807'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fieldot',
            name='end_dinner',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='fieldot',
            name='end_work',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='fieldot',
            name='starting_dinner',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='fieldot',
            name='starting_work',
            field=models.CharField(max_length=255),
        ),
    ]
