# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2017-02-15 13:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0047_company_work_documents'),
        ('work_safety', '0012_auto_20170213_1654'),
    ]

    operations = [
        migrations.AddField(
            model_name='fieldot',
            name='positions',
            field=models.ManyToManyField(blank=True, to='company.EmployeePosition'),
        ),
        migrations.AddField(
            model_name='fieldot',
            name='professions',
            field=models.ManyToManyField(blank=True, to='company.WorkProfession'),
        ),
        migrations.AlterField(
            model_name='otdocument',
            name='section',
            field=models.CharField(choices=[('body_check', 'медосмотр'), ('overall', 'общее'), ('siz', 'средства индивидуальной защиты')], default='siz', max_length=100),
        ),
    ]
