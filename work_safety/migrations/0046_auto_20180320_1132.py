# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2018-03-20 08:32
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('work_safety', '0045_auto_20180213_1250'),
    ]

    operations = [
        migrations.AlterField(
            model_name='siz',
            name='position',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='sizs', to='company.EmployeePosition'),
        ),
        migrations.AlterField(
            model_name='siz',
            name='profession',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='sizs', to='company.WorkProfession'),
        ),
    ]
