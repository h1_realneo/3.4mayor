# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2017-02-18 19:35
from __future__ import unicode_literals

from django.db import migrations, models
import multiselectfield.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('work_safety', '0016_auto_20170218_2150'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fieldindocument',
            name='field_name',
            field=models.CharField(choices=[('F4', 'F4 фио'), ('F41', 'F41 фио или ничего'), ('F6', 'F6 согласовавшие'), ('F7', 'F7 дата документа'), ('F8', 'F8 номер документа'), ('F9', 'F9 фио формсет'), ('F11', 'F11 да нет'), ('F13', 'F13 профессии и должности'), ('F14', 'F14 инструкции'), ('F15', 'F14 вредные факторы')], default='F41', max_length=100),
        ),
        migrations.AlterField(
            model_name='fieldot',
            name='field_name',
            field=models.CharField(choices=[('F4', 'F4 фио'), ('F41', 'F41 фио или ничего'), ('F6', 'F6 согласовавшие'), ('F7', 'F7 дата документа'), ('F8', 'F8 номер документа'), ('F9', 'F9 фио формсет'), ('F11', 'F11 да нет'), ('F13', 'F13 профессии и должности'), ('F14', 'F14 инструкции'), ('F15', 'F14 вредные факторы')], default=('F4', 'F4 фио'), max_length=255),
        ),
        migrations.AlterField(
            model_name='fieldot',
            name='hazards',
            field=multiselectfield.db.fields.MultiSelectField(blank=True, choices=[('Физические опасные и вредные производственные факторы', (('aaq', 'движущиеся машины и механизмы;'), ('aaw', 'подвижные части производственного оборудования;'), ('aae', 'предвигающиеся изделия, заготовки, материалы;'), ('aar', 'разрушающиеся конструкции;'), ('aat', 'обрушивающиеся горные породы;'), ('aay', 'повышенная запыленность и загазованность воздуха рабочей зоны;'), ('aau', 'повышенная или пониженная температура поверхностей оборудования, материалов;'), ('aai', 'повышенная или пониженная температура воздуха рабочей зоны;'), ('aao', 'повышенный уровень шума на рабочем месте;'), ('aap', 'повышенный уровень вибрации;'), ('aaa', 'повышенный уровень инфразвуковых колебаний;'), ('aas', 'повышенный уровень ультразвука;'), ('aad', 'повышенное или пониженное барометрическое давление в рабочей зоне и его резкое изменение;'), ('aaf', 'повышенная или пониженная влажность воздуха;'), ('aag', 'повышенная или пониженная подвижность воздуха;'), ('aah', 'повышенная или пониженная ионизация воздуха;'), ('aaj', 'повышенный уровень ионизирующих излучений в рабочей зоне;'), ('aak', 'повышенное значение напряжения в электрической цепи, замыкание которой может произойти через тело человека;'), ('aal', 'повышенный уровень статического электричества;'), ('aaz', 'повышенный уровень электромагнитных излучений;'), ('aax', 'повышенная напряженность электрического поля;'), ('aac', 'повышенная напряженность магнитного поля;'), ('aav', 'отсутствие или недостаток естественного света;'), ('aab', 'недостаточная освещенность рабочей зоны;'), ('aan', 'повышенная яркость света;'), ('aam', 'пониженная контрастность;'), ('abq', 'прямая и отраженная блесткость;'), ('abw', 'повышенная пульсация светового потока;'), ('abe', 'повышенный уровень ультрафиолетовой радиации;'), ('abr', 'повышенный уровень инфракрасной радиации;'), ('abt', 'острые кромки, заусенцы и шероховатость на поверхностях заготовок, инструментов и оборудования;'), ('aby', 'расположение рабочего места на значительной высоте относительно поверхности земли (пола);'), ('abu', 'невесомость.'))), ('Химические опасные и вредные производственные факторы(по характеру воздействия на организм человека)', (('baq', 'токсические;'), ('baw', 'раздражающие;'), ('bae', 'сенсибилизирующие;'), ('bar', 'канцерогенные;'), ('bat', 'мутагенные;'), ('bay', 'влияющие на репродуктивную функцию;'))), ('Химические опасные и вредные производственные факторы(по пути проникновения в организм человека)', (('bba', 'органы дыхания;'), ('bbc', 'желудочно-кишечный тракт;'), ('bbn', 'кожные покровы и слизистые оболочки.'))), ('Биологические опасные и вредные производственные факторы', (('caa', 'патогенные микроорганизмы (бактерии, вирусы, риккетсии, спирохеты, грибы, простейшие) и продукты их жизнедеятельности.'),)), ('Психофизиологические опасные и вредные производственные факторы', (('da', 'а) физические перегрузки (статические, динамические)'), ('db', 'б) нервно-психические перегрузки (умственное перенапряжение; перенапряжение анализаторов; монотонность труда; эмоциональные перегрузки)')))], max_length=255),
        ),
    ]
