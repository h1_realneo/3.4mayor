# coding: utf8
import shutil
import os
import re
import logging
from subprocess import Popen
from django.template import Template, Context
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from .models import FieldOT, OTDocument, ReadyOTDocument
from zipfile import ZipFile
from mayor.settings import WINWORD_PATH, FINISHED_DOCUMENT, FINISHED_OLD_PATH
from company.models import Staff, Question, CompanyQuestionAnswer, StaffPosition, StaffProfession
from django.core.files import File
from company.additional_info_for_choices import CONSTRUCTIONS, BUILDINGS, PREMISES, EQUIPMENT, MEANS_TRANSPORT, \
    INTERNAL_ENGINEERING_NETWORKS as IEN, EXTERNAL_ENGINEERING_NETWORKS as EEN

APP = 'охрана_труда'
ALPHABET = [chr(x) for x in range(256, 10000) if 'а' <= chr(x) <= 'я' or 'А' <= chr(x) <= 'Я']
# fields and forms for this field
FIELDS_FORMS = {
    'F1': ['F1'], 'F2': ['F2'], 'F3': ['F3'], 'F4': ['F4', 'F41'], 'F5': ['F5'], 'F6': ['F6'], 'F7': ['F7'],
    'F8': ['F8'], 'F9': ['F9'], 'F10': ['F10'], 'F11': ['F11'], 'F12': ['F12'], 'F13': ['F13'], 'F14': ['F14'],
    'F15': ['F15'], 'F16': ['F16'], 'F17': ['F17'], 'F18': ['F18'], 'F19': ['F19'], 'F20': ['F20'], 'F21': ['F21'],
    'F22': ['F22'], 'F23': ['F23']
}
logger = logging.getLogger('django.request')


def parsing(document, company, about_staff, section='overall', preview=False):
    print(document)
    name_document = '{}.docx'.format(document)
    company_folder_name = ''
    for letter in company.genericinfocompany.short_title:
        if letter in ALPHABET:
            company_folder_name += letter

    folder_for_finished_document = '{0}{1}/{2}'.format(FINISHED_DOCUMENT, company_folder_name, section)
    if not os.path.exists(folder_for_finished_document):
        os.makedirs(folder_for_finished_document)
    document_word = os.path.abspath('{0}{1}/{2}/{3}'.format(
        FINISHED_DOCUMENT, company_folder_name, section, name_document)
    )
    context = get_context(document, company, about_staff)
    if not os.path.exists(document.path):
        error = 'Нет файла обратитесь к администратору {}'.format(document.path)
        logger.error(error)
        return error
    with ZipFile(document.path, 'r') as template_word:
        with ZipFile(document_word, 'w') as new_document_word:
            for item in template_word.infolist():
                buffer = template_word.read(item.filename)
                if item.filename in ['word/document.xml', 'word/header1.xml', 'word/footer1.xml']:
                    with template_word.open(item) as render_file:
                        content = str(render_file.read(), 'utf-8')
                        template = Template(content)
                        new_document_word.writestr(item, template.render(Context(context)).encode())
                else:
                    new_document_word.writestr(item, buffer)

    document_pdf = os.path.abspath(document_word.replace('docx', 'pdf'))
    if os.path.exists(document_pdf):
        os.remove(document_pdf)

    ready = ReadyOTDocument.objects.filter(company=company, ot_document=document).first()
    if ready:
        if ready.pdf_file:
            ready.pdf_file.delete()
    else:
        ready = ReadyOTDocument.objects.create(company=company, ot_document=document)

    if os.path.exists(document_word) and preview:
        import requests
        import requests.exceptions as exc
        with open(document_word, 'rb') as docx:
            try:
                res = requests.post(
                    url='http://127.0.0.2:9016/v1/00000000-0000-0000-0000-000000000000/convert',
                    data=docx,
                    headers={'Content-Type': 'application/octet-stream'}
                )
                f = open(document_pdf, 'wb')
                f.write(res.content)
            except exc.ConnectionError:
                process = Popen('"{}" "{}" /mExportToPDFext /q'.format(WINWORD_PATH, document_word), shell=True)
                process.wait()
                print('Документ сформирован по старому -- {}'.format(document))

        if os.path.exists(document_pdf):
            with open(document_pdf, 'rb') as f:
                ready.pdf_file.save('{}.pdf'.format(document), File(f))

    rus_section = [x[1] for x in OTDocument.SECTIONS if x[0] == section]
    document_word_old_path = '{0}{1}/{2}/{3}'.format(FINISHED_OLD_PATH, company_folder_name, APP, section)
    if rus_section:
        rus_section = rus_section[0].replace(':', '/')
        document_word_old_path = '{0}{1}/{2}/{3}'.format(FINISHED_OLD_PATH, company_folder_name, APP, rus_section)
    # добавление файла в старые папки
    if not os.path.exists(document_word_old_path):
        os.makedirs(document_word_old_path)
    try:
        shutil.copy(document_word, document_word_old_path)
    except PermissionError:
        error = 'Невозможно скопировать файл в папку {} (возможно у вас открыт файл)'.format(document_word_old_path)
        logger.error(error)
        return error
    return True


def get_context(document, company, about_staff):
    context = {}
    fields_data = FieldOT.objects.filter(document=document, company=company).select_related(
        'employee__case', 'staff_profession__case', 'staff_position__case')
    for name_model, name_forms in FIELDS_FORMS.items():
        for name_form in name_forms:
            data_form = context.get(name_model.lower(), [])
            [data_form.append(x) for x in fields_data if x.field_name.lower() == name_form.lower()]
            context.update({name_model.lower(): data_form})

    staff_position_leaders = get_leaders(company)
    boss = staff_position_leaders.get(company.boss_id, None)
    safety_health = staff_position_leaders.get(company.safety_health_id, None)
    development_program = staff_position_leaders.get(company.development_program_id, None)
    development_provision = staff_position_leaders.get(company.development_provision_id, None)

    title_company_ot = ''
    if safety_health:
        company_ot = company if safety_health.staff_id in about_staff.staff_id else safety_health.staff.company
        if company_ot:
            title_company_ot = '{} {}'.format(
                company_ot.genericinfocompany.short_ownership, company_ot.genericinfocompany.short_title
            )

    get_related_context(document, company, context)
    people_who_known = get_list_known_order(context, boss, company) if document.section_document == 'order' else []

    get_answer(context, company)
    type_ownership = get_type_ownership(company)
    company_dangers = get_scroll_dangers_work(company)
    authorized = get_authorized(company)
    industrialist = check_industrialist(company)
    instructions_number = get_instructions_number(company)
    try:
        context.update({
            'transport': [x[1].lower() for x in MEANS_TRANSPORT if x[0] in company.genericinfocompany.means_transport],
            'equipment': [x[1].lower() for x in EQUIPMENT if x[0] in company.genericinfocompany.equipment],
            'ien': [x[1].lower() for x in IEN if x[0] in company.genericinfocompany.internal_engineering_networks],
            'een': [x[1].lower() for x in EEN if x[0] in company.genericinfocompany.external_engineering_networks],
            'transports': company.genericinfocompany.means_transport,
            'equipments': company.genericinfocompany.equipment,
            'iens': company.genericinfocompany.internal_engineering_networks,
            'eens': company.genericinfocompany.external_engineering_networks,
            'i': 1,
            'industrial': industrialist,
            'known': people_who_known,
            'legal_address': company.genericinfocompany.address,
            'address': company.address,
            'dangers': company_dangers,
            'orders': company.work_documents.all().filter(section_document='order'),
            'provisions': company.work_documents.all().filter(section_document='provision'),
            'instructions': get_instructions(instructions_number),
            'ownership': type_ownership,
            'first_authorized': authorized[0],
            'second_authorized': authorized[1],
            'number_protocol': authorized[2],
            'person_authorized': authorized[3],
            'safety_health': safety_health,
            'company_ot': title_company_ot,
            'company_id': company.id,
            'dev_scroll': safety_health,
            'subordination': company.genericinfocompany.subordination,
            'characteristic': company.genericinfocompany.characteristic,
            'documents': [doc.document_code for doc in OTDocument.objects.filter(company=company)],
            'short_ownership': '{}'.format(company.genericinfocompany.short_ownership),
            'company_name': '{} {}'.format(company.genericinfocompany.short_ownership,
                                           company.genericinfocompany.short_title),
            'company_short_case': company.genericinfocompany.short_title_case,
            'staff': Staff.objects.filter(company=company).select_related('case').order_by('surname'),
            'staff_profession': StaffProfession.objects.filter(
                staff__in=Staff.objects.filter(company=company)).select_related('case', 'staff__case'),
            'staff_position': StaffPosition.objects.filter(
                staff__in=Staff.objects.filter(company=company)).select_related('case', 'staff__case'),
            'unique_profession': StaffProfession.objects.filter(
                staff__in=Staff.objects.filter(company=company)).select_related(
                'case', 'staff__case').distinct('work_profession', 'rank'),
            'unique_position': StaffPosition.objects.filter(
                staff__in=Staff.objects.filter(company=company)).select_related(
                'case', 'staff__case').distinct('derivative_position', 'employee_position', 'category'),
            'company_title': company.genericinfocompany.full_title,
            'company_ownership': company.genericinfocompany.ownership,
            'full_name': company.genericinfocompany.full_title,
            'director': boss,
            'person_safety_health': safety_health,
            'dev_instruction': person_dev_instruction(company, document),
            'person_development_program': development_program,
            'person_development_provision': development_provision,
            'instructions_number': instructions_number,
            'number_footer': get_number_footer(document) or ''
        })
    except ObjectDoesNotExist:
        return {}
    return context


def get_answer(context, company):
    questions = {}
    [questions.update({x[0]: x[1]}) for x in Question.objects.filter(division='work_safety').values_list('id', 'code')]
    answers_company = CompanyQuestionAnswer.objects.filter(company=company).values_list('question_id', 'answer')
    [context.update({'q_{}'.format(questions[x[0]]): x[1]}) for x in answers_company if questions.get(x[0], None)]
    return context


def get_leaders(company):
    id_staff_position = {}
    id_safety_health = company.safety_health_id
    id_boss = company.boss_id
    id_development_instruction = company.development_instruction_id
    id_development_program = company.development_program_id
    id_development_provision = company.development_provision_id
    leaders = [id_safety_health, id_boss, id_development_instruction, id_development_program, id_development_provision]
    [id_staff_position.update({x.id: x}) for x in StaffPosition.objects.filter(
        id__in=leaders).select_related('case', 'staff__case')]
    return id_staff_position


def get_list_known_order(context, director, company):
    people_who_known = []
    result = {}
    for data in context.values():
        if data:
            for value in data:
                if hasattr(value, 'staff_position') or hasattr(value, 'staff_profession'):
                    position = getattr(value, 'staff_position')
                    if position and hasattr(position, 'staff'):
                        fio_pos = position.staff.surname, position.staff.initials
                    else:
                        fio_pos = ''
                    profession = getattr(value, 'staff_profession')
                    if profession and hasattr(profession, 'staff'):
                        fio_prof = profession.staff.surname, profession.staff.initials
                    else:
                        fio_prof = ''

                    if position and fio_pos not in result and director != position:
                        result.update({fio_pos: position})
                    if profession and fio_prof not in result and director != profession:
                        result.update({fio_prof: profession})
    for person in result.values():
        people_who_known.append(person)
    return people_who_known


def get_related_context(document, company, context):
    fields = document.related_fields.all()
    if fields:
        for field in fields:
            document = field.document
            field_name = field.field_name
            position_number = field.position_number
            related_data = FieldOT.objects.filter(
                company=company, document=document, position_number=position_number, field_name=field_name).select_related(
                'employee__case', 'staff_position__case', 'staff_profession__case'
            )
            context.update(
                {'{}_{}_{}'.format(document.document_code, field_name, position_number).lower(): related_data}
            )
        return context
    return context


def normalize_title(title):
    short_title = ''
    for letter in title:
        if letter in ALPHABET:
            short_title += letter
    return short_title


def get_type_ownership(company):
    not_society = ['уп', 'частное']
    for ownership in not_society:
        if ownership in company.genericinfocompany.short_ownership.lower():
            return 'Предприятие'
    return 'Общество'


def get_scroll_dangers_work(company):

    from work_safety._factors import WORKS_OF_INCREASED_DANGER as dangers
    unpack_dangers = []
    for danger in dangers:
        if not isinstance(danger[1], tuple):
            unpack_dangers.append(danger)
        else:
            for x in danger[1]:
                unpack_dangers.append(x)

    q = Question.objects.filter(choices='WORK_DANGERS').first()
    company_answer = company.question.filter(question=q).first()
    answer = getattr(company_answer, 'answer')
    if answer:
        answers = answer.split(' ')
        return [x[1] for x in unpack_dangers if x[0] in answers]
    return


def person_dev_instruction(company, document):
    if document.section_document == 'instruction':
        # hard code
        return document.dev_instruction.filter(company=company).first()


def get_authorized(company):
    person_authorized = company.person_authorized
    if person_authorized:
        person_authorized = person_authorized.staffposition_set.first() or person_authorized.staffprofession_set.first()
    if person_authorized and company.personauthorizedcompany.chairperson:
        return ['Председатель ППО',  company.genericinfocompany.short_title_case.gent, '', person_authorized]
    elif person_authorized and company.personauthorizedcompany.public_inspector:
        return ['Общественный инспектор по охране',  'труда профсоюзной организации', '', person_authorized]
    elif person_authorized and company.personauthorizedcompany.protocol_number:
        return ['Протокол заседания',  'Профсоюзного комитета', '№ {}'.format(company.personauthorizedcompany.protocol_number), '']
    elif person_authorized:
        try:
            return ['Уполномоченное лицо работников',  company.genericinfocompany.short_title_case.gent, '', person_authorized]
        except Exception:
            return ['Уполномоченное лицо работников',  '', '', person_authorized]
    else:
        return ['',  '', '', '']


def check_industrialist(company):
    for kind_activity in company.kind_activities.all():
        if kind_activity.production:
            return True
    return False


def get_instructions_number(company):
    instructions_number = {}
    instructions = company.work_documents.filter(section_document='instruction')
    numbers = FieldOT.objects.filter(company=company, document__in=instructions, field_name='F8', position_number=1).select_related('document')
    [instructions_number.update({data.document: data.document_number}) for data in numbers]
    return instructions_number


def get_instructions(instructions_number):
    instructions = [[ins, num]for ins, num in instructions_number.items()]
    instructions.sort(key=lambda x: int(x[1]) if x[1].isdigit() else 0)
    return instructions


def get_number_footer(document):
    number_doc = re.findall(r'\d+.\d+', document.title)
    number_doc = number_doc[0] if number_doc else ''
    return number_doc
