# coding: utf8
from django.views.generic import DetailView, FormView, TemplateView, CreateView, UpdateView
from .forms import *
from company._utility import AllAboutStaff
from work_safety.parsing_document import parsing
from django.http.response import HttpResponseRedirect, HttpResponseNotFound, HttpResponse
from .models import OTDocument, ReadyOTDocument
from django.http import Http404
from ._utility import get_dictionary_massive, get_persons_signed
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from company.views import check_all_cases, check_staffing_table
import json
from django.http.response import JsonResponse
from django.views.decorators.http import require_safe
from django.contrib import messages
from django.core.cache import cache
from math import floor
from django.db.models import Count, Q, Prefetch


def check_questions(self, set_message=False):
    if hasattr(self, 'login_url'):
        self.login_url = reverse('company:question', kwargs={
            'pk': self.request.user.pk, 'division': 'work_safety'
        })
    href = self.login_url if hasattr(self, 'login_url') else ''
    for answer in self.request.user.question.filter(question__division='work_safety').values_list('answer'):
        if answer[0] and 'no_m' != answer[0]:
            return True
    if set_message:
        storage = messages.get_messages(self.request)
        storage.used = True
        messages.error(self.request, 'Ответьте на вопросы <a href="{}">здесь</a>'.format(href))
    return False


def check_generic_info(self, set_message=False):
    if hasattr(self, 'login_url'):
        self.login_url = reverse('company:generic_info_update', kwargs={'pk': self.request.user.genericinfocompany.pk})
    href = self.login_url if hasattr(self, 'login_url') else ''
    if self.request.user.genericinfocompany.full_title:
        return True
    if set_message:
        storage = messages.get_messages(self.request)
        storage.used = True
        messages.error(self.request, 'Заполните Общую информацию о вашей фирме <a href="{}">здесь</a>'.format(href))
    return False


def check_position_leaders(self, set_message=False):
    if hasattr(self, 'login_url'):
        self.login_url = reverse('company:update_leaders', kwargs={'pk': self.request.user.pk, 'section': 'work'})
    href = self.login_url if hasattr(self, 'login_url') else ''
    if self.request.user.boss:
        return True
    if set_message:
        storage = messages.get_messages(self.request)
        storage.used = True
        messages.error(self.request, 'Укажите людей <a href="{}">здесь</a>'.format(href))
    return False


def check_pass_test(self, set_message=False):
    if hasattr(self, 'login_url'):
        self.login_url = reverse('work-safety:create-document-chain', kwargs={'q_pk': 0})
    href = self.login_url if hasattr(self, 'login_url') else ''

    id_exclude_fields = self.request.user.work_field_exclude.all().values_list('id')
    company_ot_documents = self.request.user.work_documents.all().values('id')
    company_ot_documents = [x['id'] for x in company_ot_documents]
    chain_ot_fields = FieldInDocument.objects.filter(element_chain=True).exclude(id__in=id_exclude_fields).values(
        'document', 'field_name', 'position_number'
    )
    all_field_ot = FieldOT.objects.filter(company=self.request.user).values_list(
        'company', 'document', 'field_name', 'position_number'
    )
    chain_ot_document = {}
    for field in chain_ot_fields:
        document_id = field['document']
        value = chain_ot_document.get(document_id, [])
        value.append(dict(name=field['field_name'], num=field['position_number']))
        chain_ot_document.update({document_id: value})
    id_documents_chain = [key for key in chain_ot_document.keys()]
    id_documents_chain.sort()
    for id_document in id_documents_chain:
        if id_document in company_ot_documents:
            chain_fields = chain_ot_document[id_document]
            for field in chain_fields:
                field_name = field['name']
                position_number = field['num']
                if not (self.request.user.id, id_document, field_name, position_number) in all_field_ot:
                    if set_message:
                        storage = messages.get_messages(self.request)
                        storage.used = True
                        messages.error(self.request, 'Ответьте на тест <a href="{}">здесь</a>'.format(href))
                    return False
    return True


@method_decorator(login_required, name='dispatch')
class SizView(FormView):
    form_class = SizForm
    template_name = 'work_safety/siz.html'

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            about_staff = AllAboutStaff(self.request.user).exclude_mayor()
            response = parsing(OTDocument.objects.first(), self.request.user, about_staff, 'siz', preview=True)
            if response:
                return HttpResponseRedirect(reverse('work-safety:siz'))
        else:
            return self.form_invalid(form)


@method_decorator(login_required, name='dispatch')
class ProfPosListView(DetailView):

    model = Company
    template_name = 'work_safety/profession_position_list.html'

    def get_context_data(self, **kwargs):
        context = super(ProfPosListView, self).get_context_data(**kwargs)
        about_staff = AllAboutStaff(self.object).exclude_mayor()
        positions, professions = [], []
        [positions.append([id_pos, title]) for id_pos, title in about_staff.get_positions.items()]
        positions.sort(key=lambda x: x[1])
        [professions.append([id_prof, title]) for id_prof, title in about_staff.get_professions.items()]
        professions.sort(key=lambda x: x[1])
        positions.extend(professions)
        context['profession_position'] = positions
        return context


@method_decorator(login_required, name='dispatch')
class WorksWithSizsView(TemplateView):

    template_name = 'work_safety/list_works_with_siz.html'

    def get_context_data(self, **kwargs):
        context = super(WorksWithSizsView, self).get_context_data(**kwargs)
        about_staff = AllAboutStaff(self.request.user).exclude_mayor()
        positions = [id_pos for id_pos, title in about_staff.get_positions.items()]
        professions = [id_prof for id_prof, title in about_staff.get_professions.items()]
        positions = EmployeePosition.objects.filter(id__in=positions).prefetch_related(
            'sizs',
            Prefetch(
                'staffposition_set',
                queryset=StaffPosition.objects.filter(staff__in=about_staff.staff_id).select_related('staff')
            )
        )
        professions = WorkProfession.objects.filter(id__in=professions).prefetch_related(
            'sizs',
            Prefetch(
                'staffprofession_set',
                queryset=StaffProfession.objects.filter(staff__in=about_staff.staff_id).select_related('staff')
            )
        )
        professions_with_siz = [prof for prof in professions if prof.sizs.exists()]
        positions_with_siz = [pos for pos in positions if pos.sizs.exists()]
        context['professions'], context['positions'] = professions_with_siz, positions_with_siz
        return context


@method_decorator(login_required, name='dispatch')
class SizCreateView(CreateView):

    form_class = SizCreateFormSet
    template_name = 'work_safety/siz_create.html'

    def get_form_kwargs(self):
        kwargs = super(SizCreateView, self).get_form_kwargs()
        kwargs.update({'company': self.request.user, 'pk': self.kwargs.get('pk')})
        kwargs.pop('instance', None)
        return kwargs

    def get_context_data(self, **kwargs):
        try:
            context = super(SizCreateView, self).get_context_data(**kwargs)
        except AttributeError:
            raise Http404('No %s matches the given query.' % self.kwargs.get('pk'))
        pk = self.kwargs.get('pk')
        work = None
        if pk:
            profession = WorkProfession.objects.filter(id=int(pk)).first()
            position = EmployeePosition.objects.filter(id=int(pk)).first()
            work = profession if profession else position
        if work and context:
            context['work'] = work
        return context

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        pk = self.kwargs.get('pk')
        position, profession, company = None, None, self.request.user
        if pk:
            profession = WorkProfession.objects.filter(id=int(pk)).first()
            position = EmployeePosition.objects.filter(id=int(pk)).first()
        if position or profession:
            instances = form.save(commit=False)
            for instance in instances:
                instance.add_by_company, instance.profession, instance.position = company, profession, position
                instance.save()
            form.save()
            return HttpResponseRedirect(reverse('work-safety:siz-create', kwargs={'pk': pk}))
        return HttpResponseNotFound('<h1>Page not found</h1>')


@method_decorator(login_required, name='dispatch')
class SizUpdateView(FormView):

    form_class = SizUpdateFormSet
    template_name = 'work_safety/siz_update.html'

    def get_context_data(self, **kwargs):
        context = super(SizUpdateView, self).get_context_data()
        form = context['form']
        context['d'], context['m'] = get_dictionary_massive(form)
        return context

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_form_kwargs(self):
        kwargs = super(SizUpdateView, self).get_form_kwargs()
        company = self.request.user
        about_staff = AllAboutStaff(company).exclude_mayor()
        professions = [x for x in about_staff.get_professions.keys()]
        positions = [x for x in about_staff.get_positions.keys()]
        queryset = Siz.objects.filter(
            (Q(profession_id__in=professions) | Q(position_id__in=positions)) & ~Q(add_by_company=company)
        ).select_related(
            'profession', 'position'
        ).prefetch_related('exception_company').order_by('profession', 'position', 'id')
        exception_siz = [x[0] for x in Siz.objects.filter(exception_company=company).values_list('id')]
        kwargs['form_kwargs'] = {'exception_siz': exception_siz, 'company': company}
        kwargs['company'] = company
        kwargs['queryset'] = queryset
        return kwargs

    def get_success_url(self):
        return reverse('work-safety:instructions')


@method_decorator(login_required, name='dispatch')
class DocumentCreate(CreateView):
    template_name = 'work_safety/create_work_safety_document.html'
    all_forms = all_forms()
    form_kwargs = None
    object = None
    prefix = None
    document = None
    forms_instances = None
    about_staff = None
    formsets_prefix = FORMSETS_PREFIX
    staff = None
    some_pos = None
    some_prof = None

    def get_context_data(self, **kwargs):
        context = super(DocumentCreate, self).get_context_data(**kwargs)
        context['document'] = self.document
        context['staff'] = Staff.objects.filter(company=self.request.user)
        context['staff_work'] = self.about_staff.exclude_mayor().get_staff_works_read()
        context['formset'] = self.formsets_prefix
        context['method'] = self.request.method
        context['ready_document'] = ReadyOTDocument.objects.filter(
            company=self.request.user, ot_document=self.document).first()
        return context

    def get_forms(self, exclude_fields, render_fields=None):
        render_forms = []
        if render_fields is None:
            return
        fields = [(x.field_name, x.position_number, x.prefix, x.label) for x in render_fields if x not in exclude_fields]
        [self.forms_instances.update({(x.field_name, x.position_number): x}) for x in
         FieldOT.objects.filter(company=self.request.user, document=self.document)]
        for field in fields:
            form_name, position, prefix, label, name = field[0], field[1], field[2], field[3], field[0]
            self.prefix = prefix
            self.form_kwargs = dict(
                document=self.document,
                company=self.request.user,
                field_name=name,
                position_number=position,
                label=label,
                about_staff=self.about_staff,
                staff_queryset=self.staff,
                some_prof=self.some_prof,
                some_pos=self.some_pos,
                forms_instances=self.forms_instances
            )
            form = self.get_form(form_name)
            render_forms.append(form)
        return render_forms

    def get_form(self, form_name=None):
        if form_name:
            form_class = self.all_forms.get(form_name)
            return form_class(**self.get_form_kwargs())
        return

    def delete_data_exclude_fields(self, exclude_fields):
        fields = exclude_fields.values_list('document_id', 'field_name', 'position_number')
        if fields:
            company = self.request.user
            queries = [Q(company=company, document=f[0], field_name=f[1], position_number=f[2]) for f in fields]
            # Take one Q object from the list
            query = queries.pop()
            # Or the Q object with the ones remaining in the list
            for item in queries:
                query |= item
            FieldOT.objects.filter(query).delete()

    def get_form_kwargs(self):
        form_name = self.form_kwargs.get('field_name')
        form = self.all_forms.get(form_name)
        kwargs = dict(prefix=self.prefix, form_kwargs=self.form_kwargs)
        if form and self.request.method == 'POST':
            kwargs.update({'data': self.request.POST})
        if not hasattr(form, 'management_form'):
            instance = self.forms_instances.get(
                (self.form_kwargs['field_name'], self.form_kwargs['position_number']), None)
            kwargs.update({'instance': instance})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.forms_instances = {}
        self.about_staff = self.company_staff if hasattr(self, 'company_staff') else AllAboutStaff(self.request.user)
        self.about_staff.add_mayor()
        self.staff = Staff.objects.filter(id__in=self.about_staff.staff_id).order_by('surname')
        self.some_prof = StaffProfession.objects.filter(staff_id__in=self.about_staff.get_staff_some_work())
        self.some_pos = StaffPosition.objects.filter(staff_id__in=self.about_staff.get_staff_some_work())
        exclude_fields = self.request.user.work_field_exclude.all()
        document = OTDocument.objects.filter(id=int(kwargs.get('pk', -1))).first()
        fields = document.fields.all().order_by('index_number') if document else kwargs.pop('fields', None)
        if not fields and not document:
            raise Http404
        self.document = document or self.document
        self.delete_data_exclude_fields(exclude_fields)
        render_forms = self.get_forms(exclude_fields, render_fields=fields)
        return self.render_to_response(
            self.get_context_data(form=render_forms)
        )

    def post(self, request, *args, **kwargs):
        self.forms_instances = {}
        exclude_fields = self.request.user.work_field_exclude.all()
        self.about_staff = self.company_staff if hasattr(self, 'company_staff') else AllAboutStaff(self.request.user)
        self.about_staff.add_mayor()
        self.staff = Staff.objects.filter(id__in=self.about_staff.staff_id).order_by('surname')
        self.some_prof = StaffProfession.objects.filter(staff_id__in=self.about_staff.get_staff_some_work())
        self.some_pos = StaffPosition.objects.filter(staff_id__in=self.about_staff.get_staff_some_work())
        errors_in_forms = False
        company = self.request.user
        document = OTDocument.objects.filter(id=int(kwargs.get('pk', -1))).first()
        fields = document.fields.all().order_by('index_number') if document else kwargs.pop('fields', None)
        if not fields and not document:
            raise Http404
        self.document = fields.first().document if fields else document
        if document and not fields:
            response = parsing(document, self.request.user, self.about_staff, document.section, preview=True)
            if response:
                return HttpResponseRedirect(reverse('work-safety:list-document', kwargs={'section': document.section}))
        render_forms = self.get_forms(exclude_fields, render_fields=fields)
        if render_forms:
            for form in render_forms:
                if not form.is_valid():
                    errors_in_forms = True
            if not errors_in_forms:
                for form in render_forms:
                    if not hasattr(form, 'management_form'):
                        instance = form.save(commit=False)
                        instance.company = company
                        instance.document = self.document
                        instance.position_number = form.position_number
                        instance.field_name = form.field_name
                        instance.label = form.label
                        instance.save()
                        form.save_m2m()
                    else:
                        instances = form.save(commit=False)
                        [obj.delete() for obj in form.deleted_objects]
                        for cls in instances:
                            cls.company = company
                            cls.document = self.document
                            cls.position_number = form.position_number
                            cls.field_name = form.field_name
                            cls.label = form.label
                            cls.save()
                        form.save_m2m()
            else:
                return self.form_invalid(render_forms)
            if not document:
                success_url = kwargs.get('success_url')
                return HttpResponseRedirect(success_url)
            response = parsing(document, self.request.user, self.about_staff, document.section, preview=True)
            if response:
                error = response if isinstance(response, str) else None
                if error:
                    messages.error(self.request, '{}'.format(error))
                return HttpResponseRedirect(reverse('work-safety:list-document', kwargs={'section': document.section}))
        return HttpResponseRedirect(reverse('work-safety:create-document', kwargs={'pk': document.pk}))


@method_decorator(login_required, name='dispatch')
class ChainDocumentCreate(PermissionRequiredMixin, DocumentCreate):
    template_name = 'work_safety/create_document_chain.html'
    chain_documents = []

    def get_context_data(self, **kwargs):
        context = super(ChainDocumentCreate, self).get_context_data(**kwargs)
        context['document'] = self.document
        questions_remain = len([x for x in self.chain_documents if not x[1]])
        context['documents_chain'] = self.chain_documents
        context['questions_remain'] = questions_remain
        context['q_pk'] = int(self.kwargs.get('q_pk', '0'))+1
        return context

    def get_document_in_chain(self):
        return_chain_documents = []
        id_exclude_fields = self.request.user.work_field_exclude.all().values_list('id')
        company_ot_documents = self.request.user.work_documents.all().values('id')
        company_ot_documents = [x['id'] for x in company_ot_documents]
        chain_ot_fields = FieldInDocument.objects.filter(element_chain=True).exclude(id__in=id_exclude_fields).values(
            'document', 'field_name', 'position_number'
        )
        all_field_ot = FieldOT.objects.filter(company=self.request.user).values_list(
            'company', 'document', 'field_name', 'position_number'
        )
        chain_ot_document = {}
        for field in chain_ot_fields:
            document_id = field['document']
            value = chain_ot_document.get(document_id, [])
            value.append(dict(name=field['field_name'], num=field['position_number']))
            chain_ot_document.update({document_id: value})
        id_documents_chain = [key for key in chain_ot_document.keys()]
        id_documents_chain.sort()
        for id_document in id_documents_chain:
            if id_document in company_ot_documents:
                ready = True
                chain_fields = chain_ot_document[id_document]
                for field in chain_fields:
                    field_name = field['name']
                    position_number = field['num']
                    if not (self.request.user.id, id_document, field_name, position_number) in all_field_ot:
                        ready = False
                        break
                return_chain_documents.append([id_document, ready])
        q_pk = self.kwargs.get('q_pk', '0')
        try:
            id_document = return_chain_documents[int(q_pk)][0]
            return id_document, return_chain_documents
        except IndexError:
            return None, return_chain_documents

    def get(self, request, *args, **kwargs):
        id_document, self.chain_documents = self.get_document_in_chain()
        if id_document is None:
            return HttpResponseRedirect(reverse('work-safety:instructions'))
        self.document = OTDocument.objects.get(id=id_document)
        render_fields = self.document.fields.filter(element_chain=True).order_by('index_number')
        kwargs.update({'fields': render_fields})
        response = super(ChainDocumentCreate, self).get(request, *args, **kwargs)
        return response

    def post(self, request, *args, **kwargs):
        document_id, documents = self.get_document_in_chain()
        if document_id is None:
            return HttpResponseRedirect(reverse('home'))
        render_fields = OTDocument.objects.get(id=document_id).fields.filter(element_chain=True).order_by('index_number')
        success_url = reverse('work-safety:instructions')
        q_pk = self.kwargs.get('q_pk', '0')
        for i, doc in enumerate(documents, 0):
            if not doc[1] and int(q_pk) < i:
                success_url = reverse('work-safety:create-document-chain', kwargs={'q_pk': i})
                break
        kwargs.update({'fields': render_fields, 'success_url': success_url})
        response = super(ChainDocumentCreate, self).post(request, *args, **kwargs)
        return response

    def has_permission(self):
        if not check_generic_info(self, set_message=True):
            return False
        elif not check_questions(self, set_message=True):
            return False
        elif not check_position_leaders(self, set_message=True):
            return False
        elif not check_staffing_table(self, set_message=True):
            return False
        elif not check_all_cases(self, set_message=True):
            return False
        return True


@method_decorator(login_required, name='dispatch')
class CreateDocumentsOTView(ChainDocumentCreate, TemplateView):
    template_name = 'work_safety/create_documents_ot.html'
    pk = None

    def get(self, request, *args, **kwargs):
        return self.render_to_response({})

    def post(self, request, *args, **kwargs):
        ot_documents = self.request.user.work_documents.all().order_by('id')
        count_documents = ot_documents.count()
        import time
        start = time.time()
        time_processing = None
        num_document = cache.get(self.request.user) + 1 if cache.get(self.request.user) else 1
        if num_document < count_documents:
            if num_document == 1:
                cache.set('{}_time'.format(self.request.user.id), start, 60*15)
            cache.set(self.request.user, num_document, 60*0.5)
            document = ot_documents[num_document - 1]
        elif num_document == count_documents:
            document = ot_documents.last()
            time_processing = int(time.time() - cache.get('{}_time'.format(self.request.user.id)))
            cache.delete_many(['{}_time'.format(self.request.user.id), self.request.user])
        else:
            return
        about_staff = self.about_staff or AllAboutStaff(self.request.user)
        error = parsing(document, self.request.user, about_staff, document.section, preview=False)
        error = error if isinstance(error, str) else None
        percent = floor(100*int(num_document)/int(count_documents))
        return JsonResponse({'percent': percent, 'time': time_processing, 'error': error})

    def has_permission(self):
        if not check_pass_test(self, set_message=True):
            return False
        elif not check_all_cases(self, set_message=True):
            return False
        return super(CreateDocumentsOTView, self).has_permission()


def get_branch(branch, company, section):
    company_documents_id = [x.id for x in company.work_documents.all()]
    count_ot_doc = DocumentStructure.objects.get(url=branch.url).ot_documents.filter(id__in=company_documents_id).count()
    if not branch.documentstructure_set.all():
        if branch.url == section:
            return {'text': branch.title, 'href': branch.url, 'tags': [count_ot_doc], 'state': {'selected': 'true'}}
        return {'text': branch.title, 'href': branch.url, 'tags': [count_ot_doc]}
    children = []
    for br in branch.documentstructure_set.all():
        children.append(get_branch(br, company, section))
    if branch.url == section:
        return {'text': branch.title, 'nodes': children, 'href': branch.url, 'tags': [count_ot_doc], 'state': {'selected': 'true'}}
    return {'text': branch.title, 'nodes': children, 'href': branch.url, 'tags': [count_ot_doc]}


def get_branch_new(id_branch, all_branches, id_branch_ot_doc, section):
    children = [branch for branch, data in all_branches.items() if id_branch == data['section']]
    data = all_branches[id_branch]
    count_ot_doc = len(id_branch_ot_doc[id_branch])
    if not children:
        if data['url'] == section:
            return {
                'text': data['title'], 'href': data['url'], 'tags': [count_ot_doc], 'state': {'selected': 'true'}
            }
        return {'text': data['title'], 'href': data['url'], 'tags': [count_ot_doc]}
    nodes = []
    for child in children:
        nodes.append(get_branch_new(child, all_branches, id_branch_ot_doc, section))
    if data['url'] == section:
        return {
            'text': data['title'],
            'nodes': nodes,
            'href': data['url'],
            'tags': [count_ot_doc],
            'state': {'selected': 'true'}
        }
    return {'text': data['title'], 'nodes': nodes, 'href': data['url'], 'tags': [count_ot_doc]}


@method_decorator(login_required, name='dispatch')
class TreeView(TemplateView):
    template_name = 'work_safety/list_document_in_section.html'
    section = None
    
    def get_context_data(self, **kwargs):
        context = super(TreeView, self).get_context_data()
        section = kwargs.get('section')
        if section not in [x[0] for x in SECTIONS] + ['ot']:
            raise Http404
        company_documents = [x[0] for x in self.request.user.work_documents.values_list('id')]
        documents = DocumentStructure.objects.get(url=section).ot_documents.filter(id__in=company_documents)
        id_branch_data = {}
        id_branch_ot_doc = {}
        all_branches = DocumentStructure.objects.values('id', 'url', 'title', 'section', 'ot_documents')
        for branch in all_branches:
            ot_documents = id_branch_ot_doc.get(branch['id'], [])
            document = branch.get('ot_documents', None)
            if document in company_documents:
                ot_documents.append(document)
            id_branch_ot_doc.update({branch['id']: ot_documents})
            id_branch_data.update({branch['id']: {
                'url': branch['url'], 'title': branch['title'], 'section': branch['section']
            }})
        top_branch_id = [id_branch for id_branch, data in id_branch_data.items() if not data['section']]

        tree = [get_branch_new(id_branch, id_branch_data, id_branch_ot_doc, section) for id_branch in top_branch_id]
        tree = json.dumps(tree)
        context['tree'] = tree
        documents = [doc for doc in documents]
        documents.sort(key=lambda x: int(''.join(re.findall(r'\d+', x.title))) if re.findall(r'\d+', x.title) else 0)
        context['documents'] = documents
        ready = [(x[0], x[1]) for x in self.request.user.readyotdocument_set.values_list('ot_document', 'pdf_file')]
        context['ready'] = [x[0] for x in ready]
        context['ready_pdf'] = [x[0] for x in ready if x[1]]
        return context


@require_safe
@login_required
def get_documents_by_section(request, section=None):
    if not [x for x in DocumentStructure.SECTIONS if section == x[0]]:
        return JsonResponse({})
    company = request.user
    documents = []
    section_documents = DocumentStructure.objects.get(url=section).ot_documents.filter(company=company).only(
        'title', 'id').order_by('id')
    section_documents = [x for x in section_documents]
    section_documents.sort(key=lambda x: int(''.join(re.findall(r'\d+', x.title))) if re.findall(r'\d+', x.title) else 0)
    ready = [(x[0], x[1]) for x in request.user.readyotdocument_set.filter(
        ot_document_id__in=section_documents).values_list('ot_document', 'pdf_file')]
    ready_word = [x[0] for x in ready]
    ready_pdf = [x[0] for x in ready if x[1]]
    for document in section_documents:
        if document.id in ready_word and document.id in ready_pdf:
            documents.append(['{} ({})'.format(document.title, document.document_code),
                              reverse('work-safety:create-document', args=(document.id,)),
                              reverse('doc_ot', args=(company.pk, section, document.id)),
                              document.id
                              ])
        else:
            documents.append(['{} ({})'.format(document.title, document.document_code),
                              reverse('work-safety:create-document', args=(document.id,)),
                              '',
                              document.id
                              ])
    return JsonResponse({'documents': documents, 'ready_word': ready_word})


@method_decorator(login_required, name='dispatch')
class OTInstructionView(TemplateView):
    template_name = 'work_safety/instructions_user.html'
    login_url = None

    def get_context_data(self, **kwargs):
        context = super(OTInstructionView, self).get_context_data(**kwargs)
        context['staff_check'] = check_staffing_table(self)
        context['generic_info'] = check_generic_info(self)
        context['questions'] = check_questions(self)
        context['leaders'] = check_position_leaders(self)
        context['test'] = check_pass_test(self)
        context['cases'] = check_all_cases(self)
        return context


class DataFilingView(FormView):
    form_class = DataFilingForm
    template_name = 'work_safety/data_filing.html'

    def get_form_kwargs(self):
        kwargs = super(DataFilingView, self).get_form_kwargs()
        kwargs['company'] = self.request.user
        return kwargs

    def get_success_url(self):
        return reverse('work-safety:data_filing')


@method_decorator(login_required, name='dispatch')
class DownloadOTDocumentView(FormView):
    form_class = OTDownloadForm
    template_name = 'work_safety/download_documents.html'

    def get_form_kwargs(self, *args):
        kwargs = super(DownloadOTDocumentView, self).get_form_kwargs()
        kwargs['initial'] = {'work_documents': args[2]}
        kwargs['prefix'] = args[0]
        kwargs['choices'] = args[1]
        return kwargs

    def get_form(self, form_class=None):
        form = []
        if form_class is None:
            form_class = self.get_form_class()
            url_count_doc = DocumentStructure.objects.annotate(count_doc=Count('ot_documents')).order_by('-section').only(
                'id', 'url')
            all_documents = [x for x in OTDocument.objects.values_list('id', 'title', 'documentstructure__id')]
            all_documents.sort(key=lambda x: int(''.join(re.findall(r'\d+', x[1]))) if re.findall(r'\d+', x[1]) else 0)
            company_documents = [x[0] for x in self.request.user.work_documents.values_list('id')]
            for document_structure in url_count_doc:
                if document_structure.count_doc:
                    url = document_structure.url
                    choices = [(x[0], x[1]) for x in all_documents if x[2] == document_structure.id]
                    form.append(form_class(**self.get_form_kwargs(url, choices, company_documents)))
        return form

    def get_context_data(self, **kwargs):
        context = super(DownloadOTDocumentView, self).get_context_data()
        about_staff = AllAboutStaff(self.request.user).exclude_mayor()
        staff_positions = [about_staff.staff_position[x] for x in about_staff.get_staff_unique_staff_position()]
        staff_professions = [about_staff.staff_profession[x] for x in about_staff.get_staff_unique_staff_profession()]
        positions = ['{} {} {}'.format(x['derivative'], about_staff.positions[x['pos_id']], x['category']) for x in staff_positions]
        professions = ['{} {}'.format(about_staff.professions[x['prof_id']], x['rank']) for x in staff_professions]
        context['positions'] = positions
        context['professions'] = professions
        context['staff_work'] = about_staff.get_staff_works_read()
        return context

    def post(self, request, *args, **kwargs):
        forms = self.get_form()
        for form in forms:
            if not form.is_valid():
                return self.form_invalid(forms)
        return self.form_valid(forms)

    def form_valid(self, forms):
        all_save_documents = []
        for form in forms:
            ot_documents = form.cleaned_data['work_documents']
            [all_save_documents.append(x) for x in ot_documents]
        self.request.user.work_documents.set(OTDocument.objects.filter(id__in=all_save_documents))
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('work-safety:create-document-chain', kwargs={'q_pk': '0'})


class GenerateOTDocuments(FormView):
    form_class = OTDocumentForm
    template_name = 'work_safety/create_select_ot_docs.html'

    def get_form_kwargs(self):
        kwargs = super(GenerateOTDocuments, self).get_form_kwargs()
        kwargs['queryset'] = self.request.user.work_documents.all().order_by('section')
        return kwargs

    def form_valid(self, form):
        documents = form.cleaned_data['documents']
        about_staff = AllAboutStaff(self.request.user)
        for document in documents:
            parsing(document, self.request.user, about_staff, document.section, preview=False)
        return HttpResponseRedirect(reverse('work-safety:instructions'))


class DownloadMedicalDataView(FormView):
    form_class = DownloadMedicalDataForm
    template_name = 'work_safety/download_medical_data.html'

    def form_valid(self, form):
        import xlrd
        from company._utility import get_distance
        import datetime
        document_xls = form.cleaned_data['file_excel']
        document = xlrd.open_workbook(file_contents=document_xls.read())
        sheet = document.sheet_by_index(0)
        staff = Staff.objects.filter(company=self.request.user).values('id', 'surname', 'initials')
        staff_work = {}
        positions = StaffPosition.objects.filter(staff__in=[x['id'] for x in staff]).select_related('staff').values(
            'case__nomn', 'id', 'staff')
        professions = StaffProfession.objects.filter(staff__in=[x['id'] for x in staff]).select_related('staff').values(
            'staff', 'case__nomn', 'id')
        for profession in professions:
            for person in staff:
                id_person = person['id']
                fio = '{} {}'.format(person['surname'], person['initials'])
                fio = ''.join(re.findall(r'[а-я]', fio.lower()))
                id_profession = profession['id']
                prof = '{}'.format(profession['case__nomn'])
                prof = ''.join(re.findall(r'[а-я]', prof.lower()))
                if id_person == profession['staff']:
                    if not prof or not fio:
                        break
                    staff_work.update({(id_person, id_profession, 'prof'): [fio, prof]})
                    break
        for position in positions:
            for person in staff:
                id_person = person['id']
                fio = '{} {}'.format(person['surname'], person['initials'])
                fio = ''.join(re.findall(r'[а-я]', fio.lower()))
                id_position = position['id']
                pos = '{}'.format(position['case__nomn'])
                pos = ''.join(re.findall(r'[а-я]', pos.lower()))
                if id_person == position['staff']:
                    if not pos or not fio:
                        break
                    staff_work.update({(id_person, id_position, 'pos'): [fio, pos]})
                    break
        result = {}
        excel_data_row = []
        for row_num in range(sheet.nrows):
            row = sheet.row_values(row_num)
            fio, work, sex, birthday, address, num_med, date_med = row[1], row[2], row[3], row[4], row[5], row[6], row[7]
            fio = re.findall(r'[а-я]+\b', fio.lower())
            if not len(fio) == 3:
                continue
            fio = ''.join([fio[0], fio[1][0], fio[2][0]])
            work = ''.join(re.findall(r'[а-я]', work.lower()))
            sex = 'masc' if sex.lower().startswith('м') else 'femn'
            if isinstance(birthday, float):
                value_cell = sheet.cell_value(rowx=row_num, colx=4)
                birthday = datetime.datetime(*xlrd.xldate_as_tuple(value_cell, document.datemode))
            else:
                birthday = re.findall(r'\d{2}.\d{2}.\d{4}', birthday)
                birthday = datetime.datetime.strptime(birthday[0], '%d.%m.%Y') if birthday else None
            address = address.strip()
            if isinstance(num_med, float):
                num_med = '{}'.format(int(num_med))
            else:
                num_med = num_med.strip() if len(num_med.strip()) > 1 else ''
            if isinstance(date_med, float):
                value_cell = sheet.cell_value(rowx=row_num, colx=7)
                date_med = datetime.datetime(*xlrd.xldate_as_tuple(value_cell, document.datemode))
            else:
                date_med = re.findall(r'\d{2}.\d{2}.\d{4}', date_med)
                date_med = datetime.datetime.strptime(date_med[0], '%d.%m.%Y') if date_med else None
            excel_data_row.append([fio, work, sex, birthday, address, num_med, date_med])
        for data in excel_data_row:
            fio_excel = data[0]
            work_excel = data[1]
            buffer = []
            for key, value in staff_work.items():
                fio = value[0]
                if fio_excel == fio:
                    buffer.append([key, value])
            if len(buffer) == 0:
                for key, value in staff_work.items():
                    fio = value[0]
                    distance = get_distance(fio_excel, fio)
                    if distance == 1 and fio_excel and fio:
                        result.update({key: data})
                        break
            elif len(buffer) == 1:
                result.update({buffer[0][0]: data})

            else:
                key_data_distance = [[key, get_distance(work_excel, value[1])] for key, value in buffer]
                key_data_distance.sort(key=lambda x: x[1])
                key = key_data_distance[0][0]
                result.update({key: data})
        all_staff = Staff.objects.filter(id__in=[x[0] for x in result.keys()])
        all_staff_positions = StaffPosition.objects.filter(id__in=[x[1] for x in result.keys() if x[2] == 'pos'])
        all_staff_professions = StaffProfession.objects.filter(id__in=[x[1] for x in result.keys() if x[2] == 'prof'])
        for key, value in result.items():
            for person in all_staff:
                if person.id == key[0]:
                    staff = person
                    break
            if key[2] == 'prof':
                for staff_prof in all_staff_professions:
                    if staff_prof.id == key[1]:
                        staff_profession = staff_prof
                        break
            if key[2] == 'pos':
                for staff_pos in all_staff_positions:
                    if staff_pos.id == key[1]:
                        staff_position = staff_pos
                        break
            staff.date_birthday = value[3]
            staff.sex = value[2]
            staff.place_residence = value[4]
            staff._add_staff = False
            staff.save()
            if key[2] == 'pos':
                staff_position.medical_certificate_number = value[5]
                staff_position.date_issue_medical_certificate = value[6]
                staff_position._add_staff_position = False
                staff_position.save()
            elif key[2] == 'prof':
                staff_profession.medical_certificate_number = value[5]
                staff_profession.date_issue_medical_certificate = value[6]
                staff_profession._add_staff_profession = False
                staff_profession.save()
        return HttpResponseRedirect(reverse('company:position_profession', args=(self.request.user.pk,)))


class WorkFieldExcludeUpdateView(UpdateView):
    model = Company
    template_name = 'work_safety/update_fields_document.html'
    form_class = UpdateWorkExcludeFieldForm

    def get_form_kwargs(self):
        kwargs = super(WorkFieldExcludeUpdateView, self).get_form_kwargs()
        try:
            ot_document = OTDocument.objects.get(id=int(self.kwargs['pk_doc']))
        except Exception:
            raise Http404
        kwargs['queryset'] = ot_document.fields.filter(~Q(label=''))
        kwargs['redirect_url'] = self.request.META.get('HTTP_REFERER') if self.request.method == 'GET' else None
        return kwargs

    def get_context_data(self, **kwargs):

        context = super(WorkFieldExcludeUpdateView, self).get_context_data(**kwargs)
        try:
            ot_document = OTDocument.objects.get(id=int(self.kwargs['pk_doc']))
        except Exception:
            raise Http404
        context['document'] = ot_document
        return context

    def form_valid(self, form):
        try:
            ot_document = OTDocument.objects.get(id=int(self.kwargs['pk_doc']))
        except Exception:
            raise Http404
        fields = ot_document.fields.filter(~Q(label=''))
        for field in fields:
            if field in form.cleaned_data['work_field_exclude']:
                self.object.work_field_exclude.add(field)
            else:
                self.object.work_field_exclude.remove(field)
        redirect_url = form.cleaned_data['redirect_url']
        if redirect_url:
            return HttpResponseRedirect(redirect_url)
        else:
            return HttpResponseRedirect(reverse('work-safety:instructions'))

    def get_success_url(self):
        return reverse('work-safety:update_field', kwargs={'pk': self.kwargs['pk'], 'pk_doc': self.kwargs['pk_doc']})


def get_person_signed_document(request):
    get_persons_signed(request.user)
    return HttpResponseRedirect(reverse('work-safety:instructions'))


def create_num_instructions(request):
    instructions = request.user.work_documents.filter(section_document='instruction')
    fields_data = FieldOT.objects.filter(
        document__in=instructions, company=request.user, field_name='F8', position_number=1).values_list('document', 'document_number')
    doc_date = {}
    fields_data = [x for x in fields_data]
    fields_data.sort(key=lambda x: int(x[1]) if x[1] else 0)
    count = 1 if not fields_data else int(fields_data[-1][-1]) + 1
    [doc_date.update({doc: date}) for doc, date in fields_data]
    for doc in instructions:
        if doc.id not in doc_date.keys():
            FieldOT.objects.create(document=doc, company=request.user, field_name='F8', position_number=1, document_number=count)
            count += 1
        elif not doc_date.get(doc.id):
            field = FieldOT.objects.get(document=doc, company=request.user, field_name='F8', position_number=1)
            field.document_number = count
            field.save()
            count += 1
    return HttpResponseRedirect(reverse('work-safety:instructions'))