from django.contrib import admin
from .models import *
from django import forms
admin.site.register(ReadyOTDocument)
admin.site.register(Siz)
admin.site.register(PlaceRegistration)


@admin.register(FieldOT)
class FieldOTAdmin(admin.ModelAdmin):
    def get_form(self, request, obj=None, **kwargs):
        fields = [x.name for x in FieldOT._meta.fields]
        exclude = []
        related_fields = ['subdivisions', 'professions', 'positions', 'instructions']
        for field in fields:
            if not getattr(obj, field):
                exclude.append(field)
        for field in related_fields:
            if not getattr(getattr(obj, field), 'all')():
                exclude.append(field)

        self.exclude = exclude
        form = super(FieldOTAdmin, self).get_form(request, obj, **kwargs)
        return form

    def get_queryset(self, request):
        qs = super(FieldOTAdmin, self).get_queryset(request)
        qs = qs.select_related('document', 'company')
        return qs


@admin.register(OTDocument)
class OTDocumentAdmin(admin.ModelAdmin):
    filter_horizontal = ('related_fields',)
    ordering = ['document_code']
    list_display = ('document_code', 'title', 'section', 'default_document')
    actions = ['make_default_document_true']

    class Media:
        css = {
            'all': ('work_safety/resize-widget.css',),
        }

    def get_field_queryset(self, db, db_field, request):
        queryset = super(OTDocumentAdmin, self).get_field_queryset(db, db_field, request)
        if db_field.name == 'related_fields':
            queryset = queryset.select_related('document')
        return queryset

    def make_default_document_true(self, request, queryset):
        for field in queryset:
            field.default_document = True
            field.save()
    make_default_document_true.short_description = "Make default doc True"


@admin.register(DocumentStructure)
class DocumentStructureAdmin(admin.ModelAdmin):
    filter_horizontal = ('ot_documents',)
    ordering = ['section', 'title']
    list_display = ('title', 'section', 'url')

    class Media:
        css = {
            'all': ('work_safety/resize-widget.css',),
        }


@admin.register(FieldInDocument)
class FieldInDocumentAdmin(admin.ModelAdmin):
    ordering = ['document__document_code']
    list_display = ('document', 'field_name', 'prefix', 'position_number', 'element_chain')
    actions = ['make_element_chain_true']

    def make_element_chain_true(self, request, queryset):
        for field in queryset:
            field.element_chain = True
            field.save()
    make_element_chain_true.short_description = "Make element chain field True"


class OTDocumentGenericInfoAdminForm(forms.ModelForm):
    from django.contrib.admin.widgets import FilteredSelectMultiple
    buildings = forms.MultipleChoiceField(
        choices=BUILDINGS, required=False,
        widget=FilteredSelectMultiple(verbose_name='building', is_stacked=False))
    constructions = forms.MultipleChoiceField(
        choices=CONSTRUCTIONS, required=False,
        widget=FilteredSelectMultiple(verbose_name='constructions', is_stacked=False))
    premises = forms.MultipleChoiceField(
        choices=PREMISES, required=False,
        widget=FilteredSelectMultiple(verbose_name='premises', is_stacked=False))

    class Meta:
        model = OTDocumentGenericInfo
        fields = '__all__'
        widgets = {
            'means_transport': forms.SelectMultiple,
            'external_engineering_networks': forms.SelectMultiple,
            'internal_engineering_networks': forms.SelectMultiple,
            'equipment': forms.SelectMultiple,
        }


@admin.register(OTDocumentGenericInfo)
class OTDocumentGenericInfoAdmin(admin.ModelAdmin):
    form = OTDocumentGenericInfoAdminForm
    filter_horizontal = ('ot_documents',)

    class Media:
        css = {
            'all': ('work_safety/resize-widget.css',),
        }