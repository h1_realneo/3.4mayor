# coding: utf8
from __future__ import unicode_literals
from django.conf import settings
from company.models import *
from django.core.files.storage import FileSystemStorage
import os

from django.contrib.auth.models import AbstractBaseUser
from django.db import models
from fire_safety.models import Company
from ._factors import DANGEROUS_HARMFUL_PRODUCTION_FACTORS, WORKS_OF_INCREASED_DANGER, SECTIONS, SIZ


class Siz(models.Model):
    profession = models.ForeignKey(WorkProfession, on_delete=models.CASCADE, related_name='sizs', null=True, blank=True)
    position = models.ForeignKey(EmployeePosition, on_delete=models.CASCADE, related_name='sizs', null=True, blank=True)
    add_by_company = models.ForeignKey(Company, blank=True, null=True, related_name='add_company')
    exception_company = models.ManyToManyField(Company, blank=True, related_name='exception_company')
    title_document = models.CharField(max_length=600, blank=True)
    section = models.CharField(max_length=500, blank=True)
    chapter = models.CharField(max_length=500, blank=True)
    date_published = models.DateField(null=True, blank=True)
    condition = models.CharField(max_length=600, blank=True)
    name_thing = models.CharField(max_length=500)
    marking = models.CharField(max_length=500, blank=True)
    time_wearing = models.CharField(max_length=30, blank=True)

    def __str__(self):
        return '{}'.format(self.name_thing)


class OTDocument(models.Model):

    SECTIONS = SECTIONS

    NOTHING = ''
    OT_SCROLL = 'scroll'
    OT_ORDER = 'order'
    OT_INSTRUCTION = 'instruction'
    OT_PROGRAM = 'program'
    OT_PROVISION = 'provision'
    OT_PROTOCOL = 'protocol'
    OT_JOURNAL = 'journal'
    OT_BLANK = 'blank'
    OT_SCHEDULE = 'schedule'
    SECTIONS_DOC = (
        (NOTHING, 'Ничего'),
        (OT_SCROLL, 'Перечень'),
        (OT_ORDER, 'Приказ'),
        (OT_INSTRUCTION, 'Инструкция'),
        (OT_PROGRAM, 'Программа'),
        (OT_PROVISION, 'Положение'),
        (OT_PROTOCOL, 'Протокол'),
        (OT_JOURNAL, 'Журнал'),
        (OT_BLANK, 'Бланк'),
        (OT_SCHEDULE, 'График'),
    )

    document_code = models.IntegerField(unique=True, null=True)
    related_fields = models.ManyToManyField('FieldInDocument', blank=True, related_name='related_field')
    title = models.CharField(max_length=300, default='первый')
    path = models.FilePathField(blank=True, null=True, max_length=300, path=settings.FILE_PATH_OT_DOCUMENT,
                                recursive=True)
    section = models.CharField(max_length=100, choices=SECTIONS, default=SIZ)
    section_document = models.CharField(max_length=100, choices=SECTIONS_DOC, default=NOTHING, blank=True)
    default_document = models.BooleanField('Стандартно необходимый документ', default=False)

    def __str__(self):
        return "{}".format(self.title)

    # def get_absolute_url(self):
    #     return reverse('fire:create', kwargs={'pk': self.pk})

    class Meta:
        ordering = ('section',)
        verbose_name = 'Документы охраны труда'
        verbose_name_plural = 'Документы охраны труда'


class FieldInDocument(models.Model):
    F3 = 'F3'
    F4 = 'F4'
    F41 = 'F41'
    F6 = 'F6'
    F7 = 'F7'
    F8 = 'F8'
    F9 = 'F9'
    F11 = 'F11'
    F13 = 'F13'
    F14 = 'F14'
    F15 = 'F15'
    F16 = 'F16'
    F17 = 'F17'
    F18 = 'F18'
    F19 = 'F19'
    F20 = 'F20'
    F21 = 'F21'
    F22 = 'F22'
    F23 = 'F23'
    FIELDS = (
        (F3, 'F3 дополнительный параграф'),
        (F4, 'F4 фио'),
        (F41, 'F41 фио или ничего'),
        (F6, 'F6 согласовавшие'),
        (F7, 'F7 дата документа'),
        (F8, 'F8 номер документа'),
        (F9, 'F9 фио формсет'),
        (F11, 'F11 да нет'),
        (F13, 'F13 профессии и должности'),
        (F14, 'F14 инструкции'),
        (F15, 'F15 вредные факторы'),
        (F16, 'F16 время стажировки(профессии)'),
        (F17, 'F17 виды инструктажей'),
        (F18, 'F18 ответственные за инструктаж в подразделениях'),
        (F19, 'F19 разные должности(приставка, категория, название)'),
        (F20, 'F20 разные профессии(название, разряд)'),
        (F21, 'F21 график работы организации'),
        (F22, 'F22 время стажировки(должности)'),
        (F23, 'F23 начальник подразделения'),
    )
    document = models.ForeignKey(OTDocument, related_name='fields')
    label = models.TextField(max_length=300, blank=True)
    field_name = models.CharField(max_length=100, choices=FIELDS, default=F41)
    prefix = models.CharField(max_length=100, blank=True)
    position_number = models.IntegerField(blank=True, null=True)
    index_number = models.IntegerField('Порядковый номер поля на форме', blank=True, null=True)
    required_field = models.BooleanField('Необходимое поле?', blank=True, default=False)
    element_chain = models.BooleanField('Поле для цепочки вопросов', blank=True, default=False)

    def __str__(self):
        return '{}, {}, {}, {}'.format(self.document.title, self.field_name, self.prefix, self.label)

    class Meta:
        verbose_name = 'Поля в документах'
        verbose_name_plural = 'Поля в документах'


class FieldOT(models.Model):
    INDUCTION_TRAINING = 'i_t'
    PRIMARY_INSTRUCTION = 'p_i'
    UNSCHEDULED_BRIEFING = 'u_b'
    VIEW_INSTRUCTIONS = (
        (INDUCTION_TRAINING, 'вводный'),
        (PRIMARY_INSTRUCTION, 'первичный'),
        (UNSCHEDULED_BRIEFING, 'повторный/внеплановый/целевой'),
    )
    MONDAY = 'm'
    TUESDAY = 'tu'
    WEDNESDAY = 'w'
    THURSDAY = 'th'
    FRIDAY = 'f'
    SATURDAY = 'st'
    SUNDAY = 'sn'
    DAYS = (
        (MONDAY, 'понедельник'),
        (TUESDAY, 'вторник'),
        (WEDNESDAY, 'среда'),
        (THURSDAY, 'четверг'),
        (FRIDAY, 'пятница'),
        (SATURDAY, 'суббота'),
        (SUNDAY, 'воскресенье'),
    )
    field_name = models.CharField(max_length=255, choices=FieldInDocument.FIELDS, default=FieldInDocument.FIELDS[0])
    company = models.ForeignKey('company.Company', blank=True, null=True)
    document = models.ForeignKey(OTDocument, blank=True, null=True)
    position_number = models.IntegerField(blank=True, null=True)
    label = models.CharField(max_length=255, blank=True)
    # field's name
    employee = models.ForeignKey('company.Staff', blank=True, null=True, verbose_name='сотрудник')
    date = models.CharField('дата подписи', max_length=255, blank=True)
    document_number = models.CharField('номер документа', max_length=255, blank=True)
    paragraph = models.TextField('дополнительный пункт', max_length=800, blank=True)
    instructions = models.ManyToManyField(OTDocument, related_name='dev_instruction', blank=True)
    hazards = MultiSelectField(max_length=255, blank=True, choices=DANGEROUS_HARMFUL_PRODUCTION_FACTORS)
    time_briefing = models.CharField('количество дней стажировки', max_length=255, blank=True)
    view_instructions = MultiSelectField(max_length=255, blank=True, choices=VIEW_INSTRUCTIONS)
    subdivision = models.ForeignKey('fire_safety.Subdivision', blank=True, related_name='subdivision', null=True)
    subdivisions = models.ManyToManyField('fire_safety.Subdivision', blank=True)
    trigger = models.BooleanField('Да Нет', max_length=255, blank=True, default=False)
    # if employee have some professions in this field we save one of them that use in this field
    staff_profession = models.ForeignKey(StaffProfession, blank=True, null=True, verbose_name='профессия')
    staff_position = models.ForeignKey(StaffPosition, blank=True, null=True, verbose_name='должность')
    professions = models.ManyToManyField(StaffProfession, related_name='professions', blank=True)
    positions = models.ManyToManyField(StaffPosition, related_name='positions', blank=True)
    profession = models.ForeignKey(StaffProfession, related_name='profession', blank=True, null=True)
    position = models.ForeignKey(StaffPosition, related_name='position', blank=True, null=True)
    # for graphic work
    starting_work = models.CharField(max_length=255)
    end_work = models.CharField(max_length=255)
    starting_dinner = models.CharField(max_length=255)
    end_dinner = models.CharField(max_length=255)
    days = MultiSelectField(max_length=255, choices=DAYS, null=True)

    class Meta:
        verbose_name = 'Поле'
        verbose_name_plural = 'Поля'

    def __str__(self):
        return '{} {} {} {}'.format(self.company, self.document, self.field_name, self.position_number)


class OTStorage(FileSystemStorage):
    def get_available_name(self, name, max_length=None):
        name = name.replace('_', ' ')
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name


fs = OTStorage(location=settings.MEDIA_ROOT)


def ot_document_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/company_<id>/ot_<section>/document_<id>/<filename>
    return 'ot/{}/{}/{}/{}'.format(
        instance.company.id, instance.ot_document.section, instance.ot_document.id, filename
    )


class ReadyOTDocument(models.Model):
    company = models.ForeignKey(Company)
    ot_document = models.ForeignKey(OTDocument)
    pdf_file = models.FileField(upload_to=ot_document_directory_path, storage=fs, max_length=350)
    word_file = models.FileField(upload_to=ot_document_directory_path, blank=True)

    def __str__(self):
        return '{} {}'.format(self.company, self.ot_document)


class DocumentStructure(models.Model):
    SECTIONS = SECTIONS

    url = models.CharField(max_length=255, choices=SECTIONS, blank=True)
    title = models.CharField(max_length=255)
    section = models.ForeignKey('self', blank=True, null=True, on_delete=models.CASCADE)
    ot_documents = models.ManyToManyField(OTDocument, blank=True)

    def __str__(self):
        if self.section:
            return '{} {}'.format(self.title, self.section)
        return '{}'.format(self.title)


class OTDocumentGenericInfo(models.Model):
    ot_documents = models.ManyToManyField(OTDocument)
    # Questions about fixed assets from generic info company model from company
    buildings = MultiSelectField('buildings', choices=BUILDINGS, max_length=1000, blank=True)
    constructions = MultiSelectField('constructions', choices=CONSTRUCTIONS, max_length=1000, blank=True)
    premises = MultiSelectField('premises', choices=PREMISES, max_length=1000, blank=True)
    means_transport = MultiSelectField('means_transport', choices=MEANS_TRANSPORT,  max_length=1000, blank=True)
    external_engineering_networks = MultiSelectField('external_networks', choices=EEN, max_length=1000, blank=True)
    internal_engineering_networks = MultiSelectField('internal_networks', choices=IEN, max_length=1000, blank=True)
    equipment = MultiSelectField('equipment', choices=EQUIPMENT, max_length=1000, blank=True)

    class Meta:
        verbose_name = 'Связь общей информации с документами'
        verbose_name_plural = 'Связи общей информации с документами'

    def __str__(self):
        documents = self.ot_documents.all()
        documents = [document.__str__() for document in documents]
        title = ' '.join(documents)
        return '{}'.format(title)
