from django.apps import AppConfig


class WorkSafetyConfig(AppConfig):
    name = 'work_safety'
