# coding: utf8
from .parsing_document import get_context
from company.models import Company
from .models import FieldOT, OTDocument
from company._utility import AllAboutStaff
from django.template import Template, Context
from zipfile import ZipFile
import re
from mayor.settings import FINISHED_OLD_PATH
from work_safety.parsing_document import person_dev_instruction


def get_dictionary_massive(form):

    """
    try get massive and dictionary for normal unpack sizs in massive record number change
    profession or position. In dictionary record count row for profession or position
    """

    previous_work = None
    previous_title = None
    previous_chapter = None
    massive = list()
    for i, f in enumerate(form.queryset):
        prof = f.profession_id
        pos = f.position_id
        work = prof or pos
        if work != previous_work or f.title_document != previous_title or f.chapter != previous_chapter:
            massive.append(i)
        previous_title = f.title_document
        previous_work = work
        previous_chapter = f.chapter
    dictionary = dict()
    for x in massive:
        if x == massive[-1]:
            num = len([x for x in form.queryset]) - x
            dictionary.update({x: num})
        else:
            index_x = massive.index(x)
            index_next = index_x + 1
            dictionary.update({x: massive[index_next] - x})
    return dictionary, massive


def get_persons_signed(company):
    # old style doc
    company_id = company.pk
    path, company_folder_name = get_path(company)
    base_sections = ['order', 'program', 'scroll', 'provision', 'instruction']
    without_ins_order = ['program', 'scroll', 'provision']
    company = Company.objects.get(id=company_id)
    director = company.boss
    p_ot = company.safety_health
    p_a = company.person_authorized if not company.personauthorizedcompany.protocol_number else None
    p_a = p_a.staffposition_set.first() or p_a.staffprofession_set.first() if p_a else ''
    work_documents = list(company.work_documents.all())
    doc_persons, person_docs = {}, {}
    agreement = FieldOT.objects.filter(document=OTDocument.objects.get(document_code=91), company=company, field_name='F9')
    agreement = [x.staff_position for x in agreement]
    commision_ed = FieldOT.objects.filter(document=OTDocument.objects.get(document_code=20), company=company, field_name='F9')
    commision_ed = [x.staff_position for x in commision_ed]
    context = {}
    for doc in work_documents:
        if doc.section_document == 'order':
            context = get_context(doc, company, AllAboutStaff(company).exclude_mayor())
            for person in context['known'] + [director]:
                docs = person_docs.get(person, [])
                if doc not in docs:
                    docs.append(doc)
                person_docs.update({person: docs})
        if doc.section_document in base_sections or doc.document_code in [52]:
            for person in agreement:
                docs = person_docs.get(person, [])
                if doc not in docs:
                    docs.append(doc)
                person_docs.update({person: docs})
        if doc.section_document in base_sections and doc.section_document != 'order':
            for person in [p_a] if p_a else []:
                docs = person_docs.get(person, [])
                if doc not in docs:
                    docs.append(doc)
                person_docs.update({person: docs})
        if doc.section_document == 'instruction':
            signed_instructions = [p_ot]
            person = person_dev_instruction(company, doc)
            if person:
                signed_instructions.append(person.staff_position)
            for person in signed_instructions:
                docs = person_docs.get(person, [])
                if doc not in docs:
                    docs.append(doc)
                person_docs.update({person: docs})
        if doc.section_document in without_ins_order or doc.document_code in [52, 1, 184, 185]:
            docs = person_docs.get(p_ot, [])
            if doc not in docs:
                docs.append(doc)
            person_docs.update({p_ot: docs})
        if doc.section in ['adm', 'off', 'worker'] or doc.document_code in [26, 71]:
            for person in commision_ed:
                docs = person_docs.get(person, [])
                if doc not in docs:
                    docs.append(doc)
                person_docs.update({person: docs})

    for person, docs in person_docs.items():
        for doc in docs:
            persons = doc_persons.get(doc, [])
            persons.append(person)
            doc_persons.update({doc: persons})

    list_documents = []
    work_documents.sort(key=sort_work_document)
    for doc in work_documents:
        sections = OTDocument.SECTIONS
        numbers = ''.join(re.findall('\d', doc.title))
        persons = doc_persons.get(doc, '')
        if persons:
            persons = ['{} {}'.format(p.staff.surname, p.staff.initials) for p in persons]
            persons = ' '.join(persons)
        if numbers and doc.section not in ['l_cy']:
            number_readable, title = '', ''
            for letter in doc.title:
                if letter.isdigit() or letter in ['-']:
                    number_readable += letter
                else:
                    title += letter
            title, number_readable = title.strip(), number_readable.strip()
            list_documents.append([number_readable, title, '', persons])
        else:
            for section_en, section_ru in sections:
                if doc.section == section_en:
                    list_documents.append(['', doc.title, section_ru, persons])
                    break
    context.update({'signed': person_docs, 'list_documents': list_documents})
    with ZipFile('C:/projects/documents/work_safety/Ответственные ОТ(новый).docx', 'r') as template_word:
        with ZipFile('{}Ответственные ОТ {}(новый стиль).docx'.format(path, company_folder_name), 'w') as new_document_word:
            for item in template_word.infolist():
                buffer = template_word.read(item.filename)
                if item.filename == 'word/document.xml' or item.filename == 'word/header1.xml':
                    with template_word.open(item) as render_file:
                        content = str(render_file.read(), 'utf-8')
                        template = Template(content)
                        new_document_word.writestr(item, template.render(Context(context)).encode())
                else:
                    new_document_word.writestr(item, buffer)
    with ZipFile('C:/projects/documents/work_safety/Ответственные ОТ(старый).docx', 'r') as template_word:
        with ZipFile('{}Ответственные ОТ {}(старый стиль).docx'.format(path, company_folder_name), 'w') as new_document_word:
            for item in template_word.infolist():
                buffer = template_word.read(item.filename)
                if item.filename == 'word/document.xml' or item.filename == 'word/header1.xml':
                    with template_word.open(item) as render_file:
                        content = str(render_file.read(), 'utf-8')
                        template = Template(content)
                        new_document_word.writestr(item, template.render(Context(context)).encode())
                else:
                    new_document_word.writestr(item, buffer)
    return None


def sort_work_document(document):
    rank = None
    sections = OTDocument.SECTIONS
    numbers = ''.join(re.findall('\d{2}[-]\d{1,2}', document.title))
    if numbers and document.section != 'l_cy':
        numbers = ''.join([n for n in numbers if n.isdigit()])
        numbers = '{}{}0{}'.format(numbers[0], numbers[1], numbers[2]) if len(numbers) < 4 else numbers
        return int(numbers)
    for i, (section_en, section_ru) in enumerate(sections, 10):
        if document.section == section_en:
            rank = i * 1000
    title = document.title.strip().ljust(4, 'а')
    for i in range(4):
        rank += ord(title[i])
    return rank


def get_path(company):
    import os
    alphabet = [chr(x) for x in range(256, 10000) if 'а' <= chr(x) <= 'я' or 'А' <= chr(x) <= 'Я']
    company_folder_name = ''
    for letter in company.genericinfocompany.short_title:
        if letter in alphabet:
            company_folder_name += letter
    path = '{0}{1}/'.format(FINISHED_OLD_PATH, company_folder_name)
    if not os.path.exists(path):
        os.makedirs(path)
    return path, company_folder_name
