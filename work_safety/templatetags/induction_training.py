# coding: utf8
from django import template
from company.models import Staff, StaffPosition, StaffProfession, Company
from work_safety.models import FieldOT, OTDocument
from dateutil.relativedelta import relativedelta
from datetime import date, timedelta, datetime

register = template.Library()


@register.filter(name='get_induction_training')
def get_induction_training(person_other, person_subdivisions):
    if not person_other and not person_subdivisions:
        return
    result = []
    subdivision_person = {}
    if person_other:
        field = person_other.first()
        work_person_other = field.staff_position or field.staff_profession or field.employee.staffposition_set.first()
        instructor_fio = '{} {}'.format(work_person_other.staff.surname, work_person_other.staff.initials)
        instructor_position = work_person_other.case.nomn
        date_ins = work_person_other.date_recruitment
        subdivision_person = {'': [instructor_fio, instructor_position, date_ins]}
        company = work_person_other.staff.company
        [subdivision_person.update({sub: [instructor_fio, instructor_position, date_ins]}) for sub in company.subdivision_set.all()]
    else:
        field = person_subdivisions.first()
        work_person_other = field.staff_position or field.staff_profession or field.employee.staffposition_set.first()
        company = work_person_other.staff.company
        for person_sub in person_subdivisions:
            work = person_sub.staff_position or person_sub.staff_profession or person_sub.employee.staffposition_set.first()
            instructor_fio = '{} {}'.format(work.staff.surname, work.staff.initials)
            instructor_position = work.case.nomn
            for sub in person_sub.subdivisions.all():
                subdivision_person.update({sub: [instructor_fio, instructor_position, work.date_recruitment]})

    staff = [x[0] for x in Staff.objects.filter(company=company).values_list('id')]
    staff_positions = StaffPosition.objects.filter(staff_id__in=staff).select_related('staff', 'case', 'subdivision')
    staff_professions = StaffProfession.objects.filter(staff_id__in=staff).select_related('staff', 'case', 'subdivision')
    for person in staff_positions:
        date_recruitment = person.date_recruitment or ''
        if not date_recruitment:
            continue
        fio = person.staff
        position = person.case.nomn.capitalize()
        subdivision = person.subdivision or ''
        who_instructs_subdivision = subdivision_person.get(subdivision)
        instructor_fio = who_instructs_subdivision[0]
        instructor_position = who_instructs_subdivision[1]
        if date_recruitment and who_instructs_subdivision[2]:
            if date_recruitment < who_instructs_subdivision[2]:
                instructor_fio, instructor_position = 'Согласно приказа', ''
        result.append([date_recruitment, fio, position, subdivision, instructor_fio, instructor_position, True])

    for person in staff_professions:
        date_recruitment = person.date_recruitment or ''
        if not date_recruitment:
            continue
        fio = person.staff
        position = person.case.nomn.capitalize()
        subdivision = person.subdivision or ''
        who_instructs_subdivision = subdivision_person.get(subdivision)
        instructor_fio = who_instructs_subdivision[0]
        instructor_position = who_instructs_subdivision[1]
        if date_recruitment and who_instructs_subdivision[2]:
            if date_recruitment < who_instructs_subdivision[2]:
                instructor_fio, instructor_position = 'Согласно приказа', ''
        result.append([date_recruitment, fio, position, subdivision, instructor_fio, instructor_position, True])

    result.sort(key=lambda x: x[0] if x[0] else date.today())
    result.sort(key=lambda x: subdivision_person.get(x[3]))
    result = [[x[0].strftime('%d.%m.%Y'), x[1], x[2], x[3], x[4], x[5], x[6]] if x[0] else [x[0], x[1], x[2], x[3], x[4], x[5], x[6]] for x in result]
    answer = [[]]
    instructor_data = None
    for data in result:
        instructor_info = subdivision_person.get(data[3])
        if instructor_data != instructor_info:
            answer.append([data])
        else:
            answer[-1].append(data)
        instructor_data = instructor_info
    if [] in answer:
        answer.remove([])
    buffer_answer = answer.copy()
    answer.clear()
    for data in buffer_answer:
        subdivisions = [x[3] for x in data]
        subdivisions.sort(key=lambda x: '{}'.format(x))
        if subdivisions[0] != subdivisions[-1]:
            data = [[x[0], x[1], x[2], x[3], x[4], x[5], False] for x in data]
        answer.append(data)
    return answer
