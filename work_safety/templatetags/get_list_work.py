# coding: utf8
from builtins import AttributeError

from django import template
import pymorphy2
from work_safety.models import FieldOT
from django.db.models.query import QuerySet
morph = pymorphy2.MorphAnalyzer()
register = template.Library()

CASES = ['', 'nomn', 'gent', 'datv', 'accs', 'ablt', 'loct']
GROUPS = {'ph': 'a', 'ch_ch': 'ba', 'ch_p': 'bb', 'bi': 'c', 'ps': 'd'}


@register.filter(name='g_l_w')
def get_list_work(queryset):
    """
    находит все профессии и должности для даннного кверисета
    """
    staff_prof_pos = []
    if isinstance(queryset, QuerySet):
        for q in queryset:
            if isinstance(q, FieldOT):
                work = q.staff_position if q.staff_position else q.staff_profession
                staff_prof_pos.append(work)
        return staff_prof_pos
    return ''