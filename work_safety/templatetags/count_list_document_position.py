from django import template
from company.models import StaffPosition, StaffProfession, Staff
from pyphrasy.inflect import PhraseInflector

register = template.Library()


@register.filter(name='count_list_people_position')
def get_count_in_document(chairman, commission):
    try:
        company = chairman.first().staff_position.staff.company
        director = company.boss
        # staff_profession = StaffProfession.objects.filter(staff__in=Staff.objects.filter(company=company)).select_related(
        #     'case', 'staff__case')
        staff_position = StaffPosition.objects.filter(staff__in=Staff.objects.filter(company=company)).select_related(
            'case', 'staff__case')
    except Exception:
        return
    try:
        if not director or not commission or not chairman or not staff_position:
            return 0
        if commission.count() == 2:
            count = 7
        elif commission.count() == 3:
            count = 6
        elif commission.count() == 4:
            count = 5
        else:
            count = 5
        commission = [person.staff_position for person in list(commission) + list(chairman)]
        people_exclude = commission + [director] if director not in commission else commission
        staff_position = staff_position.exclude(id__in=[x.id for x in people_exclude])
        new_staff_position, new_staff_profession = [], []

        if staff_position.count() % count:
            for x in list(staff_position) + [None for x in range(count - staff_position.count() % count)]:
                new_staff_position.append(x)
        else:
            for x in list(staff_position):
                new_staff_position.append(x)

        # if staff_profession.count() % count:
        #     for x in list(staff_profession) + [None for x in range(count - staff_profession.count() % count)]:
        #         new_staff_profession.append(x)
        # else:
        #     for x in list(staff_profession):
        #         new_staff_profession.append(x)

        count_lists = (len(new_staff_position) + len(new_staff_profession))/count
        count_lists = int(count_lists)
        all_pos_prof = new_staff_position + new_staff_profession
        result = [(x, count) if not i % count else (x, i % count) for i, x in enumerate(all_pos_prof, 1)]
        return result, count, range(count_lists)
    except Exception:
        return 0