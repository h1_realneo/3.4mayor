# coding: utf8
from django import template
from django.db.models import Q

from company.models import Company, Staff, StaffProfession, StaffPosition

register = template.Library()


@register.filter(name='get_sizs')
def get_sizs(company_id):
    if company_id and isinstance(company_id, int):
        company = Company.objects.filter(id=company_id).first()
        positions, professions, siz_pos, siz_prof = [], [], [], []
        from itertools import chain
        staff = Staff.objects.filter(company=company)
        staff_positions = StaffPosition.objects.filter(staff__in=staff).select_related(
            'employee_position', 'case'
        ).prefetch_related(
            'employee_position__sizs', 'employee_position__sizs__exception_company'
        ).distinct('derivative_position', 'employee_position', 'category')
        staff_professions = StaffProfession.objects.filter(staff__in=staff).select_related(
            'work_profession', 'case'
        ).prefetch_related(
            'work_profession__sizs', 'work_profession__sizs__exception_company'
        ).distinct('work_profession', 'rank')
        for position in staff_positions:
            sizs = [x for x in position.employee_position.sizs.all() if company in x.exception_company.all()]
            sizs.sort(key=lambda x: len(x.condition))
            position_code = position.employee_position.position_code
            title_position = position.case.nomn
            if position.category:
                title_position = '{} {} категории'.format(position.case.nomn, position.category)
            if sizs:
                siz_pos.append((position_code, title_position, sizs))
        for profession in staff_professions:
            sizs = [x for x in profession.work_profession.sizs.all() if company in x.exception_company.all()]
            sizs.sort(key=lambda x: len(x.condition))
            profession_code = profession.work_profession.profession_code
            title_profession = profession.case.nomn
            if profession.rank:
                title_profession = '{} {} разряда'.format(profession.case.nomn, profession.rank)
            if sizs:
                siz_prof.append((profession_code, title_profession, sizs))
        return [x for x in chain(siz_prof, siz_pos)]
    return []

