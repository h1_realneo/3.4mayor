# coding: utf8
from django import template
from company.models import Company

register = template.Library()


@register.filter(name='sub_ins')
def get_instruction_by_subdivisions(company_id):
    subdivision_instructions = []
    try:
        company = Company.objects.get(id=company_id)
        staff = company.staff.all()
        instructions = company.work_documents.filter(section_document='instruction')
    except Exception:
        return None
    subdivisions = company.subdivision_set.all()
    if not subdivisions:
        return None
    for subdivision in subdivisions:
        staff_positions = subdivision.staffposition_set.filter(staff__in=staff).prefetch_related('work_documents')
        staff_professions = subdivision.staffprofession_set.filter(staff__in=staff).prefetch_related('work_documents')
        ins = []
        [ins.append(doc) for pos in staff_positions for doc in pos.work_documents.all() if doc in instructions and doc not in ins]
        [ins.append(doc) for prof in staff_professions for doc in prof.work_documents.all() if doc in instructions and doc not in ins]
        subdivision_instructions.append((subdivision, ins))
    return subdivision_instructions
