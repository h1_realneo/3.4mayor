# coding: utf8
from django import template
from company.models import Staff, StaffPosition, StaffProfession, Company
from work_safety.models import FieldOT, OTDocument
from dateutil.relativedelta import relativedelta
from datetime import date, timedelta, datetime

register = template.Library()


@register.filter(name='get_briefing_book')
def get_briefing_book(company_id):
    result = []
    today = date.today()
    one_year_later = today + relativedelta(years=+1)
    company = Company.objects.get(id=company_id)
    instructions = company.work_documents.filter(section_document='instruction')
    fields_data = FieldOT.objects.filter(company=company, document__in=instructions, field_name='F8', position_number=1).select_related('document')
    instruction_num = {}
    for field in fields_data:
        instruction_num.update({field.document.id: field.document_number})
    staff = [x[0] for x in Staff.objects.filter(company=company).values_list('id')]
    staff_positions = StaffPosition.objects.filter(staff_id__in=staff).select_related('staff', 'case', 'subdivision').prefetch_related('work_documents')
    with_order = 'Согласно приказа'
    staff_professions = StaffProfession.objects.filter(staff_id__in=staff).select_related('staff', 'case', 'subdivision').prefetch_related('work_documents')
    data = FieldOT.objects.filter(document__in=OTDocument.objects.filter(id__in=[36, 28, 29, 34, 37]), company=company).select_related(
        'document', 'staff_position', 'staff_profession', 'staff_profession__staff', 'staff_position__staff').prefetch_related('subdivisions')
    instructors = {}
    instructions_date = today
    probation_pos, probation_prof = {}, {}
    positions_except_briefing, professions_except_briefing = StaffPosition.objects.none(), StaffProfession.objects.none()
    pos_prof_except_briefing = []
    reason_ins = 'приказ  о проведении внепланового инструктажа'
    try:
        doc = OTDocument.objects.get(document_code=76)
        num = FieldOT.objects.get(document=doc, company=company, field_name='F8', position_number=1).document_number
        date_data = FieldOT.objects.get(document=doc, company=company, field_name='F7', position_number=1).date
        reason_ins = 'приказ от {} № {}'.format(date_data, num)
    except Exception:
        pass
    exists_subdivision = company.subdivision_set.exists()
    for field in data:
        if field.document_id == 36:
            instructor = field.staff_position or field.staff_profession
            instructor = getattr(instructor, 'staff', '')
            if field.position_number == 1 and field.field_name == 'F41':
                if exists_subdivision:
                    for f in company.subdivision_set.all():
                        instructors.update({f: instructor})
                else:
                    instructors.update({None: instructor})
            elif exists_subdivision and field.field_name == 'F18':
                for f in field.subdivisions.all():
                    instructors.update({f: instructor})
        elif field.field_name == 'F22' and field.document_id == 28:
            for staff_pos in staff_positions:
                if staff_pos.id == field.position_id:
                    probation_pos.update({
                        (staff_pos.employee_position_id, staff_pos.derivative_position, staff_pos.category): field.time_briefing
                    })
        elif field.field_name == 'F16' and field.document_id == 29:
            for staff_prof in staff_professions:
                if staff_prof.id == field.profession_id:
                    probation_prof.update({(staff_prof.work_profession_id, staff_prof.rank): field.time_briefing})
        elif field.field_name == 'F7' and field.document_id == 37 and field.position_number == 1:
            instructions_date = datetime.strptime(field.date, '%d.%m.%Y').date() if field.date else today
        elif field.field_name in ['F19', 'F20'] and field.document_id == 34 and field.position_number == 1:
            positions_except_briefing = positions_except_briefing or field.positions.all()
            professions_except_briefing = professions_except_briefing or field.professions.all()

    for staff_position in staff_positions:
        pos_id = staff_position.employee_position_id
        category = staff_position.category
        derivative_position = staff_position.derivative_position
        if positions_except_briefing:
            for pos in positions_except_briefing:
                if pos.derivative_position == derivative_position and pos.category == category and pos.employee_position_id == pos_id:
                    pos_prof_except_briefing.append(pos)
    for staff_profession in staff_professions:
        prof_id = staff_profession.work_profession_id
        rank = staff_profession.rank
        if professions_except_briefing:
            for prof in professions_except_briefing:
                if prof.work_profession_id == prof_id and rank == prof.rank:
                    pos_prof_except_briefing.append(prof)

    for person in staff_positions:
        date_recruitment = person.date_recruitment
        if not date_recruitment or person in pos_prof_except_briefing or person.staff in instructors.values():
            continue
        fio = person.staff
        position = person.case.nomn.capitalize()
        type_briefing = 'Первичный'
        docs = ''
        reason = ''
        subdivision = person.subdivision
        man_in_charge = with_order if date_recruitment and date_recruitment < today else instructors.get(subdivision)
        time_briefing_pos = ''
        style_journal = False
        if date_recruitment:
            days_briefing_pos = probation_pos.get((person.employee_position_id, person.derivative_position, person.category), '')
            if days_briefing_pos:
                list_day_briefing = [1 for x in range(int(days_briefing_pos))]
                next_day_briefing = date_recruitment
                while True:
                    next_day_briefing += relativedelta(days=+1)
                    if not next_day_briefing.isoweekday() in [6, 7]:
                        list_day_briefing.pop()
                    if not list_day_briefing:
                        last_day_briefing = next_day_briefing
                        break
                time_briefing_pos = '{} - {}'.format(
                    date_recruitment.strftime('%d.%m.%y'), last_day_briefing.strftime('%d.%m.%y')
                )
            if person not in pos_prof_except_briefing:
                man_in_charge = man_in_charge or ''
                result.append([date_recruitment, fio, position, type_briefing, docs, man_in_charge, time_briefing_pos, subdivision, style_journal, reason])
        if date_recruitment:
            date_recruitment = person.date_recruitment
        else:
            if person.frequency_instruction_three_month:
                date_recruitment = instructions_date - relativedelta(months=+6)
            else:
                date_recruitment = instructions_date - relativedelta(months=+12)
        while date_recruitment < one_year_later:
            prev_date = date_recruitment
            if person.frequency_instruction_three_month:
                date_recruitment += relativedelta(months=+3)
            else:
                date_recruitment += relativedelta(months=+6)
            type_briefing = 'Повторный'
            if instructions_date:
                man_in_charge = with_order if date_recruitment < instructions_date else instructors.get(subdivision)
                style_journal = False if date_recruitment < instructions_date else True
            else:
                style_journal = False if date_recruitment < today else True
                man_in_charge = with_order if date_recruitment and date_recruitment < today else instructors.get(subdivision)

            if prev_date < instructions_date <= date_recruitment:
                type_briefing = 'Внеплановый'
                date_recruitment = instructions_date
                man_in_charge = instructors.get(subdivision)
                style_journal = True
            docs = [instruction_num.get(x.id) for x in person.work_documents.all() if x.id in instruction_num.keys()] if style_journal else ''
            reason = reason_ins if style_journal else ''
            man_in_charge = man_in_charge or ''
            result.append([date_recruitment, fio, position, type_briefing, docs, man_in_charge, '', subdivision, style_journal, reason])
    for person in staff_professions:
        date_recruitment = person.date_recruitment
        if not date_recruitment or person in pos_prof_except_briefing or person.staff in instructors.values():
            continue
        fio = person.staff
        position = person.case.nomn.capitalize()
        type_briefing = 'Первичный'
        docs = ''
        subdivision = person.subdivision
        man_in_charge = with_order if date_recruitment and date_recruitment < today else instructors.get(subdivision)
        time_briefing_prof = ''

        style_journal = False
        if date_recruitment:
            days_briefing_prof = probation_prof.get((person.work_profession_id, person.rank), '')
            if days_briefing_prof:
                list_day_briefing = [1 for x in range(int(days_briefing_prof))]
                next_day_briefing = date_recruitment
                while True:
                    next_day_briefing += relativedelta(days=+1)
                    if not next_day_briefing.isoweekday() in [6, 7]:
                        list_day_briefing.pop()
                    if not list_day_briefing:
                        last_day_briefing = next_day_briefing
                        break

                time_briefing_prof = '{} - {}'.format(
                    date_recruitment.strftime('%d.%m.%y'), last_day_briefing.strftime('%d.%m.%y')
                )
            man_in_charge = man_in_charge or ''
            reason = reason_ins if style_journal else ''
            result.append([date_recruitment, fio, position, type_briefing, docs, man_in_charge, time_briefing_prof, subdivision, style_journal, reason])
        if date_recruitment:
            date_recruitment = person.date_recruitment
        else:
            if person.frequency_instruction_three_month:
                date_recruitment = instructions_date - relativedelta(months=+6)
            else:
                date_recruitment = instructions_date - relativedelta(months=+12)
        while date_recruitment < one_year_later:
            prev_date = date_recruitment

            if person.frequency_instruction_three_month:
                date_recruitment += relativedelta(months=+3)
            else:
                date_recruitment += relativedelta(months=+6)
            type_briefing = 'Повторный'
            if instructions_date:
                man_in_charge = with_order if date_recruitment < instructions_date else instructors.get(subdivision)
                style_journal = False if date_recruitment < instructions_date else True
            else:
                man_in_charge = with_order if date_recruitment and date_recruitment < today else instructors.get(subdivision)
                style_journal = False if date_recruitment < today else True
            if prev_date < instructions_date <= date_recruitment:
                type_briefing = 'Внеплановый'
                date_recruitment = instructions_date
                man_in_charge = instructors.get(subdivision)
                style_journal = True
            docs = [instruction_num.get(x.id) for x in person.work_documents.all() if x.id in instruction_num.keys()] if style_journal else ''
            reason = reason_ins if style_journal else ''
            man_in_charge = man_in_charge or ''
            result.append([date_recruitment, fio, position, type_briefing, docs, man_in_charge, '', subdivision, style_journal, reason])
    result.sort(key=lambda x: x[0] if x[0] else 1000)
    result.sort(key=lambda x: '{}'.format(x[7]) if x[7] else '')
    result = [[x[0].strftime('%d.%m.%Y'), x[1], x[2], x[3], x[4], x[5], x[6], x[7], x[8], x[9]] for x in result if x[0]]
    title_subdivision = None
    answer = []
    for data in result:
        if title_subdivision == data[7]:
            if answer:
                answer[-1].append(data)
            else:
                answer.append([data])
        else:
            title_subdivision = data[7]
            answer.append([data])
    answer = [a + [['', '', '', '', '', '', '', '', '', '', a[-1][5]] for i in range(16)] for a in answer]
    return answer
