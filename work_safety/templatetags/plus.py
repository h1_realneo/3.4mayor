from django import template

register = template.Library()


@register.filter(name='pl')
def plus(x, y):
    try:
        x = int(x)
        y = int(y)
        return x + y

    except Exception:
        return 0