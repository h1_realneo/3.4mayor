from django import template
from company.models import StaffPosition, StaffProfession, Company
import datetime
from dateutil.relativedelta import relativedelta
register = template.Library()


@register.filter(name='schedule_check_knowledge')
def get_schedule_check_knowledge(company_id, prof_or_pos):
    if not company_id or not prof_or_pos:
        return
    result = []
    staff = Company.objects.get(id=company_id).staff.all()
    if prof_or_pos == 'pos':
        workers = StaffPosition.objects.filter(staff__in=staff).select_related('case', 'staff')
        verification_frequency = 3
    else:
        verification_frequency = 1
        workers = StaffProfession.objects.filter(staff__in=staff).select_related('case', 'staff')
    for worker in workers:
        date = worker.date_recruitment
        if date:
            while date < datetime.date.today():
                date += relativedelta(years=+verification_frequency)
                result.append([worker, date])
    result.sort(key=lambda x: x[1] if x[1] else datetime.date.today())
    result = [[x[0], x[1].strftime('%d.%m.%Y')] for x in result]
    return result
