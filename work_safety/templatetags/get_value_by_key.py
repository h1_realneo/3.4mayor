# coding: utf8
from django import template
register = template.Library()


@register.filter(name='get_value_by_key')
def get_value(value, key):
    return value.get(key, '')

