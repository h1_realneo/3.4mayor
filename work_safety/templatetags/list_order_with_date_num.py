from django import template
from company.models import Company
from work_safety.models import FieldOT
import re

register = template.Library()


@register.filter(name='list_order')
def get_list_order(company_id):
    try:
        company = Company.objects.get(id=company_id)
    except Exception:
        return []
    id_title = {}
    doc_data = {}
    result = []
    orders = company.work_documents.filter(section_document='order').values_list('title', 'id')
    [id_title.update({id_doc: title_doc}) for title_doc, id_doc in orders]
    doc_date_num = FieldOT.objects.filter(
        company=company, document__in=[x for x in id_title.keys()], field_name__in=['F7', 'F8'], position_number=1
    ).values_list('document_id', 'date', 'document_number')

    for doc, date, num in doc_date_num:
        d, n = doc_data.get(doc, ['', ''])
        d = d or date
        n = n or num
        doc_data.update({doc: [d, n]})
    for order in orders:
        date, num = doc_data.get(order[1], ['', ''])
        result.append([order[0], date, num])
    result.sort(
        key=lambda x: int(''.join(re.findall(r'\d+', x[2])) if re.findall(r'\d+', x[2]) else 1000 + int(''.join(re.findall(r'\d+', x[0]))))
    )
    return result
