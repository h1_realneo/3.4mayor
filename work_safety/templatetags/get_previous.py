# coding: utf8
from django import template
register = template.Library()


@register.filter(name='get_previous')
def get_previous(value, chain):
    massive = [x for x in chain]
    for x in massive:
        if value == x:
            return massive[(massive.index(x)-1)]
    return ''

