from django import template

register = template.Library()


@register.filter(name='multiplication')
def multiplication(x, y):
    if not x or not y:
        return 0
    try:
        x = int(x)
        y = int(y)
        return x * y
    except Exception:
        return 0