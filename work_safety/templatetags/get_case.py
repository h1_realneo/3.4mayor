# coding: utf8
from builtins import AttributeError

from django import template
import pymorphy2
from pyphrasy.inflect import PhraseInflector
import xlrd
import os

morph = pymorphy2.MorphAnalyzer()
register = template.Library()

CASES = ['', 'nomn', 'gent', 'datv', 'accs', 'ablt', 'loct']


@register.filter(name='g_c')
def get_case(word, case):
    if not isinstance(word, str) or case not in CASES:
        return ''
    # word = morph.parse(word)[0]
    # word_in_case = word.inflect({case}).word if word.inflect({case}) else ''
    # return word_in_case
    path = os.path.dirname(os.path.realpath('__file__'))
    rb = xlrd.open_workbook(os.path.join(path, 'cases.xlsx'))
    sheet = rb.sheet_by_index(0)
    val_nomn_case = [sheet.row_values(rownum)[0].lower() for rownum in range(sheet.nrows)]
    if word.lower() in val_nomn_case:
        all_cases = sheet.row_values(val_nomn_case.index(word.lower()))
        return all_cases[CASES.index(case)]
    inflector = PhraseInflector(morph)
    return inflector.inflect(word, case)
