from django import template
import pymorphy2
from work_safety.models import FieldOT
morph = pymorphy2.MorphAnalyzer()
register = template.Library()


@register.filter(name='g_m_o')
def get_mode_operation(queryset, arg):
    if not queryset or not arg:
        return
    week = [x[0] for x in FieldOT.DAYS]
    days = []
    [days.append([y, x.starting_work, x.end_work, x.starting_dinner, x.end_dinner]) for x in queryset for y in x.days]
    weekend = []
    days_in_queryset = [x[0] for x in days]
    [weekend.append(x) for x in week if x not in days_in_queryset]
    data = []
    if arg == 'w':
        return [x[1] for x in FieldOT.DAYS if x[0] in weekend]
    elif arg == 'j':
        days_equal_work = {}
        for day in days:
            if not days_equal_work.get((day[1], day[2])):
                days_equal_work.update({(day[1], day[2]): [day[0]]})
            else:
                value = days_equal_work.get((day[1], day[2]))
                value.append(day[0])
                days_equal_work.update({(day[1], day[2]): value})
        values = [[key, value] for key, value in days_equal_work.items()]
        for value in values:
            starting_work, end_work = value[0][0], value[0][1]
            result = get_norm(value[1])
            data.append([result, starting_work, end_work])
        return data
    elif arg == 'd':
        days_equal_dinner = {}
        for day in days:
            if not days_equal_dinner.get((day[3], day[4])):
                days_equal_dinner.update({(day[3], day[4]): [day[0]]})
            else:
                value = days_equal_dinner.get((day[3], day[4]))
                value.append(day[0])
                days_equal_dinner.update({(day[3], day[4]): value})
        values = [[key, value] for key, value in days_equal_dinner.items()]
        for value in values:
            starting_dinner, end_dinner = value[0][0], value[0][1]
            result = get_norm(value[1])
            data.append([result, starting_dinner, end_dinner])
        return data
    return []


def get_norm(data):
    week = [x[0] for x in FieldOT.DAYS]
    index_for_delete = []
    data = normalize_data(data)
    for x in data:
        if x == data[0] or x == data[-1]:
            continue
        index_in_list = data.index(x)
        previous_in_data = data[index_in_list-1]
        next_in_data = data[index_in_list+1]
        index_in_week = week.index(x)
        previous_in_week = week[index_in_week-1] if not index_in_week == 0 else week[-1]
        next_in_week = week[index_in_week+1] if not index_in_week == 6 else week[0]
        if previous_in_data == previous_in_week and next_in_data == next_in_week:
            index_for_delete.append(index_in_list)
    new_data = ['-' if data.index(x) in index_for_delete else x for x in data]
    dict_week = {}
    [dict_week.update({x[0]: x[1]}) for x in FieldOT.DAYS]
    new_data = [dict_week.get(x) if dict_week.get(x) else x for x in new_data]
    result = []
    for day in new_data:
        if new_data[-1] == day:
            result.append(day)
            continue
        index_day = new_data.index(day)
        next_day = new_data[index_day+1]
        if day != '-' and next_day != '-':
            day = '{}, '.format(day)
        result.append(day)
    result = ''.join(result)
    result = result.replace('-----', '-').replace('----', '-').replace('---', '-').replace('--', '-')
    return result


def normalize_data(data):
    week = [x[0] for x in FieldOT.DAYS]
    for i in range(0, len(data)):
        last_day = data[-1]
        first_day = data[0]
        next_day_week = week[week.index(last_day)+1] if last_day != week[-1] else week[0]
        if first_day == next_day_week:
            data = [last_day] + data[:-1]
    return data
