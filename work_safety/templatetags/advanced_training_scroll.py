from django import template
from company.models import StaffPosition, StaffProfession, Company
import datetime
from django.db.models import Q
from dateutil.relativedelta import relativedelta
register = template.Library()


@register.filter(name='advanced_training_scroll')
def get_advanced_training_scroll(fields_ot_positions, fields_ot_professions):
    result = []
    if not fields_ot_positions and not fields_ot_professions:
        return result
    today = datetime.date.today()
    staff = fields_ot_positions.first().company.staff.all()
    positions = fields_ot_positions.first().positions.all().values_list(
        'employee_position', 'derivative_position', 'category')
    professions = fields_ot_professions.first().professions.all().values_list(
        'work_profession', 'rank')
    if positions:
        querysets = [
            Q(employee_position=pos, derivative_position=derivative, category=cat) for pos, derivative, cat in positions
            ]
        queryset = querysets.pop()
        for q in querysets:
            queryset |= q
        staff_positions = StaffPosition.objects.filter(queryset).filter(staff__in=staff).select_related('case', 'staff')
        for staff_pos in staff_positions:
            date = staff_pos.date_issue_certificate_prof_development or staff_pos.date_recruitment
            if date:
                while date < today:
                    if date + relativedelta(years=+5) > today:
                        result.append([staff_pos, date])
                        break
                    date += relativedelta(years=+5)
    if professions:
        querysets = [
            Q(work_profession=prof, rank=rank) for prof, rank in professions
            ]
        queryset = querysets.pop()
        for q in querysets:
            queryset |= q
        staff_professions = StaffProfession.objects.filter(queryset).filter(staff__in=staff).select_related('case', 'staff')
        for staff_prof in staff_professions:
            date = staff_prof.date_issue_certificate_prof_development or staff_prof.date_recruitment
            if date:
                while date < today:
                    if date + relativedelta(years=+3) > today:
                        result.append([staff_prof, date])
                        break
                    date += relativedelta(years=+3)
    result.sort(key=lambda x: x[1] if x[1] else datetime.date.today())
    result = [[x[0], x[1].strftime('%d.%m.%Y')] if x[1] else [x[0], ''] for x in result]
    return result
