# coding: utf8
from django import template
from company.models import Staff, StaffPosition, Company

register = template.Library()


@register.filter(name='get_accountant')
def get_accountant(company_id):
    company = Company.objects.get(id=company_id)
    positions = StaffPosition.objects.filter(staff__in=Staff.objects.filter(company=company)).select_related(
        'employee_position',
    )
    for position in positions:
        if company.safety_health == position:
            continue
        tittle_position = '{} {}'.format(
            position.derivative_position, position.employee_position.name_post_office_worker)
        title = tittle_position.strip().lower()
        if 'главн' in title and 'бухгалтер' in title and 'заместит' not in title:
            return position
        elif 'главн' in title and 'бухгалтер' in title:
            return position
    return company.boss
