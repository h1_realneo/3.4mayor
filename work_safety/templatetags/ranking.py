# coding: utf8
from django import template
from django.db.models.query import QuerySet
register = template.Library()
RUS_ALPHABET = [chr(x) for x in range(256, 10000) if 'а' <= chr(x) <= 'я' or 'А' <= chr(x) <= 'Я']


@register.filter(name='ranking')
def get_ranking(works):
    from company.models import StaffProfession, StaffPosition
    if not works:
        return ''
    if isinstance(works, QuerySet):
        works = works.select_related('employee_position', 'staff__case', 'case')
    if not isinstance(works, list):
        works = list(works)

    def sort_by_ranking(work):
        if isinstance(work, StaffProfession):
            return 100
        if isinstance(work, StaffPosition):
            tittle_position = '{} {}'.format(
                work.derivative_position, work.employee_position.name_post_office_worker)
            title = tittle_position.strip().lower()
            if ('директор' in title or 'управляющ' in title) and 'заместит' not in title:
                return 0
            elif 'директор' in title or 'управляющ' in title:
                return 1
            elif 'главн' in title and 'заместит' not in title:
                return 2
            elif 'главн' in title:
                return 3
            elif 'начальник' in title and 'заместит' not in title:
                return 4
            elif 'начальник' in title:
                return 5
            else:
                first_letter = work.employee_position.name_post_office_worker.lower()[0]
                return 6 + RUS_ALPHABET.index(first_letter) if first_letter in RUS_ALPHABET else 6
    works.sort(key=sort_by_ranking)
    return works
