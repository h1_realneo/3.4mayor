# coding: utf8
from builtins import AttributeError

from django import template
import pymorphy2
from work_safety._factors import DANGEROUS_HARMFUL_PRODUCTION_FACTORS as f
morph = pymorphy2.MorphAnalyzer()
register = template.Library()

CASES = ['', 'nomn', 'gent', 'datv', 'accs', 'ablt', 'loct']
GROUPS = {'ph': 'aa', 'pb': 'ab', 'ch_ch': 'ba', 'ch_p': 'bb', 'bi': 'c', 'ps': 'd'}


@register.filter(name='g_h')
def get_case(hazards, group):
    all_hazards = [b for x in f for b in x[1]]
    try:
        hazards = hazards[0].hazards
    except Exception:
        return ''
    if group not in GROUPS.keys():
        return ''
    hazards_by_group = [x for x in hazards if x.startswith(GROUPS.get(group))]
    values = [x[1] for x in all_hazards if x[0] in hazards_by_group]
    if values:
        return values
    return []


