# coding: utf8
from django import template
from company.models import StaffPosition, StaffProfession, Staff, Company
from work_safety.models import OTDocument
from pyphrasy.inflect import PhraseInflector
from itertools import chain
import datetime
from dateutil.relativedelta import relativedelta
register = template.Library()


@register.filter(name='periodic_medical_examinations')
def get_periodic_medical_examinations(company_id):
    title_months = {'01': 'январь', '02': 'февраль', '03': 'март', '04': 'апрель', '05': 'май ', '06': 'июнь',
                    '07': 'июль', '08': 'август', '09': 'сентябрь', '10': 'октябрь', '11': 'ноябрь', '12': 'декабрь'}
    result = []
    today = datetime.date.today()
    # previous variant why date
    six_month_later = datetime.datetime.strptime('01.07.2018', '%d.%m.%Y').date()

    try:

        company = Company.objects.get(id=company_id)
        safety_health = company.safety_health
        divisions = company.subdivision_set.all()
        staff = company.staff.all()
        staff_position = StaffPosition.objects.filter(staff__in=staff).select_related(
            'subdivision', 'case', 'staff__case', 'employee_position').prefetch_related('factors_productions')
        staff_profession = StaffProfession.objects.filter(staff__in=staff).select_related(
            'subdivision', 'case', 'staff__case', 'work_profession').prefetch_related('factors_productions')
        prof_duplicate, pos_duplicate = {}, {}
        for pos in staff_position:
            count = pos_duplicate.get((pos.employee_position, pos.derivative_position), 0)
            count += 1
            pos_duplicate.update({(pos.employee_position, pos.derivative_position): count})
        for prof in staff_profession:
            count = prof_duplicate.get(prof.work_profession, 0)
            count += 1
            prof_duplicate.update({prof.work_profession: count})
        responsible_divisions = {}
        for field in OTDocument.objects.get(document_code=61).fieldot_set.filter(company=company, field_name='F23').select_related(
            'staff_position', 'staff_profession', 'subdivision', 'staff_profession__staff', 'staff_position__staff',
            'staff_position__case', 'staff_profession__case'
        ):
            responsible_divisions.update({field.subdivision: field.staff_position or field.staff_profession})
    except Exception:
        return
    all_staff = [x for x in chain(staff_position, staff_profession)]
    if divisions:
        for division in divisions:
            responsible = responsible_divisions.get(division, safety_health)
            result.append([division, responsible, []])
            for person in all_staff:
                if person.subdivision == division:
                    persons = result[-1][-1]
                    persons.append(person)
                    result[-1][-1] = persons
    else:
        result.append(['', safety_health, []])
        for person in all_staff:
            persons = result[-1][-1]
            persons.append(person)
            result[-1][-1] = persons
    new_result = []
    for data in result:
        if data[2]:
            person_next_med = []
            for person in data[2]:
                if not person.date_issue_medical_certificate and person.factors_productions.all():
                    person_next_med.append([person, today + relativedelta(months=+1)])
                elif person.factors_productions.all():
                    prev_med = person.date_issue_medical_certificate
                    years = [x.scan_frequency for x in person.factors_productions.all()]
                    if 1 in years:
                        person_next_med.append([person, prev_med + relativedelta(years=+1)])
                    elif 2 in years:
                        person_next_med.append([person, prev_med + relativedelta(years=+2)])
                    elif 3 in years:
                        person_next_med.append([person, prev_med + relativedelta(years=+3)])
                    if person_next_med[-1][1] < today:
                        person_next_med[-1] = [person, today + relativedelta(months=+1)]
                    if person_next_med[-1][-1].isoweekday() in [6, 7]:
                        person_next_med[-1] = [person, person_next_med[-1][1] + relativedelta(days=+2)]
            # previous variant with six_month_later
            # person_next_med = [x for x in person_next_med if x[1] < six_month_later]
            # person_next_med = [x for x in person_next_med]
            person_next_med.sort(key=lambda x: x[1])
            buffer = person_next_med.copy()
            person_next_med.clear()
            for count, person in enumerate(buffer):
                prev_date = buffer[count-1][1]
                next_date = buffer[count][1]
                if prev_date.strftime('%m.%Y') != next_date.strftime('%m.%Y') or count == 0:
                    person_next_med.append(title_months.get(next_date.strftime('%m'), '   '))
                person.append(count+1)
                person_next_med.append(person)
            len_med = len(buffer)
            if data[0]:
                len_worker = data[0].staffposition_set.count() + data[0].staffprofession_set.count()
            else:
                len_worker = len(data[2])
            person_next_med = map(
                lambda x: x if len(x) > 3 else [x[0], x[1].strftime('%d.%m.%Y'), x[2]], person_next_med
            )
            person_next_med = [x for x in person_next_med]
            new_result.append([data[0], data[1], person_next_med, len_med, len_worker])
    [new_result.remove(x) for x in new_result.copy() if x[2] == []]
    workers_with_factors = [y[0] for x in new_result for y in x[2] if len(y) == 3]
    works_del_duplicate = {}
    staff_position_factors = [x for x in staff_position if x in workers_with_factors]
    staff_profession_factors = [x for x in staff_profession if x in workers_with_factors]
    for work in staff_position_factors + staff_profession_factors:
        factors = ''.join([b.factor for b in work.factors_productions.all()])
        if isinstance(work, StaffPosition):
            derivative, position = work.derivative_position, work.employee_position
            key = (derivative, position, factors)
        else:
            rank, profession = work.rank, work.work_profession
            key = (rank, profession, factors)
        unique_staff_positions = works_del_duplicate.get(key, [])
        unique_staff_positions.append(work)
        works_del_duplicate.update({key: unique_staff_positions})
    all_works = [
        [y, y.factors_productions.all()] for x in works_del_duplicate.values() for y in x
        ]
    all_works = [[x, y, (today - x.date_recruitment).days] for x, y in all_works if x.date_recruitment]
    all_works = map(lambda x: [x[0], x[1], get_year_month_from_days(x[2])], all_works)
    all_works = [x for x in all_works]
    works_del_duplicate = [
        [x[0], [b for b in x[0].factors_productions.all()]] for x in works_del_duplicate.values()
    ]
    works_del_duplicate = [
        [x[0], x[1], prof_duplicate.get(x[0].work_profession)] if isinstance(x[0], StaffProfession) else [x[0], x[1], pos_duplicate.get((x[0].employee_position, x[0].derivative_position))] for x in works_del_duplicate
    ]
    works_del_duplicate.sort(
        key=lambda x: '{}{}'.format(x[0].subdivision.title, x[0].case.nomn) if x[0].subdivision else x[0].case.nomn
    )
    all_works.sort(key=lambda x: x[0].case.nomn)
    return new_result, works_del_duplicate, all_works


def get_year_month_from_days(days):
    if days < 30:
        return "%02d мес." % (1,)
    months = days//30
    years = months//12
    months -= years*12
    if years and months:
        return '{} г. {} мес.'.format(years, months)
    elif years:
        return '{} г.'.format(years)
    elif months:
        return "%02d мес." % (months,)
    else:
        return ''
