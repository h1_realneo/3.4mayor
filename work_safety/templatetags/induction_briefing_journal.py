# coding: utf8
from django import template
from company.models import Staff, StaffPosition, Company, StaffProfession

register = template.Library()


@register.filter(name='get_staff_briefing_journal')
def get_staff_briefing_journal(company_id):
    result = []
    company = Company.objects.get(id=company_id)
    staff = Staff.objects.filter(company=company).prefetch_related(
        'staffposition_set', 'staffprofession_set').order_by('employment_date')
    staff_professions = StaffProfession.objects.filter(staff__in=staff).prefetch_related('case')
    staff_positions = StaffPosition.objects.filter(staff__in=staff).prefetch_related('case')
    for person in staff:
        date = person.employment_date.strftime('%d.%m.%y') if person.employment_date else ''
        person_title = '{} {}'.format(person.surname, person.initials)
        work_title = ''
        if person.staffposition_set.exists():
            for position in person.staffposition_set.all():
                if position.id == company.boss_id:
                    break
                for pos in staff_positions:
                    if pos == position:
                        work_title = pos.case.nomn.capitalize()
                        break
                result.append([date, person_title, work_title])
        elif person.staffprofession_set.exists():
            for profession in person.staffprofession_set.all():
                for prof in staff_professions:
                    if prof == profession:
                        work_title = prof.case.nomn.capitalize()
                        break
                result.append([date, person_title, work_title])
    return result


