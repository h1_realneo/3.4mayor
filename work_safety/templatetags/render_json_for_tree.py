# coding: utf8
from builtins import AttributeError

from django import template
import pymorphy2

morph = pymorphy2.MorphAnalyzer()
register = template.Library()

CASES = ['', 'nomn', 'gent', 'datv', 'accs', 'ablt', 'loct']


@register.filter(name='render_json')
def render(text):
    dict_for_replace = {'"nodes"': 'nodes', '"text"': 'text', '"href"': 'href', '"tags"': 'tags', "'state'": 'state',
                        "'selected'": 'selected', "'true'": 'true'}
    for key in dict_for_replace.keys():
        text = text.replace(key, dict_for_replace.get(key))
    return text
