# coding: utf8
from django import template

register = template.Library()
RUS_ALPHABET = [chr(x) for x in range(256, 10000) if 'а' <= chr(x) <= 'я' or 'А' <= chr(x) <= 'Я']


@register.filter(name='cut_before')
def cut_before(text):
    if not text or not isinstance(text, str):
        try:
            text = text.__str__()
        except Exception:
            return ''
    new_text = text
    for letter in text:
        if letter not in RUS_ALPHABET:
            new_text = new_text[1:]
            continue
        else:
            break
    return new_text.replace('  ', ' ').replace('   ', ' ')
