# coding: utf8
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView
from .views import *
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^siz/$', SizView.as_view(), name='siz',),
    url(r'^siz/list_works/$', WorksWithSizsView.as_view(), name='works_with_siz',),
    url(r'^siz/(?P<pk>[0-9]+)/$', ProfPosListView.as_view(), name='siz-list-prof-pos',),
    url(r'^siz/create/(?P<pk>[0-9]+)/$', SizCreateView.as_view(), name='siz-create',),
    url(r'^siz/update/$', SizUpdateView.as_view(), name='siz-update',),
    url(r'^create_document/(?P<pk>[0-9]+)/$', DocumentCreate.as_view(), name='create-document',),
    url(r'^create_document/chain/(?P<q_pk>[0-9]+)/$', ChainDocumentCreate.as_view(), name='create-document-chain',),
    url(r'^create_document/ot/$', CreateDocumentsOTView.as_view(), name='create-document-ot',),
    url(r'^section_(?P<section>[\w-]+)/$', TreeView.as_view(), name='list-document',),
    url(r'^section/(?P<section>[\w-]+)/$', get_documents_by_section, name='get-documents',),
    url(r'^data_filing/$', DataFilingView.as_view(), name='data_filing',),
    url(r'^download_documents/$', DownloadOTDocumentView.as_view(), name='download_documents'),
    url(r'^create_select_ot_docs/$', GenerateOTDocuments.as_view(), name='create_select_ot_docs'),
    url(r'^instructions/$', OTInstructionView.as_view(), name='instructions'),
    url(r'^medical_data/$', DownloadMedicalDataView.as_view(), name='medical_data'),
    url(r'^number_instructions/$', create_num_instructions, name='number_instructions'),
    url(r'^signed_documents/$', get_person_signed_document, name='signed_documents'),
    url(r'^update_field/(?P<pk>[0-9]+)/(?P<pk_doc>[0-9]+)/$', WorkFieldExcludeUpdateView.as_view(), name='update_field'),
]
