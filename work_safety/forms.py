# coding: utf8
from django import forms
from django.forms import BaseModelFormSet, modelformset_factory
from django.forms import ModelForm
from .models import *
from django.db.models import Q
from django.core.exceptions import ValidationError
from fire_safety.forms import customize_style
from fire_safety.models import Subdivision
from company.widgets import StaffSelect, StaffPositions, StaffProfessions, StaffPositionWidget, StaffProfessionWidget
from company.form_fields import WithoutRepeatQueryModelChoiceField


class FieldExcludeModelChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return "{}".format(obj.label)


class UpdateWorkExcludeFieldForm(ModelForm):
    redirect_url = forms.CharField(widget=forms.HiddenInput, required=False)

    class Meta:
        model = Company
        fields = ('work_field_exclude',)
        field_classes = {
            'work_field_exclude': FieldExcludeModelChoiceField
        }
        widgets = {
            'work_field_exclude': forms.CheckboxSelectMultiple
        }

    def __init__(self, *args, **kwargs):
        queryset = kwargs.pop('queryset', ())
        redirect_url = kwargs.pop('redirect_url', None)
        super(UpdateWorkExcludeFieldForm, self).__init__(*args, **kwargs)
        self.fields['redirect_url'].initial = redirect_url
        self.fields['work_field_exclude'].queryset = queryset
        self.fields['work_field_exclude'].empty_label = None


class DownloadMedicalDataForm(forms.Form):
    file_excel = forms.FileField(max_length=100, label='данные о мед справках')


class CustomModelChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        title_section = ''
        for section in OTDocument.SECTIONS:
            if section[0] == obj.section:
                title_section = section[1]
        return '{} - {}'.format(title_section.upper(), obj.title.lower())


class OTDocumentForm(forms.Form):
    documents = CustomModelChoiceField(
        required=False,
        queryset=OTDocument.objects.all(),
        label='Документы, которые нужно сформировать',
        widget=forms.CheckboxSelectMultiple(attrs={'class': 'selectpicker', 'multiple': ''})
    )

    def __init__(self, *args, **kwargs):
        documents = kwargs.pop('queryset', None)
        super(OTDocumentForm, self).__init__(*args, **kwargs)
        if documents:
            self.fields['documents'].queryset = documents
        else:
            self.fields['documents'].queryset = OTDocument.objects.none()


class StaffProfSubForm(ModelForm):
    class Meta:
        model = StaffProfession
        fields = ['subdivision']


class StaffPosSubForm(ModelForm):
    class Meta:
        model = StaffPosition
        fields = []


StaffProfSubFormset = modelformset_factory(StaffProfession, form=StaffProfSubForm)
StaffPosSubFormset = modelformset_factory(StaffPosition, form=StaffPosSubForm)


class SizForm(forms.Form):
    siz = forms.CharField(max_length=100, required=False)


class SizCreateCustomFormSet(BaseModelFormSet):
    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company', None)
        pk = kwargs.pop('pk', None)
        super(SizCreateCustomFormSet, self).__init__(*args, **kwargs)
        self.prefix = 'siz_formset'
        position = EmployeePosition.objects.filter(id=pk).first()
        profession = WorkProfession.objects.filter(id=pk).first()
        work = profession if profession else position
        self.queryset = work.sizs.all().filter(add_by_company=company)


class SizCreateForm(forms.ModelForm):

    class Meta:
        model = Siz
        fields = ('name_thing', 'condition', 'marking', 'time_wearing')
        labels = {
            'name_thing': 'Средство индифидуальной защиты',
            'condition': 'Условие',
            'marking': 'Маркировка',
            'time_wearing': 'Время носки'
        }
        widgets = {
            'name_thing': forms.TextInput(attrs={'class': 'form-control'}),
            'condition': forms.TextInput(attrs={'class': 'form-control'}),
            'marking': forms.TextInput(attrs={'class': 'form-control'}),
            'time_wearing': forms.TextInput(attrs={'class': 'form-control'}),
        }

SizCreateFormSet = modelformset_factory(Siz, form=SizCreateForm, formset=SizCreateCustomFormSet, can_delete=True)


class SizUpdateForm(forms.ModelForm):
    del_siz = forms.BooleanField(label='Убрать', required=False)

    class Meta:
        model = Siz
        fields = []

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company', None)
        exception_siz = kwargs.pop('exception_siz', [])
        super(SizUpdateForm, self).__init__(*args, **kwargs)
        if self.instance.id not in exception_siz:
            self.initial.update({'del_siz': True})


class SizBaseModelFormset(BaseModelFormSet):
    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company', None)
        super(SizBaseModelFormset, self).__init__(*args, **kwargs)

    def clean(self):
        super(SizBaseModelFormset, self).clean()
        del_sizs = []
        for form in self.forms:
            del_siz = form.cleaned_data.get('del_siz')
            if not del_siz:
                del_sizs.append(form.instance.id)
        self.company.exception_company.set(list(Siz.objects.filter(id__in=del_sizs)), clear=True)


SizUpdateFormSet = modelformset_factory(
    Siz, form=SizUpdateForm, formset=SizBaseModelFormset, can_delete=False, extra=0, max_num=3000)


class SelectOneDate(forms.SelectMultiple):
    def __init__(self, *args, **kwargs):
        self.dates = kwargs.pop('dates', {})
        self.docid_data = kwargs.pop('docid_data', {})
        self.section_label = {}
        for section, label_rus in OTDocument.SECTIONS:
            self.section_label.update({section: label_rus})
        super(SelectOneDate, self).__init__(*args, **kwargs)

    def render_option(self, selected_choices, option_value, option_label):
        from django.utils.safestring import mark_safe
        from django.utils.encoding import (force_str, force_text, python_2_unicode_compatible,)
        # look at the original for something to start with
        if option_value is None:
            option_value = ''
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''
        date = self.dates.get(int(option_value), None)
        doc = self.docid_data.get(int(option_value), None)
        add_to_label = ''
        if doc.section_document not in ('scroll', 'order', 'program', 'provision'):
            add_to_label = self.section_label.get(doc.section, '')
        if not date:
            return '<option data-subtext="{} у этого документа нет дат" value="{}"{}>{}</option>'.format(
                add_to_label, option_value, selected_html, force_text(option_label))
        else:
            return '<option data-subtext="{} дата документа:{}" value="{}"{}>{}</option>'.format(
                add_to_label, date, option_value, selected_html, force_text(option_label))


class SelectTwoDate(forms.SelectMultiple):
    def __init__(self, *args, **kwargs):
        self.date_two = kwargs.pop('date_two', {})
        self.date_three = kwargs.pop('date_three', {})
        super(SelectTwoDate, self).__init__(*args, **kwargs)

    def render_option(self, selected_choices, option_value, option_label):
        from django.utils.safestring import mark_safe
        from django.utils.encoding import (force_str, force_text, python_2_unicode_compatible,)
        # look at the original for something to start with
        if option_value is None:
            option_value = ''
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''
        date_two = self.date_two.get(int(option_value), '')
        date_three = self.date_three.get(int(option_value), '')
        if not date_two and not date_three:
            return '<option data-subtext="у этого документа нет дат" value="{}"{}>{}</option>'.format(
                option_value, selected_html, force_text(option_label))
        else:
            return '<option data-subtext="дата документа:{}  дата разработки: {}" value="{}"{}>{}</option>'.format(
                date_two, date_three, option_value, selected_html, force_text(option_label))


class DataFilingForm(forms.Form):

    date_one = forms.CharField(required=False, label='указать дату для документов',
                               widget=forms.TextInput(attrs={'class': 'datepicker_generic_info'}))
    documents_one_date = forms.ModelMultipleChoiceField(required=False, label='документы', queryset=OTDocument.objects.none())
    date_two = forms.CharField(required=False, label='указать дату для документов',
                               widget=forms.TextInput(attrs={'class': 'datepicker_generic_info'}))
    date_three = forms.CharField(required=False, label='указать дату разработки документов',
                                 widget=forms.TextInput(attrs={'class': 'datepicker_generic_info'}))
    documents_two_date = forms.ModelMultipleChoiceField(required=False, label='документы', queryset=OTDocument.objects.none())

    def __init__(self, *args, **kwargs):
        from work_safety._factors import FUNCTIONAL_RESPONSIBILITIES as F_R, CHECK_KNOWLEDGE_WORK as C_K_W, \
            EDUCATION_ITR, ADMINISTRATION, OFFICIALS, WORKER
        self.company = kwargs.pop('company')
        company = self.company
        documents_one_date = company.work_documents.filter(
            Q(section_document__in=[
                OTDocument.OT_PROVISION, OTDocument.OT_PROGRAM, OTDocument.OT_ORDER, OTDocument.OT_SCROLL
            ]) |
            Q(section__in=[F_R, C_K_W, EDUCATION_ITR, ADMINISTRATION, OFFICIALS, WORKER])).order_by('section')
        documents_two_date = company.work_documents.filter(section_document__in=[OTDocument.OT_INSTRUCTION]).order_by(
            'section'
        )
        values_one_date = FieldOT.objects.filter(
            document__in=documents_one_date, company=company, field_name='F7', position_number=1
        ).values_list('document_id', 'date')
        values_two_date = FieldOT.objects.filter(
            document__in=documents_two_date, company=company, field_name='F7', position_number=1
        ).values_list('document_id', 'date')
        values_three_date = FieldOT.objects.filter(
            document__in=documents_two_date, company=company, field_name='F7', position_number=2
        ).values_list('document_id', 'date')
        document_date_one, document_date_two, document_date_three = {}, {}, {}
        docid_data = {}
        [docid_data.update({doc.id: doc}) for doc in documents_one_date]
        [document_date_one.update({doc: date}) for doc, date in values_one_date]
        [document_date_two.update({doc: date}) for doc, date in values_two_date]
        [document_date_three.update({doc: date}) for doc, date in values_three_date]
        super(DataFilingForm, self).__init__(*args, **kwargs)
        self.fields['documents_one_date'].widget = SelectOneDate(
            dates=document_date_one, docid_data=docid_data, attrs={'class': 'selectpicker'})
        self.fields['documents_one_date'].queryset = documents_one_date
        self.fields['documents_two_date'].widget = SelectTwoDate(
            date_two=document_date_two, date_three=document_date_three, attrs={'class': 'selectpicker'})
        self.fields['documents_two_date'].queryset = documents_two_date
        customize_style(self)

    def clean(self):
        import datetime
        cleaned_data = super(DataFilingForm, self).clean()
        date_one = cleaned_data.get("date_one")
        documents_one_date = cleaned_data.get("documents_one_date")
        date_two = cleaned_data.get("date_two")
        date_three = cleaned_data.get("date_three")
        documents_two_date = cleaned_data.get("documents_two_date")
        created_field_date = FieldOT.objects.filter(company=self.company, field_name='F7', position_number__in=[1, 2])
        if documents_one_date:
            date = ''
            try:
                date = datetime.datetime.strptime(date_one, '%Y-%m-%d').date().strftime('%d.%m.%Y') if date_one else date
            except Exception:
                msg = "Неверно указана дата"
                self.add_error('date_one', msg)
            created_field_date.filter(document__in=documents_one_date).update(date=date)
            not_create_yet = documents_one_date.exclude(id__in=[x.document_id for x in created_field_date])
            FieldOT.objects.bulk_create(
                [FieldOT(
                    document=doc, company=self.company, field_name='F7', position_number=1, date=date
                ) for doc in not_create_yet]
            )

        if documents_two_date:
            date = ''
            date_dev = ''
            try:
                date = datetime.datetime.strptime(date_two, '%Y-%m-%d').date().strftime('%d.%m.%Y') if date_two else date
            except Exception:
                msg = "Неверно указана дата"
                self.add_error('date_one', msg)
            try:
                date_dev = datetime.datetime.strptime(date_three, '%Y-%m-%d').date().strftime('%d.%m.%Y') if date_three else date_dev
            except Exception:
                msg = "Неверно указана дата"
                self.add_error('date_three', msg)
            created_field_date.filter(document__in=documents_two_date, position_number=1).update(date=date)
            created_field_date.filter(document__in=documents_two_date, position_number=2).update(date=date_dev)
            not_create_yet_date = documents_two_date.exclude(
                id__in=[x.document_id for x in created_field_date if x.position_number == 1])
            not_create_yet_date_dev = documents_two_date.exclude(
                id__in=[x.document_id for x in created_field_date if x.position_number == 2])
            FieldOT.objects.bulk_create(
                [FieldOT(
                    document=doc, company=self.company, field_name='F7', position_number=1, date=date
                ) for doc in not_create_yet_date]
            )
            FieldOT.objects.bulk_create(
                [FieldOT(
                    document=doc, company=self.company, field_name='F7', position_number=2, date=date
                ) for doc in not_create_yet_date_dev]
            )


class OTDownloadForm(forms.Form):
    work_documents = forms.MultipleChoiceField(
        choices=[], required=False, widget=forms.SelectMultiple(attrs={'class': 'select_ot_document'})
    )

    def __init__(self, *args, **kwargs):
        choices = kwargs.pop('choices')
        url_rus = {}
        [url_rus.update({key: value}) for key, value in DocumentStructure.SECTIONS]
        super(OTDownloadForm, self).__init__(*args, **kwargs)
        self.fields['work_documents'].choices = choices
        self.fields['work_documents'].label = url_rus.get(self.prefix, 'work_documents')
        customize_style(self)


class Base(ModelForm):

    def __init__(self, *args, **kwargs):
        kwargs.pop('queryset', None)
        form_kwargs = kwargs.pop('form_kwargs', None)
        self.label = form_kwargs.get('label', None)
        self.company = form_kwargs.get('company', None)
        self.position_number = form_kwargs.get('position_number', None)
        self.document = form_kwargs.get('document', None)
        self.field_name = form_kwargs.get('field_name', None)
        self.about_staff = about_staff = form_kwargs.get('about_staff', None)
        staff_queryset = form_kwargs.get('staff_queryset', None)
        some_pos = form_kwargs.get('some_pos', None)
        some_prof = form_kwargs.get('some_prof', None)
        about_staff.add_mayor()
        staff_work = {}
        staff_profession, staff_position = about_staff.get_staff_professions, about_staff.get_staff_positions
        positions, professions = about_staff.get_positions, about_staff.get_professions
        [staff_work.update({x: about_staff.get_works_read_by_staff(x)}) for x in about_staff.staff_id]
        super(Base, self).__init__(*args, **kwargs)
        if self.fields.get('employee'):
            self.fields['employee'].queryset = staff_queryset
            self.fields['employee'].widget.staff_work = staff_work
            css_class = self.fields['employee'].widget.attrs.get('class')
            css_class = 'employee {}'.format(css_class) if css_class else 'employee'
            self.fields['employee'].widget.attrs.update({'class': css_class})
        if self.fields.get('staff_profession'):
            self.fields['staff_profession'].queryset = some_prof
            self.fields['staff_profession'].widget.attrs.update({'class': 'professions'})
        if self.fields.get('staff_position'):
            self.fields['staff_position'].queryset = some_pos
            self.fields['staff_position'].widget.attrs.update({'class': 'positions'})
        if self.fields.get('positions'):
            self.fields['positions'].queryset = StaffPosition.objects.filter(id__in=about_staff.exclude_mayor().get_staff_unique_staff_position())
            self.fields['positions'].widget.staff_position = staff_position
            self.fields['positions'].widget.positions = positions
        if self.fields.get('professions'):
            self.fields['professions'].queryset = StaffProfession.objects.filter(id__in=about_staff.exclude_mayor().get_staff_unique_staff_profession())
            self.fields['professions'].widget.staff_profession = staff_profession
            self.fields['professions'].widget.professions = professions
        if self.fields.get('position'):
            self.fields['position'].queryset = StaffPosition.objects.filter(id__in=about_staff.exclude_mayor().get_staff_unique_staff_position())
            self.fields['position'].widget.staff_position = staff_position
            self.fields['position'].widget.positions = positions
        if self.fields.get('profession'):
            self.fields['profession'].queryset = StaffProfession.objects.filter(id__in=about_staff.exclude_mayor().get_staff_unique_staff_profession())
            self.fields['profession'].widget.staff_profession = staff_profession
            self.fields['profession'].widget.professions = professions
        customize_style(self)

    def save(self, commit=True):
        employee_id = getattr(self.instance, 'employee_id')
        staff_position_id = getattr(self.instance, 'staff_position_id')
        staff_profession_id = getattr(self.instance, 'staff_profession_id')
        if employee_id:
            staff_positions, staff_professions = self.about_staff.get_staff_positions, self.about_staff.get_staff_professions
            positions = [key for key, value in staff_positions.items() if value['staff_id'] == employee_id]
            professions = [key for key, value in staff_professions.items() if value['staff_id'] == employee_id]
            if staff_position_id not in positions and staff_profession_id not in professions:
                if positions:
                    self.instance.staff_position_id = positions[0]
                    self.instance.staff_profession_id = None
                else:
                    self.instance.staff_profession_id = professions[0]
                    self.instance.staff_position_id = None
            else:
                if staff_position_id:
                    self.instance.staff_profession_id = None
                else:
                    self.instance.staff_position_id = None
        return super(Base, self).save(commit)


class BaseForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.forms_instances = kwargs.pop('forms_instances', None)
        self.label = kwargs.pop('label', None)
        self.company = kwargs.pop('company', None)
        self.position_number = kwargs.pop('position_number', None)
        self.document = kwargs.pop('document', None)
        self.field_name = kwargs.pop('field_name', None)
        self.about_staff = about_staff = kwargs.pop('about_staff', None)
        staff_queryset = kwargs.pop('staff_queryset', None)
        some_pos = kwargs.pop('some_pos', None)
        some_prof = kwargs.pop('some_prof', None)
        about_staff.add_mayor()
        staff_work = {}
        staff_profession, staff_position = about_staff.get_staff_professions, about_staff.get_staff_positions
        positions, professions = about_staff.get_positions, about_staff.get_professions
        [staff_work.update({x: about_staff.get_works_read_by_staff(x)}) for x in about_staff.staff_id]
        super(BaseForm, self).__init__(*args, **kwargs)
        if self.fields.get('employee'):
            self.fields['employee'].queryset = staff_queryset
            css_class = self.fields['employee'].widget.attrs.get('class')
            css_class = 'employee {}'.format(css_class) if css_class else 'employee'
            self.fields['employee'].widget.attrs.update({'class': css_class})
            self.fields['employee'].widget.staff_work = staff_work
        if self.fields.get('staff_profession'):
            self.fields['staff_profession'].queryset = some_prof
            self.fields['staff_profession'].widget.attrs.update({'class': 'professions'})
        if self.fields.get('staff_position'):
            self.fields['staff_position'].queryset = some_pos
            self.fields['staff_position'].widget.attrs.update({'class': 'positions'})
        if self.fields.get('positions'):
            self.fields['positions'].queryset = StaffPosition.objects.filter(id__in=about_staff.exclude_mayor().get_staff_unique_staff_position())
            self.fields['positions'].widget.staff_position = staff_position
            self.fields['positions'].widget.positions = positions
        if self.fields.get('professions'):
            self.fields['professions'].queryset = StaffProfession.objects.filter(id__in=about_staff.exclude_mayor().get_staff_unique_staff_profession())
            self.fields['professions'].widget.staff_profession = staff_profession
            self.fields['professions'].widget.professions = professions
        if self.fields.get('position'):
            self.fields['position'].queryset = StaffPosition.objects.filter(id__in=about_staff.exclude_mayor().get_staff_unique_staff_position())
            self.fields['position'].widget.staff_position = staff_position
            self.fields['position'].widget.positions = positions
        if self.fields.get('profession'):
            self.fields['profession'].queryset = StaffProfession.objects.filter(id__in=about_staff.exclude_mayor().get_staff_unique_staff_profession())
            self.fields['profession'].widget.staff_profession = staff_profession
            self.fields['profession'].widget.professions = professions
        customize_style(self)

    def save(self, commit=True):

        employee_id = getattr(self.instance, 'employee_id')
        staff_position_id = getattr(self.instance, 'staff_position_id')
        staff_profession_id = getattr(self.instance, 'staff_profession_id')
        if employee_id:
            staff_positions, staff_professions = self.about_staff.get_staff_positions, self.about_staff.get_staff_professions
            positions = [key for key, value in staff_positions.items() if value['staff_id'] == employee_id]
            professions = [key for key, value in staff_professions.items() if value['staff_id'] == employee_id]
            if staff_position_id not in positions and staff_profession_id not in professions:
                if positions:
                    self.instance.staff_position_id = positions[0]
                    self.instance.staff_profession_id = None
                else:
                    self.instance.staff_profession_id = professions[0]
                    self.instance.staff_position_id = None
            else:
                if staff_position_id:
                    self.instance.staff_profession_id = None
                else:
                    self.instance.staff_position_id = None
        return super(BaseForm, self).save(commit)


class BaseFormSet(BaseModelFormSet):
    def __init__(self, *args, **kwargs):
        self.forms_instances = kwargs.get('form_kwargs').get('forms_instances')
        self.label = kwargs.get('form_kwargs').get('label')
        self.company = kwargs.get('form_kwargs').get('company')
        self.position_number = kwargs.get('form_kwargs').get('position_number')
        self.document = kwargs.get('form_kwargs').get('document')
        self.field_name = kwargs.get('form_kwargs').get('field_name')
        super(BaseFormSet, self).__init__(*args, **kwargs)
        self.queryset = self.model.objects.filter(
            company=self.company, document=self.document, position_number=self.position_number, field_name=self.field_name
        )
        self.extra = 0 if self.forms_instances.get((self.field_name, self.position_number), None) else 1

    def clean(self):
        super(BaseFormSet, self).clean()
        if self.field_name == 'F21':
            week = []
            for form in self.forms:
                days = form.cleaned_data.get('days')
                if days:
                    for day in days:
                        if day in week:
                            form.add_error('days', 'Вы уже указали')
                            break
                        week.append(day)


class F3Form(Base):
    class Meta:
        model = FieldOT
        fields = ['paragraph']
        widgets = {
            "paragraph": forms.Textarea(attrs={'rows': 2}),
            }


class F4Form(Base):
    class Meta:
        model = FieldOT
        fields = ['employee', 'staff_profession', 'staff_position']
        widgets = {
            'employee': StaffSelect(attrs={'class': 'select_employee'}),
        }
        field_classes = {
            'employee': WithoutRepeatQueryModelChoiceField,
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }

    def clean_employee(self):
        data = self.cleaned_data['employee']
        if not data:
            raise ValidationError("Укажите сотрудника фирмы", code='invalid')
        return data


class F41Form(Base):
    class Meta:
        model = FieldOT
        fields = ['employee', 'staff_profession', 'staff_position']
        widgets = {
            'employee': StaffSelect(attrs={'class': 'select_employee'}),
        }
        field_classes = {
            'employee': WithoutRepeatQueryModelChoiceField,
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }


class F7Form(Base):
    class Meta:
        model = FieldOT
        fields = ['date']
        widgets = {
            "date": forms.TextInput(attrs={'class': 'date'}),
            }


class F8Form(Base):
    class Meta:
        model = FieldOT
        fields = ['document_number']


class F11Form(Base):
    class Meta:
        model = FieldOT
        fields = ['trigger']


class F13Form(Base):
    class Meta:
        model = FieldOT
        fields = ['professions', 'positions']
        widgets = {
            'positions': StaffPositions(attrs={'class': 'selectpicker', 'multiple': ''}),
            'professions': StaffPositions(attrs={'class': 'selectpicker', 'multiple': ''}),
        }
        labels = {
            'positions': 'Должности',
            'professions': 'Профессии'
        }


class F15Form(Base):
    class Meta:
        model = FieldOT
        fields = ['hazards']
        widgets = {
            'hazards': forms.CheckboxSelectMultiple(),
        }
        labels = {
            'hazards': 'Вредные факторы',
        }


class F19Form(Base):
    class Meta:
        model = FieldOT
        fields = ['positions']
        widgets = {
            'positions': StaffPositions(attrs={'class': 'selectpicker', 'multiple': ''}),
        }
        labels = {
            'positions': 'Должности',
        }


class F20Form(Base):
    class Meta:
        model = FieldOT
        fields = ['professions']
        widgets = {
            'professions': StaffProfessions(attrs={'class': 'selectpicker', 'multiple': ''}),
        }
        labels = {
            'professions': 'Профессии'
        }


# Forms for Formsets
class F6Form(BaseForm):

    class Meta:
        model = FieldOT
        fields = ['employee', 'staff_profession', 'staff_position']
        widgets = {
            'employee': StaffSelect(attrs={'class': 'select_employee'}),
        }
        field_classes = {
            'employee': WithoutRepeatQueryModelChoiceField,
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }


class F9Form(BaseForm):
    employee = WithoutRepeatQueryModelChoiceField(
        label='Сотрудник',
        queryset=Staff.objects.none(),
        required=True,
        widget=StaffSelect(attrs={'class': 'select_employee'}),
        error_messages={'required': "Укажите сотрудника"}
    )

    class Meta:
        model = FieldOT
        fields = ['employee', 'staff_profession', 'staff_position']
        field_classes = {
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }


class F14Form(BaseForm):
    employee = WithoutRepeatQueryModelChoiceField(
        label='Сотрудник',
        queryset=Staff.objects.none(),
        required=True,
        widget=StaffSelect(attrs={'class': 'select_employee'}),
        error_messages={'required': "Укажите сотрудника"}
    )

    class Meta:
        model = FieldOT
        fields = ['employee', 'staff_profession', 'staff_position', 'instructions']
        field_classes = {
            'instructions': forms.ModelMultipleChoiceField,
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }
        labels = {
            'instructions': 'Инструкции'
        }
        widgets = {
            'instructions': forms.SelectMultiple(attrs={'class': 'selectpicker'}),
        }

    def __init__(self, *args, **kwargs):
        super(F14Form, self).__init__(*args, **kwargs)
        company = kwargs.get('company')
        self.fields.get('instructions').queryset = company.work_documents.filter(section_document='instruction')


class F16Form(BaseForm):
    class Meta:
        model = FieldOT
        fields = ['profession', 'time_briefing']
        labels = {
            'profession': 'Профессии'
        }
        widgets = {
            'profession': StaffProfessionWidget(attrs={'class': 'selectpicker'}),
        }


class F17Form(BaseForm):
    class Meta:
        model = FieldOT
        fields = ['employee', 'staff_profession', 'staff_position', 'view_instructions']
        labels = {
            'view_instructions': 'Виды иннструктажей(директор уже назначен):'
        }
        widgets = {
            'view_instructions': forms.SelectMultiple(attrs={'class': 'selectpicker', 'multiple': ''}),
            'employee': StaffSelect(attrs={'class': 'select_employee'}),
        }
        field_classes = {
            'employee': WithoutRepeatQueryModelChoiceField,
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }


class F18Form(BaseForm):
    class Meta:
        model = FieldOT
        fields = ['employee', 'staff_profession', 'staff_position', 'subdivisions']
        widgets = {
            'subdivisions': forms.SelectMultiple(attrs={'class': 'subdivision'}),
            'employee': StaffSelect(attrs={'class': 'select_employee'}),
        }
        labels = {
            'subdivisions': 'Подразделения',
        }
        field_classes = {
            'employee': WithoutRepeatQueryModelChoiceField,
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }

    def __init__(self, *args, **kwargs):
        super(F18Form, self).__init__(*args, **kwargs)
        self.fields.get('subdivisions').queryset = Subdivision.objects.filter(company=self.company)


class F21Form(BaseForm):
    class Meta:
        model = FieldOT
        fields = ['starting_dinner', 'end_dinner', 'starting_work', 'end_work', 'days']
        widgets = {
            'days': forms.SelectMultiple(attrs={'class': 'selectpicker'}),
            'starting_dinner': forms.TextInput(attrs={'class': 'timepicker'}),
            'end_dinner': forms.TextInput(attrs={'class': 'timepicker'}),
            'starting_work': forms.TextInput(attrs={'class': 'timepicker'}),
            'end_work': forms.TextInput(attrs={'class': 'timepicker'}),
        }
        labels = {
            'starting_dinner': 'начало обеда',
            'end_dinner': 'конец обеда',
            'starting_work': 'начало работа',
            'end_work': 'конец работы',
            'days': 'дни'
        }


class F22Form(BaseForm):
    class Meta:
        model = FieldOT
        fields = ['position', 'time_briefing']
        labels = {
            'position': 'Должность'
        }
        widgets = {
            'position': StaffPositionWidget(attrs={'class': 'selectpicker'}),
        }


class F23Form(BaseForm):
    class Meta:
        model = FieldOT
        fields = ['subdivision', 'employee', 'staff_profession', 'staff_position']
        labels = {
            'subdivision': 'Подразделение'
        }
        widgets = {
            'employee': StaffSelect(attrs={'class': 'select_employee'}),
            'subdivision': forms.Select(attrs={'class': 'subdivision'}),
        }
        field_classes = {
            'employee': WithoutRepeatQueryModelChoiceField,
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }

    def __init__(self, *args, **kwargs):
        super(F23Form, self).__init__(*args, **kwargs)
        self.fields.get('subdivision').queryset = Subdivision.objects.filter(company=self.company)

F6Formset = modelformset_factory(FieldOT, form=F6Form, formset=BaseFormSet, extra=0, can_delete=True)
F9Formset = modelformset_factory(FieldOT, form=F9Form, formset=BaseFormSet, extra=0, can_delete=True)
F14Formset = modelformset_factory(FieldOT, form=F14Form, formset=BaseFormSet, extra=0, can_delete=True)
F16Formset = modelformset_factory(FieldOT, form=F16Form, formset=BaseFormSet, extra=0, can_delete=True)
F17Formset = modelformset_factory(FieldOT, form=F17Form, formset=BaseFormSet, extra=0, can_delete=True)
F18Formset = modelformset_factory(FieldOT, form=F18Form, formset=BaseFormSet, extra=0, can_delete=True)
F21Formset = modelformset_factory(FieldOT, form=F21Form, formset=BaseFormSet, extra=0, can_delete=True)
F22Formset = modelformset_factory(FieldOT, form=F22Form, formset=BaseFormSet, extra=0, can_delete=True)
F23Formset = modelformset_factory(FieldOT, form=F23Form, formset=BaseFormSet, extra=0, can_delete=True)

FORMSETS_PREFIX = ['formset_fio%s' % x for x in range(1, 10)] + \
                  ['agreement1', 'dev_instruction', 'time_briefing_pos', 'time_briefing_prof', 'view_instructions',
                   'subdivisions', 'days', 'subdivision1']


def all_forms():
    return dict(
        F3=F3Form,
        F4=F4Form,
        F41=F41Form,
        F7=F7Form,
        F8=F8Form,
        F11=F11Form,
        F13=F13Form,
        F15=F15Form,
        F19=F19Form,
        F20=F20Form,
        # formsets
        F6=F6Formset,
        F9=F9Formset,
        F14=F14Formset,
        F16=F16Formset,
        F17=F17Formset,
        F18=F18Formset,
        F21=F21Formset,
        F22=F22Formset,
        F23=F23Formset,
    )

