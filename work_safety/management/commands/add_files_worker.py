# coding: utf8
import re
from django.core.management.base import BaseCommand
import os
from work_safety.models import OTDocument, DocumentStructure, FieldInDocument


class Command(BaseCommand):

    def handle(self, *args, **options):
        ot_documents = OTDocument.objects.all()
        last_code = 1000 if not ot_documents.filter(document_code=1000) else ot_documents.latest('document_code').document_code + 1
        for x in os.walk('C:\\projects\\documents\\work_safety\\_инструкции'):
            print(x[0])
            for y in x[2]:
                result = re.findall(r'\d{5}', y)
                if not result:
                    result = ['']
                path = '{}\\{}'.format(x[0], y)
                section_document = 'ins'
                title = y.replace(result[0], '').replace('.docx', '')
                if ot_documents.filter(path=path):
                    continue
                new_doc = OTDocument.objects.create(
                    document_code=last_code, path=path, title=title, section_document='instruction', section=section_document)
                FieldInDocument.objects.create(document=new_doc, position_number=1, field_name='F7', prefix='date1', label='дата документа')
                FieldInDocument.objects.create(document=new_doc, position_number=2, field_name='F7', prefix='date2', label='дата разработки')
                FieldInDocument.objects.create(document=new_doc, position_number=1, field_name='F8', prefix='doc_num1')
                DocumentStructure.objects.get(url=section_document).ot_documents.add(new_doc)
                last_code += 1

        for x in os.walk('C:\\projects\\documents\\work_safety\\_карты опасностей и рисков\\администрация'):
            print(x[0])
            for y in x[2]:
                result = re.findall(r'\d{5}', y)
                if not result:
                    result = ['']
                path = '{}\\{}'.format(x[0], y)
                section_document = 'adm'
                title = y.replace(result[0], '').replace('.docx', '')
                if ot_documents.filter(path=path):
                    continue
                new_doc = OTDocument.objects.create(
                    document_code=last_code, path=path, title=title, section=section_document)
                field = OTDocument.objects.get(document_code=182).fields.first()
                new_doc.related_fields.add(field)
                FieldInDocument.objects.create(document=new_doc, position_number=1, field_name='F7', prefix='date1')
                DocumentStructure.objects.get(url=section_document).ot_documents.add(new_doc)
                last_code += 1

        for x in os.walk('C:\\projects\\documents\\work_safety\\_карты опасностей и рисков\\должн.лица'):
            print(x[0])
            for y in x[2]:
                result = re.findall(r'\d{5}', y)
                if not result:
                    result = ['']
                path = '{}\\{}'.format(x[0], y)
                section_document = 'off'
                title = y.replace(result[0], '').replace('.docx', '')
                if ot_documents.filter(path=path):
                    continue
                new_doc = OTDocument.objects.create(
                    document_code=last_code, path=path, title=title, section=section_document)
                field = OTDocument.objects.get(document_code=182).fields.first()
                new_doc.related_fields.add(field)
                FieldInDocument.objects.create(document=new_doc, position_number=1, field_name='F7', prefix='date1')
                DocumentStructure.objects.get(url=section_document).ot_documents.add(new_doc)
                last_code += 1

        for x in os.walk('C:\\projects\\documents\\work_safety\\_карты опасностей и рисков\\рабочие'):
            print(x[0])
            for y in x[2]:
                result = re.findall(r'\d{5}', y)
                if not result:
                    result = ['']
                path = '{}\\{}'.format(x[0], y)
                section_document = 'worker'
                title = y.replace(result[0], '').replace('.docx', '')
                if ot_documents.filter(path=path):
                    continue
                new_doc = OTDocument.objects.create(
                    document_code=last_code, path=path, title=title, section=section_document)
                field = OTDocument.objects.get(document_code=182).fields.first()
                new_doc.related_fields.add(field)
                FieldInDocument.objects.create(document=new_doc, position_number=1, field_name='F7', prefix='date1')
                DocumentStructure.objects.get(url=section_document).ot_documents.add(new_doc)
                last_code += 1

        for x in os.walk('C:\\projects\\documents\\work_safety\\_функциональные обязанности'):
            print(x[0])
            for y in x[2]:
                result = re.findall(r'\d{5}', y)
                if not result:
                    result = ['']
                path = '{}\\{}'.format(x[0], y)
                section_document = 'fun_res'
                title = y.replace(result[0], '').replace('.docx', '')
                if ot_documents.filter(path=path):
                    continue
                new_doc = OTDocument.objects.create(
                    document_code=last_code, path=path, title=title, section=section_document)
                FieldInDocument.objects.create(document=new_doc, position_number=1, field_name='F7', prefix='date1')
                DocumentStructure.objects.get(url=section_document).ot_documents.add(new_doc)
                last_code += 1

        for x in os.walk('C:\\projects\\documents\\work_safety\\_программы обучения\\программа обучения рабочих'):
            print(x[0])
            for y in x[2]:
                result = re.findall(r'\d{5}', y)
                if not result:
                    result = ['']
                path = '{}\\{}'.format(x[0], y)
                section_document = 'ch_kn_work'
                title = y.replace(result[0], '').replace('.docx', '')
                if ot_documents.filter(path=path):
                    continue
                new_doc = OTDocument.objects.create(
                    document_code=last_code, path=path, title=title, section=section_document)
                FieldInDocument.objects.create(document=new_doc, position_number=1, field_name='F7', prefix='date1')
                DocumentStructure.objects.get(url=section_document).ot_documents.add(new_doc)
                last_code += 1

        for x in os.walk('C:\\projects\\documents\\work_safety\\_программы обучения\\программа обучения специалистов'):
            print(x[0])
            for y in x[2]:
                result = re.findall(r'\d{5}', y)
                if not result:
                    result = ['']
                path = '{}\\{}'.format(x[0], y)
                section_document = 'ed_itr'
                title = y.replace(result[0], '').replace('.docx', '')
                if ot_documents.filter(path=path):
                    continue
                new_doc = OTDocument.objects.create(
                    document_code=last_code, path=path, title=title, section=section_document)
                FieldInDocument.objects.create(document=new_doc, position_number=1, field_name='F7', prefix='date1')
                DocumentStructure.objects.get(url=section_document).ot_documents.add(new_doc)
                last_code += 1
