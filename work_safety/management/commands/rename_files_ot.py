# coding: utf8
import re
from django.core.management.base import BaseCommand
import xlrd
import xlwt
import os
from work_safety.models import OTDocument

path_file_attachment = 'D:/projects/Загруженные в программу документы ОТ.xlsx'


def del_spaces(text):
    if not isinstance(text, str):
        return text
    spaces = re.findall(r'\s{2,}', text)
    for space in spaces:
        text = text.replace(space, ' ')
    return text


class Command(BaseCommand):

    def handle(self, *args, **options):

        wb = xlwt.Workbook()
        ws = wb.add_sheet('Test')
        ot = {}
        [ot.update({x[0]: [x[1], x[2]]}) for x in OTDocument.objects.values_list('document_code', 'title', 'path')]
        document = xlrd.open_workbook(path_file_attachment)
        sheet = document.sheet_by_index(0)
        for row_num in range(2, sheet.nrows):
            row = sheet.row_values(row_num)
            if row[4]:
                code = int(row[4])
                ot_document = OTDocument.objects.get(document_code=code)
                data = ot.get(code, '')
                old_name, old_path = data[0], data[1]
                new_name = '{} {}'.format(row[5], row[6])
                new_name = del_spaces(new_name).strip()
                ot_document.title = new_name
                name_folder = os.path.dirname(old_path)
                new_path = '{}\\{}.docx'.format(name_folder, new_name)
                try:
                    os.rename(old_path, new_path)
                except Exception:
                    print(old_path)
                ot_document.path = new_path
                ot_document.save()


