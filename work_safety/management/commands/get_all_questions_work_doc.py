# coding: utf8
from django.core.management.base import BaseCommand
from work_safety.models import OTDocument, DocumentStructure, SECTIONS
from work_safety.views import get_branch
from company.models import Company
import xlwt
from django.db.models import Q
import re
HIERARCHY_TREE = {
    'СИЗ': 7,
    'Медосмотры': 3,
    'Вводный инструктаж': 4,
    'Стажировка': 6,
    'Инструктажи': 8,
    'Инструкции': 9,
    'СУОТ': 11,
    'Обучение': 5,
    'Общее': 2,
    'заполнение журналов': 12,
}


def sort_documents(document):
    all_digit = re.findall(r'\d+', '{}'.format(document))
    code = ''.join(all_digit)
    code = int(code) if code else 0
    return code


class Command(BaseCommand):
    i = 0
    count_unique_field = 0
    wb = xlwt.Workbook()
    ws = wb.add_sheet('Test')
    font0 = xlwt.Font()
    font0.name = 'Times New Roman'
    font0.bold = True
    style0 = xlwt.XFStyle()
    style0.font = font0
    count_document = 0

    def handle(self, *args, **options):
        company = Company.objects.last()
        if not company:
            return
        tree = [get_branch(x, company, SECTIONS[0][0]) for x in DocumentStructure.objects.filter(section=None)]
        trees_sort = lambda tr: int(HIERARCHY_TREE.get(tr.get('text')))
        tree.sort(key=trees_sort)
        for branch in tree:
            self.write_branch(branch)
        self.ws.write(0, 0, 'Количество вопросов = {}'.format(self.count_unique_field), self.style0)
        self.wb.save('Вопросы по ОТ.xls')

    def write_branch(self, branch, j=0):
        text = branch.get('text')
        section = branch.get('href')
        self.i += 1
        documents = DocumentStructure.objects.get(url=section).ot_documents.all()
        documents = list(documents)
        documents.sort(key=sort_documents)
        self.ws.write(self.i, j, '{}'.format(text), self.style0)
        for document in documents:
            fields = document.fields.filter(~Q(field_name__in=['F6', 'F7', 'F8']))
            if fields:
                for field in fields:
                    field_labels = {
                        'F15': 'Вредные факторы для таблицы',
                        'F16': 'Время стажировки профессий',
                        'F22': 'Время стажировки руководителей',
                        'F19': 'Перечень должностей',
                        'F20': 'Перечень профессий'
                    }
                    label = field.label if field.label else field_labels.get(field.field_name, '')
                    self.i += 1
                    self.count_unique_field += 1
                    self.ws.write(self.i, j, '4.{} {}'.format(self.count_unique_field, label).strip().capitalize())
                    self.ws.write(self.i, j+1, '{}'.format(document).strip())
                    self.ws.write(self.i, j+2, '{}'.format(field.field_name).strip())

        if branch.get('nodes'):
            for x in branch.get('nodes'):
                self.i += 1
                self.write_branch(x, j)
