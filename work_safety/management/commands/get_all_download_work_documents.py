# coding: utf8
import re
from django.core.management.base import BaseCommand
import os
from work_safety.models import OTDocument, DocumentStructure, SECTIONS
from work_safety.views import get_branch
from company.models import Company
import xlwt


class Command(BaseCommand):
    i = 0
    wb = xlwt.Workbook()
    ws = wb.add_sheet('Test')
    font0 = xlwt.Font()
    font0.name = 'Times New Roman'
    font0.bold = True
    style0 = xlwt.XFStyle()
    style0.font = font0
    count_document = 0

    def handle(self, *args, **options):
        company = Company.objects.last()
        if not company:
            return
        tree = [get_branch(x, company, SECTIONS[0][0]) for x in DocumentStructure.objects.filter(section=None)]
        for branch in tree:
            self.write_branch(branch)
        self.ws.write(0, 2, 'Количество документов = {}'.format(self.count_document), self.style0)
        self.ws.write(
            0, 8, 'знак "+" перед названием документа говорит о том, что документ подтягивается всегда', self.style0)
        self.wb.save('Загруженные в программу документы ОТ.xls')

    def write_branch(self, branch, j=0):
        text = branch.get('text')
        section = branch.get('href')
        self.i += 1
        documents = DocumentStructure.objects.get(url=section).ot_documents.all()
        self.ws.write(self.i, j, '{}: {}'.format(text, documents.count()), self.style0)
        j += 1
        for document in documents.order_by('default_document', 'title'):
            self.count_document += 1
            self.i += 1
            # default = '+' if document.default_document else ''
            self.ws.write(self.i, j, '{}'.format(document).strip())
            # self.ws.write(self.i, j+1, '{}'.format(os.path.split(document.path)[1]).strip())
        if branch.get('nodes'):
            for x in branch.get('nodes'):
                # self.i += 1
                self.write_branch(x, j)
