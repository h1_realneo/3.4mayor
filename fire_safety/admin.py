from django.contrib import admin
from .models import *
from django.contrib.admin.widgets import AdminFileWidget
from django import forms


class FieldInDocumentAdmin(admin.ModelAdmin):
    model = FieldInDocument
    ordering = ['document', 'field_name']
    actions = ['make_required_field_false']

    def get_queryset(self, request):
        qs = super(FieldInDocumentAdmin, self).get_queryset(request)
        qs = qs.select_related('document')
        return qs

    def make_required_field_false(self, request, queryset):
        for field in queryset:
            field.required_field = False
            field.save()
    make_required_field_false.short_description = "Make required field False"

admin.site.register(FieldInDocument, FieldInDocumentAdmin)


@admin.register(FireDocument)
class FireDocumentAdmin(admin.ModelAdmin):
    model = FireDocument
    filter_horizontal = ('related_fields',)
    ordering = ['document_code']
    actions = ['make_default_document_true']

    def get_field_queryset(self, db, db_field, request):
        queryset = super(FireDocumentAdmin, self).get_field_queryset(db, db_field, request)
        if db_field.name == 'related_fields':
            queryset = queryset.select_related('document')
        return queryset

    def make_default_document_true(self, request, queryset):
        for field in queryset:
            field.default_document = True
            field.save()
    make_default_document_true.short_description = "Make default doc True"


@admin.register(Field)
class FireFieldAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super(FireFieldAdmin, self).get_queryset(request)
        qs = qs.select_related('document', 'company')
        return qs


@admin.register(Subdivision)
class SubdivisionAdmin(admin.ModelAdmin):
    list_display = ('company', 'title')


admin.site.register(ReadyDocument)


