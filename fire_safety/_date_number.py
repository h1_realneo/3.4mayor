# coding: utf8
from .models import *


def filing_date(form, section, company):
    # orders
    date = form.cleaned_data.get('date')
    # scrolls, provision, program,
    agreement = form.cleaned_data.get('date_agreement')
    approve = form.cleaned_data.get('date_approve')
    #  instruction
    agreement_top = form.cleaned_data.get('date_agreement_top')
    agreement_bottom = form.cleaned_data.get('date_agreement_bottom')
    approve_top = form.cleaned_data.get('date_approve')
    development = form.cleaned_data.get('date_development')

    for document in company.fire_documents.all().filter(section=section):
        if section == 'order':
            Field.objects.update_or_create(
                company=company, document=document, position_number=1, field_name='F7',
                defaults={'date': date})
        elif section == 'scroll' or section == 'provision' or section == 'program':
            Field.objects.update_or_create(
                company=company, document=document, position_number=1, field_name='F7',
                defaults={'date': agreement})
            Field.objects.update_or_create(
                company=company, document=document, position_number=2, field_name='F7',
                defaults={'date': approve})
        else:
            Field.objects.update_or_create(
                company=company, document=document, position_number=1, field_name='F7',
                defaults={'date': agreement_top})
            Field.objects.update_or_create(
                company=company, document=document, position_number=2, field_name='F7',
                defaults={'date': approve_top})
            Field.objects.update_or_create(
                company=company, document=document, position_number=3, field_name='F7',
                defaults={'date': agreement_bottom})
            Field.objects.update_or_create(
                company=company, document=document, position_number=4, field_name='F7',
                defaults={'date': development})

