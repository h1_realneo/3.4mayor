# coding: utf8
import shutil

from subprocess import Popen
from django.template import Template, Context
from django.core.exceptions import ObjectDoesNotExist

from .models import *
from zipfile import ZipFile
from mayor.settings import WINWORD_PATH, FINISHED_DOCUMENT, FINISHED_OLD_PATH, TMP_FOLDER
from company.models import Staff
from django.core.files import File

APP = 'пожарная безопасность'
FIELDS = ['F1', 'F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9', 'F10', 'F11', 'F12']
ALPHABET = [chr(x) for x in range(256, 10000) if 'а' <= chr(x) <= 'я' or 'А' <= chr(x) <= 'Я']
# fields and forms for this field
FIELDS_FORMS = {'F1': ['F1'], 'F2': ['F2'], 'F3': ['F3'], 'F4': ['F4', 'F41'], 'F5': ['F5'], 'F6': ['F6'], 'F7': ['F7'],
                'F8': ['F8'], 'F9': ['F9'], 'F10': ['F10'], 'F11': ['F11'], 'F12': ['F12'], 'F13': ['F13']}

MAX_PATH = 260


def parsing(document, company, section='order', preview=False):
    context = {}
    if isinstance(document, FireDocument):
        for name_model, forms in FIELDS_FORMS.items():
            context.update({name_model.lower(): Field.objects.filter(
                document=document, company=company, field_name__in=forms)
            })

    questions = {}
    [questions.update({x[0]: x[1]}) for x in Question.objects.filter(division='fire_safety').values_list('id', 'code')]
    answers_company = CompanyQuestionAnswer.objects.filter(company=company).values_list('question_id', 'answer')
    [context.update({'q_{}'.format(questions[x[0]]): x[1]}) for x in answers_company if questions.get(x[0], None)]
    related_context = get_related_context(document, company)
    related_fields = get_list(document, company)
    context.update(related_fields)
    context.update(related_context)

    people_who_known = get_list_known_order(context, company.boss) if section == 'order' else []

    authorized = get_authorized(company)
    try:
        context.update({
            'i': 1,
            'known': people_who_known,
            'address': company.address,
            'first_authorized': authorized[0],
            'second_authorized': authorized[1],
            'number_protocol': authorized[2],
            'person_authorized': authorized[3],
            'instructions': FireDocument.objects.filter(company=company, section='instruction'),
            'documents': [doc.document_code for doc in FireDocument.objects.filter(company=company)],
            'company_name': '{} {}'.format(company.genericinfocompany.short_ownership,
                                           company.genericinfocompany.short_title),
            'number_of_staff': Staff.objects.filter(company=company).count(),
            'company_full_name': [company.genericinfocompany.ownership, company.genericinfocompany.full_title],
            'director': company.boss,
            'person_safety_health': company.safety_health,
            'person_development_instruction': company.development_instruction,
            'person_development_program': company.development_program,
            'person_development_provision': company.development_provision,
        })
    except ObjectDoesNotExist:
        pass
    name_document = '{}.docx'.format(document.__str__())
    company_folder_name = ''
    for letter in company.genericinfocompany.short_title:
        if letter in ALPHABET:
            company_folder_name += letter

    folder_for_finished_document = '{0}{1}/{2}'.format(FINISHED_DOCUMENT, company_folder_name, section)
    if not os.path.exists(folder_for_finished_document):
        os.makedirs(folder_for_finished_document)
    document_word = os.path.abspath('{0}{1}/{2}/{3}'.format(
        FINISHED_DOCUMENT, company_folder_name, section, name_document)
    )
    with ZipFile(document.path, 'r') as template_word:
        with ZipFile(document_word, 'w') as new_document_word:
            for item in template_word.infolist():
                buffer = template_word.read(item.filename)
                if item.filename == 'word/document.xml' or item.filename == 'word/header1.xml':
                    with template_word.open(item) as render_file:
                        content = str(render_file.read(), 'utf-8')
                        template = Template(content)
                        new_document_word.writestr(item, template.render(Context(context)).encode())
                else:
                    new_document_word.writestr(item, buffer)

    document_pdf = os.path.abspath(document_word.replace('docx', 'pdf'))
    if os.path.exists(document_pdf):
        os.remove(document_pdf)
    if os.path.exists(document_word):
        import requests
        import requests.exceptions as exc
        with open(document_word, 'rb') as docx:
            try:
                res = requests.post(
                    url='http://127.0.0.2:9016/v1/00000000-0000-0000-0000-000000000000/convert',
                    data=docx,
                    headers={'Content-Type': 'application/octet-stream'}
                )
                f = open(document_pdf, 'wb')
                f.write(res.content)
            except exc.ConnectionError:
                process = Popen('"{}" "{}" /mExportToPDFext /q'.format(WINWORD_PATH, document_word), shell=True)
                process.wait()
                print('сформирован по СТАРОМУ - {}'.format(document))
    else:
        return

    if os.path.exists(document_pdf):
        with open(document_pdf, 'rb') as f:
            if isinstance(document, FireDocument):
                ready = ReadyDocument.objects.filter(company=company, fire_document=document).first()
                if ready:
                    ready.pdf_file.delete()
                else:
                    ready = ReadyDocument.objects.create(company=company, fire_document=document)
                ready.pdf_file.save('{}.pdf'.format(document.__str__()), File(f))
    rus_section = [x[1] for x in FireDocument.SECTIONS if x[0] == section]
    document_word_old_path = '{0}{1}/{2}/{3}'.format(FINISHED_OLD_PATH, company_folder_name, APP, section)
    if rus_section:
        rus_section = rus_section[0].replace(':', '/')
        document_word_old_path = '{0}{1}/{2}/{3}'.format(FINISHED_OLD_PATH, company_folder_name, APP, rus_section)
    # добавление файла в старые папки
    if not os.path.exists(document_word_old_path):
        os.makedirs(document_word_old_path)
    if not len(os.path.abspath('{}/{}'.format(document_word_old_path, name_document))) < MAX_PATH:
        while not len(os.path.abspath('{}/{}'.format(document_word_old_path, name_document))) < MAX_PATH:
            name_document_reverse = [letter for letter in name_document]
            name_document_reverse.reverse()
            for index, letter in enumerate(name_document_reverse):
                if letter is '.':
                    name_document_reverse.pop(index + 1)
                    break
            name_document_reverse.reverse()
            name_document = ''.join(name_document_reverse)
        word = os.path.abspath('{0}{1}/{2}/{3}'.format(FINISHED_DOCUMENT, company_folder_name, section, name_document))
        os.rename(document_word, word)
        document_word = word
    shutil.copy(document_word, document_word_old_path)
    return 1


def get_list_known_order(context, director):
    people_who_known = []
    result = {}
    for data in context.values():
        if data:
            for value in data:
                if hasattr(value, 'staff_position') or hasattr(value, 'staff_profession'):
                    position = getattr(value, 'staff_position')
                    if position and hasattr(position, 'staff'):
                        fio_pos = position.staff.surname, position.staff.initials
                    else:
                        fio_pos = ''
                    profession = getattr(value, 'staff_profession')
                    if profession and hasattr(profession, 'staff'):
                        fio_prof = profession.staff.surname, profession.staff.initials
                    else:
                        fio_prof = ''

                    if position and fio_pos not in result and director != position:
                        result.update({fio_pos: position})
                    if profession and fio_prof not in result and director != profession:
                        result.update({fio_prof: profession})
    for person in result.values():
        people_who_known.append(person)
    return people_who_known


def get_related_context(document, company):
    related_context = {}
    fields = document.related_fields.all()
    if fields:
        for field in fields:
            document = field.document
            field_name = field.field_name
            position_number = field.position_number
            related_data = Field.objects.filter(
                company=company, document=document, position_number=position_number, field_name=field_name)
            related_context.update({'{}_{}_{}'.format(document.document_code, field_name, position_number).lower(): related_data})
        return related_context
    return related_context


def get_list(document, company):
    context = {}
    # related_info = RelatedField.objects.filter(document=document)
    # for fields in related_info:
    #     fields_values = []
    #     title = fields.title
    #     for field in fields.related_fields.all():
    #         name = field.field_name
    #         if field.field_name == 'F41':
    #             name = 'f4'
    #         number_position = field.number_position
    #         document = field.document
    #         model = apps.get_model(app_label=APP, model_name=name)
    #         values = list(model.objects.filter(document=document, company=company, number_position=number_position))
    #         [fields_values.append(field_value) for field_value in values]
    #     context.update({title: fields_values})
    return context


def find_director(company):
    if company.boss:
        return company.boss


def occupational_safety_health(company):
    if company.safety_health:
        person = company.safety_health
        return person


def normalize_title(title):
    short_title = ''
    for letter in title:
        if letter in ALPHABET:
            short_title += letter
    return short_title


def get_authorized(company):
    person_authorized = company.person_authorized
    if person_authorized:
        person_authorized = person_authorized.staffposition_set.first() or person_authorized.staffprofession_set.first()
    if person_authorized and company.personauthorizedcompany.chairperson:
        return ['Председатель ППО',  company.genericinfocompany.short_title_case.gent, '', person_authorized]
    elif person_authorized and company.personauthorizedcompany.public_inspector:
        return ['Общественный инспектор по охране',  'труда профсоюзной организации', '', person_authorized]
    elif person_authorized and company.personauthorizedcompany.protocol_number:
        return ['Протокол заседания',  'Профсоюзного комитета', '№ {}'.format(company.personauthorizedcompany.protocol_number), '']
    elif person_authorized:
        return ['Уполномоченное лицо работников',  company.genericinfocompany.short_title_case.gent, '', person_authorized]
    else:
        return ['',  '', '', '']