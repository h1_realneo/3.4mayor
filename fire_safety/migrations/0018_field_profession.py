# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-11-04 12:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fire_safety', '0017_auto_20161102_1326'),
    ]

    operations = [
        migrations.AddField(
            model_name='field',
            name='profession',
            field=models.CharField(blank=True, max_length=300),
        ),
    ]
