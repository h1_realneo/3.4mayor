# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-11-21 18:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0030_auto_20161121_1505'),
        ('fire_safety', '0023_auto_20161121_0031'),
    ]

    operations = [
        migrations.AddField(
            model_name='samefield',
            name='staff_positions',
            field=models.ManyToManyField(blank=True, to='company.StaffPosition'),
        ),
        migrations.AddField(
            model_name='samefield',
            name='staff_professions',
            field=models.ManyToManyField(blank=True, to='company.StaffProfession'),
        ),
    ]
