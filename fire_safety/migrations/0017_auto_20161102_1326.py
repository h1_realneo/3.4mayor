# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-11-02 10:26
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('fire_safety', '0016_auto_20161102_1317'),
    ]

    operations = [
        migrations.AlterField(
            model_name='field',
            name='employee',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='company.Staff'),
        ),
    ]
