# coding: utf8
from django.shortcuts import HttpResponse, HttpResponseRedirect, render_to_response
from django.http import JsonResponse
from django.db.models import Q
from django.http import Http404
from django.views.generic import CreateView, FormView, TemplateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_GET
from django.shortcuts import get_object_or_404
from .parsing_document import parsing
from .forms import *
from .models import *
from company.models import Staff
import json
from company.views import get_staff_work
from company._utility import AllAboutStaff

from ._date_number import filing_date
from .ready import get_ready_documents


@require_GET
@login_required
def autocomplete(request):
    text = request.GET['name']
    names = Staff.objects.filter(Q(name__istartswith=text, company=request.user))
    good_names = [x.name for x in names]
    return JsonResponse({'name': good_names})


@method_decorator(login_required, name='dispatch')
class DocumentCreate(CreateView):
    template_name = 'fire_safety/create.html'
    all_forms = all_forms()
    formsets_prefix = FORMSETS_PREFIX
    object = None
    document = None
    forms_instances = None
    about_staff = None
    form_kwargs = None
    staff = None
    some_pos = None
    some_prof = None

    def get_context_data(self, **kwargs):
        context = super(DocumentCreate, self).get_context_data(**kwargs)
        context['document'] = self.document
        context['staff'] = Staff.objects.filter(company=self.request.user)
        context['staff_work'] = self.about_staff.exclude_mayor().get_staff_works_read()
        context['formset'] = self.formsets_prefix
        context['method'] = self.request.method
        context['ready_document'] = ReadyDocument.objects.filter(
            company=self.request.user, fire_document=context['document']).first()
        return context

    def get_forms(self, render_fields=None):
        render_forms = []
        if render_fields is None:
            return
        fields = [(x.field_name, x.position_number, x.prefix, x.label) for x in render_fields]
        [self.forms_instances.update({(x.field_name, x.position_number): x}) for x in
         Field.objects.filter(company=self.request.user, document=self.document)]
        for field in fields:
            form_name, position, prefix, label, name = field[0], field[1], field[2], field[3], field[0]
            self.prefix = prefix
            self.form_kwargs = dict(
                document=self.document,
                company=self.request.user,
                field_name=name,
                position_number=position,
                label=label,
                about_staff=self.about_staff,
                staff_queryset=self.staff,
                some_pos=self.some_pos,
                some_prof=self.some_prof
            )
            form = self.get_form(form_name)
            render_forms.append(form)
        return render_forms

    def get_form(self, form_name=None):
        if form_name:
            form_class = self.all_forms.get(form_name)
            return form_class(**self.get_form_kwargs())
        return

    def delete_data_exclude_fields(self, exclude_fields):
        company = self.request.user
        for field in exclude_fields:
            Field.objects.filter(
                company=company,
                document=field.document,
                field_name=field.field_name,
                position_number=field.position_number,
            ).delete()

    def get_form_kwargs(self):
        form_name = self.form_kwargs.get('field_name')
        form = self.all_forms.get(form_name)
        kwargs = dict(prefix=self.prefix, form_kwargs=self.form_kwargs)
        if form and self.request.method == 'POST':
            kwargs.update({'data': self.request.POST})
        if not hasattr(form, 'management_form'):
            instance = self.forms_instances.get(
                (self.form_kwargs['field_name'], self.form_kwargs['position_number']), None)
            kwargs.update({'instance': instance})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.forms_instances = {}
        self.about_staff = AllAboutStaff(self.request.user).add_mayor()
        self.staff = Staff.objects.filter(id__in=self.about_staff.staff_id).order_by('surname')
        self.some_prof = StaffProfession.objects.filter(staff_id__in=self.about_staff.get_staff_some_work())
        self.some_pos = StaffPosition.objects.filter(staff_id__in=self.about_staff.get_staff_some_work())
        document = FireDocument.objects.filter(id=int(kwargs.get('pk', -1))).first()
        fields = document.fields.all().order_by('index_number') if document else kwargs.pop('fields', None)
        if not fields and not document:
            raise Http404
        self.document = fields.first().document if fields else document
        render_forms = self.get_forms(render_fields=fields)
        return self.render_to_response(
            self.get_context_data(form=render_forms)
        )

    def post(self, request, *args, **kwargs):
        self.forms_instances = {}
        self.about_staff = AllAboutStaff(self.request.user).add_mayor()
        self.staff = Staff.objects.filter(id__in=self.about_staff.staff_id).order_by('surname')
        self.some_prof = StaffProfession.objects.filter(staff_id__in=self.about_staff.get_staff_some_work())
        self.some_pos = StaffPosition.objects.filter(staff_id__in=self.about_staff.get_staff_some_work())
        errors_in_forms = False
        company = self.request.user
        document = FireDocument.objects.filter(id=int(kwargs.get('pk', -1))).first()
        fields = document.fields.all().order_by('index_number') if document else kwargs.pop('fields', None)
        if not fields and not document:
            raise Http404
        self.document = fields.first().document if fields else document
        render_forms = self.get_forms(render_fields=fields)
        if render_forms:
            for form in render_forms:
                if not form.is_valid():
                    errors_in_forms = True
            if not errors_in_forms:
                for form in render_forms:
                    if not hasattr(form, 'management_form'):
                        instance = form.save(commit=False)
                        instance.company = company
                        instance.document = self.document
                        instance.position_number = form.position
                        instance.field_name = form.field_name
                        instance.label = form.label
                        instance.save()

                    else:
                        instances = form.save(commit=False)
                        [obj.delete() for obj in form.deleted_objects]
                        for cls in instances:
                            cls.company = company
                            cls.document = self.document
                            cls.position_number = form.position
                            cls.field_name = form.field_name
                            cls.label = form.label
                            cls.save()

            else:
                return self.form_invalid(render_forms)
            response = parsing(document=document, company=self.request.user, section=document.section, preview=True)
            if response:
                return HttpResponseRedirect(reverse('fire:list', kwargs={'section': self.kwargs['section']}))
        return HttpResponseRedirect(reverse('fire:create', kwargs={'pk': document.pk, 'section': self.kwargs['section']}))


@method_decorator(login_required, name='dispatch')
class ManyDocumentCreate(FormView):
    model = FireDocument
    form_class = SelectDocumentForm
    template_name = 'fire_safety/list.html'

    def get_context_data(self, **kwargs):
        context = super(ManyDocumentCreate, self).get_context_data(**kwargs)
        section = self.kwargs['section']
        context['section'] = section
        document_ready = []
        documents = self.request.user.fire_documents.all().filter(section=section).order_by('document_code')
        ready_documents = ReadyDocument.objects.filter(
            fire_document__in=documents, company=self.request.user).values_list('fire_document_id')
        for document in documents:
            if (document.id,) in ready_documents:
                document_ready.append([document, True])
            else:
                document_ready.append([document, False])
        context['documents'] = document_ready
        documents_id = get_ready_documents(section, self.request.user)
        ready_documents = FireDocument.objects.filter(id__in=documents_id).order_by('document_code')
        context['form'] = SelectDocumentForm(documents=ready_documents)
        return context

    def post(self, request, *args, **kwargs):
        section = self.kwargs['section']
        documents_id = get_ready_documents(section, self.request.user)
        ready_documents = FireDocument.objects.filter(id__in=documents_id).order_by('document_code')
        form = SelectDocumentForm(request.POST, documents=ready_documents)
        if form.is_valid():
            documents = form.cleaned_data['documents']
            for document in documents:
                parsing(document=document, company=self.request.user, section=section)
            return HttpResponseRedirect(reverse('fire:list', kwargs={'section': section}))
        return HttpResponse('Форма не валидна')


@method_decorator(login_required, name='dispatch')
class DataFiling(FormView):
    template_name = 'fire_safety/data_filing.html'
    forms = {'order': FilingOrderForm, 'scroll': FilingScrollForm, 'instruction': FilingInstructionForm,
             'program': FilingScrollForm, 'provision': FilingScrollForm}

    def get_form_class(self):
        return self.forms.get(self.kwargs.get('section'))

    def get_context_data(self, **kwargs):
        context = super(DataFiling, self).get_context_data(**kwargs)
        context['section'] = self.kwargs['section']
        return context

    def post(self, request, *args, **kwargs):
        section = self.kwargs.get('section')
        if section:
            form = self.forms.get(section)(request.POST)
            if form.is_valid():
                data = [form.cleaned_data.get(field) for field in form.fields]
                if [x for x in data if x != '']:
                    filing_date(form, section, request.user)
                    return HttpResponseRedirect(reverse('fire:list', kwargs={'section': section}))
                else:
                    return HttpResponse('заполните хоть одно поле')
            else:
                return HttpResponse('Форма не валидна')


def get_profession(request):
    staff_id = request.GET.get('staff_id')
    positions, professions = '<option value="">-----</option>', '<option value="">------</option>'
    if staff_id:
        staff = get_object_or_404(Staff, id=staff_id)
        if staff:
            pos_count = StaffPosition.objects.filter(staff=staff).count()
            prof_count = StaffProfession.objects.filter(staff=staff).count()
            for x in StaffProfession.objects.filter(staff=staff):
                professions += '<option value="{}">{}</option>'.format(x.id, x.work_profession)
            for y in StaffPosition.objects.filter(staff=staff):
                positions += '<option value="{}">{}</option>'.format(y.id, y.employee_position)
            if pos_count > 0 and prof_count > 0:
                return JsonResponse({'professions': professions, 'positions': positions})
            elif prof_count > 1:
                return JsonResponse({'professions': professions, 'positions': ''})
            elif pos_count > 1:
                return JsonResponse({'professions': '', 'positions': positions})
    return JsonResponse({'professions': '', 'positions': ''})


@method_decorator(login_required, name='dispatch')
class InstructionView(TemplateView):
    template_name = 'fire_safety/instruction.html'

