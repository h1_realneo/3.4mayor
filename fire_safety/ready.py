from .models import *

# from ._date_number import filing_data


def get_ready_documents(section, company):
    ready_documents_id = []
    document_related_field = {}
    documents_in_section = company.fire_documents.all().filter(section=section)
    company_fire_documents = [x[0] for x in company.fire_documents.all().values_list('id')]
    all_required_field = [x[0] for x in FieldInDocument.objects.filter(required_field=True).values_list('id')]
    related_field = FieldInDocument.objects.filter(related_field__in=documents_in_section).values_list(
        'related_field', 'id', 'document', 'position_number', 'field_name'
    )
    fields_in_section = FieldInDocument.objects.filter(document__in=documents_in_section).values_list(
        'document', 'id', 'field_name', 'position_number'
    )
    filing_field = Field.objects.filter(document__in=FireDocument.objects.all(), company=company).values_list(
        'field_name', 'position_number', 'document'
    )
    for value in related_field:
        related_doc, id_field, document, position_number, field_name = value[0], value[1], value[2], value[3], value[4]
        fields_in_doc = document_related_field.get(related_doc, [])
        fields_in_doc.append((id_field, document, field_name, position_number))
        document_related_field.update({related_doc: fields_in_doc})
    for document in documents_in_section:
        ready = True
        for field in [x for x in fields_in_section if x[0] == document.id]:
            if field[1] in all_required_field:
                if (field[2], field[3], field[0]) not in filing_field:
                    ready = False
        for field in document_related_field.get(document.id, []):
            if field[0] in all_required_field:
                if field[1] in company_fire_documents:
                    if (field[2], field[3], field[1]) not in filing_field:
                        ready = False
        if ready:
            ready_documents_id.append(document.id)
    if company.safety_health_id and company.boss_id:
        return ready_documents_id
    return []
