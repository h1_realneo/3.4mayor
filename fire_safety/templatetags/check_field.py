# coding: utf8
from django import template
from builtins import AttributeError, ValueError
from django.db.models.query import QuerySet
from fire_safety.templatetags import person, work
from fire_safety.models import Field
from work_safety.models import FieldOT

register = template.Library()
CASES = ['nomn', 'gent', 'datv', 'accs', 'ablt', 'loct']

ATTR = {'f': 'fs_pt',
        's': 'system',
        'par': 'paragraph',
        'e': 'employee',
        'pro': 'staff_profession',
        'tr': 'trigger',
        'pos': 'staff_position',
        'sub': 'subdivision',
        'subs': 'subdivisions',
        'date': 'date',
        'doc_n': 'document_number',
        'r': 'room',
        'ph': 'phone_number'
        }


@register.filter(name='c')
def check_field(field, args):
    args_list = args.split(',')
    if not field or not args_list:
        return False
    attr = args_list[0]
    if isinstance(field, Field) or isinstance(field, FieldOT):
        return getattr(field, ATTR.get(attr))
    if isinstance(field, QuerySet) or isinstance(field, list):
        for x in field:
            if not getattr(x, ATTR.get(attr)):
                return False
    return True
