# coding: utf8
from django import template
from fire_safety.templatetags.work import get_work
from fire_safety.templatetags.person import get_person
register = template.Library()
CASES = ['nomn', 'gent', 'datv', 'accs', 'ablt', 'loct']


@register.filter(name='w_p')
def get_person_and_work(field, args):
    person = get_person(field, args)
    work = get_work(field, args)
    return '{} {}'.format(work, person)
