# coding: utf8
from django import template
from fire_safety.models import Field
from work_safety.models import FieldOT
register = template.Library()
CASES = ['nomn', 'gent', 'datv', 'accs', 'ablt', 'loct']

ATTR = {'f': 'fs_pt',
        's': 'system',
        'par': 'paragraph',
        'e': 'employee',
        'pro': 'staff_profession',
        'tr': 'trigger',
        'pos': 'staff_position',
        'sub': 'subdivision',
        'date': 'date',
        'doc_n': 'document_number',
        'r': 'room',
        'ph': 'phone_number',
        'prof': 'professions',
        'posit': 'positions',
        't_b': 'time_briefing'
        }


@register.filter(name='g')
def get_value(field, args):
    args_list = args.split(',')
    if not field or not args_list:
        return ''
    attr = args_list[0]
    for x in field:
        if hasattr(x, ATTR.get(attr)):
            y = getattr(x, ATTR.get(attr))
            if hasattr(y, 'all'):
                return y.all()
            return '{}'.format(y)
    return ''
