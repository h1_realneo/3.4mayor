# coding: utf8
from builtins import AttributeError

from django import template
import pymorphy2

morph = pymorphy2.MorphAnalyzer()
register = template.Library()
MONTHS = [u'', u'январь', u'февраль', u'март', u'апрел', u'май', u'июнь', u'июль', u'август', u'сентябрь',
          u'октябрь', u'ноябрь', u'декабрь']

CASES = ['', 'nomn', 'gent', 'datv', 'accs', 'ablt', 'loct']


@register.filter(name='list_to_str')
def list_to_str(list_of_data):
    if list_of_data:
        list_of_data = list_of_data[2:-2].split("', '")
        line = ''
        for record in list_of_data:
            if record != list_of_data[-1]:
                line += record + ', '
            else:
                line += record
        return line
    else:
        return False
