# coding: utf8
from django import template
from fire_safety.models import *
from company.models import Staff

from builtins import AttributeError
import pymorphy2

morph = pymorphy2.MorphAnalyzer()
register = template.Library()
CASES = ['', 'nomn', 'gent', 'datv', 'accs', 'ablt', 'loct']


@register.filter(name='plus')
def get_info(num, args):
    args = args.split(',')
    word = args[0]
    if len(args) == 2:
        number = int(args[1])
        context = plus_in_context(word, number)
    else:
        context = plus_in_context(word)
    return context.get(word)


def plus_in_context(word, number=None, max_depth=4):
    import inspect
    stack = inspect.stack()[2:max_depth]
    context = {}
    for frame_info in stack:
        frame = frame_info[0]
        arg_info = inspect.getargvalues(frame)
        if 'context' in arg_info.locals:
            context = arg_info.locals['context']
            break
    value = context.get(word)
    if number:
        context.update({word: number+1})
    else:
        context.update({word: value+1})
    return context
