from itertools import chain
from fire_safety.models import *
from work_safety.models import FieldOT
from django import template
register = template.Library()


@register.filter(name='R')
def delete_reiteration_data(iteration, arg=None):
    result = []
    persons = []
    if not arg:
        for field in iteration:
            if isinstance(field, Field) or isinstance(field, FieldOT):
                person_work = field.staff_position if field.staff_position else field.staff_profession
                if person_work not in persons:
                    persons.append(person_work)
                    result.append(field)
            elif isinstance(field, StaffPosition):
                if field not in persons:
                    persons.append(field)
                    result.append(field)
            elif isinstance(field, StaffProfession):
                if field not in persons:
                    persons.append(field)
                    result.append(field)
            else:
                pass
    return result
