# coding: utf8
from builtins import TypeError
from django.db.models.query import QuerySet
from django import template
import datetime

register = template.Library()

MONTHS = ['', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня',
          'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря']


@register.filter(name='d')
def my_date(field, arg='f'):
    if isinstance(field, datetime.date):
        return '{}.{}.{}'.format(field.day, field.month, field.year)
    if not field:
        return ''
    if isinstance(field, QuerySet):
        field = field.first()
    elif isinstance(field, list):
        field = field[0]
    if hasattr(field, 'date'):
        date = field.date
    else:
        date = field
    # date saved like '30.11.2016' string format
    if date:
        # full - return date like 11.11.2016
        if arg == 'f':
            return '{}'.format(date)
        # day - return the day number in a date
        elif arg == 'd':
            return '{}'.format(date[0:2])
        # month - return the month number in a date
        elif arg == 'm':
            return '{}'.format(date[3:5])
        # month_word - return the month name in a date
        elif arg == 'm_w':
            return '{}'.format(MONTHS[int(date[3:5])])
        # year - return the year number in a date
        elif arg == 'y':
            return '{}'.format(date[-4:])
        # year_last - return the last digit of the year date
        elif arg == 'y_l':
            return '{}'.format(date[-1])
        # year_two_last - return the two last digit of the year date
        elif arg == 'y_t_l':
            return '{}'.format(date[-2:])
        else:
            return ''
    else:
        return ''
