# coding: utf8
from django import template
from builtins import IndexError
from django.db.models.query import QuerySet
from company.models import *
register = template.Library()
CASES = ['nomn', 'gent', 'datv', 'accs', 'ablt', 'loct']


@register.filter(name='p')
def get_person(field, args):
    # for order where known_order in list of str
    if isinstance(field, str):
        return field
    args_list = args.split(',')
    case = args_list[0]
    initials_first = ''
    try:
        initials_first = args_list[1]
    except IndexError:
        pass
    if not field:
        return ''
    if isinstance(field, QuerySet):
        field = field.first()
    elif isinstance(field, list):
        field = field[0]
    elif isinstance(field, StaffPosition) or isinstance(field, StaffProfession):
        staff = field.staff
        surname = getattr(staff.case, case)
        if initials_first:
            return '{} {}'.format(staff.initials, surname)
        return '{} {}'.format(surname, staff.initials)

    if getattr(field.employee, 'case_id'):
        surname = getattr(field.employee.case, case)
        initials = field.employee.initials
    else:
        try:
            surname = field.employee.surname
            initials = field.employee.initials
        except Exception:
            surname, initials = '', ''
    if initials_first:
        return '{} {}'.format(initials.upper(), surname.title())
    return '{} {}'.format(surname.title(), initials.upper())
