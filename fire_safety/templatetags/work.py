# coding: utf8
from django import template
from django.db.models.query import QuerySet
from company.models import *
import pymorphy2
from fire_safety.models import Field
from work_safety.models import FieldOT
register = template.Library()
CASES = ['nomn', 'gent', 'datv', 'accs', 'ablt', 'loct']
morph = pymorphy2.MorphAnalyzer()


@register.filter(name='w')
def get_work(field, args):
    args_list = args.split(',')
    case = args_list[0]
    if case not in CASES or not field:
        return ''
    if isinstance(field, WorkProfession):
        return field.name_working_career
    if isinstance(field, EmployeePosition):
        return field.name_post_office_worker
    if isinstance(field, StaffPosition):
        return get_value_staff_position(field, case)
    if isinstance(field, StaffProfession):
        return get_value_staff_profession(field, case)
    if isinstance(field, QuerySet):
        field = field.first()
    if isinstance(field, list):
        field = field[0]
    if isinstance(field, Staff):
        staff_position = StaffPosition.objects.filter(staff=field).first()
        staff_profession = StaffProfession.objects.filter(staff=field).first()
        if staff_position:
            return get_value_staff_position(staff_position, case)
        elif staff_profession:
            return get_value_staff_profession(staff_profession, case)
        return 'Этот человек не занимает никакой должности'

    if not isinstance(field, FieldOT) and not isinstance(field, Field):
        return ''

    if field.staff_position and field.staff_profession:
        return 'Выбрано сразу две должности для этого поля'
    elif field.staff_position:
        return get_value_staff_position(field.staff_position, case)
    elif field.staff_profession:
        return get_value_staff_profession(field.staff_profession, case)

    if not field.employee:
        return ''
    employee = field.employee
    staff_pos = StaffPosition.objects.filter(staff=employee).first()
    if staff_pos:
        return get_value_staff_position(staff_pos, case)
    staff_prof = StaffProfession.objects.filter(staff=employee).first()
    if staff_prof:
        return get_value_staff_profession(staff_prof, case)
    return ''


def get_value_staff_profession(instance, case):
    if not isinstance(instance, StaffProfession) and getattr(instance, 'case_id'):
        return ''
    rank = instance.rank if instance.rank else ''
    if rank:
        return '{} {} разряда'.format(getattr(instance.case, case), rank)
    return '{}'.format(getattr(instance.case, case))


def get_value_staff_position(instance, case):
    if isinstance(instance, StaffPosition) and getattr(instance, 'case_id'):
        category = instance.category if instance.category else ''
        if category:
            return '{} {} категории'.format(getattr(instance.case, case), category)
        return '{}'.format(getattr(instance.case, case))
    return ''