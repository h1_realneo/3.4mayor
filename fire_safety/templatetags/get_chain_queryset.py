from itertools import chain
from django import template
from company.models import *
from fire_safety.models import *
from work_safety.models import FieldOT
register = template.Library()


@register.filter(name='Q')
def get_chain_queryset(first, second):
    first = [] if not first else first
    second = [] if not second else second
    if isinstance(second, Field) or isinstance(second, StaffPosition) or isinstance(second, StaffProfession) or isinstance(second, FieldOT) or isinstance(second, Staff):
        second = [second]
    result_list = list(chain(first, second))
    return result_list
