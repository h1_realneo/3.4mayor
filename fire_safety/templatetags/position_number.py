# coding: utf8
from django import template
from builtins import AttributeError, ValueError

register = template.Library()
CASES = ['', 'nomn', 'gent', 'datv', 'accs', 'ablt', 'loct']


@register.filter(name='n')
def select_field_by_number(fields, position_number):
    if not position_number or not fields:
        return []
    try:
        position_number = int(position_number)
    except ValueError:
        return 'Неверный аргумент для фильтра field_number'
    answer = []
    [answer.append(field) for field in fields if field.position_number == position_number]
    return answer

