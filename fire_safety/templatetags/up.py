# coding: utf8
from django import template
register = template.Library()


@register.filter(name='u')
def up_first_letter(text):
    if not isinstance(text, str) or not text:
        try:
            text = text.__str__()
        except Exception:
            return text
    try:
        return '{}{}'.format(text[0].upper(), text[1:])
    except IndexError:
        return ''
