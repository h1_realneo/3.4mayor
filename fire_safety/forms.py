# coding: utf8
from django import forms
from django.forms import ModelForm, BaseModelFormSet
from django.forms import modelformset_factory
from .models import Subdivision, FireDocument, Field
from django.core.exceptions import ValidationError
from company.widgets import StaffSelect
from company.form_fields import WithoutRepeatQueryModelChoiceField


def customize_style(self):
    name_fields = []
    for field in self.fields:
        name_fields.append(field)
    for name_field in name_fields:
        field = self.fields.get(name_field)
        value = field.widget.attrs.get('class')
        if name_field == 'hazards':
            continue
        if value:
            field.widget.attrs.update({'class': value + ' form-control'})
        else:
            field.widget.attrs.update({'class': 'form-control'})


class SelectDocumentForm(forms.Form):
    documents = forms.ModelMultipleChoiceField(
        queryset=FireDocument.objects.all(),
        label='Документы, которые можно сформировать',
        widget=forms.SelectMultiple(attrs={'class': 'selectpicker', 'multiple': ''})
    )

    def __init__(self, *args, **kwargs):
        name_fields = []
        documents = kwargs.pop('documents', None)
        super(SelectDocumentForm, self).__init__(*args, **kwargs)
        self.fields['documents'].queryset = documents
        for field in self.fields:
            name_fields.append(field)
        for name_field in name_fields:
            field = self.fields.get(name_field)
            value = field.widget.attrs.get('class')
            if value:
                field.widget.attrs.update({'class': value + ' form-control'})
            else:
                field.widget.attrs.update({'class': 'form-control'})


class DateFilingForm(forms.Form):
    def __init__(self, *args, **kwargs):
        name_fields = []
        super(DateFilingForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            name_fields.append(field)
        for name_field in name_fields:
            if name_field != 'number':
                field = self.fields.get(name_field)
                field.widget.attrs.update({'class': 'form-control date', 'readonly': "true"})
            else:
                field = self.fields.get(name_field)
                field.widget.attrs.update({'class': 'form-control'})


class FilingOrderForm(DateFilingForm):
    date = forms.CharField(required=False, label='Дата')


class FilingScrollForm(DateFilingForm):
    date_agreement = forms.CharField(required=False, label='Дата согласования')
    date_approve = forms.CharField(required=False, label='Дата утверждения')


class FilingInstructionForm(DateFilingForm):
    date_agreement_top = forms.CharField(required=False, label='Дата Согласования вверху документа')
    date_agreement_bottom = forms.CharField(required=False, label='Дата Согласования внизу документа')
    date_approve = forms.CharField(required=False, label='Дата утверждения')
    date_development = forms.CharField(required=False, label='Дата разработки')


class Base(ModelForm):

    def __init__(self, *args, **kwargs):
        kwargs.pop('queryset', None)
        form_kwargs = kwargs.pop('form_kwargs', None)
        self.label = form_kwargs.get('label', None)
        self.company = form_kwargs.get('company', None)
        self.position = form_kwargs.get('position_number', None)
        self.document = form_kwargs.get('document', None)
        self.field_name = form_kwargs.get('field_name', None)
        about_staff = form_kwargs.get('about_staff', None)
        staff_queryset = form_kwargs.get('staff_queryset', None)
        some_pos = form_kwargs.get('some_pos', None)
        some_prof = form_kwargs.get('some_prof', None)
        about_staff.add_mayor()
        staff_work = {}
        [staff_work.update({x: about_staff.get_works_read_by_staff(x)}) for x in about_staff.staff_id]
        super(Base, self).__init__(*args, **kwargs)
        if self.fields.get('employee'):
            self.fields['employee'].queryset = staff_queryset
            self.fields['employee'].widget.staff_work = staff_work
            css_class = self.fields['employee'].widget.attrs.get('class')
            css_class = 'employee {}'.format(css_class) if css_class else 'employee'
            self.fields['employee'].widget.attrs.update({'class': css_class})
        if self.fields.get('staff_profession'):
            self.fields['staff_profession'].queryset = some_prof
            self.fields['staff_profession'].widget.attrs.update({'class': 'professions'})
        if self.fields.get('staff_position'):
            self.fields['staff_position'].queryset = some_pos
            self.fields['staff_position'].widget.attrs.update({'class': 'positions'})
        customize_style(self)

    def save(self, commit=True):
        instance = super(Base, self).save(commit=True)
        if instance:
            if instance.employee and not instance.staff_profession and not instance.staff_position:
                if instance.employee.employee_positions.all():
                    instance.staff_position = instance.employee.staffposition_set.all().first()
                else:
                    instance.staff_profession = instance.employee.staffprofession_set.all().first()
        instance.save()
        return instance


class BaseForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.label = kwargs.pop('label', None)
        self.company = kwargs.pop('company', None)
        self.position = kwargs.pop('position_number', None)
        self.document = kwargs.pop('document', None)
        self.field_name = kwargs.pop('field_name', None)
        about_staff = kwargs.pop('about_staff', None)
        staff_queryset = kwargs.pop('staff_queryset', None)
        some_pos = kwargs.pop('some_pos', None)
        some_prof = kwargs.pop('some_prof', None)
        about_staff.add_mayor()
        staff_work = {}
        [staff_work.update({x: about_staff.get_works_read_by_staff(x)}) for x in about_staff.staff_id]
        super(BaseForm, self).__init__(*args, **kwargs)
        if self.fields.get('employee'):
            self.fields['employee'].queryset = staff_queryset
            css_class = self.fields['employee'].widget.attrs.get('class')
            css_class = 'employee {}'.format(css_class) if css_class else 'employee'
            self.fields['employee'].widget.attrs.update({'class': css_class})
            self.fields['employee'].widget.staff_work = staff_work
        if self.fields.get('staff_profession'):
            self.fields['staff_profession'].queryset = some_prof
            self.fields['staff_profession'].widget.attrs.update({'class': 'professions'})
        if self.fields.get('staff_position'):
            self.fields['staff_position'].queryset = some_pos
            self.fields['staff_position'].widget.attrs.update({'class': 'positions'})
        customize_style(self)

    def save(self, commit=True):
        instance = super(BaseForm, self).save(commit=True)
        if instance:
            if instance.employee and not instance.staff_profession and not instance.staff_position:
                if instance.employee.employee_positions.all():
                    instance.staff_position = instance.employee.staffposition_set.all().first()
                else:
                    instance.staff_profession = instance.employee.staffprofession_set.all().first()
        instance.save()
        return instance


class BaseFormSet(BaseModelFormSet):
    def __init__(self, *args, **kwargs):
        self.label = kwargs.get('form_kwargs').get('label')
        self.company = kwargs.get('form_kwargs').get('company')
        self.position = kwargs.get('form_kwargs').get('position_number')
        self.document = kwargs.get('form_kwargs').get('document')
        self.field_name = kwargs.get('form_kwargs').get('field_name')
        super(BaseFormSet, self).__init__(*args, **kwargs)
        self.queryset = self.model.objects.filter(
            company=self.company, document=self.document, position_number=self.position, field_name=self.field_name
        )
        self.extra = 1 if not self.queryset.exists() else 0


class F1Form(Base):
    class Meta:
        model = Field
        fields = ['fs_pt']


class F3Form(Base):
    class Meta:
        model = Field
        fields = ['paragraph']
        widgets = {
            "paragraph": forms.Textarea(attrs={'rows': 3}),
            }


class F4Form(Base):
    class Meta:
        model = Field
        fields = ['employee', 'staff_profession', 'staff_position']
        widgets = {
            'employee': StaffSelect(attrs={'class': 'select_employee'}),
        }
        field_classes = {
            'employee': WithoutRepeatQueryModelChoiceField,
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }

    def clean_employee(self):
        data = self.cleaned_data['employee']
        if not data:
            raise ValidationError("Укажите сотрудника фирмы", code='invalid')
        return data


class F41Form(Base):
    class Meta:
        model = Field
        fields = ['employee', 'staff_profession', 'staff_position']
        widgets = {
            'employee': StaffSelect(attrs={'class': 'select_employee'}),
        }
        field_classes = {
            'employee': WithoutRepeatQueryModelChoiceField,
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }


class F7Form(Base):
    class Meta:
        model = Field
        fields = ['date']
        widgets = {
            "date": forms.TextInput(attrs={'class': 'date'}),
            }


class F8Form(Base):
    class Meta:
        model = Field
        fields = ['document_number']


class F11Form(Base):
    class Meta:
        model = Field
        fields = ['trigger']


class F2Form(BaseForm):

    class Meta:
        model = Field
        fields = ['system', 'employee', 'staff_profession', 'staff_position']
        widgets = {
            'system': forms.SelectMultiple(attrs={'class': 'selectpicker'}),
            'employee': StaffSelect(attrs={'class': 'select_employee'}),
        }
        field_classes = {
            'employee': WithoutRepeatQueryModelChoiceField,
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }


class F5Form(BaseForm):
    class Meta:
        model = Field
        fields = ['subdivision', 'employee', 'staff_profession', 'staff_position']
        widgets = {
            "subdivision": forms.Select(attrs={'class': 'selectpicker'}),
            'employee': StaffSelect(attrs={'class': 'select_employee'}),
            }
        field_classes = {
            'employee': WithoutRepeatQueryModelChoiceField,
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }

    def __init__(self, *args, **kwargs):
        super(F5Form, self).__init__(*args, **kwargs)
        self.fields.get('subdivision').queryset = Subdivision.objects.filter(company=self.company)


class F6Form(BaseForm):
    class Meta:
        model = Field
        fields = ['employee', 'staff_profession', 'staff_position']
        widgets = {
            'employee': StaffSelect(attrs={'class': 'select_employee'}),
        }
        field_classes = {
            'employee': WithoutRepeatQueryModelChoiceField,
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }


class F9Form(BaseForm):
    class Meta:
        model = Field
        fields = ['employee', 'staff_profession', 'staff_position']
        widgets = {
            'employee': StaffSelect(attrs={'class': 'select_employee'}),
        }
        field_classes = {
            'employee': WithoutRepeatQueryModelChoiceField,
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }


class F10Form(BaseForm):
    class Meta:
        model = Field
        fields = ['room']


class F12Form(BaseForm):
    class Meta:
        model = Field
        fields = ['employee', 'phone_number', 'staff_profession', 'staff_position']
        widgets = {
            'employee': StaffSelect(attrs={'class': 'select_employee'}),
        }
        field_classes = {
            'employee': WithoutRepeatQueryModelChoiceField,
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }


class F13Form(BaseForm):
    class Meta:
        model = Field
        fields = ['subdivisions', 'employee', 'staff_profession', 'staff_position']
        widgets = {
            "subdivisions": forms.SelectMultiple(attrs={'class': 'selectpicker'}),
            'employee': StaffSelect(attrs={'class': 'select_employee'}),
            }
        field_classes = {
            'employee': WithoutRepeatQueryModelChoiceField,
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }

    def __init__(self, *args, **kwargs):
        super(F13Form, self).__init__(*args, **kwargs)
        self.fields.get('subdivisions').queryset = Subdivision.objects.filter(company=self.company)


F2Formset = modelformset_factory(Field, form=F2Form, formset=BaseFormSet, extra=0, can_delete=True)
F5Formset = modelformset_factory(Field, form=F5Form, formset=BaseFormSet, extra=0, can_delete=True)
F6Formset = modelformset_factory(Field, form=F6Form, formset=BaseFormSet, extra=0, can_delete=True)
F9Formset = modelformset_factory(Field, form=F9Form, formset=BaseFormSet, extra=0, can_delete=True)
F10Formset = modelformset_factory(Field, form=F10Form, formset=BaseFormSet, extra=0, can_delete=True)
F12Formset = modelformset_factory(Field, form=F12Form, formset=BaseFormSet, extra=0, can_delete=True)
F13Formset = modelformset_factory(Field, form=F13Form, formset=BaseFormSet, extra=0, can_delete=True)


def all_forms():
    return dict(F1=F1Form,  F3=F3Form, F4=F4Form, F41=F41Form, F5=F5Formset, F6=F6Formset, F2=F2Formset, F7=F7Form,
                F8=F8Form, F9=F9Formset, F10=F10Formset, F11=F11Form, F12=F12Formset, F13=F13Formset)

FORMSETS_PREFIX = ['agreement1', 'subdivision1', 'system1', 'room1', 'telephone1'] + ['formset_fio%s' % x for x in range(1, 10)]