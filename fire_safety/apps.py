from django.apps import AppConfig


class FireSafetyConfig(AppConfig):
    name = 'fire_safety'
