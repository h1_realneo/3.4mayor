# coding: utf8
from django.db import models
from django.core.urlresolvers import reverse
from django.conf import settings
from company.models import *
from django.core.files.storage import FileSystemStorage
import os


class FireDocument(models.Model):
    FIRE_SCROLL = 'scroll'
    FIRE_ORDER = 'order'
    FIRE_INSTRUCTION = 'instruction'
    FIRE_PROGRAM = 'program'
    FIRE_PROVISION = 'provision'
    OTHER = 'other'
    SECTIONS = (
        (FIRE_SCROLL, 'перечени'),
        (FIRE_ORDER, 'приказы'),
        (FIRE_INSTRUCTION, 'инструкции'),
        (FIRE_PROGRAM, 'программы'),
        (FIRE_PROVISION, 'положения'),
        (OTHER, 'остальное'),
    )

    document_code = models.IntegerField(unique=True, null=True)
    related_fields = models.ManyToManyField('FieldInDocument', blank=True, related_name='related_field')
    title = models.CharField(max_length=300, default=u'первый')
    path = models.FilePathField(blank=True, null=True, max_length=300, path=settings.FILE_PATH_FIRE_DOCUMENT,
                                recursive=True)
    section = models.CharField(max_length=100, choices=SECTIONS, default=FIRE_ORDER)
    default_document = models.BooleanField('Стандартно необходимый документ', default=False)

    def __str__(self):
        return "{} {}".format(self.document_code, self.title)

    def get_absolute_url(self):
        return reverse('fire:create', kwargs={'pk': self.pk})

    class Meta:
        verbose_name = u'Документы пожарной безопасности'
        verbose_name_plural = u'Документы пожарной безопасности'


class FieldInDocument(models.Model):
    F1 = 'F1'
    F2 = 'F2'
    F3 = 'F3'
    F4 = 'F4'
    F41 = 'F41'
    F5 = 'F5'
    F6 = 'F6'
    F7 = 'F7'
    F8 = 'F8'
    F9 = 'F9'
    F10 = 'F10'
    F11 = 'F11'
    F12 = 'F12'
    F13 = 'F13'
    FIELDS = (
        (F1, u'F1 от пт'),
        (F2, u'F2 инженерные системы'),
        (F3, u'F3 дополнительный параграф'),
        (F4, u'F4 фио'),
        (F41, u'F41 фио или ничего'),
        (F5, u'F5 фио структурное подразделение'),
        (F6, u'F6 согласовавшие'),
        (F7, u'F7 дата документа'),
        (F8, u'F8 номер документа'),
        (F9, u'F9 фио формсет'),
        (F10, u'F10 помещения формсет'),
        (F11, u'F11 да нет'),
        (F12, u'F12 фио телефон'),
        (F13, u'F13 фио структурные подразделения'),
    )
    document = models.ForeignKey(FireDocument, related_name='fields')
    label = models.TextField(max_length=300, blank=True)
    field_name = models.CharField(max_length=100, choices=FIELDS, default=F1)
    prefix = models.CharField(max_length=100, blank=True)
    position_number = models.IntegerField(blank=True, null=True)
    index_number = models.IntegerField('Порядковый номер поля на форме', blank=True, null=True)
    required_field = models.BooleanField('Необходимое поле?', blank=True, default=False)

    def __str__(self):
        return '{}, {}, {}, {}'.format(self.document.title, self.field_name, self.prefix, self.label)

    class Meta:
        verbose_name = u'Поля в документах'
        verbose_name_plural = u'Поля в документах'


class Subdivision(models.Model):
    company = models.ForeignKey('company.Company')
    title = models.CharField(max_length=255)

    def __str__(self):
        return '{}'.format(self.title)

    class Meta:
        verbose_name = u'Название подразделение'
        verbose_name_plural = u'Названия подразделений'


class Field(models.Model):
    P1 = 'технического оборудования'
    P2 = 'вентиляционных систем и систем кондиционирования воздуха'
    P3 = 'средств связи и оповещения'
    P4 = 'отопительных систем'
    P5 = 'электроустановок молниезащитных и заземляющих устройств'
    P6 = 'технических средств противопожарной защиты'
    PLACES = (
        (P1, 'технического оборудования'),
        (P2, 'вентиляционных систем и систем кондиционирования воздуха'),
        (P3, 'средств связи и оповещения'),
        (P4, 'отопительных систем'),
        (P5, 'электроустановок, молниезащитных и заземляющих устройств'),
        (P6, 'технических средств противопожарной защиты'),
    )
    FIRE_SAFETY = 'пожарной безопасности'
    PROTECTION_WORK = 'охране труда'
    FIRE_WORK = (
        (FIRE_SAFETY, 'пожарной безопасности'),
        (PROTECTION_WORK, 'охране труда'),
    )
    field_name = models.CharField(max_length=255, choices=FieldInDocument.FIELDS, default=FieldInDocument.FIELDS[0])
    company = models.ForeignKey('company.Company', blank=True, null=True)
    document = models.ForeignKey(FireDocument, blank=True, null=True)
    position_number = models.IntegerField(blank=True, null=True)
    label = models.CharField(max_length=300, blank=True)
    # field's name
    fs_pt = models.CharField('пт или от', max_length=30, choices=FIRE_WORK, default=FIRE_SAFETY)
    system = MultiSelectField(max_length=255, blank=True, choices=PLACES)
    paragraph = models.TextField('дополнительный пункт', max_length=400, blank=True)
    employee = models.ForeignKey('company.Staff', blank=True, null=True, verbose_name='сотрудник')
    # if employee have some professions in this field we save one of them that use in this field
    staff_profession = models.ForeignKey(StaffProfession, blank=True, null=True, verbose_name='профессия')
    staff_position = models.ForeignKey(StaffPosition, blank=True, null=True, verbose_name='должность')
    subdivision = models.ForeignKey(Subdivision, verbose_name='Подразделение',  null=True)
    subdivisions = models.ManyToManyField(Subdivision, verbose_name='Подразделения', related_name='subs')
    date = models.CharField(u'дата подписи', max_length=255, blank=True)
    document_number = models.CharField(u'номер документа', max_length=255, blank=True)
    room = models.CharField(u'помещение', max_length=255)
    trigger = models.BooleanField(u'Да Нет', max_length=255, blank=True, default=False)
    phone_number = models.CharField(u'номер телефона', max_length=255)

    class Meta:
        verbose_name = u'Поле'
        verbose_name_plural = u'Поля'

    def __str__(self):
        return '{} {} {} {}'.format(self.company, self.document, self.field_name, self.position_number)


class OverwriteStorage(FileSystemStorage):

    def get_available_name(self, name, max_length=None):
        name = name.replace('_', ' ')
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name
fs = OverwriteStorage(location=settings.MEDIA_ROOT)


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/company_<id>/fire_<section>/document_<id>/<filename>
    return 'fire/{}/{}/{}/{}'.format(
        instance.company.id, instance.fire_document.section, instance.fire_document.id, filename
    )


class ReadyDocument(models.Model):
    company = models.ForeignKey(Company)
    fire_document = models.ForeignKey(FireDocument)
    pdf_file = models.FileField(upload_to=user_directory_path, storage=fs, max_length=350)
    word_file = models.FileField(upload_to=user_directory_path, blank=True)

    def __str__(self):
        return '{} {}'.format(self.company, self.fire_document)
