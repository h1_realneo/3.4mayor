# coding: utf8
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView
from .views import *

urlpatterns = [
    url(r'^auto/$', autocomplete, name='autocomplete'),
    url(r'^dynamic_employee/$', get_profession, name='dynamic'),
    url(r'^instructions/$', InstructionView.as_view(), name='instructions'),
    url(r'^(?P<section>[\w-]+)/$', ManyDocumentCreate.as_view(), name='list'),
    url(r'^(?P<section>[\w-]+)/data_filing/$', DataFiling.as_view(), name='data_filing'),
    url(r'^(?P<section>[\w-]+)/(?P<pk>[0-9]+)/$', DocumentCreate.as_view(), name='create'),


]
