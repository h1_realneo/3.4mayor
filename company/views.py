# coding: utf8
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.http.response import HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.mixins import PermissionRequiredMixin
from fire_safety.models import Subdivision, FireDocument
from django.core.urlresolvers import reverse
from django.db.models import Q
from .forms import AllCasesFormset, StaffSurnameInFormset, TelephoneFormset, GenericInfoCompanyForm, SectionActivityForm, \
    CaseForm, CompanyUpdateEKCDaEKTCForm, CreateLoginPasswordForm, PositionEKCD, ProfessionETKC, CompanyUpdateForm, \
    PersonAgreementFormset, CompanyUpdateLeadersForm, PersonAuthorizedForm, StaffForm, WorkProfessionFormset, \
    EmployeePositionFormset, StaffProfessionFormset, StaffPositionFormset, AnswerFormset, LoginForm, \
    StaffPositionUpdateFormset, StaffProfessionUpdateFormset, FactorProductionFormset, StaffUpdateFormset, \
    StaffProfessionWorkInstructionFormset, StaffPositionWorkInstructionFormset, CompanySubdivisionFormset
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Company, Case, GenericInfoCompany, StaffProfession, StaffPosition, Staff, WorkProfession, \
    EmployeePosition, CompanyQuestionAnswer, Question, PersonAuthorizedCompany, AdditionalInfo, KindActivity
from ._utility import get_staff_surname_initials, get_subdivision, AllAboutStaff
from django.http import JsonResponse
import pymorphy2
from pyphrasy.inflect import PhraseInflector
from django.http import Http404
from work_safety.models import OTDocument, FieldInDocument
from company.additional_info_for_choices import CONSTRUCTIONS, BUILDINGS, PREMISES, EQUIPMENT, MEANS_TRANSPORT, \
    INTERNAL_ENGINEERING_NETWORKS as IEN, EXTERNAL_ENGINEERING_NETWORKS as EEN
from django.contrib import messages
import datetime
import math
import re
from subprocess import Popen
from mayor.settings import CONVERT_EXCEL_TO_HTML
CASES = ['nomn', 'gent', 'datv', 'accs', 'ablt', 'loct']
morph = pymorphy2.MorphAnalyzer()


def check_staffing_table(self, set_message=False):
    if hasattr(self, 'login_url'):
        self.login_url = reverse('company:position_profession', kwargs={'pk': self.request.user.pk})
    href = self.login_url if hasattr(self, 'login_url') else ''
    self.company_staff = AllAboutStaff(self.request.user) if not hasattr(self, 'company_staff') else self.company_staff
    self.company_staff.exclude_mayor()
    staff_works = self.company_staff.get_staff_works()
    all_staff = self.company_staff.staff
    for staff_id, works in staff_works.items():
        if not works['pos'] and not works['prof']:
            if set_message:
                person = '{} {}'.format(all_staff[staff_id]['surname'], all_staff[staff_id]['initials'])
                messages.error(
                    self.request, 'Укажите должность/профессию для {} <a href="{}">здесь</a>'.format(person, href))
            return False
    return True


def check_all_cases(self, set_message=False):
    if hasattr(self, 'login_url'):
        self.login_url = reverse('company:all_cases', kwargs={'section': 'work'})
    href = self.login_url if hasattr(self, 'login_url') else ''
    self.company_staff = AllAboutStaff(self.request.user) if not hasattr(self, 'company_staff') else self.company_staff
    self.company_staff.exclude_mayor()
    cases_checked = Case.objects.filter(id__in=self.company_staff.get_all_cases().keys()).values_list('checked')
    for checked in cases_checked:
        if not checked[0]:
            if set_message:
                storage = messages.get_messages(self.request)
                storage.used = True
                messages.error(
                        self.request, 'Проверьте автоматически сгенерированные падежи <a href="{}">здесь</a>'.format(href))
            return False
    return True


def check_user(self):
    if self.request.genericinfocompany.id == GenericInfoCompany.objects.filter(id=self.kwargs.get('pk')):
        return True
    return False


def get_cases(word, case=None):
    """try to get russian cases for word"""
    import xlrd
    import os
    path = os.path.dirname(os.path.realpath('__file__'))
    rb = xlrd.open_workbook(os.path.join(path, 'cases.xlsx'))
    sheet = rb.sheet_by_index(0)
    val_nomn_case = [sheet.row_values(rownum)[0].lower() for rownum in range(sheet.nrows)]
    inflector = PhraseInflector(morph)
    cases = {}
    if case:
        if word.lower() in val_nomn_case:
            all_cases = sheet.row_values(val_nomn_case.index(word.lower()))
            return all_cases[CASES.index(case)]
        return inflector.inflect(word, case)
    else:
        if word.lower() in val_nomn_case:
            values = sheet.row_values(val_nomn_case.index(word.lower()))
            [cases.update({case: values[i]}) for i, case in enumerate(CASES)]
            return cases
        [cases.update({case: inflector.inflect(word, case)}) for case in CASES]
        return cases


def get_staff_work(company):
    person_works = []
    id_person = {}
    id_person_some_work = []
    all_id = []
    staff_works = {}
    staff_id = Staff.objects.filter(company=company).values_list('id')
    staff_id_surname_initials = Staff.objects.filter(company=company).values('id', 'surname', 'initials')
    [id_person.update(
        {x.get('id'): '{} {}'.format(x.get('surname'), x.get('initials'))}) for x in staff_id_surname_initials]
    staff_position = StaffPosition.objects.filter(staff__id__in=staff_id).order_by('staff').values(
        'staff', 'derivative_position', 'employee_position', 'category'
    )
    positions_id = [x.get('employee_position') for x in staff_position]
    positions_id_name = EmployeePosition.objects.filter(id__in=positions_id).values('id', 'name_post_office_worker')
    staff_profession = StaffProfession.objects.filter(staff__id__in=staff_id).order_by('staff').values(
        'staff', 'work_profession', 'rank'
    )
    profession_id = [x.get('work_profession') for x in staff_profession]
    profession_id_name = WorkProfession.objects.filter(id__in=profession_id).values('id', 'name_working_career')

    for value in staff_position:
        staff = value.get('staff')
        der, pos, category = value.get('derivative_position'), value.get('employee_position'), value.get('category')
        for pos_id in positions_id_name:
            if pos_id.get('id') == pos:
                pos = pos_id.get('name_post_office_worker')
                break
        works = staff_works.get(staff, ['{} {} {}'.format(der, pos, category)])
        if staff in staff_works.keys():
            works.append('{} {} {}'.format(der, pos, category))
        staff_works.update({staff: works})
    for value in staff_profession:
        staff = value.get('staff')
        prof, rank = value.get('work_profession'), value.get('rank')
        for prof_id in profession_id_name:
            if prof_id.get('id') == prof:
                prof = prof_id.get('name_working_career')
                break
        works = staff_works.get(staff, ['{} {}'.format(prof, rank)])
        if staff in staff_works.keys():
            works.append('{} {}'.format(prof, rank))
        staff_works.update({staff: works})
    [id_person_some_work.append(staff) for staff, works in staff_works.items() if len(works) > 1]
    for staff, works in staff_works.items():
        works = ', '.join(works).strip()
        staff = '{}'.format(id_person.get(staff)).title()
        person_works.append('{} {}'.format(staff, works))
    return person_works, id_person_some_work


class CompanyList(ListView):
    model = Company
    template_name = 'company/list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        company_generic_info = []
        company_list = context['company_list'].exclude(id=Company.objects.first().id).order_by('-pk')
        generic_info = GenericInfoCompany.objects.filter(company__in=company_list)
        for company in company_list:
            for info in generic_info:
                if info.company_id == company.id:
                    company_generic_info.append([company, info])
                    if self.request.user.id == company.id:
                        context['current_info_id'] = info.id
        context['company_generic_info'] = company_generic_info
        return context


class StaffPositionProfessionView(DetailView):

    context_object_name = 'company'
    model = Company
    template_name = 'company/staff_profession_position_detail.html'

    def get_context_data(self, **kwargs):
        context = super(StaffPositionProfessionView, self).get_context_data(**kwargs)
        about_staff = AllAboutStaff(self.request.user).exclude_mayor()
        cases_id = []
        context['staff'] = about_staff.staff
        [cases_id.append(x['case_id']) for x in about_staff.staff.values()]
        positions = about_staff.get_positions
        professions = about_staff.get_professions
        all_staff_positions = about_staff.staff_position
        all_staff_professions = about_staff.staff_profession
        context['all_staff_positions'] = all_staff_positions
        [cases_id.append(x['case_id']) for x in all_staff_positions.values()]
        context['all_staff_professions'] = all_staff_professions
        [cases_id.append(x['case_id']) for x in all_staff_professions.values()]
        context['positions'] = positions
        context['professions'] = professions
        staff_pos_prof = {}
        for staff_position_id, value in all_staff_positions.items():
            staff_id = value['staff_id']
            pos_prof_value = staff_pos_prof.get(staff_id, {'pos': [], 'prof': []})
            pos_value, prof_value = pos_prof_value['pos'], pos_prof_value['prof']
            pos_value.append(staff_position_id)
            pos_prof_value.update({'pos': pos_value, 'prof': prof_value})
            staff_pos_prof.update({staff_id: pos_prof_value})
        for staff_profession_id, value in all_staff_professions.items():
            staff_id = value['staff_id']
            pos_prof_value = staff_pos_prof.get(staff_id, {'pos': [], 'prof': []})
            pos_value, prof_value = pos_prof_value['pos'], pos_prof_value['prof']
            prof_value.append(staff_profession_id)
            pos_prof_value.update({'pos': pos_value, 'prof': prof_value})
            staff_pos_prof.update({staff_id: pos_prof_value})

        staff_without_work = staff_pos_prof.keys()
        for staff_id in about_staff.staff_id:
            if staff_id not in staff_without_work:
                staff_pos_prof.update({staff_id: {'pos': [], 'prof': []}})
        context['staff_pos_prof'] = staff_pos_prof
        staff_position_text = {}
        for staff_pos_id, value in all_staff_positions.items():
            title_position = positions.get(value['pos_id'])
            text = '{} {} {}'.format(value['derivative'], title_position, value['category']).strip()
            staff_position_text.update({staff_pos_id: text})
        staff_profession_text = {}
        for staff_prof_id, value in all_staff_professions.items():
            title_profession = professions.get(value['prof_id'])
            text = '{} {}'.format(title_profession, value['rank']).strip()
            staff_profession_text.update({staff_prof_id: text})
        context['staff_profession_text'] = staff_profession_text
        context['staff_position_text'] = staff_position_text
        cases = {}
        for x in Case.objects.filter(id__in=cases_id).values_list():
            cases.update({x[0]: {
                'nomn': x[1], 'gent': x[2], 'datv': x[3], 'accs': x[4], 'ablt': x[5], 'loct': x[6]
            }
                          })
        context['cases'] = cases
        return context


class CompanyCreate(CreateView):
    model = Company
    form_class = CreateLoginPasswordForm
    template_name = 'company/create.html'

    def form_valid(self, form):
        super(CompanyCreate, self).form_valid(form)
        self.object.set_password(self.request.POST['password'])
        self.object.is_admin = True
        self.object.save()
        self.object.fire_documents.set(FireDocument.objects.filter(default_document=True))
        self.object.work_documents.set(OTDocument.objects.filter(default_document=True))
        GenericInfoCompany.objects.bulk_create([GenericInfoCompany(company=self.object)])
        CompanyQuestionAnswer.objects.bulk_create(
            [CompanyQuestionAnswer(company=self.object, question=x) for x in Question.objects.all()]
        )
        user = authenticate(username=self.object.username, password=self.request.POST['password'])
        login(self.request, user)
        return redirect(reverse('company:staff_subdivision_download', kwargs={'pk': self.object.pk}))


class CompanyUpdateEKCDaEKTC(UpdateView):
    model = Company
    form_class = CompanyUpdateEKCDaEKTCForm
    template_name = 'company/update_ekcd_etkc.html'

    def get_context_data(self, **kwargs):
        context = super(CompanyUpdateEKCDaEKTC, self).get_context_data(**kwargs)
        context['profession_etkc'] = [(x.code, x.title) for x in ProfessionETKC.objects.all()]
        context['position_ekcd'] = [(x.code, x.title) for x in PositionEKCD.objects.all()]
        return context


class CompanyUpdate(UpdateView):
    model = Company
    form_class = CompanyUpdateForm
    template_name = 'company/update.html'

    def get_context_data(self, **kwargs):
        context = super(CompanyUpdate, self).get_context_data(**kwargs)
        # context['agreement'] = AdditionalInfo.objects.filter(company=self.object)
        # context['person_agreement_order_form'] = PersonAgreementFormset(
        #     prefix='agreement1', form_kwargs={'company': self.object})
        all_staff = AllAboutStaff(self.request.user)
        context['staff_work'] = all_staff.get_staff_works_read()
        context['profession_etkc'] = [(x.code, x.title) for x in ProfessionETKC.objects.all()]
        context['position_ekcd'] = [(x.code, x.title) for x in PositionEKCD.objects.all()]
        return context

    def get_form_kwargs(self):
        kwargs = super(CompanyUpdate, self).get_form_kwargs()
        kwargs['company'] = self.request.user
        return kwargs

    def form_valid(self, form):
        super(CompanyUpdate, self).form_valid(form)
        # additional_form = PersonAgreementFormset(self.request.POST, prefix='agreement1', form_kwargs={'company': self.object})
        # if additional_form.is_valid():
        #     pass
        # filing_data(self.object)
        return redirect(self.get_success_url())

    def form_invalid(self, form):
        person_agreement_order_form = PersonAgreementFormset()
        agreement = AdditionalInfo.objects.filter(company=self.object)
        return self.render_to_response(self.get_context_data(
            agreement=agreement, form=form, person_agreement_order_form=person_agreement_order_form
            )
        )

    def get_success_url(self):
        return reverse('company:update_leaders', kwargs={'section': 'fire', 'pk': self.object.pk})


class CompanyLeadersUpdateView(PermissionRequiredMixin, UpdateView):
    model = Company
    form_class = CompanyUpdateLeadersForm
    second_form = PersonAuthorizedForm
    template_name = 'company/update_leaders.html'
    about_staff = None

    def get_context_data(self, **kwargs):
        context = super(CompanyLeadersUpdateView, self).get_context_data(**kwargs)
        context['staff_work'] = self.about_staff.get_staff_works_read()
        obj = PersonAuthorizedCompany.objects.filter(company=self.request.user).first()
        if not obj:
            obj = PersonAuthorizedCompany.objects.create(company=self.request.user)
        context['person_authorized_form'] = PersonAuthorizedForm(instance=obj)
        return context

    def get_form_kwargs(self):
        kwargs = super(CompanyLeadersUpdateView, self).get_form_kwargs()
        self.about_staff = self.company_staff if hasattr(self, 'company_staff') else AllAboutStaff(self.request.user)
        kwargs['about_staff'] = self.about_staff
        return kwargs

    def post(self, request, *args, **kwargs):
        self.about_staff = self.company_staff if hasattr(self, 'company_staff') else AllAboutStaff(self.request.user)
        form = self.form_class(request.POST, instance=self.get_object(), about_staff=self.about_staff)
        form2 = PersonAuthorizedForm(request.POST, instance=PersonAuthorizedCompany.objects.get(company=request.user))
        if form2.is_valid() and form.is_valid():
            form2.save()
            form.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(self.get_context_data(form=form, form2=form2))

    def get_success_url(self):
        section = self.kwargs.get('section', None)
        if section in ['work', 'fire']:
            return reverse('company:all_cases', kwargs={'section': section})
        else:
            return reverse('work-safety:download_documents')

    def has_permission(self):
        return check_staffing_table(self, set_message=True)


class DownloadStaffSubdivisionView(UpdateView):
    model = Company
    form_class = StaffForm
    template_name = 'company/subdivision_staff_download.html'

    def post(self, request, *args, **kwargs):
        start = datetime.datetime.now().timestamp()
        form = self.get_form()
        company = self.object = self.get_object()

        if form.is_valid():
            if self.request.FILES.get('subdivision'):
                subdivisions = get_subdivision(self.request.FILES.get('subdivision'))
                Subdivision.objects.filter(company=company).delete()
                for subdivision in subdivisions:
                    Subdivision.objects.create(company=company, title=subdivision)
            else:
                # create and delete subdivision for delete fields
                Subdivision.objects.create(company=self.request.user, title='')
                company.subdivision_set.all().delete()
            if self.request.FILES.get('people_profession'):
                company.staff.all().delete()
                surname_initials_date = get_staff_surname_initials(self.request.FILES.get('people_profession'), company)
                staff, staff_professions, staff_positions, works_code = [], [], [], []
                positions, professions = [], []
                for surname, initials, date, prof, pos, sub, birth in surname_initials_date:
                    profession, rank = prof.get('profession', None), prof.get('rank', '')
                    position, category = pos.get('position', None), pos.get('category', '')
                    derivative = pos.get('derivative', '')
                    if profession:
                        staff_professions.append((surname, initials, date, profession, rank, sub))
                        works_code.append('{}'.format(profession.profession_code))
                        professions.append(profession)
                    elif position:
                        staff_positions.append((surname, initials, date, position, category, derivative, sub))
                        works_code.append('{}'.format(position.position_code))
                        positions.append(position)
                    person_cases = get_cases(surname)
                    [person_cases.update({case: value.title()}) for case, value in person_cases.copy().items()]
                    case = Case.objects.create(**person_cases)
                    staff.append(
                        Staff(
                            company=company,
                            case=case,
                            surname=surname,
                            initials=initials,
                            employment_date=date,
                            date_birthday=birth,
                        )
                    )
                Staff.objects.bulk_create(staff)
                staff = list(company.staff.values_list('id', 'surname', 'initials', 'employment_date'))
                staff_profession_for_bulk, staff_position_for_bulk = [], []
                for staff_position in staff_positions:
                    surname, initials, date, sub = staff_position[0], staff_position[1], staff_position[2], staff_position[6]
                    for person in staff.copy():
                        if surname == person[1] and initials == person[2] and date == person[3]:
                            staff.remove(person)
                            person_id = person[0]
                            pos, category, derivative = staff_position[3], staff_position[4], staff_position[5]
                            category = category if category in pos.categories else ''
                            if derivative.lower() in ['заместитель', 'помощник']:
                                title = '{} {}'.format(derivative, get_cases(pos.name_post_office_worker, case='gent'))
                            else:
                                title = '{} {}'.format(derivative, pos.name_post_office_worker)
                            pos_cases = {}
                            [pos_cases.update({case: value.lower()}) for case, value in get_cases(title).items()]
                            case = Case.objects.create(**pos_cases)
                            staff_position_for_bulk.append(
                                StaffPosition(
                                    staff_id=person_id,
                                    case=case,
                                    employee_position=pos,
                                    category=category,
                                    derivative_position=derivative,
                                    date_recruitment=date,
                                    subdivision_id=sub
                                )
                            )
                            break

                StaffPosition.objects.bulk_create(staff_position_for_bulk)

                for staff_profession in staff_professions:
                    surname, initials, date, sub = staff_profession[0], staff_profession[1], staff_profession[2], staff_profession[5]
                    for person in staff.copy():
                        if surname == person[1] and initials == person[2] and date == person[3]:
                            staff.remove(person)
                            person_id = person[0]
                            prof, rank = staff_profession[3], staff_profession[4]
                            rank = rank if rank in prof.ranks else ''
                            prof_cases = {}
                            [prof_cases.update({case: value}) for case, value in get_cases(prof.name_working_career).items()]
                            case = Case.objects.create(**prof_cases)
                            staff_profession_for_bulk.append(
                                StaffProfession(
                                    staff_id=person_id,
                                    case=case,
                                    work_profession=prof,
                                    rank=rank,
                                    date_recruitment=date,
                                    subdivision_id=sub
                                )
                            )
                            break
                StaffProfession.objects.bulk_create(staff_profession_for_bulk)
                etkc = WorkProfession.objects.filter(id__in=[x[3].id for x in staff_professions]).prefetch_related(
                    'professions_etkc')
                etkc = [y for x in etkc for y in x.professions_etkc.all()]
                ekcd = EmployeePosition.objects.filter(id__in=[x[3].id for x in staff_positions]).prefetch_related(
                    'positions_ekcd')
                ekcd = [y for x in ekcd for y in x.positions_ekcd.all()]
                company.positions_ekcd.set(ekcd), company.professions_etkc.set(etkc)
                # add doc by codes in document files
                queries = [Q(path__icontains=code) for code in works_code]
                query = queries.pop()
                for item in queries:
                    query |= item
                work_documents = [x[0] for x in OTDocument.objects.filter(query).values_list('id')]
                [work_documents.append(x[0]) for x in OTDocument.objects.filter(default_document=True).values_list('id')]
                positions = EmployeePosition.objects.filter(
                    id__in=[x.id for x in positions]).prefetch_related('work_documents')
                [work_documents.append(doc.id) for pos in positions for doc in pos.work_documents.all()]
                professions = WorkProfession.objects.filter(
                    id__in=[x.id for x in professions]).prefetch_related('work_documents')
                [work_documents.append(doc.id) for prof in professions for doc in prof.work_documents.all()]
                company.work_documents.set(OTDocument.objects.filter(id__in=work_documents))

            if company.staff.all():
                end = datetime.datetime.now().timestamp()
                time_download = '{}'.format(math.ceil(end - start))
                messages.error(self.request, 'Время обработки штатного: {} (c)'.format(time_download))
                self.request.FILES.get('people_profession').name = '{}.xlsx'.format(company.id)
                company.people_profession = self.request.FILES.get('people_profession')
                company.save()
                # get excel document for staff
                # people_profession_excel = obj.people_profession.path
                # process = Popen('{} """{}"""'.format(CONVERT_EXCEL_TO_HTML, people_profession_excel), shell=True)
                # process.wait()
                return HttpResponseRedirect(self.get_success_url())
            else:
                form.add_error('people_profession', 'Не удалось считать сотрудников из штатного, Первый столбец '
                                                    'фамилия инициалы, второй должность или профессия')
                return self.form_invalid(form)
        return self.form_invalid(form)

    def get_success_url(self):
        return reverse('home')


class CompanyDelete(DeleteView):
    model = Company
    success_url = reverse_lazy('company:list')
    template_name = 'company/delete.html'


@csrf_protect
def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(
                        reverse('company:generic_info_detail', args=(user.genericinfocompany.pk,))
                    )
    else:
        if request.GET.get('login'):
            form = LoginForm(initial={'username': request.GET.get('login')})
        else:
            form = LoginForm()

    return render(request, 'company/login.html', {'form': form})


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('company:list'))


class QuestionView(FormView):
    division = None
    pk = None
    queryset = None
    form_class = AnswerFormset
    template_name = 'company/question.html'

    def get_form_kwargs(self):
        kwargs = super(QuestionView, self).get_form_kwargs()
        divisions = [x[0] for x in Question.DIVISION]
        if self.kwargs.get('division', None) not in divisions:
            raise Http404("Division does not exist")
        choices = Question.objects.filter(division=self.kwargs['division']).values_list(
            'id', 'choices', 'multiple', 'help_text')
        self.queryset = CompanyQuestionAnswer.objects.filter(company=self.request.user).filter(
            question__in=[x[0] for x in choices]).select_related('question')
        kwargs['queryset'] = self.queryset
        kwargs['form_kwargs'] = {'choices': choices}
        return kwargs

    def form_valid(self, form):
        form.save()
        return super(QuestionView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(QuestionView, self).get_context_data(**kwargs)
        context['question_text'] = [[q.id, q.question.text] for q in self.queryset]
        return context

    def add_remove_documents_fields(self):
        division = self.kwargs.get('division', None)
        company = self.request.user
        exclude_fields = []
        add_documents = []
        remove_documents = []
        question_answer = {}
        [question_answer.update({answer.question_id: answer.answer}) for answer in self.queryset]
        if division == 'work_safety':
            field_related_questions = FieldInDocument.objects.filter(
                Q(work_field_no__isnull=False) | Q(work_field_yes__isnull=False))
            company_work_documents = company.work_documents.values_list('id')
            company_work_documents = [x[0] for x in company_work_documents]
            work_documents_related_question = OTDocument.objects.filter(
                Q(work_yes__isnull=False) | Q(work_no__isnull=False)
            ).values_list('id')
            work_documents_related_question = [x[0] for x in work_documents_related_question]
            questions = Question.objects.filter(division=division).prefetch_related(
                'work_doc_yes', 'work_doc_no', 'work_field_exclude_yes', 'work_field_exclude_no'
            )
            for question in questions:
                answer = question_answer.get(question.id)
                if answer == 'yes' or question.multiple and answer:
                    [add_documents.append(x.id) for x in question.work_doc_yes.all()]
                    [exclude_fields.append(field) for field in question.work_field_exclude_yes.all()]
                elif answer == 'no' or question.multiple and not answer:
                    [add_documents.append(x.id) for x in question.work_doc_no.all()]
                    [exclude_fields.append(field) for field in question.work_field_exclude_no.all()]
            [remove_documents.append(doc) for doc in work_documents_related_question if doc not in add_documents]
            company_work_documents += add_documents
            [company_work_documents.remove(doc) for doc in remove_documents if doc in company_work_documents]
            company.work_documents.set(company_work_documents)
            [company.work_field_exclude.add(x) if x in exclude_fields else company.work_field_exclude.remove(x) for x in field_related_questions]
        elif division == 'fire_safety':
            company_fire_documents = company.fire_documents.values_list('id')
            company_fire_documents = [x[0] for x in company_fire_documents]
            fire_documents_related_question = FireDocument.objects.filter(
                Q(question_yes__isnull=False) | Q(question_no__isnull=False)
            ).values_list('id')
            fire_documents_related_question = [x[0] for x in fire_documents_related_question]
            questions = Question.objects.filter(division=division).prefetch_related(
                'documents_answer_yes', 'documents_answer_no',
            )
            for question in questions:
                answer = question_answer.get(question.id)
                if answer == 'yes' or question.multiple and answer:
                    [add_documents.append(x.id) for x in question.documents_answer_yes.all()]
                elif answer == 'no' or question.multiple and not answer:
                    [add_documents.append(x.id) for x in question.documents_answer_no.all()]
            [remove_documents.append(doc) for doc in fire_documents_related_question if doc not in add_documents]
            company_fire_documents += add_documents
            [company_fire_documents.remove(doc) for doc in remove_documents if doc in company_fire_documents]
            company.fire_documents.set(company_fire_documents)

    def get_success_url(self):
        self.add_remove_documents_fields()
        if self.kwargs.get('division', None) == 'fire_safety':
            return reverse('fire:list', kwargs={'section': 'order'})
        return reverse('company:position_profession', kwargs={'pk': self.kwargs.get('pk')})


class StaffView(PermissionRequiredMixin, DetailView):
    model = Company
    template_name = 'company/staff_detail.html'
    success_url = '/company/'

    def get_context_data(self, **kwargs):
        context = super(StaffView, self).get_context_data(**kwargs)
        staff = self.object.staff.all().order_by('surname')
        context['staff'] = staff
        cases_id = [x.case_id for x in staff]
        cases = {}
        for x in Case.objects.filter(id__in=cases_id).values_list():
            cases.update({x[0]: {
                'nomn': x[1], 'gent': x[2], 'datv': x[3], 'accs': x[4], 'ablt': x[5], 'loct': x[6]
            }
                          })
        context['cases'] = cases
        return context

    def get_success_url(self):
        return reverse('company:staff_detail', kwargs={'pk': self.kwargs.get('pk')})

    def has_permission(self):
        return self.request.user.pk == int(self.kwargs.get('pk'))


class EmployeePositionView(UpdateView):
    model = Staff
    form_class = StaffPositionFormset
    template_name = 'company/staff_position.html'

    def get_form_kwargs(self):
        kwargs = super(EmployeePositionView, self).get_form_kwargs()
        kwargs.update(form_kwargs={'person': self.object, 'company': self.request.user})
        return kwargs

    def get_success_url(self):
        return reverse('company:position_profession', kwargs={'pk': self.request.user.pk})


def get_categories_and_list_position(request):
    letters_in_position = request.GET.get('position')
    position_select = request.GET.get('position_select')
    code = request.GET.get('code')
    if letters_in_position:
        works = EmployeePosition.objects.filter(
            name_post_office_worker__icontains=letters_in_position
        ).prefetch_related('sizs').select_related('company')
        json = []
        create_title = 'создан'
        for work in works:
            if work.position_code and work.company:
                position_code = 'должность создана {} соответствует коду {}'.format(work.company, work.position_code)
            else:
                position_code = work.position_code or work.company or create_title
            title_work = '{}'.format(work.name_post_office_worker)
            sizs = '+' if work.sizs.exists() else '-'
            categories = ' '.join(work.categories)
            create = True if position_code == create_title else False
            json.append({
                'label': '{}     КОД({}) СИЗ({}) КАТЕГОРИИ({})'.format(title_work, position_code, sizs, categories),
                'value': work.name_post_office_worker, 'code': work.id, 'create': create
            })
        json.sort(key=lambda data: 100 + len('{}'.format(data.get('value', ''))) if data['create'] else len('{}'.format(data.get('value', ''))))
        return JsonResponse({'json': json})
    if position_select:
        work = EmployeePosition.objects.filter(name_post_office_worker__iexact=position_select, id=code)
        if work.count() == 1:
            categories = work.first().categories
            return JsonResponse({'json': categories})
    return JsonResponse({'json': [str(x) for x in range(5)]})


class EmployeeProfessionView(UpdateView):
    model = Staff
    form_class = StaffProfessionFormset
    template_name = 'company/staff_profession.html'

    def get_form_kwargs(self):
        kwargs = super(EmployeeProfessionView, self).get_form_kwargs()
        kwargs.update(form_kwargs={'person': self.object, 'company': self.object.company})
        return kwargs

    def get_success_url(self):
        return reverse('company:position_profession', kwargs={'pk': self.request.user.pk})


def get_ranks_and_list_work_professions(request):
    letters_in_profession = request.GET.get('work_profession')
    work_profession_select = request.GET.get('work_profession_select')
    code = request.GET.get('code')
    if letters_in_profession:
        works = WorkProfession.objects.filter(
            name_working_career__icontains=letters_in_profession).prefetch_related('sizs').select_related('company')
        json = []
        create_title = 'создан'
        for work in works:
            title_work = '{}'.format(work.name_working_career)
            if work.profession_code and work.company:
                profession_code = 'профессия создана {} соответствует коду {}'.format(work.company, work.profession_code)
            else:
                profession_code = work.profession_code or work.company or create_title
            sizs = '+' if work.sizs.exists() else '-'
            ranks = ' '.join(work.ranks)
            create = True if profession_code == create_title else False
            json.append({
                'label': '{}     КОД({}) СИЗ({}) РАЗРЯДЫ({})'.format(title_work, profession_code, sizs, ranks),
                'value': work.name_working_career, 'code': work.id, 'create': create
            })
        json.sort(key=lambda data: 100 + len('{}'.format(data.get('value', ''))) if data['create'] else len('{}'.format(data.get('value', ''))))
        return JsonResponse({'json': json})
    if work_profession_select:
        work = WorkProfession.objects.filter(name_working_career__iexact=work_profession_select, id=code)
        if work.count() == 1:
            ranks = work.first().ranks
            return JsonResponse({'json': ranks})
    return JsonResponse({'json': [str(x) for x in range(13)]})


class AddProfessionView(UpdateView):
    model = Company
    form_class = WorkProfessionFormset
    template_name = 'company/add_profession.html'

    def get_success_url(self):
        return reverse('company:position_profession', kwargs={'pk': self.kwargs.get('pk')})


class AddPositionView(UpdateView):
    model = Company
    form_class = EmployeePositionFormset
    template_name = 'company/add_position.html'

    def get_success_url(self):
        return reverse('company:position_profession', kwargs={'pk': self.kwargs.get('pk')})


class UpdateCasesView(UpdateView):
    model = Case
    form_class = CaseForm
    template_name = 'company/change_cases.html'

    def get_success_url(self, **kwargs):
        case = Case.objects.get(id=self.kwargs.get('pk'))
        if case.genericinfocompany_set.first():
            return reverse('company:generic_info_detail', kwargs={'pk': self.request.user.genericinfocompany.pk})
        return reverse('company:position_profession', kwargs={'pk': self.request.user.pk})


class StaffCreate(CreateView):
    model = Staff
    fields = ['surname', 'initials', 'employment_date']

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.initials = instance.initials.title()
        instance.company = self.request.user
        instance.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self, **kwargs):
        return reverse('company:staff_detail', kwargs={'pk': self.request.user.pk})


class StaffUpdate(UpdateView):
    model = Staff
    fields = ['surname', 'initials']

    def get_success_url(self, **kwargs):
        return reverse('company:staff_detail', kwargs={'pk': self.request.user.pk})


class StaffSurnameUpdateView(UpdateView):
    model = Company
    form_class = StaffSurnameInFormset
    template_name = 'company/update_staff_fio.html'

    def get_form_kwargs(self):
        kwargs = super(StaffSurnameUpdateView, self).get_form_kwargs()
        kwargs['instance'] = self.request.user
        kwargs['prefix'] = 'staff_surname_formset'
        return kwargs

    def get_success_url(self, **kwargs):
        return reverse('company:staff_detail', kwargs={'pk': self.request.user.pk})


class StaffPosStaffProfUpdateView(FormView):
    form_class = StaffPositionUpdateFormset
    second_form_class = StaffProfessionUpdateFormset
    template_name = 'company/staff_pos_prof_form.html'

    def get(self, request, *args, **kwargs):
        staff = Staff.objects.filter(company=self.request.user)
        kwargs = dict(company=self.request.user)
        queryset_form = StaffPosition.objects.filter(staff__in=staff).select_related('staff', 'employee_position')
        form = self.form_class(queryset=queryset_form, prefix='pos', form_kwargs=kwargs)
        queryset_second_form = StaffProfession.objects.filter(staff__in=staff).select_related('staff', 'work_profession')
        second_form = self.second_form_class(queryset=queryset_second_form, prefix='prof', form_kwargs=kwargs)
        return self.render_to_response(self.get_context_data(form=form, second_form=second_form))

    def post(self, request, *args, **kwargs):
        staff = Staff.objects.filter(company=self.request.user)
        kwargs = dict(company=self.request.user)
        queryset_form = StaffPosition.objects.filter(staff__in=staff).select_related(
            'staff', 'employee_position')
        queryset_second_form = StaffProfession.objects.filter(staff__in=staff).select_related(
            'staff', 'work_profession')
        form = self.form_class(
            self.request.POST, queryset=queryset_form, prefix='pos', form_kwargs=kwargs)
        second_form = self.second_form_class(
            self.request.POST, queryset=queryset_second_form, prefix='prof', form_kwargs=kwargs)
        if form.is_valid() and second_form.is_valid():
            form.save(), second_form.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self, **kwargs):
        return reverse('company:position_profession', args=(self.request.user.pk,))


class StaffDelete(DeleteView):
    model = Staff

    def get_success_url(self, **kwargs):
        return reverse('company:staff_detail', kwargs={'pk': self.request.user.pk})


class ActivityDetail(UpdateView):
    model = Company
    form_class = SectionActivityForm
    template_name = 'company/activity_detail.html'

    def get_context_data(self, **kwargs):
        context = super(ActivityDetail, self).get_context_data(**kwargs)
        sections_url = [chr(x) for x in range(256) if 'A' <= chr(x) <= 'U']
        context['sections'] = KindActivity.objects.filter(section_url__in=sections_url).distinct('section_url')
        context['subsections'] = KindActivity.objects.exclude(subsection_url='').distinct('subsection_url')
        context['kind_activity'] = KindActivity.objects.all().order_by('name_group')
        return context


class GenericInfoDetail(LoginRequiredMixin, DetailView):

    model = GenericInfoCompany
    template_name = 'company/generic_info_detail.html'

    def get_context_data(self, **kwargs):
        context = super(GenericInfoDetail, self).get_context_data(**kwargs)
        context['buildings'] = BUILDINGS
        context['constructions'] = CONSTRUCTIONS
        context['premises'] = PREMISES
        context['equipment'] = EQUIPMENT
        context['means_transport'] = MEANS_TRANSPORT
        context['ien'] = IEN
        context['een'] = EEN
        return context


class GenericInfoUpdate(UpdateView):
    model = GenericInfoCompany
    form_class = GenericInfoCompanyForm
    template_name = 'company/generic_info_update.html'

    def get_context_data(self, **kwargs):
        context = super(GenericInfoUpdate, self).get_context_data(**kwargs)
        formset = TelephoneFormset(instance=self.request.user.genericinfocompany, prefix='telephone_generic')
        context['formset'] = formset
        return context

    def post(self, request, *args, **kwargs):
        formset = TelephoneFormset(self.request.POST, instance=self.request.user.genericinfocompany, prefix='telephone_generic')
        form_class = self.get_form_class()
        self.object = self.get_object()
        form = self.get_form(form_class)
        if form.is_valid() and formset.is_valid():
            return self.form_valid(form, formset)
        else:
            return self.form_invalid(form, formset)

    def form_invalid(self, form, formset=None):
        return self.render_to_response(self.get_context_data(form=form, formset=formset))

    def form_valid(self, form, formset=None):
        form.save()
        formset.save()
        return HttpResponseRedirect(self.get_success_url())


class AllCasesFormView(FormView):
    form_class = AllCasesFormset
    template_name = 'company/all_cases.html'

    def get_form_kwargs(self):
        kwargs = super(AllCasesFormView, self).get_form_kwargs()
        all_staff = AllAboutStaff(self.request.user).exclude_mayor()
        case_id_transcript = all_staff.get_all_cases()
        kwargs['queryset'] = Case.objects.filter(id__in=[x for x in case_id_transcript.keys()]).select_related().order_by('nomn')
        kwargs['form_kwargs'] = dict(case_id_transcript=case_id_transcript)
        return kwargs

    def form_valid(self, form):
        form.save()
        return super(AllCasesFormView, self).form_valid(form)

    def get_success_url(self, **kwargs):
        section = self.kwargs.get('section', None)
        if section == 'work':
            return reverse('work-safety:download_documents')
        elif section == 'fire':
            return reverse('company:question', kwargs={'pk': self.request.user.pk, 'division': 'fire_safety'})
        return reverse('company:all_cases')


class FactorProductionView(FormView):
    form_class = FactorProductionFormset
    template_name = 'company/factors_production.html'

    def get_form_kwargs(self):
        kwargs = super(FactorProductionView, self).get_form_kwargs()
        kwargs['company'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()
        return super(FactorProductionView, self).form_valid(form)

    def get_success_url(self, **kwargs):
        return reverse('company:factors_production')


class StaffUpdateView(FormView):
    form_class = StaffUpdateFormset
    template_name = 'company/staff_update_form.html'

    def get_form_kwargs(self):
        kwargs = super(StaffUpdateView, self).get_form_kwargs()
        kwargs['queryset'] = Staff.objects.filter(company=self.request.user).prefetch_related(
            'employee_positions', 'work_professions'
        )
        return kwargs

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self, **kwargs):
        return reverse('company:staff_detail', args=(self.request.user.pk,))


class InstructionWorkerView(FormView):
    form_class = StaffPositionWorkInstructionFormset
    second_form_class = StaffProfessionWorkInstructionFormset
    template_name = 'company/worker_instructions.html'

    def get(self, request, *args, **kwargs):
        staff = Staff.objects.filter(company=self.request.user)
        kwargs['instructions'] = self.request.user.work_documents.filter(section_document='instruction')
        queryset_form = StaffPosition.objects.filter(staff__in=staff).order_by('employee_position').distinct('employee_position').select_related('employee_position').prefetch_related('work_documents')
        form = self.form_class(queryset=queryset_form, prefix='pos', form_kwargs=kwargs)
        queryset_second_form = StaffProfession.objects.filter(staff__in=staff).order_by('work_profession').distinct('work_profession').select_related('work_profession').prefetch_related('work_documents')
        second_form = self.second_form_class(queryset=queryset_second_form, prefix='prof', form_kwargs=kwargs)
        return self.render_to_response(self.get_context_data(form=form, second_form=second_form))

    def post(self, request, *args, **kwargs):
        staff = Staff.objects.filter(company=self.request.user)
        kwargs['instructions'] = self.request.user.work_documents.filter(section_document='instruction')
        queryset_form = StaffPosition.objects.filter(staff__in=staff).order_by('employee_position').distinct('employee_position').select_related('employee_position').prefetch_related('work_documents')
        form = self.form_class(self.request.POST, queryset=queryset_form, prefix='pos', form_kwargs=kwargs)
        queryset_second_form = StaffProfession.objects.filter(staff__in=staff).order_by('work_profession').distinct('work_profession').select_related('work_profession').prefetch_related('work_documents')
        second_form = self.second_form_class(self.request.POST, queryset=queryset_second_form, prefix='prof', form_kwargs=kwargs)
        if form.is_valid() and second_form.is_valid():
            form.save(), second_form.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            print(form.errors)
            print(second_form.errors)
            return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self, **kwargs):
        return reverse('company:worker_instructions')


class CompanySubdivisionUpdateView(UpdateView):
    model = Company
    form_class = CompanySubdivisionFormset
    template_name = 'company/subdivision_update.html'

    def get_context_data(self, **kwargs):
        context = super(CompanySubdivisionUpdateView, self).get_context_data(**kwargs)
        context['formset'] = context['form']
        return context

    def get_success_url(self):
        return reverse('home')