import xlrd
import re
import datetime
from builtins import ValueError
from company.models import EmployeePosition, WorkProfession, StaffPosition, StaffProfession, GenericInfoCompany, \
    Company, Case, Staff
from django.db.models.signals import pre_save, post_delete, post_save
from django.dispatch import receiver
from work_safety.models import OTDocument, OTDocumentGenericInfo
from fire_safety.models import Subdivision
from django.db.models import Q
from math import fabs
from itertools import chain
import os
import shutil


def delete_gt_one_space(word):
    spaces = re.findall(r'\s{2,}', word)
    for space in spaces:
        word = word.replace(space, '')
    return word


def get_staff_surname_initials(document_xls, company):
    surname_initials = []
    document = xlrd.open_workbook(file_contents=document_xls.read())
    sheet = document.sheet_by_index(0)
    professions, positions = {}, {}
    [professions.update({x.id: x.name_working_career}) for x in WorkProfession.objects.filter(company=None)]
    [positions.update({x.id: x.name_post_office_worker}) for x in EmployeePosition.objects.filter(company=None)]
    results = {}
    subdivisions = company.subdivision_set.all()
    subdivision_id = None
    for row_num in range(sheet.nrows):

        row = sheet.row_values(row_num)
        employment_date = None
        birthday_date = None
        surname, initials, pos_or_prof = '', '', ''
        work, position, = {}, {}
        if row[1].strip() == '' and row[2].strip() == '' and row[3].strip() == '' and subdivisions:
            subdivision = row[0].strip().lower()
            subs = [[s.id, s.title.lower()] for s in subdivisions]
            for sub_id, sub_title in subs:
                if get_distance(sub_title, subdivision) < 2:
                    subdivision_id = sub_id
                    break
            continue
        try:
            if row[0] != '' and isinstance(row[0], str):
                surname = row[0].strip().title()
                surname = delete_gt_one_space(surname)
            if row[1] != '' and isinstance(row[1], str):
                initials = row[1].strip().title()
            if len(surname.split(' ')) == 3 and not initials:
                initials = surname.split(' ')[1:]
                initials = '{}. {}.'.format(initials[0][0], initials[1][0])
                surname = surname.split(' ')[0]
        except IndexError as ind:
            print('IndexError {}'.format(ind))

        try:
            if row[2]:
                if isinstance(row[2], str):
                    employment_date = analyze_date(str(row[2]))
                else:
                    try:
                        date_tuple = xlrd.xldate_as_tuple(sheet.row(row_num)[2].value, document.datemode)
                        year, month, day = date_tuple[0], date_tuple[1], date_tuple[2]
                        employment_date = datetime.date(year, month, day)
                    except TypeError as t_e:
                        print('TypeError {}'.format(t_e))
                    except Exception as e:
                        print('Exception {}'.format(e))
        except IndexError as ind:
            print('IndexError {}'.format(ind))
        try:
            if row[3]:
                if isinstance(row[3], str):
                    word = row[3].strip().lower()
                    if results.get(word):
                        job = results.get(word)
                        work = job[1] if job[0] == 'prof' else {}
                        position = job[1] if job[0] == 'pos' else {}
                    if not work and not position:
                        position = check_in_position(word)
                        if not position:
                            work = check_in_work(word)
                    if not work and not position:
                        position = search_in_positions(word, positions)
                        if not position:
                            work = search_in_professions(word, professions)
                    if not work and not position:
                        try:
                            pos_or_prof = row[4].strip().lower()
                        except Exception:
                            pass
                        if pos_or_prof == 'д':
                            position = create_position(word, company)
                        elif pos_or_prof == 'п':
                            work = create_work(word, company)
                    if work:
                        job = ['prof', work]
                    elif position:
                        job = ['pos', position]
                    else:
                        job = [{}, {}]
                    results.update({word: job})
        except IndexError as ind:
            print('IndexError {}'.format(ind))
        try:
            if row[5]:
                if isinstance(row[5], str):
                    birthday_date = analyze_date(str(row[5]))
                else:
                    try:
                        date_tuple = xlrd.xldate_as_tuple(sheet.row(row_num)[5].value, document.datemode)
                        year, month, day = date_tuple[0], date_tuple[1], date_tuple[2]
                        birthday_date = datetime.date(year, month, day)
                    except TypeError as t_e:
                        print('TypeError {}'.format(t_e))
                    except Exception as e:
                        print('Exception {}'.format(e))
        except IndexError as ind:
            print('IndexError {}'.format(ind))
        if surname or work or position:
            surname_initials.append([surname, initials, employment_date, work, position, subdivision_id, birthday_date])
    return surname_initials


def check_in_position(title_position):
    derivative = ''
    derivative_positions = [derivative[0].replace('-', '').lower() for derivative in StaffPosition.DERIVATIVE]
    rus_letters_in_word = ''.join(re.findall(r'[а-я]+', title_position.lower()))
    digit_in_word = re.findall(r'[0-9]+', title_position.lower())
    for digit in digit_in_word:
        title_position = title_position.replace(digit, '')
    digit_in_word = ''.join(digit_in_word)
    title_position = delete_gt_one_space(title_position).lower().strip()
    position = EmployeePosition.objects.filter(name_post_office_worker__iexact=title_position).first()
    if position:
        position = position if position else ''
        result = {'derivative': derivative, 'position': position, 'category': digit_in_word} if position else {}
        return result
    for derivative_position in derivative_positions:
        if rus_letters_in_word.startswith(derivative_position):
            index_derivative = derivative_positions.index(derivative_position)
            derivative = StaffPosition.DERIVATIVE[index_derivative][0]
            title_position = title_position.replace(derivative.lower(), '').strip()
            break
    position = EmployeePosition.objects.filter(name_post_office_worker__iexact=title_position).first()
    position = position if position else ''
    result = {'derivative': derivative, 'position': position, 'category': digit_in_word} if position else {}
    return result


def check_in_work(title_work):
    digit_in_word = re.findall(r'[0-9]+', title_work.lower())
    for digit in digit_in_word:
        title_work = title_work.replace(digit, '')
    digit_in_word = ''.join(digit_in_word)
    title_work = delete_gt_one_space(title_work).lower().strip()
    work = WorkProfession.objects.filter(name_working_career__iexact=title_work).first()
    result = {'profession': work, 'rank': digit_in_word} if work else {}
    return result


def create_position(title_position, company):
    derivative = ''
    title_position = title_position.lower().strip().capitalize()
    derivative_positions = [derivative[0].replace('-', '').lower() for derivative in StaffPosition.DERIVATIVE]
    rus_letters_in_word = ''.join(re.findall(r'[а-я]+', title_position.lower()))
    for derivative_position in derivative_positions:
        if rus_letters_in_word.startswith(derivative_position):
            index_derivative = derivative_positions.index(derivative_position)
            derivative = StaffPosition.DERIVATIVE[index_derivative][0]
            title_position = title_position.lower().replace(derivative_position, '').strip()
            break
    position = EmployeePosition.objects.create(
        company=company, name_post_office_worker=title_position, categories=['1', '2', '3', '4']
    )
    return {'derivative': derivative, 'position': position, 'category': ''}


def create_work(title_work, company):
    title_work = title_work.lower().strip().capitalize()
    ranks = ['{}'.format(x) for x in range(1, 13)]
    work = WorkProfession.objects.create(company=company, name_working_career=title_work, ranks=ranks)
    return {'profession': work, 'rank': ''}


def get_subdivision(document_xls):
    subdivisions = []
    document = xlrd.open_workbook(file_contents=document_xls.read())
    sheet = document.sheet_by_index(0)
    for rownum in range(sheet.nrows):
        row = sheet.row_values(rownum)
        for subdivision in row:
            if subdivision != '':
                subdivisions.append(subdivision)
    return subdivisions


def analyze_date(date_string):
    result_analyzing = None
    day, month, year = '', '', ''
    date_string = date_string.replace('о', '0').replace('o', '0')
    first_variant = re.findall(r'\d', date_string)
    second_variant = date_string.split('.')
    third_variant = date_string.strip().split(' ')
    if len(first_variant) == 8:
        day, month, year = ''.join(first_variant[:2]), ''.join(first_variant[2:4]), ''.join(first_variant[-4:])
    elif len(second_variant) == 3:
        day, month, year = second_variant[0], second_variant[1], second_variant[2]
    elif len(third_variant) == 3:
        day, month, year = third_variant[0], third_variant[1], third_variant[2]
    if month and day and year:
        try:
            result_analyzing = datetime.date(int(year), int(month), int(day))
        except ValueError as ind:
            print("ValueError error({}):".format(ind))
        except Exception as e:
            print("Exception({}):".format(e))
    return result_analyzing


def get_distance(a, b):
    """Calculates the Levenshtein distance between a and b."""
    n, m = len(a), len(b)
    if n > m:
        # Make sure n <= m, to use O(min(n,m)) space
        a, b = b, a
        n, m = m, n

    current_row = range(n+1) # Keep current and previous row, not entire matrix
    for i in range(1, m+1):
        previous_row, current_row = current_row, [i]+[0]*n
        for j in range(1, n+1):
            add, delete, change = previous_row[j]+1, current_row[j-1]+1, previous_row[j-1]
            if a[j-1] != b[i-1]:
                change += 1
            current_row[j] = min(add, delete, change)

    return current_row[n]


def search_in_professions(analyze_word, professions):
    professions = professions
    find_record = {}
    value_distance = 30
    error_percentage = 0.2
    rank_in_word = ''.join(re.findall(r'[1-8]+', analyze_word.lower()))
    rus_letters_in_word = ''.join(re.findall(r'[а-я]+', analyze_word.lower()))
    for id_prof, profession in professions.items():
        rus_letters_in_prof = ''.join(re.findall(r'[а-я]+', profession.lower()))
        length_word = len(rus_letters_in_prof)
        length_analyze_word = len(rus_letters_in_word)
        if fabs(length_analyze_word - length_word) > 1:
            continue
        distance = get_distance(rus_letters_in_prof, rus_letters_in_word)
        percent = distance/length_word
        if distance < value_distance and percent < error_percentage and length_word >= length_analyze_word:
            value_distance = distance
            find_record.clear()
            find_record.update({'profession': WorkProfession.objects.get(id=int(id_prof)), 'rank': rank_in_word or {}})
    return find_record


def search_in_positions(analyze_word, positions):
    derivative_positions = [derivative[0].replace('-', '').lower() for derivative in StaffPosition.DERIVATIVE]
    positions = positions
    find_record = {}
    value_distance = 30
    error_percentage = 0.2
    derivative = ''
    category_in_word = ''.join(re.findall(r'[1-4]+', analyze_word.lower()))
    rus_letters_in_word = ''.join(re.findall(r'[а-я]+', analyze_word.lower()))
    for derivative_position in derivative_positions:
        if rus_letters_in_word.startswith(derivative_position):
            rus_letters_in_word = rus_letters_in_word.replace(derivative_position, '')
            index_derivative = derivative_positions.index(derivative_position)
            derivative = StaffPosition.DERIVATIVE[index_derivative][0]
            break
    for id_pos, position in positions.items():
        rus_letters_in_pos = ''.join(re.findall(r'[а-я]+', position.lower()))
        length_word = len(rus_letters_in_pos)
        length_analyze_word = len(rus_letters_in_word)
        if fabs(length_analyze_word - length_word) > 1:
            continue
        distance = get_distance(rus_letters_in_pos, rus_letters_in_word)
        percent = distance/length_word
        if distance < value_distance and percent < error_percentage and length_word >= length_analyze_word:
            value_distance = distance
            find_record.clear()
            find_record.update({'derivative': derivative,
                                'position': EmployeePosition.objects.get(id=int(id_pos)),
                                'category': category_in_word or {}})
    return find_record


@receiver(pre_save, sender=StaffPosition)
def add_staff_position(sender, **kwargs):
    from company.views import get_cases
    instance = kwargs.get('instance')
    if not getattr(instance, '_add_staff_position', True):
        return
    company = instance.staff.company
    staff = StateStaffTable()
    staff = staff.get_staff(company)
    staff.state = False
    company_work_doc = company.work_documents.all()
    old_instance = StaffPosition.objects.get(id=instance.pk) if instance.pk else instance

    add_work_documents = instance.employee_position.work_documents.all()
    this_work = instance.employee_position
    this_code = str(this_work.position_code)
    old_work = old_instance.employee_position
    old_code = str(old_work.position_code)
    derivative = instance.derivative_position
    if derivative.lower() in ['заместитель', 'помощник']:
        title = '{} {}'.format(derivative, get_cases(this_work.name_post_office_worker, case='gent'))
    else:
        title = '{} {}'.format(derivative, this_work.name_post_office_worker)
    pos_cases = {}
    [pos_cases.update({case: value.lower()}) for case, value in get_cases(title).items()]

    if this_work != old_work:
        [setattr(instance.case, key, value) for key, value in pos_cases.items()]
        instance.case.checked = False
        delete_work_doc = old_instance.employee_position.work_documents.all()
        staff_positions = StaffPosition.objects.filter(
            staff__in=staff.staff_id).exclude(id=old_instance.pk).select_related('employee_position')
        all_code_without_this = []
        for x in staff_positions:
            all_code_without_this.append(str(x.employee_position.position_code))
        if old_code not in all_code_without_this:
            if add_work_documents:
                for ot_document in add_work_documents.filter(path__icontains=old_code):
                    company.work_documents.remove(ot_document)

        doc = list(OTDocument.objects.filter(employeeposition__in=[x.employee_position for x in staff_positions]))
        [company.work_documents.remove(x) for x in delete_work_doc if x not in doc]
    else:
        instance.case = Case.objects.create(**pos_cases)
    instance.case.save()
    [company.work_documents.add(x) for x in OTDocument.objects.filter(path__icontains=this_code)]
    [company.work_documents.add(x) for x in add_work_documents if x not in company_work_doc]


@receiver(pre_save, sender=StaffProfession)
def add_staff_profession(sender, **kwargs):
    from company.views import get_cases
    instance = kwargs.get('instance')
    if not getattr(instance, '_add_staff_profession', True):
        return
    company = instance.staff.company
    staff = StateStaffTable()
    staff = staff.get_staff(company)
    staff.state = False
    company_work_doc = company.work_documents.all()
    old_instance = StaffProfession.objects.get(pk=instance.pk) if instance.pk else instance
    add_work_documents = instance.work_profession.work_documents.all()
    this_work = instance.work_profession
    this_code = '{}'.format(this_work.profession_code)
    old_work = old_instance.work_profession
    old_code = '{}'.format(old_work.profession_code)
    prof_cases = {}
    [prof_cases.update({case: value.lower()}) for case, value in get_cases(this_work.name_working_career).items()]
    if this_work != old_work:
        [setattr(instance.case, key, value) for key, value in prof_cases.items()]
        instance.case.checked = False
        delete_work_doc = old_instance.work_profession.work_documents.all()
        staff_professions = StaffProfession.objects.filter(
            staff__in=staff.staff_id).exclude(id=old_instance.pk).select_related('work_profession')

        all_code_without_this = []
        for staff_profession in staff_professions:
            all_code_without_this.append('{}'.format(staff_profession.work_profession.profession_code))

        if old_code not in all_code_without_this:
            if add_work_documents.exists():
                for ot_document in add_work_documents.filter(path__icontains=old_code):
                    company.work_documents.remove(ot_document)
        doc = OTDocument.objects.filter(
            workprofession__in=[x.work_profession for x in staff_professions]).distinct()
        [company.work_documents.remove(x) for x in delete_work_doc if x not in doc]
    else:
        instance.case = Case.objects.create(**prof_cases)
    instance.case.save()
    [company.work_documents.add(x) for x in OTDocument.objects.filter(path__icontains=this_code)]
    [company.work_documents.add(x) for x in add_work_documents if x not in company_work_doc]


@receiver(post_delete, sender=StaffPosition)
def delete_staff_position(sender, **kwargs):
    instance = kwargs.get('instance')
    if not instance.pk:
        return
    company = instance.staff.company
    if not company:
        return
    staff = StateStaffTable()
    staff = staff.get_staff(company)
    staff.state = False

    this_code = str(instance.employee_position.position_code)
    work_documents = instance.employee_position.work_documents.all()

    staff_positions = StaffPosition.objects.filter(
        staff__in=staff.staff_id).exclude(pk=instance.pk).select_related('employee_position')
    doc_without_pos = OTDocument.objects.filter(employeeposition__in=[x.employee_position for x in staff_positions])
    all_code_without_this = []
    for x in staff_positions:
        all_code_without_this.append(str(x.employee_position.position_code))
    if this_code not in all_code_without_this:
        for ot_document in OTDocument.objects.filter(path__icontains=this_code):
            company.work_documents.remove(ot_document)

    [company.work_documents.remove(x) for x in work_documents if x not in doc_without_pos]


@receiver(post_delete, sender=StaffProfession)
def delete_staff_profession(sender, **kwargs):
    instance = kwargs.get('instance')
    if not instance.pk:
        return
    company = instance.staff.company
    if not company:
        return
    staff = StateStaffTable()
    staff = staff.get_staff(company)
    staff.state = False

    this_code = str(instance.work_profession.profession_code)
    work_documents = instance.work_profession.work_documents.all()

    staff_professions = StaffProfession.objects.filter(
        staff__in=staff.staff_id).exclude(pk=instance.pk).select_related('work_profession')
    doc_without_pos = OTDocument.objects.filter(workprofession__in=[x.work_profession for x in staff_professions])
    all_code_without_this = []
    for x in staff_professions:

        all_code_without_this.append(str(x.work_profession.profession_code))

    if this_code not in all_code_without_this:
        [company.work_documents.remove(doc) for doc in OTDocument.objects.filter(path__icontains=this_code)]
    [company.work_documents.remove(x) for x in work_documents if x not in doc_without_pos]


@receiver(pre_save, sender=GenericInfoCompany)
def create_cases_company_name(sender, **kwargs):
    instance = kwargs.get('instance')
    word = '{} {}'.format(instance.short_ownership, instance.short_title)
    if not word.strip():
        return
    cases = ['nomn', 'gent', 'datv', 'accs', 'ablt', 'loct']
    if instance.short_title_case:
        if word != instance.short_title_case.nomn:
            for case in cases:
                setattr(instance.short_title_case, case, word)
            instance.short_title_case.checked = False
            instance.short_title_case.save()
    else:
        case_value = {}
        [case_value.update({case: word}) for case in cases]
        instance.short_title_case = Case.objects.create(**case_value)
        instance.save()


@receiver(pre_save, sender=Subdivision)
def add_document_when_subdivision(sender, **kwargs):
    instance = kwargs.get('instance')
    company = instance.company
    documents_for_subdivision = [61]
    documents = [OTDocument.objects.get(document_code=x) for x in documents_for_subdivision]
    [company.work_documents.add(document) for document in documents]
    # delete/add fields
    company.work_field_exclude.remove(OTDocument.objects.get(id=36).fields.get(field_name='F18'))
    company.work_field_exclude.add(OTDocument.objects.get(id=36).fields.get(field_name='F41'))
    company.work_field_exclude.remove(OTDocument.objects.get(document_code=75).fields.get(field_name='F18'))
    company.work_field_exclude.remove(OTDocument.objects.get(document_code=56).fields.get(field_name='F18'))
    company.work_field_exclude.add(OTDocument.objects.get(document_code=56).fields.get(field_name='F4', position_number=2))
    company.work_field_exclude.remove(OTDocument.objects.get(document_code=58).fields.get(field_name='F18'))
    company.work_field_exclude.add(OTDocument.objects.get(document_code=58).fields.get(field_name='F4'))
    company.work_field_exclude.remove(OTDocument.objects.get(document_code=59).fields.get(field_name='F18'))
    company.work_field_exclude.add(OTDocument.objects.get(document_code=59).fields.get(field_name='F4'))
    company.work_field_exclude.remove(OTDocument.objects.get(document_code=60).fields.get(field_name='F18'))
    company.work_field_exclude.add(OTDocument.objects.get(document_code=60).fields.get(field_name='F4'))


@receiver(post_delete, sender=Subdivision)
def delete_document_when_subdivision(sender, **kwargs):
    instance = kwargs.get('instance')
    company = instance.company
    documents_for_subdivision = [61]
    if not company.subdivision_set.all():
        documents = [OTDocument.objects.get(document_code=x) for x in documents_for_subdivision]
        [company.work_documents.remove(document) for document in documents]
    # delete/add fields
        company.work_field_exclude.add(OTDocument.objects.get(id=36).fields.get(field_name='F18'))
        company.work_field_exclude.remove(OTDocument.objects.get(id=36).fields.get(field_name='F41'))
        company.work_field_exclude.add(OTDocument.objects.get(document_code=75).fields.get(field_name='F18'))
        company.work_field_exclude.add(OTDocument.objects.get(document_code=56).fields.get(field_name='F18'))
        company.work_field_exclude.remove(OTDocument.objects.get(document_code=56).fields.get(field_name='F4', position_number=2))
        company.work_field_exclude.add(OTDocument.objects.get(document_code=58).fields.get(field_name='F18'))
        company.work_field_exclude.remove(OTDocument.objects.get(document_code=58).fields.get(field_name='F4'))
        company.work_field_exclude.add(OTDocument.objects.get(document_code=59).fields.get(field_name='F18'))
        company.work_field_exclude.remove(OTDocument.objects.get(document_code=59).fields.get(field_name='F4'))
        company.work_field_exclude.add(OTDocument.objects.get(document_code=60).fields.get(field_name='F18'))
        company.work_field_exclude.remove(OTDocument.objects.get(document_code=60).fields.get(field_name='F4'))


@receiver(pre_save, sender=GenericInfoCompany)
def add_document_when_generic_info(sender, **kwargs):
    instance = kwargs.get('instance')
    document_info = OTDocumentGenericInfo.objects.all()
    attrs = ['buildings', 'constructions', 'premises', 'means_transport', 'external_engineering_networks',
             'internal_engineering_networks', 'equipment']
    add_documents = []
    [add_documents.append(doc) for doc in instance.company.work_documents.all()]
    for data in document_info:
        documents = data.ot_documents.all()
        get_result = False
        for document in documents:
            for attr in attrs:
                condition = getattr(data, attr)
                value = getattr(instance, attr)
                result = list(set(condition) & set(value))
                if result:
                    get_result = True
                    if document not in add_documents:
                        add_documents.append(document)
                    break
            if not get_result:
                if document in add_documents:
                    add_documents.remove(document)
    instance.company.work_documents.set(add_documents)


def _delete_folder(path):
    """ Deletes file from filesystem. """
    if os.path.isdir(path):
        shutil.rmtree(path)


@receiver(post_delete, sender=Company)
def delete_file(sender, instance, *args, **kwargs):
    """ Deletes people_profession files on `post_delete` """
    if instance.people_profession:
        folder_path = os.path.abspath(os.path.abspath(os.path.join(instance.people_profession.path, os.pardir)))
        _delete_folder(folder_path)


@receiver(post_save, sender=Staff)
def create_update_case_surname(sender, instance, *args, **kwargs):
    from company.views import get_cases, CASES
    cases = {}
    staff = StateStaffTable()
    staff = staff.get_staff(instance.company)
    staff.state = False
    if not getattr(instance, '_add_staff', True):
        return
    surname_automatic_cases = get_cases(instance.surname)
    [cases.update({case: surname_automatic_cases.get(case).title()}) for case in CASES]
    if getattr(instance, 'case_id'):
        [setattr(instance.case, key, value.title()) for key, value in cases.items()]
        instance.case.checked = False
        instance.case.save()
    else:
        case = Case.objects.create(**cases)
        instance.case = case
        instance.save()


@receiver(post_delete, sender=Staff)
def delete_case(sender, instance, *args, **kwargs):
    staff = StateStaffTable()
    staff = staff.get_staff(instance.company)
    staff.state = False
    if instance.case_id:
        instance.case.delete()


class StaffInfo:

    def __init__(self, company=None, include_mayor=True):
        if not company:
            raise Exception('Need to specify company')
        self._relevant_state = False
        self.include_mayor = include_mayor
        self.staff_id = []
        self.staff = {}
        self.positions = {}
        self.professions = {}
        self.staff_works = {}
        self.staff_position = {}
        self.staff_profession = {}
        self.mayor_staff_id = []
        self.mayor_staff = {}
        self.mayor_positions = {}
        self.mayor_professions = {}
        self.mayor_staff_position = {}
        self.mayor_staff_profession = {}
        self.company = company
        self._get_all_staff()
        self._get_all_staff_positions()
        self._get_all_staff_professions()
        self._get_positions()
        self._get_professions()
        self.update_staffing_table()

    def get_state(self):
        return self._relevant_state

    def set_state(self, value):
        if value is True or value is False:
            self._relevant_state = value

    state = property(get_state, set_state)

    def get_staff_positions(self):
        return self.staff_position.copy()

    get_staff_positions = property(get_staff_positions)

    def get_staff_professions(self):
        return self.staff_profession.copy()

    get_staff_professions = property(get_staff_professions)

    def get_positions(self):
        return self.positions.copy()

    get_positions = property(get_positions)

    def get_professions(self):
        return self.professions.copy()

    get_professions = property(get_professions)

    def update_staffing_table(self):
        staff = StateStaffTable()
        staff.save_staff(self, self.company)

    def _get_all_staff(self, staff=None):
        if not staff:
            company_mayor = Company.objects.first()
            self.mayor_staff_id = [x[0] for x in Staff.objects.filter(company=company_mayor).values_list('id')]
            staff = Staff.objects.filter(Q(company=self.company) | Q(company=company_mayor)).values(
                'id', 'surname', 'initials', 'case')
        self.staff.clear()
        for st in staff:
            self.staff.update({
                st['id']: {
                    'surname': st['surname'],
                    'initials': st['initials'],
                    'case_id': st['case']
                }
            })
        self.staff_id = [x for x in self.staff.keys()]
        return self.staff

    def _get_all_staff_positions(self):
        staff_position = StaffPosition.objects.filter(staff_id__in=self.staff_id).values(
            'id', 'staff', 'derivative_position', 'category', 'employee_position', 'case'
        )
        self.staff_position.clear()
        for st_pos in staff_position:
            self.staff_position.update({
                st_pos['id']: {
                    'staff_id': st_pos['staff'],
                    'derivative': st_pos['derivative_position'],
                    'category': st_pos['category'],
                    'pos_id': st_pos['employee_position'],
                    'case_id': st_pos['case']
                }
            })
        return self.staff_position

    def _get_all_staff_professions(self):
        staff_profession = StaffProfession.objects.filter(staff_id__in=self.staff_id).values(
            'id', 'staff', 'rank', 'work_profession', 'case'
        )
        self.staff_profession.clear()
        for st_pr in staff_profession:
            self.staff_profession.update({
                st_pr['id']: {
                    'staff_id': st_pr['staff'],
                    'rank': st_pr['rank'],
                    'prof_id': st_pr['work_profession'],
                    'case_id': st_pr['case']
                }
            })
        return self.staff_profession

    def _get_work_professions(self):
        work_prof_id = [x['prof_id'] for x in self.staff_profession.values()]
        professions = WorkProfession.objects.filter(id__in=work_prof_id).values(
            'id', 'name_working_career', 'profession_code',)
        self.professions.clear()
        [self.professions.update({x['id']: x['name_working_career']}) for x in professions]

    def _get_professions(self):
        if self.staff_profession and not self.professions:
            self._get_work_professions()
        return self.professions

    def _get_work_positions(self):
        work_pos_id = [x['pos_id'] for x in self.staff_position.values()]
        positions = EmployeePosition.objects.filter(id__in=work_pos_id).values(
            'id', 'name_post_office_worker', 'position_code')
        self.professions.clear()
        [self.positions.update({x['id']: x['name_post_office_worker']}) for x in positions]

    def _get_positions(self):
        if self.staff_position and not self.positions:
            self._get_work_positions()
        return self.positions

    def get_staff_some_work(self):
        id_staff_some_work = []
        all_id = []
        [all_id.append(x['staff_id']) for x in self.staff_profession.values()]
        [all_id.append(x['staff_id']) for x in self.staff_position.values()]
        for staff_id in all_id.copy():
            all_id.remove(staff_id)
            if staff_id in all_id:
                id_staff_some_work.append(staff_id)
        return id_staff_some_work

    def get_person_works(self, staff_id=None):
        if not (isinstance(staff_id, int) and staff_id in self.staff_id):
            return None
        person_works = {}
        for staff_pos_id, value in self.staff_position.items():
            if staff_id == value['staff_id']:
                if person_works.get(staff_id, None):
                    pos = person_works[staff_id]['pos']
                    pos.append(staff_pos_id)
                else:
                    pos = [staff_pos_id]
                person_works.update({staff_id: {'pos': pos, 'prof': []}})
        for staff_prof_id, value in self.staff_profession.items():
            if staff_id == value['staff_id']:
                if person_works.get(staff_id, None):
                    pos = person_works[staff_id]['pos']
                    prof = person_works[staff_id]['prof']
                    prof.append(staff_prof_id)
                else:
                    pos = []
                    prof = [staff_prof_id]
                person_works.update({staff_id: {'pos': pos, 'prof': prof}})
        result = person_works or {staff_id: {'pos': [], 'prof': []}}
        return result

    def get_staff_works(self):
        if not self.staff_works:
            [self.staff_works.update(self.get_person_works(staff_id)) for staff_id in self.staff_id]
        return self.staff_works

    def get_staff_works_read(self):
        staff_works_read = list()
        works = list()
        for staff, value in self.get_staff_works().items():
            person = '{} {}'.format(self.staff[staff]['surname'], self.staff[staff]['initials'])
            positions, professions = value.get('pos'), value.get('prof')
            for position in positions:
                pos_id = self.staff_position[position]['pos_id']
                name_pos = self._get_positions()[pos_id]
                derivative = self.staff_position[position]['derivative']
                category = self.staff_position[position]['category']
                work = '{} {} {}'.format(derivative, name_pos, category).strip()
                works.append(work)
            for profession in professions:
                prof_id = self.staff_profession[profession]['prof_id']
                name_prof = self._get_professions()[prof_id]
                rank = self.staff_profession[profession]['rank']
                work = '{} {}'.format(name_prof, rank).strip()
                works.append(work)
            staff_works_read.append('{} {}'.format(person, ', '.join(works)))
            works.clear()
        staff_works_read.sort()
        return staff_works_read

    def get_works_read_by_staff(self, staff_id=None):
        if not (isinstance(staff_id, int) and staff_id in self.staff_id):
            return ''
        works = list()
        positions, professions = self.get_staff_works()[staff_id]['pos'], self.get_staff_works()[staff_id]['prof']
        for position in positions:
            pos_id = self.staff_position[position]['pos_id']
            name_pos = self._get_positions()[pos_id]
            derivative = self.staff_position[position]['derivative']
            category = self.staff_position[position]['category']
            work = '{} {} {}'.format(derivative, name_pos, category).strip()
            works.append(work)
        for profession in professions:
            prof_id = self.staff_profession[profession]['prof_id']
            name_prof = self._get_professions()[prof_id]
            rank = self.staff_profession[profession]['rank']
            work = '{} {}'.format(name_prof, rank).strip()
            works.append(work)
        works = ', '.join(works)
        return works

    def get_staff_unique_staff_position(self):
        staff_pos_value = []
        staff_pos_id_unique = []
        for staff_pos_id, value in self.staff_position.items():
            if (value['pos_id'], value['derivative'], ['category']) not in staff_pos_value:
                staff_pos_value.append((value['pos_id'], value['derivative'], ['category']))
                staff_pos_id_unique.append(staff_pos_id)
        return staff_pos_id_unique

    def get_staff_unique_staff_profession(self):
        staff_prof_value = []
        staff_prof_id_unique = []
        for staff_prof_id, value in self.staff_profession.items():
            if (value['prof_id'], value['rank']) not in staff_prof_value:
                staff_prof_value.append((value['prof_id'], value['rank']))
                staff_prof_id_unique.append(staff_prof_id)
        return staff_prof_id_unique

    def get_all_cases(self):
        self._get_professions()
        self._get_positions()
        cases_id = []
        case_id_transcript = {}
        for person in self.staff.values():
            if person.get('surname', None):
                cases_id.append(person.get('case_id', None))
                case_id_transcript.update({
                    person.get('case_id'): '{} {}'.format(person['surname'], person['initials'])
                })
        for staff_pos in self.staff_position.values():
            cases_id.append(staff_pos.get('case_id', None))
            name_position = self.positions.get(staff_pos['pos_id'])
            case_id_transcript.update({
                staff_pos.get('case_id'): '{} {} {}'.format(
                    staff_pos['derivative'], name_position, staff_pos['category']).strip()
            })
        for staff_prof in self.staff_profession.values():
            cases_id.append(staff_prof.get('case_id', None))

            name_profession = self.professions.get(staff_prof['prof_id'])
            case_id_transcript.update({
                staff_prof.get('case_id'): '{} {}'.format(name_profession, staff_prof.get('rank')).strip()
            })
        company = self.company.genericinfocompany
        case_id_transcript.update({
            company.short_title_case_id: '{} {}'.format(company.short_ownership, company.short_title)
        })
        return case_id_transcript

    def exclude_mayor(self):
        if self.include_mayor:
            mayor_staff_id = self.mayor_staff_id
            self.include_mayor = False
            for st in mayor_staff_id:
                positions, professions = [], []
                self.staff_id.remove(st)
                data_staff = self.staff.pop(st, None)
                self.mayor_staff.update({st: data_staff})
                staff_pos = self.staff_position
                staff_prof = self.staff_profession
                for key, value in staff_pos.copy().items():
                    if value['staff_id'] == st:
                        self.mayor_staff_position.update({key: value})
                        positions.append(staff_pos.pop(key))
                for key, value in staff_prof.copy().items():
                    if value['staff_id'] == st:
                        self.mayor_staff_profession.update({key: value})
                        professions.append(staff_pos.pop(key))
                for pos in positions:
                    data = self.positions.pop(pos['pos_id'])
                    self.mayor_positions.update({pos['pos_id']: data})
                for prof in professions:
                    data = self.professions.pop(prof['prof_id'])
                    self.mayor_professions.update({prof['prof_id']: data})
            self.staff_works.clear()
            self.get_staff_works()
        self.update_staffing_table()
        return self

    def add_mayor(self):
        if not self.include_mayor:
            self.include_mayor = True
            self.staff_id += self.mayor_staff_id
            [self.staff.update({key: value}) for key, value in self.mayor_staff.items()]
            [self.positions.update({key: value}) for key, value in self.mayor_positions.items()]
            [self.professions.update({key: value}) for key, value in self.mayor_professions.items()]
            [self.staff_position.update({key: value}) for key, value in self.mayor_staff_position.items()]
            [self.staff_profession.update({key: value}) for key, value in self.mayor_staff_profession.items()]
            self.staff_works.clear()
            self.get_staff_works()
        self.update_staffing_table()
        return self


class StateStaffTable:

    _staffing_table = {}
    instance = None

    def __new__(cls, *args, **kwargs):
        if cls.instance is None:
            return super(StateStaffTable, cls).__new__(cls)
        else:
            return cls.instance

    def get_staff(self, company):
        staff = self._staffing_table.get(company.id, None) or self.__AllAboutStaff(company)
        return staff

    def save_staff(self, staff, company):
        staff.state = True
        self._staffing_table.update({company.id: staff})

    class __AllAboutStaff(StaffInfo):
        pass


class AllAboutStaff:
    company = None

    def __init__(self, company, **kwargs):
        self.company = company
        staff = StateStaffTable()
        staff = staff.get_staff(company)
        if not staff.state:
            staff.__init__(company)
            staff.exclude_mayor()

    def __getattr__(self, name):
        staff = StateStaffTable()
        instance = staff.get_staff(self.company)
        return getattr(instance, name)
