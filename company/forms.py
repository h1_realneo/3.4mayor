# coding: utf8
from django import forms
from django.forms import HiddenInput
from django.forms import ModelForm, inlineformset_factory, formset_factory, modelformset_factory
from django.core.exceptions import ValidationError
from django.contrib.auth import authenticate
from work_safety.models import Siz
from fire_safety.models import Subdivision
from .models import *
import re
import pymorphy2
from pyphrasy.inflect import PhraseInflector
from fire_safety.forms import customize_style
from .widgets import StaffSelect, StaffPositionPersonWorkWidget
from ._utility import AllAboutStaff
from django.db.models import Q

morph = pymorphy2.MorphAnalyzer()
CASES = ['nomn', 'gent', 'datv', 'accs', 'ablt', 'loct']

ALPHABET = 'ёйцукенгшщзхъфывапролджэячсмитьбюЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ .'


def enumerate_choices(choices):
    new_choices = []
    count = 0
    for choice in choices:
        if isinstance(choice[1], str):
            count += 1
            new_choices.append((choice[0], '{}.    {}'.format(count, choice[1])))
        elif isinstance(choice[1], tuple):
            title = choice[0]
            pack_choice = []
            for x in choice[1]:
                count += 1
                pack_choice.append((x[0], '{}.    {}'.format(count, x[1])))
            new_choices.append((title, pack_choice))
    return new_choices


class LoginForm(forms.Form):
    username = forms.CharField(required=True)
    password = forms.CharField(required=True, widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        customize_style(self)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        if username and password:
            user = authenticate(username=username, password=password)
            if not user:
                raise ValidationError('Вы указали неверный логин или пароль', code='invalid')


class CreateLoginPasswordForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = Company
        fields = ['username']

    def __init__(self, *args, **kwargs):
        super(CreateLoginPasswordForm, self).__init__(*args, **kwargs)
        customize_style(self)

fields_exclude = ['document', 'company', 'label', 'number_position', 'position', 'position_system',
                  'position_subdivision', 'position_agreement', 'position_formset']


class PersonAgreementForm(forms.Form):
    agreement = forms.ModelChoiceField(label='согласующие', queryset=StaffPosition.objects.none())

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company', None)
        super(PersonAgreementForm, self).__init__(*args, **kwargs)
        customize_style(self)
        if self.company:
            staff_id = Staff.objects.filter(company=self.company)
            self.fields['agreement'].queryset = StaffPosition.objects.filter(staff__in=staff_id)

PersonAgreementFormset = formset_factory(PersonAgreementForm, extra=1)


class CompanyUpdateForm(ModelForm):
    class Meta:
        model = Company
        fields = ['development_instruction', 'development_provision', 'development_program']
        widgets = {
            'development_instruction': StaffPositionPersonWorkWidget(attrs={'class': 'select_employee'}),
            'development_provision': StaffPositionPersonWorkWidget(attrs={'class': 'select_employee'}),
            'development_program': StaffPositionPersonWorkWidget(attrs={'class': 'select_employee'}),
        }

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company', None)
        super(CompanyUpdateForm, self).__init__(*args, **kwargs)
        customize_style(self)
        about_staff = AllAboutStaff(company).add_mayor()
        queryset = StaffPosition.objects.filter(staff_id__in=about_staff.staff_id)
        staff_position = about_staff.get_staff_positions
        positions = about_staff.get_positions
        staff = about_staff.staff.copy()
        self.fields['development_program'].widget.positions = positions
        self.fields['development_provision'].widget.positions = positions
        self.fields['development_instruction'].widget.positions = positions

        self.fields['development_program'].widget.staff_position = staff_position
        self.fields['development_provision'].widget.staff_position = staff_position
        self.fields['development_instruction'].widget.staff_position = staff_position

        self.fields['development_program'].widget.staff = staff
        self.fields['development_provision'].widget.staff = staff
        self.fields['development_instruction'].widget.staff = staff

        self.fields['development_program'].queryset = queryset
        self.fields['development_provision'].queryset = queryset
        self.fields['development_instruction'].queryset = queryset


class PersonAuthorizedForm(ModelForm):
    class Meta:
        model = PersonAuthorizedCompany
        fields = ['public_inspector', 'chairperson', 'protocol_number']
        labels = {
            'public_inspector': 'общественный инспектор',
            'chairperson': 'председатель ППО',
            'protocol_number': 'номер протокола',
        }

    def __init__(self, *args, **kwargs):
        super(PersonAuthorizedForm, self).__init__(*args, **kwargs)
        customize_style(self)


class CompanyUpdateLeadersForm(ModelForm):
    class Meta:
        model = Company
        fields = ['address', 'boss', 'person_authorized', 'safety_health']
        widgets = {
            'boss': StaffPositionPersonWorkWidget(attrs={'class': 'select_employee'}),
            'person_authorized': StaffSelect(attrs={'class': 'select_employee'}),
            'safety_health': StaffPositionPersonWorkWidget(attrs={'class': 'select_employee'}),
        }
        labels = {
            'address': 'Адрес для приказов(точно так же будет в приказах)'
        }

    def __init__(self, *args, **kwargs):
        about_staff = kwargs.pop('about_staff', None)
        super(CompanyUpdateLeadersForm, self).__init__(*args, **kwargs)
        about_staff.add_mayor()

        staff_position_mayor = about_staff.get_staff_positions
        positions_mayor = about_staff.get_positions
        self.fields['safety_health'].widget.staff_position = staff_position_mayor
        self.fields['safety_health'].widget.positions = positions_mayor
        self.fields['safety_health'].widget.staff = about_staff.staff.copy()
        self.fields['safety_health'].queryset = StaffPosition.objects.filter(staff_id__in=about_staff.staff_id)

        about_staff.exclude_mayor()

        staff_position = about_staff.get_staff_positions
        positions = about_staff.get_positions
        self.fields['boss'].widget.staff_position = staff_position
        self.fields['boss'].widget.positions = positions
        self.fields['boss'].widget.staff = about_staff.staff.copy()
        self.fields['boss'].queryset = StaffPosition.objects.filter(staff_id__in=about_staff.staff_id)

        staff_work = {}
        [staff_work.update({x: about_staff.get_works_read_by_staff(x)}) for x in about_staff.staff_id]
        self.fields['person_authorized'].widget.staff_work = staff_work
        self.fields['person_authorized'].queryset = Staff.objects.filter(id__in=about_staff.staff_id)

        customize_style(self)


class CompanyUpdateEKCDaEKTCForm(ModelForm):
    class Meta:
        model = Company
        fields = ['positions_ekcd', 'professions_etkc']
        widgets = {
            'positions_ekcd': forms.SelectMultiple(attrs={'class': 'bootstrap-select'}),
            'professions_etkc': forms.SelectMultiple(attrs={'class': 'bootstrap-select'})
        }

    def __init__(self, *args, **kwargs):
        super(CompanyUpdateEKCDaEKTCForm, self).__init__(*args, **kwargs)
        self.fields['professions_etkc'].queryset = ProfessionETKC.objects.all().exclude(code__in=[1])
        self.fields['positions_ekcd'].queryset = PositionEKCD.objects.all().exclude(code__in=[1])
        customize_style(self)


class StaffForm(ModelForm):
    subdivision = forms.FileField(label='Подразделения, в один столбец', required=False)

    class Meta:
        model = Company
        fields = ['people_profession']
        labels = {
            'people_profession': 'Штатное расписание',
        }
        error_messages = {
            'people_profession': {
                'required': "Необходимо указать документ со штатным расписание",
            }
        }


class AddStaffForm(ModelForm):
    class Meta:
        model = Staff
        exclude = ['company']

        widgets = {
            'surname': forms.TextInput(attrs={'class': 'form-control'}),
            'profession': forms.TextInput(attrs={'class': 'form-control'}),
            'sex': forms.Select(attrs={'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company', None)
        super(AddStaffForm, self).__init__(*args, **kwargs)

    def clean_surname(self):
        surname = self.cleaned_data.get('surname')
        for letter in surname:
            if letter not in ALPHABET:
                raise ValidationError("Имя должно состоять из русских букв")
        if surname != '':
            name_patronymic = re.findall(r'\w{2,}[ ]\w[.]\w[.]', surname)
            if not name_patronymic:
                raise ValidationError("Правильно введите данные, например  Иванов И.И.")
            fio = name_patronymic[0].split(' ')
            surname = fio[0].title()
            person = '{} {}'.format(surname, fio[1].upper())
            self.cleaned_data['name'] = person
            return person
        return surname


class CompanyQuestionAnswerForm(ModelForm):
    class Meta:
        model = CompanyQuestionAnswer
        fields = ['answer']

    def __init__(self, *args, **kwargs):
        choices = kwargs.pop('choices')
        super(CompanyQuestionAnswerForm, self).__init__(*args, **kwargs)
        multiple, choice, help_text = '', '', ''
        for data in choices:
            if data[0] == self.instance.question_id:
                choice, multiple, help_text = data[1], data[2], data[3]
                break
        if multiple:
            self.fields['answer'].widget = forms.SelectMultiple()
            self.fields['answer'].widget.attrs = {
                'class': 'selectpicker_question form-control input-sm', 'multiple': ''}
            choices = getattr(CompanyQuestionAnswer, choice)
            choices = enumerate_choices(choices)
        else:
            self.fields['answer'].widget = forms.Select()
            self.fields['answer'].widget.attrs = {'class': 'selectpicker_question form-control input-sm'}
            choices = getattr(CompanyQuestionAnswer, choice)
        self.fields['answer'].widget.choices = choices
        self.fields['answer'].help_text = help_text

AnswerFormset = modelformset_factory(CompanyQuestionAnswer, form=CompanyQuestionAnswerForm, extra=0, can_delete=False)


class StaffChangeForm(forms.ModelForm):
    class Meta:
        model = Staff
        fields = ['surname', 'initials']

StaffFormset = inlineformset_factory(Company, Staff, form=StaffChangeForm, extra=1, can_delete=True)


class StaffPositionForm(ModelForm):
    position_visible_in_form = forms.CharField(
        label='профессия', required=False, widget=forms.TextInput(attrs={'class': 'autocomplete-position'}))

    class Meta:
        model = StaffPosition
        fields = ['employee_position', 'derivative_position', 'category', ]
        widgets = {
            'category': forms.Select(attrs={'class': 'selectpicker-category'}),
            'employee_position': forms.HiddenInput(),
            }

    def __init__(self, *args, **kwargs):
        self.person = kwargs.pop('person', None)
        self.company = kwargs.pop('company', None)
        super(StaffPositionForm, self).__init__(*args, **kwargs)
        if self.initial.get('employee_position'):
            position_id = self.initial.get('employee_position')
            self.initial.update(
                {'position_visible_in_form': EmployeePosition.objects.get(id=position_id).name_post_office_worker}
            )
        customize_style(self)

    def clean(self):
        category = self.cleaned_data.get('category')
        employee_position = self.cleaned_data.get('employee_position')
        if employee_position:
            categories = employee_position.categories + ['']
            if category not in categories and (category and not categories or categories and not category):
                msg = "Неправильная категория для {}".format(employee_position)
                self.add_error('category', msg)

StaffPositionFormset = inlineformset_factory(Staff, StaffPosition, form=StaffPositionForm, extra=1)


class StaffProfessionForm(ModelForm):
    profession_visible_in_form = forms.CharField(
        label='профессия', required=False, widget=forms.TextInput(attrs={'class': 'autocomplete-profession'}),)

    class Meta:
        model = StaffProfession
        fields = ['work_profession', 'rank']
        widgets = {
            'rank': forms.Select(attrs={'class': 'selectpicker-rank'}),
            'work_profession': forms.HiddenInput(),
            }

    def __init__(self, *args, **kwargs):
        self.person = kwargs.pop('person', None)
        self.company = kwargs.pop('company', None)
        super(StaffProfessionForm, self).__init__(*args, **kwargs)
        work_profession_id = self.initial.get('work_profession')
        if work_profession_id:
            self.initial.update(
                {'profession_visible_in_form': WorkProfession.objects.get(id=work_profession_id).name_working_career}
            )
        customize_style(self)

    def clean(self):
        rank = self.cleaned_data.get('rank')
        work_profession = self.cleaned_data.get('work_profession')
        if work_profession:
            ranks = work_profession.ranks + ['']
            if rank not in ranks and (ranks and not rank or rank and not ranks):
                msg = "Неправильный разряд для {}".format(work_profession)
                self.add_error('rank', msg)


StaffProfessionFormset = inlineformset_factory(Staff, StaffProfession, form=StaffProfessionForm, extra=1)


class AddProfessionForm(ModelForm):
    class Meta:
        model = WorkProfession
        fields = ['profession_code', 'name_working_career', 'ranks']
        field_classes = {
            'profession_code': forms.ChoiceField
        }
        widgets = {
            'ranks': forms.SelectMultiple(attrs={'class': 'selectpicker'}),
        }

    def __init__(self, *args, **kwargs):
        self.person = kwargs.pop('person', None)
        super(AddProfessionForm, self).__init__(*args, **kwargs)
        professions = WorkProfession.objects.filter(profession_code__isnull=False).order_by('name_working_career')
        choices = [(x.profession_code, x.name_working_career) for x in professions]
        self.fields['profession_code'].choices = [(None, '-------')] + choices
        customize_style(self)

    def clean_profession_code(self):
        data = self.cleaned_data['profession_code']
        if not data:
            return None
        return data

    def save(self, commit=True):
        instance = super(AddProfessionForm, self).save(commit=True)
        Siz.objects.filter(profession=instance).delete()
        if instance.profession_code:
            profession = WorkProfession.objects.filter(company=None).filter(
                profession_code=instance.profession_code).first()
            for siz in profession.sizs.filter(add_by_company=None):
                siz.pk = None
                siz.profession = instance
                siz.save()
        return instance

WorkProfessionFormset = inlineformset_factory(Company, WorkProfession, form=AddProfessionForm, extra=1)


class AddPositionForm(ModelForm):
    categories = forms.MultipleChoiceField(
        label='возможные варианты категорий для должности', required=False, choices=((x, x) for x in range(1, 5)),
        widget=forms.SelectMultiple(attrs={'class': 'selectpicker'}),
        )

    class Meta:
        model = EmployeePosition
        fields = ['position_code', 'name_post_office_worker', 'categories']
        field_classes = {
            'position_code': forms.ChoiceField
        }

    def __init__(self, *args, **kwargs):
        self.person = kwargs.pop('person', None)
        super(AddPositionForm, self).__init__(*args, **kwargs)
        positions = EmployeePosition.objects.filter(position_code__isnull=False).order_by('name_post_office_worker')
        choices = [(x.position_code, x.name_post_office_worker) for x in positions]
        self.fields['position_code'].choices = [(None, '-------')] + choices
        customize_style(self)

    def clean_categories(self):
        categories = self.cleaned_data['categories']
        categories = ','.join(self.cleaned_data['categories']) if categories else ''
        return categories

    def clean_position_code(self):
        data = self.cleaned_data['position_code']
        if not data:
            return None
        return data

    def save(self, commit=True):
        instance = super(AddPositionForm, self).save(commit=True)
        Siz.objects.filter(position=instance).delete()
        if instance.position_code:
            position = EmployeePosition.objects.filter(Q(company=None) & Q(position_code=instance.position_code)).first()
            for siz in position.sizs.filter(add_by_company=None):
                siz.pk = None
                siz.position = instance
                siz.save()
        return instance

EmployeePositionFormset = inlineformset_factory(Company, EmployeePosition, form=AddPositionForm, extra=1)


def case_clean(x):
    cases = x.get('program_generate_cases')
    nomn, gent, datv, accs, ablt, loct = x.get('nomn'), x.get('gent'), x.get('datv'), x.get('accs'), x.get('ablt'), x.get('loct')
    if cases or (nomn and gent and datv and accs and ablt and loct):
        if not cases and not (nomn and gent and datv and accs and ablt and loct):
            raise forms.ValidationError('Вы не выбрали вариант или не заполнили какой то из падежей')
        if cases and (nomn or gent or datv or accs or ablt or loct):
            raise forms.ValidationError('Когда вы выбираете наш вариант все поля с падежами должны быть пустыми')
    else:
        raise forms.ValidationError('выберете какой-нибудь вариант или заполните свой')


class AddStaffCasesForm(ModelForm):
    program_generate_cases = forms.ChoiceField(
        label='Выберите подходящий вариант', widget=forms.RadioSelect, required=False
    )

    class Meta:
        model = Case
        fields = ['program_generate_cases', 'nomn', 'gent', 'datv', 'accs', 'ablt', 'loct']
        widgets = {
            'nomn': forms.TextInput(attrs={'class': 'form-control input-sm'}),
        }

    def __init__(self, *args, **kwargs):
        super(AddStaffCasesForm, self).__init__(*args, **kwargs)
        variants = morph.parse(self.instance.surname)
        all_variant = []
        choices = [['', 'Не один из вариантов не подходит']]
        for i, variant in enumerate(variants, start=0):
            if variant.inflect({'nomn'}):
                cases_word = ' '.join([variant.inflect({case}).word.upper() for case in CASES])
                if cases_word not in all_variant:
                    all_variant.append(cases_word)
                    choices.append([i, cases_word])
        self.fields['program_generate_cases'].choices = choices

    def clean(self):
        x = super(AddStaffCasesForm, self).clean()
        case_clean(x)


class ProfessionAddCasesForm(forms.ModelForm):
    program_generate_cases = forms.ChoiceField(
        label='Выберите подходящий вариант', widget=forms.RadioSelect, required=False
    )

    class Meta:
        model = Case
        fields = ['program_generate_cases', 'nomn', 'gent', 'datv', 'accs', 'ablt', 'loct']

    def __init__(self, *args, **kwargs):
        super(ProfessionAddCasesForm, self).__init__(*args, **kwargs)
        profession = self.instance.work_profession.name_working_career
        inflector = PhraseInflector(morph)
        choices = [['', 'Не один из вариантов не подходит']]
        if profession:
            cases_profession = '; '.join([inflector.inflect(profession, case) for case in CASES])
            choices.append([1, cases_profession])
        self.fields['program_generate_cases'].choices = choices

    def clean(self):
        x = super(ProfessionAddCasesForm, self).clean()
        case_clean(x)


class PositionAddCasesForm(forms.ModelForm):
    program_generate_cases = forms.ChoiceField(
        label='Выберите подходящий вариант', widget=forms.RadioSelect, required=False
    )

    class Meta:
        model = Case
        fields = ['program_generate_cases', 'nomn', 'gent', 'datv', 'accs', 'ablt', 'loct']

    def __init__(self, *args, **kwargs):
        super(PositionAddCasesForm, self).__init__(*args, **kwargs)
        position = self.instance.employee_position.name_post_office_worker
        derivative_position = self.instance.derivative_position
        choices = [['', 'Не один из вариантов не подходит']]
        inflector = PhraseInflector(morph)
        if position:
            if derivative_position == 'Заместитель' or derivative_position == 'Помощник':
                position = '{} {}'.format(derivative_position, inflector.inflect(position, 'gent'))
            cases_profession = '; '.join([inflector.inflect(position, case) for case in CASES])
            choices.append([1, cases_profession])
        self.fields['program_generate_cases'].choices = choices

    def clean(self):
        x = super(PositionAddCasesForm, self).clean()
        case_clean(x)


class CaseForm(forms.ModelForm):
    class Meta:
        model = Case
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(CaseForm, self).__init__(*args, **kwargs)
        customize_style(self)


class SectionActivityForm(forms.ModelForm):
    class Meta:
        model = Company
        fields = ['kind_activities']
        widgets = {
            'kind_activities': forms.SelectMultiple(attrs={'class': 'select-kind-activity'}),
            }

    def __init__(self,  *args, **kwargs):
        super(SectionActivityForm, self).__init__(*args, **kwargs)
        customize_style(self)


class GenericInfoCompanyForm(forms.ModelForm):
    class Meta:
        model = GenericInfoCompany

        fields = ['ownership', 'full_title', 'short_title', 'place_registrations', 'address', 'date_registration',
                  'ynp', 'okpo', 'bank_details', 'bank_details', 'mailing_address', 'fax', 'mail', 'buildings',
                  'constructions', 'premises', 'means_transport', 'external_engineering_networks', 'short_ownership',
                  'internal_engineering_networks', 'equipment', 'characteristic', 'subordination']

        widgets = {
            'date_registration': forms.DateInput(attrs={'class': 'datepicker_generic_info'}),
            'place_registrations': forms.Select(attrs={'class': 'select-generic-form'}),
            'buildings': forms.SelectMultiple(attrs={'class': 'select-generic-form'}),
            'constructions': forms.SelectMultiple(attrs={'class': 'select-generic-form'}),
            'premises': forms.SelectMultiple(attrs={'class': 'select-generic-form'}),
            'means_transport': forms.SelectMultiple(attrs={'class': 'select-generic-form'}),
            'external_engineering_networks': forms.SelectMultiple(attrs={'class': 'select-generic-form'}),
            'internal_engineering_networks': forms.SelectMultiple(attrs={'class': 'select-generic-form'}),
            'equipment': forms.SelectMultiple(attrs={'class': 'select-generic-form'}),
        }

    def __init__(self,  *args, **kwargs):
        super(GenericInfoCompanyForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            if hasattr(self.fields[field], 'choices'):
                choices = self.fields[field].choices
                self.fields[field].choices = enumerate_choices(choices)
        customize_style(self)


class TelephoneForm(forms.ModelForm):
    class Meta:
        model = TelephoneNumber
        fields = ['telephone']
        widgets = {
            'telephone': forms.TextInput(attrs={'class': 'form-control'}),
        }

TelephoneFormset = inlineformset_factory(GenericInfoCompany, TelephoneNumber, form=TelephoneForm,
                                         fields=('telephone',), can_delete=True, extra=1)


class BaseFormSet(forms.BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        super(BaseFormSet, self).__init__(*args, **kwargs)
        self.extra = 0 if self.queryset else 1
        self.queryset = self.queryset.order_by('surname')


class StaffSurnameForm(forms.ModelForm):
    employment_date = forms.DateField(
        label='Дата приема на работу',
        required=False,
        input_formats=['%d-%m-%y', '%Y-%m-%d', '%m/%d/%Y', '%m%d/%y', ],
        widget=forms.DateInput(attrs={'class': 'form-control date_recruitment'}, format='%d-%m-%y')
    )

    class Meta:
        model = Staff
        fields = ['surname', 'initials', 'employment_date']
        widgets = {
            'surname': forms.TextInput(attrs={'class': 'form-control'}),
            'initials': forms.TextInput(attrs={'class': 'form-control'}),
        }

StaffSurnameInFormset = inlineformset_factory(
    Company, Staff, form=StaffSurnameForm, formset=BaseFormSet,  extra=0, can_delete=True)


class StaffPositionUpdateForm(forms.ModelForm):
    date_recruitment = forms.DateField(
        required=False,
        input_formats=['%d-%m-%y', '%Y-%m-%d', '%m/%d/%Y', '%m%d/%y', ],
        widget=forms.DateInput(attrs={'class': 'date_recruitment', 'size': 4}, format='%d-%m-%y')
    )
    date_issue_medical_certificate = forms.DateField(
        required=False,
        input_formats=['%d-%m-%y', '%Y-%m-%d', '%m/%d/%Y', '%m%d/%y', ],
        widget=forms.DateInput(attrs={'class': 'date_recruitment', 'size': 4}, format='%d-%m-%y')
    )
    date_issue_certificate_prof_development = forms.DateField(
        required=False,
        input_formats=['%d-%m-%y', '%Y-%m-%d', '%m/%d/%Y', '%m%d/%y', ],
        widget=forms.DateInput(attrs={'class': 'date_recruitment', 'size': 4}, format='%d-%m-%y')
    )

    class Meta:
        model = StaffPosition
        fields = ['subdivision', 'date_recruitment', 'frequency_instruction_three_month', 'medical_certificate_number',
                  'date_issue_medical_certificate', 'number_certificate_prof_development',
                  'date_issue_certificate_prof_development', 'work_experience']
        widgets = {
            'work_experience': forms.TextInput(attrs={'size': 3}),
            'medical_certificate_number': forms.TextInput(attrs={'size': 5}),
            'number_certificate_prof_development': forms.TextInput(attrs={'size': 7}),
        }

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company', None)
        super(StaffPositionUpdateForm, self).__init__(*args, **kwargs)
        self.fields['subdivision'].queryset = company.subdivision_set.all()

    def save(self, commit=True):
        staff_position = super(StaffPositionUpdateForm, self).save(commit=False)
        staff_position._add_staff_position = False
        staff_position.save()
        return staff_position

StaffPositionUpdateFormset = modelformset_factory(StaffPosition, form=StaffPositionUpdateForm, extra=0)


class StaffProfessionUpdateForm(forms.ModelForm):
    date_recruitment = forms.DateField(
        required=False,
        input_formats=['%d-%m-%y', '%Y-%m-%d', '%m/%d/%Y', '%m%d/%y', ],
        widget=forms.DateInput(attrs={'class': 'date_recruitment', 'size': 4}, format='%d-%m-%y')
    )
    date_issue_medical_certificate = forms.DateField(
        required=False,
        input_formats=['%d-%m-%y', '%Y-%m-%d', '%m/%d/%Y', '%m%d/%y', ],
        widget=forms.DateInput(attrs={'class': 'date_recruitment', 'size': 4}, format='%d-%m-%y')
    )
    date_issue_certificate_prof_development = forms.DateField(
        required=False,
        input_formats=['%d-%m-%y', '%Y-%m-%d', '%m/%d/%Y', '%m%d/%y', ],
        widget=forms.DateInput(attrs={'class': 'date_recruitment', 'size': 4}, format='%d-%m-%y')
    )

    class Meta:
        model = StaffProfession
        fields = ['subdivision', 'date_recruitment', 'frequency_instruction_three_month', 'medical_certificate_number',
                  'date_issue_medical_certificate', 'number_certificate_prof_development',
                  'date_issue_certificate_prof_development', 'work_experience']
        widgets = {
            'work_experience': forms.TextInput(attrs={'size': 3}),
            'medical_certificate_number': forms.TextInput(attrs={'size': 5}),
            'number_certificate_prof_development': forms.TextInput(attrs={'size': 7}),
        }

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company', None)
        super(StaffProfessionUpdateForm, self).__init__(*args, **kwargs)
        self.fields['subdivision'].queryset = company.subdivision_set.all()

    def save(self, commit=True):
        staff_profession = super(StaffProfessionUpdateForm, self).save(commit=False)
        staff_profession._add_staff_profession = False
        staff_profession.save()
        return staff_profession

StaffProfessionUpdateFormset = modelformset_factory(StaffProfession, form=StaffPositionUpdateForm, extra=0)


class AllCasesForm(forms.ModelForm):
    nomn = forms.CharField(max_length=400, widget=HiddenInput())

    class Meta:
        model = Case
        fields = ('nomn', 'gent', 'datv', 'accs', 'ablt', 'loct', 'checked')
        widgets = {
            'gent': HiddenInput,
            'datv': HiddenInput,
            'accs': HiddenInput,
            'ablt': HiddenInput,
            'loct': HiddenInput
        }

    def __init__(self, *args, **kwargs):
        case_id_transcript = kwargs.pop('case_id_transcript', None)
        instance = kwargs.get('instance')
        id_instance = instance.id
        super(AllCasesForm, self).__init__(*args, **kwargs)
        self.fields['nomn'].help_text = case_id_transcript.get(id_instance, '')
        customize_style(self)

    def clean_checked(self):
        data = self.cleaned_data['checked']
        if not data:
            raise forms.ValidationError("Не проверены падежи")
        return data

AllCasesFormset = modelformset_factory(Case, form=AllCasesForm, extra=0, can_delete=False)


def puck_factors_first_attachment():
    factors = FactorsProductionEnvironment.objects.filter(number_attachment='1')
    factors_with_rating = [f for f in factors if f.rating]
    factors_with_rating.sort(key=lambda f: '{}{}'.format(f.paragraph_attachment, f.rating))
    factors_without_rating = [f for f in factors if not f.rating]
    factors_without_rating.sort(key=lambda f: f.paragraph_attachment)
    factors_without_rating = [
        (f.id, '{} {}'.format(f.paragraph_attachment, f.factor)) for f in factors_without_rating
    ]
    paragraphs = []
    [paragraphs.append(f.paragraph_attachment) for f in factors_with_rating if f.paragraph_attachment not in paragraphs]
    factors_with_rating_prepared = []
    for paragraph in paragraphs:
        factors = [f for f in factors_with_rating if f.paragraph_attachment == paragraph]
        first_f = factors[0]
        factors = [(f.id, '{} класс'.format(f.rating)) for f in factors[1:]]
        factors.insert(
            0, (first_f.id, '{} класс {} {}'.format(first_f.rating, first_f.paragraph_attachment, first_f.factor))
        )
        factors_with_rating_prepared.append(('', factors))
    return factors_without_rating + factors_with_rating_prepared


class WorkFactorProductionForm(forms.Form):
    attachment1 = forms.MultipleChoiceField(
        choices=puck_factors_first_attachment(), required=False,
        widget=forms.SelectMultiple(attrs={'class': 'attachment'}))
    attachment2 = forms.MultipleChoiceField(
        choices=((str(x.id), x)for x in FactorsProductionEnvironment.objects.filter(number_attachment='2')),
        widget=forms.SelectMultiple(attrs={'class': 'attachment'}), required=False)
    attachment3 = forms.MultipleChoiceField(
        choices=((str(x.id), x)for x in FactorsProductionEnvironment.objects.filter(number_attachment='3')),
        widget=forms.SelectMultiple(attrs={'class': 'attachment'}), required=False)

    def __init__(self, *args, **kwargs):
        super(WorkFactorProductionForm, self).__init__(*args, **kwargs)
        customize_style(self)


class BaseWorkFactorProdFormset(forms.BaseFormSet):

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company', None)
        super(BaseWorkFactorProdFormset, self).__init__(*args, **kwargs)
        initials = []
        positions = StaffPosition.objects.filter(staff__in=company.staff.all()).prefetch_related(
            'factors_productions').select_related('employee_position', 'staff')
        professions = StaffProfession.objects.filter(staff__in=company.staff.all()).prefetch_related(
            'factors_productions').select_related('work_profession', 'staff')
        positions, professions = list(positions), list(professions)
        positions.sort(
            key=lambda p: '{} {} {}'.format(
                p.derivative_position,
                p.employee_position.id,
                ''.join(sorted([str(f.id) for f in p.factors_productions.all()]))
            )
        )
        professions.sort(
            key=lambda p: '{} {}'.format(
                p.work_profession,
                ''.join(sorted([str(f.id) for f in p.factors_productions.all()]))
            )
        )
        label_factors_positions, label_factors_professions = {}, {}
        for pos in positions:
            label_factors = (
                pos.derivative_position, pos.employee_position.id, frozenset(pos.factors_productions.all())
            )
            list_positions = label_factors_positions.get(label_factors, [])
            list_positions.append(pos)
            label_factors_positions.update({label_factors: list_positions})
        for prof in professions:
            label_factors = (prof.work_profession.id, frozenset(prof.factors_productions.all()))
            list_professions = label_factors_professions.get(label_factors, [])
            list_professions.append(prof)
            label_factors_professions.update({label_factors: list_professions})
        for label_factors, list_positions in label_factors_positions.items():
            position = list_positions[0]
            label = '{} {}'.format(position.derivative_position, position.employee_position).strip().capitalize()
            factors = label_factors[-1]
            initials.append({
                'label': label,
                'staff': ['{} {}'.format(p.staff.surname, p.staff.initials) for p in list_positions],
                'positions': list_positions,
                'attachment1': [f.id for f in factors if f.number_attachment == '1'],
                'attachment2': [f.id for f in factors if f.number_attachment == '2'],
                'attachment3': [f.id for f in factors if f.number_attachment == '3'],
            })
            initials.sort(key=lambda initial: initial['label'])
        self.initial = initials.copy()
        initials.clear()
        for label_factors, list_professions in label_factors_professions.items():
            profession = list_professions[0]
            label = '{}'.format(profession.work_profession).strip().capitalize()
            factors = label_factors[-1]
            initials.append({
                'label': label,
                'staff': ['{} {}'.format(p.staff.surname, p.staff.initials) for p in list_professions],
                'professions': list_professions,
                'attachment1': [f.id for f in factors if f.number_attachment == '1'],
                'attachment2': [f.id for f in factors if f.number_attachment == '2'],
                'attachment3': [f.id for f in factors if f.number_attachment == '3'],
            })
            initials.sort(key=lambda initial: initial['label'])
        self.initial += initials

    def save(self, *args, **kwargs):
        model_staff_position_factors = StaffPosition.factors_productions.through
        model_staff_profession_factors = StaffProfession.factors_productions.through
        delete_position_relate, delete_profession_relate = [], []
        add_position_factors, add_profession_factors = [], []
        for num_form, form in enumerate(self.forms):
            attachment1 = form.cleaned_data['attachment1']
            attachment2 = form.cleaned_data['attachment2']
            attachment3 = form.cleaned_data['attachment3']
            attachment = frozenset([int(a) for a in attachment1 + attachment2 + attachment3])
            initial_data = self.initial[num_form]
            positions, professions = initial_data.get('positions', []), initial_data.get('professions', [])
            in_a1, in_a2, in_a3 = initial_data['attachment1'], initial_data['attachment2'], initial_data['attachment3']
            initial_attachment = frozenset(in_a1 + in_a2 + in_a3)
            if initial_attachment != attachment and positions:
                [delete_position_relate.append(p.id) for p in positions]
                add_position_factors.append(
                    [[a for a in attachment], [p.id for p in positions]]
                )
            elif initial_attachment != attachment and professions:
                [delete_profession_relate.append(p.id) for p in professions]
                add_profession_factors.append(
                    [[a for a in attachment], [p.id for p in professions]]
                )
        if delete_position_relate:
            model_staff_position_factors.objects.filter(staffposition_id__in=delete_position_relate).delete()
            model_staff_position_factors.objects.bulk_create(
                [model_staff_position_factors(factorsproductionenvironment_id=f, staffposition_id=p)
                 for factors, positions in add_position_factors for p in positions for f in factors]
            )
        if delete_profession_relate:
            model_staff_profession_factors.objects.filter(staffprofession_id__in=delete_profession_relate).delete()
            model_staff_profession_factors.objects.bulk_create(
                [model_staff_profession_factors(factorsproductionenvironment_id=f, staffprofession_id=p)
                 for factors, professions in add_profession_factors for p in professions for f in factors]
            )


FactorProductionFormset = formset_factory(WorkFactorProductionForm, formset=BaseWorkFactorProdFormset, extra=0)


class StaffUpdateForm(forms.ModelForm):
    date_birthday = forms.DateField(
        required=False,
        input_formats=['%d-%m-%y', '%Y-%m-%d', '%m/%d/%Y', '%m%d/%y', ],
        widget=forms.DateInput(attrs={'class': 'date_recruitment', 'size': 4}, format='%d-%m-%y')
    )

    class Meta:
        model = Staff
        fields = ['sex', 'date_birthday', 'place_residence']

    def save(self, commit=False):
        self.instance._add_staff = False
        instance = super(StaffUpdateForm, self).save(commit=True)
        return instance

StaffUpdateFormset = modelformset_factory(Staff, form=StaffUpdateForm, extra=0)


class StaffPositionWorkInstructionForm(forms.ModelForm):
    class Meta:
        model = StaffPosition
        fields = ('work_documents',)
        widgets = {
            'work_documents': forms.SelectMultiple(attrs={'class': 'instructions'})
        }

    def __init__(self, *args, **kwargs):
        instructions = kwargs.pop('instructions', None)
        super(StaffPositionWorkInstructionForm, self).__init__(*args, **kwargs)
        self.fields['work_documents'].queryset = instructions
        customize_style(self)

    def save(self, commit=True):
        self.instance._add_staff_position = False
        instance = super(StaffPositionWorkInstructionForm, self).save(commit=True)

        company = instance.staff.company
        staff = Staff.objects.filter(company=company)
        instances = StaffPosition.objects.filter(
            staff__in=staff, employee_position=instance.employee_position, derivative_position=instance.derivative_position)

        for ins in instances:
            ins.factors_productions.clear()
            ins._add_staff_position = False
            [ins.work_documents.add(x) for x in self.cleaned_data['work_documents']]
        return instance

StaffPositionWorkInstructionFormset = modelformset_factory(StaffPosition, form=StaffPositionWorkInstructionForm, extra=0)


class StaffProfessionWorkInstructionForm(forms.ModelForm):
    class Meta:
        model = StaffProfession
        fields = ('work_documents',)
        widgets = {
            'work_documents': forms.SelectMultiple(attrs={'class': 'instructions'})
        }

    def __init__(self, *args, **kwargs):
        instructions = kwargs.pop('instructions', None)
        super(StaffProfessionWorkInstructionForm, self).__init__(*args, **kwargs)
        self.fields['work_documents'].queryset = instructions
        customize_style(self)

    def save(self, commit=True):
        self.instance._add_staff_position = False
        instance = super(StaffProfessionWorkInstructionForm, self).save(commit=True)
        company = instance.staff.company
        staff = Staff.objects.filter(company=company)
        instances = StaffProfession.objects.filter(staff__in=staff, work_profession=instance.work_profession)

        for ins in instances:
            ins.factors_productions.clear()
            ins._add_staff_profession = False
            [ins.work_documents.add(x) for x in self.cleaned_data['work_documents']]
        return instance

StaffProfessionWorkInstructionFormset = modelformset_factory(StaffProfession, form=StaffProfessionWorkInstructionForm, extra=0)


class CompanySubdivisionUpdateForm(ModelForm):
    class Meta:
        model = Subdivision
        fields = ('title',)
        prefix = 'company_subdivision_update'

    def __init__(self, *args, **kwargs):
        super(CompanySubdivisionUpdateForm, self).__init__(*args, **kwargs)
        customize_style(self)

CompanySubdivisionFormset = inlineformset_factory(
    Company, Subdivision, form=CompanySubdivisionUpdateForm, can_delete=True, extra=1
)