from django.contrib import admin
from .models import *


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    filter_horizontal = ('work_documents', 'fire_documents', 'electrical_documents', 'professions_etkc',
                         'positions_ekcd', 'kind_activities', 'work_field_exclude')
    exclude = ['people_profession', 'short_title', 'full_title', 'email', 'password', 'logo', 'color_logo',
               'first_name', 'last_name', 'created_at', 'updated_at', 'last_login']

    def get_field_queryset(self, db, db_field, request):
        queryset = super(CompanyAdmin, self).get_field_queryset(db, db_field, request)
        if db_field.name == 'work_field_exclude':
            queryset = queryset.select_related('document')
        return queryset


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    filter_horizontal = ('work_field_exclude_yes', 'work_field_exclude_no')


class StaffProfessionInline(admin.TabularInline):
    model = StaffProfession
    extra = 0


class StaffPositionInline(admin.TabularInline):
    model = StaffPosition
    extra = 0


@admin.register(Staff)
class CompanyStaffAdmin(admin.ModelAdmin):
    exclude = ('employment_date',)
    inlines = [
        StaffPositionInline,
        StaffProfessionInline
    ]
admin.site.register(AdditionalInfo)
admin.site.register(CompanyQuestionAnswer)
admin.site.register(WorkProfession)
admin.site.register(EmployeePosition)
admin.site.register(StaffPosition)
admin.site.register(StaffProfession)
admin.site.register(Case)
admin.site.register(GenericInfoCompany)
admin.site.register(PersonAuthorizedCompany)
admin.site.register(FactorsProductionEnvironment)