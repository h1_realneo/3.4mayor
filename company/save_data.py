# # coding: utf8
# import os
# import shutil
#
# from django.apps import apps
# from django.template import Template, Context
# from builtins import AttributeError, PermissionError
# from django.http.response import HttpResponse
# from django.core.exceptions import ObjectDoesNotExist
# from django.template import TemplateSyntaxError
#
# from fire_safety.models import FireDocument, SameFields, F4
# from zipfile import ZipFile
# from mayor.settings import BASE_DIR
# from .models import Staff, AdditionalInfo
#
# TMP_FOLDER = os.path.join(BASE_DIR, 'fire_safety\\fire_document\\temporal_folder\\')
# FINISHED_DOCUMENT = os.path.join(BASE_DIR, 'fire_safety\\fire_document\\finish\\')
# APP = 'fire_safety'
# FIELDS = ['F1', 'F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9', 'F10']
#
#
# def who_sign(company):
#
#     fields_sign = SameFields.objects.get(id=1).fields.all()
#     for field in fields_sign:
#         model = apps.get_model(app_label=APP, model_name=field.field_name)
#         sign_field = model.objects.filter(company=company, document=field.document, number_position=field.number_position).first()
#         if not sign_field or sign_field.name != company.person_sign_order:
#             model.objects.filter(company=company, document=field.document, number_position=field.number_position).delete()
#             model.objects.create(
#                 company=company, document=field.document, number_position=field.number_position,
#                 name=company.person_sign_order, label=field.label
#                 )
#
#
# def agreement(company):
#     fields_agree = SameFields.objects.get(id=2).fields.all()
#     for field in fields_agree:
#         model = apps.get_model(app_label=APP, model_name=field.field_name)
#         agreements = [field.person_agreement_order for field in AdditionalInfo.objects.filter(company=company) if field]
#         agreements_fields = model.objects.filter(
#             company=company, document=field.document, number_position=field.number_position)
#         agreement = [person.name_agreement for person in agreements_fields if person.name_agreement]
#         if not agreements or set(agreements) != set(agreement):
#             for person in [person for person in agreements_fields if person]:
#                 if person and person.name_agreement not in agreements:
#                     person.delete()
#             for person in agreements:
#                 if person not in agreement:
#                     model.objects.create(
#                         company=company, document=field.document, number_position=field.number_position,
#                         name_agreement=person, label=field.label
#                         )
#
#
# def develop_document(company):
#     document = FireDocument.objects.get(id=11)
#     F4.objects.get_or_create(
#         company=company, document=document, number_position=1,
#         defaults={'name': company.person_development_instruction, 'label': 'Разрабатывает инструкции'}
#     )
#
#     F4.objects.get_or_create(
#         company=company, document=document, number_position=3,
#         defaults={'name': company.person_development_program, 'label': 'Разрабатывает программы'}
#     )
#
#     F4.objects.get_or_create(
#         company=company, document=document, number_position=2,
#         defaults={'name': company.person_development_state, 'label': 'Разрабатывает положения'}
#     )
#
#     F4.objects.get_or_create(
#         company=company, document=document, number_position=4,
#         defaults={'name': company.person_safety_health, 'label': 'Разрабатывает перечни'}
#     )
#
#
# def filing_data(company):
#     who_sign(company)
#     agreement(company)
#     develop_document(company)
