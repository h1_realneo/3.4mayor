# coding: utf8
from __future__ import unicode_literals
import re

from django.core.urlresolvers import reverse
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import BaseUserManager
from django.db import models
from multiselectfield import MultiSelectField
from work_safety._factors import WORKS_OF_INCREASED_DANGER
from company.additional_info_for_choices import CONSTRUCTIONS, BUILDINGS, PREMISES, EQUIPMENT, MEANS_TRANSPORT, \
    INTERNAL_ENGINEERING_NETWORKS as IEN, EXTERNAL_ENGINEERING_NETWORKS as EEN


class AccountManager(BaseUserManager):
    def create_user(self, username, password=None, **kwargs):

        account = self.model(username=username)

        account.set_password(password)
        account.is_admin = True
        account.save()

        return account

    def create_superuser(self, username, password, **kwargs):
        account = self.create_user(username, password, **kwargs)

        account.is_admin = True
        account.save()

        return account


def staffing_table(instance, filename):
    # file will be uploaded to MEDIA_ROOT/staffing_table/company_<id>/<filename>
    return 'staffing_table/{}/{}'.format(
        instance.id, filename
    )


class Company(AbstractBaseUser):
    RED = 'R'
    GREEN = 'GR'
    COLOR_FOR_LOGO = (
        (RED, 'Red'),
        (GREEN, 'Green'),
    )

    email = models.EmailField(blank=True, null=True)
    username = models.CharField('Логин', max_length=255, unique=True)

    first_name = models.CharField(max_length=255, blank=True)
    last_name = models.CharField(max_length=255, blank=True)

    is_admin = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # custom fields
    fire_documents = models.ManyToManyField('fire_safety.FireDocument', blank=True)
    work_documents = models.ManyToManyField('work_safety.OTDocument', blank=True)
    work_field_exclude = models.ManyToManyField('work_safety.FieldInDocument', blank=True)
    electrical_documents = models.ManyToManyField('electrical_safety.ElSafetyDocument', blank=True)
    short_title = models.TextField('Сокращённое наименование юридического лица', max_length=300)
    full_title = models.TextField('Полное наименование юридического лица', max_length=400)
    logo = models.BooleanField(blank=True, default=False)
    color_logo = models.CharField(max_length=255, choices=COLOR_FOR_LOGO, default=False, blank=True)
    address = models.CharField('Место размещения юридического лица (юридический адрес)', max_length=300, blank=True)
    people_profession = models.FileField('Штатка', upload_to=staffing_table)

    safety_health = models.ForeignKey(
        'StaffPosition', verbose_name='На кого возложены обязанности специалиста по охране труда в организации',
        null=True, blank=True, on_delete=models.SET_NULL, related_name='safety_health'
    )
    person_authorized = models.ForeignKey(
        'Staff', verbose_name='Уполномоченное лицо по охране труда работников организации', null=True,
        blank=True, on_delete=models.SET_NULL, related_name='person_authorized'
    )
    boss = models.ForeignKey(
        'StaffPosition', verbose_name='Директор', null=True, blank=True, on_delete=models.SET_NULL, related_name='boss')
    development_instruction = models.ForeignKey(
        'StaffPosition', verbose_name='Лицо ответственное за разработку инструкций по пожарной безопасности', null=True, blank=True,
        on_delete=models.SET_NULL, related_name='development_instruction'
    )
    development_program = models.ForeignKey(
        'StaffPosition', verbose_name='Лицо ответственное за разработку программ по пожарной безопасности', null=True, blank=True,
        on_delete=models.SET_NULL, related_name='development_program'
    )
    development_provision = models.ForeignKey(
        'StaffPosition', verbose_name='Лицо ответственное за разработку положений по пожарной безопасности', null=True, blank=True,
        on_delete=models.SET_NULL, related_name='development_provision'
    )

    professions_etkc = models.ManyToManyField('ProfessionETKC', verbose_name='Номер выпуска ЕТКС', blank=True)
    positions_ekcd = models.ManyToManyField('PositionEKCD', verbose_name='Номер выпуска ЕКСД', blank=True)
    kind_activities = models.ManyToManyField('KindActivity', verbose_name='Виды деятельности', blank=True)

    objects = AccountManager()

    USERNAME_FIELD = 'username'

    def get_full_name(self):
        return ' '.join([self.first_name, self.last_name])

    def get_short_name(self):
        return self.first_name

    def get_absolute_url(self):
        return reverse('company:update', kwargs={'pk': self.pk})

    @property
    def is_staff(self):
        return self.is_admin

    def has_module_perms(self, app_label):
        return self.is_admin

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = 'Компания'
        verbose_name_plural = 'Компании'


class KindActivity(models.Model):
    code_group = models.CharField('Код группировки', max_length=10)
    name_group = models.CharField('Наименование группировки и ее описание', max_length=255)
    section = models.CharField('Секция', max_length=200)
    subsection = models.CharField('Подсекция', max_length=200, blank=True)
    section_url = models.CharField(max_length=10, blank=True)
    subsection_url = models.CharField(max_length=10, blank=True)
    production = models.BooleanField('Производственник', default=False)

    def __str__(self):
        return '{} {}'.format(self.code_group, self.name_group)


class PositionEKCD(models.Model):
    code = models.IntegerField('Номер выпуска ЕКСД')
    title = models.CharField('Наименование выпуска ЕКСД', max_length=150)

    def __str__(self):
        return '{}'.format(self.code,)


class ProfessionETKC(models.Model):
    code = models.IntegerField('Номер выпуска ЕТКС')
    title = models.TextField('Наименование разделов выпусков ЕТКС')

    def __str__(self):
        return '{}'.format(self.code)


class WorkProfession(models.Model):
    RANKS_CLASS = (
        ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'), ('6', '6'), ('7', '7'), ('8', '8'),
        ('8', '1 класс'), ('9', '2 класс'), ('10', '3 класс'), ('11', '4 класс'),
    )
    company = models.ForeignKey(Company, null=True, blank=True, on_delete=models.CASCADE)
    work_documents = models.ManyToManyField('work_safety.OTDocument', blank=True)
    new_profession_code = MultiSelectField(blank=True, max_length=500)
    profession_code = models.IntegerField('код профессии', null=True, blank=True)
    name_working_career = models.CharField('наименование профессии рабочего', max_length=300)
    ranks = MultiSelectField('разряды', max_length=60, blank=True, choices=RANKS_CLASS)
    okz = models.CharField('окз', max_length=255, blank=True)
    etkc = models.CharField('еткс', max_length=255, blank=True)
    professions_etkc = models.ManyToManyField(ProfessionETKC)

    def __str__(self):
        return '{}'.format(self.name_working_career)


class EmployeePosition(models.Model):
    CATEGORIES = (
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
    )
    company = models.ForeignKey(Company, null=True, blank=True, on_delete=models.CASCADE)
    work_documents = models.ManyToManyField('work_safety.OTDocument', blank=True)
    new_position_code = MultiSelectField(blank=True, max_length=500)
    position_code = models.IntegerField('код должности', null=True, blank=True)
    name_post_office_worker = models.CharField('наименование должности служащего', max_length=300)
    categories = MultiSelectField('категории', max_length=60, blank=True, choices=CATEGORIES)
    okz = models.CharField('окз', max_length=255, blank=True)
    ekcd = models.CharField('ексд', max_length=255, blank=True)
    positions_ekcd = models.ManyToManyField(PositionEKCD)

    def __str__(self):
        return '{}'.format(self.name_post_office_worker)


class Case(models.Model):
    nomn = models.CharField('Именительный падеж', max_length=400, blank=True)
    gent = models.CharField('Родительный падеж', max_length=400, blank=True)
    datv = models.CharField('Дательный падеж', max_length=400, blank=True)
    accs = models.CharField('Винительный падеж', max_length=400, blank=True)
    ablt = models.CharField('Творительный падеж', max_length=400, blank=True)
    loct = models.CharField('Предложный падеж', max_length=400, blank=True)
    checked = models.BooleanField('Правильно', default=False)

    def __str__(self):
        return '{} {}'.format(self.pk, self.nomn)

    def get_absolute_url(self):
        return reverse('company:change_staff_cases', kwargs={'pk': self.pk})

PTM = (
    ('pb', 'талон для ответственных за ПБ  и инструктаж'),
    ('dpd', 'талон для членов ДПД и ПТК'),
    ('fire', 'талон по огневым работам'),
)


class StaffPosition(models.Model):
    DERIVATIVE = (
        ('Вице-', 'Вице-'),
        ('Первый заместитель руководителя', 'Первый заместитель руководителя'),
        ('Заместитель', 'Заместитель'),
        ('Главный', 'Главный'),
        ('Ведущий', 'Ведущий'),
        ('Старший', 'Старший'),
        ('Младший', 'Младший'),
        ('Сменный', 'Сменный'),
        ('Первый', 'Первый'),
        ('Второй', 'Второй'),
        ('Третий', 'Третий'),
        ('Четвертый', 'Четвертый'),
        ('Районный', 'Районный'),
        ('Участвоквый', 'Участковый'),
        ('Горный', 'Горный'),
        ('Помощник', 'Помощник'),
    )
    CATEGORIES = EmployeePosition.CATEGORIES
    subdivision = models.ForeignKey('fire_safety.Subdivision', models.SET_NULL, blank=True, null=True)
    derivative_position = models.CharField('производная должности', max_length=255, blank=True, choices=DERIVATIVE)
    category = models.CharField('категория', max_length=50, blank=True, choices=CATEGORIES)
    staff = models.ForeignKey('Staff', on_delete=models.CASCADE)
    employee_position = models.ForeignKey(EmployeePosition, verbose_name='Должность')
    case = models.ForeignKey('Case', on_delete=models.SET_NULL, null=True, blank=True)
    ptm = models.CharField('вид ПТМ', blank=True, choices=PTM, max_length=30)
    frequency_instruction_three_month = models.BooleanField(default=False)
    date_recruitment = models.DateField('дата приема на работу', null=True, blank=True)
    medical_certificate_number = models.CharField(max_length=20, blank=True)
    date_issue_medical_certificate = models.DateField('дата выдачи медицинской справки', null=True, blank=True)
    number_certificate_prof_development = models.CharField(max_length=20, blank=True)
    date_issue_certificate_prof_development = models.DateField('дата повышения квалификации', null=True, blank=True)
    factors_productions = models.ManyToManyField('FactorsProductionEnvironment', blank=True)
    work_experience = models.CharField('стаж работы', max_length=20, blank=True)
    work_documents = models.ManyToManyField('work_safety.OTDocument', blank=True)

    class Meta:
        verbose_name = 'Должность сотрудника'
        verbose_name_plural = 'Должности сотрудников'

    def __str__(self):
        return '{}'.format(self.id)


class StaffProfession(models.Model):
    RANKS_CLASS = WorkProfession.RANKS_CLASS
    subdivision = models.ForeignKey('fire_safety.Subdivision', models.SET_NULL, blank=True, null=True)
    rank = models.CharField('разряд', max_length=50, blank=True, choices=RANKS_CLASS)
    staff = models.ForeignKey('Staff', on_delete=models.CASCADE)
    work_profession = models.ForeignKey(WorkProfession, verbose_name='Профессия', on_delete=models.CASCADE)
    case = models.ForeignKey('Case', on_delete=models.SET_NULL, null=True, blank=True)
    ptm = models.CharField('вид ПТМ', blank=True, choices=PTM, max_length=30)
    frequency_instruction_three_month = models.BooleanField(default=False)
    date_recruitment = models.DateField('дата приема на работу', null=True, blank=True)
    medical_certificate_number = models.CharField(max_length=20, blank=True)
    date_issue_medical_certificate = models.DateField('дата выдачи медицинской справки', null=True, blank=True)
    number_certificate_prof_development = models.CharField(max_length=20, blank=True)
    date_issue_certificate_prof_development = models.DateField('дата повышения квалификации', null=True, blank=True)
    factors_productions = models.ManyToManyField('FactorsProductionEnvironment', blank=True)
    work_experience = models.CharField('стаж работы', max_length=20, blank=True)
    work_documents = models.ManyToManyField('work_safety.OTDocument', blank=True)

    class Meta:
        verbose_name = 'Профессия сотрудника'
        verbose_name_plural = 'Профессии сотрудников'

    def __str__(self):
        return '{}'.format(self.id)


class Staff(models.Model):
    NO_MATTER = ''
    MEN = 'masc'
    WOMEN = 'femn'
    GENDER = (
        (NO_MATTER, 'Не важно'),
        (MEN, 'Мужчина'),
        (WOMEN, 'Женщина'),

    )
    company = models.ForeignKey(
        Company, related_name='staff', on_delete=models.CASCADE)
    work_professions = models.ManyToManyField(WorkProfession, blank=True, through=StaffProfession)
    employee_positions = models.ManyToManyField(EmployeePosition, blank=True, through=StaffPosition)
    case = models.ForeignKey('Case', on_delete=models.SET_NULL, null=True, blank=True)
    surname = models.CharField('Фамилия', max_length=255, blank=True)
    initials = models.CharField('Инициалы', max_length=255, null=True, blank=True)
    sex = models.CharField('Пол сотрудника', max_length=255, choices=GENDER, default=NO_MATTER, blank=True, null=True)
    employment_date = models.DateField('дата приема на работу', null=True, blank=True)
    date_birthday = models.DateField('дата рождения', null=True, blank=True)
    place_residence = models.CharField(max_length=255, blank=True)

    class Meta:
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники фирм'

    def __str__(self):
        return '{} {}'.format(self.surname, self.initials)

    def get_absolute_url(self):
        return reverse('company:employee', kwargs={'pk': self.pk})


class AdditionalInfo(models.Model):
    company = models.ForeignKey(
        Company, related_name='additional_info', on_delete=models.CASCADE)
    person_agreement_order = models.CharField(max_length=300, blank=True)

    class Meta:
        verbose_name = 'Дополнительная информация'
        verbose_name_plural = 'Дополнительная информация'

    def __str__(self):
        return '{}'.format(self.company.username)


class Question(models.Model):
    CHOICES = 'CHOICES'
    RESPONSIBLE_EXTINGUISHER = 'RESPONSIBLE_EXTINGUISHER'
    ELECTRICAL = 'ELECTRICAL'
    HEATING = 'HEATING'
    WORK_DANGERS = 'WORK_DANGERS'
    TYPES_EQUIPMENT = 'TYPES_EQUIPMENT'
    VARIANTS = (
        (CHOICES, "Стандартное"),
        (RESPONSIBLE_EXTINGUISHER, "Арендатор"),
        (ELECTRICAL, 'Электричество'),
        (HEATING, 'Отопление'),
        (WORK_DANGERS, 'Опасные работы'),
        (TYPES_EQUIPMENT, 'Виды оборудование'),
    )

    FIRE_SAFETY = 'fire_safety'
    WORK_SAFETY = 'work_safety'
    DIVISION = (
        (FIRE_SAFETY, 'пожарная безопасноть'),
        (WORK_SAFETY, 'охрана труда'),
    )
    code = models.IntegerField(unique=True, null=True)
    division = models.CharField(max_length=255, choices=DIVISION, default=FIRE_SAFETY)
    text = models.CharField('Вопрос', max_length=500)
    help_text = models.TextField('Подсказка', max_length=1000, blank=True)
    documents_answer_yes = models.ManyToManyField('fire_safety.FireDocument', related_name='question_yes', blank=True)
    documents_answer_no = models.ManyToManyField('fire_safety.FireDocument', related_name='question_no', blank=True)
    work_doc_yes = models.ManyToManyField('work_safety.OTDocument', related_name='work_yes', blank=True)
    work_doc_no = models.ManyToManyField('work_safety.OTDocument', related_name='work_no', blank=True)
    work_field_exclude_yes = models.ManyToManyField(
        'work_safety.FieldInDocument', related_name='work_field_yes', blank=True)
    work_field_exclude_no = models.ManyToManyField(
        'work_safety.FieldInDocument', related_name='work_field_no', blank=True)
    choices = models.CharField('Поле выбора', max_length=255, choices=VARIANTS, default=CHOICES)
    multiple = models.BooleanField('Множественный выбор', default=False)

    class Meta:
        ordering = ('-code',)
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы для компаний'

    def __str__(self):
        return '{} {}'.format(self.code, self.text)


class CompanyQuestionAnswer(models.Model):
    NO_MATTER = 'no_m'
    YES = 'yes'
    NO = 'no'
    CHOICES = (
        (NO_MATTER, "Без ответа"),
        (YES, "Да"),
        (NO, "Нет"),
    )
    RESPONSIBLE_EXTINGUISHER = (
        (NO_MATTER, "Мы не арендуем"),
        (YES, "Арендатор"),
        (NO, "Арендодатель"),
    )
    ELECTRICAL = (
        ('power_tool', 'Электроиструмент'),
        ('ypa', 'Упа'),
        ('electric_lamp', 'Электрический светильник'),
    )
    HEATING = (
        ('boiler', 'Котельные установки'),
        ('heat', 'Теплогенерирующие аппараты'),
        ('furnace', 'Нетеплоемкие печи'),
        ('fireplace', 'Камины'),
        ('fire_damper', 'Огнезадерживающие клапаны'),
    )
    WORK_DANGERS = WORKS_OF_INCREASED_DANGER
    TYPES_EQUIPMENT = (
        ('pr_e', 'технологическое оборудование'),
    )

    company = models.ForeignKey(Company, related_name='question')
    question = models.ForeignKey(Question)
    answer = models.CharField(max_length=400, null=True, blank=True)

    class Meta:
        unique_together = ('company', 'question')
        verbose_name = 'Вопрос Ответ'
        verbose_name_plural = 'Ответы компаний на вопросы'

    def __str__(self):
        return '{} {} {} {}'.format(self.question.code, self.company, self.answer, self.question.text)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        # if self.question.multiple and self.answer:
        self.answer = ' '.join(re.findall(r'\w{2,}', self.answer))
        super(CompanyQuestionAnswer, self).save(force_insert=False, force_update=False, using=None, update_fields=None)


class PersonAuthorizedCompany(models.Model):
    company = models.OneToOneField(Company)
    public_inspector = models.BooleanField(default=False)
    chairperson = models.BooleanField(default=False)
    protocol_number = models.CharField(blank=True, null=True, max_length=30)

    def __str__(self):
        return '{}'.format(self.company)


class GenericInfoCompany(models.Model):

    company = models.OneToOneField(Company, on_delete=models.CASCADE)
    short_title_case = models.ForeignKey('Case', null=True, on_delete=models.SET_NULL)
    ownership = models.CharField('ownership', max_length=255)
    short_ownership = models.CharField('short_ownership', max_length=255)
    full_title = models.CharField('full title', max_length=255)
    short_title = models.CharField('short title', max_length=255)
    characteristic = models.TextField('characteristic', blank=True)
    subordination = models.CharField('subordination', max_length=300, blank=True)
    date_registration = models.DateField('date registration', blank=True, null=True)
    place_registrations = models.ForeignKey('PlaceRegistration', models.SET_NULL, blank=True, null=True)
    address = models.CharField('address', max_length=255, blank=True)
    ynp = models.CharField('ynp', max_length=255, blank=True)
    okpo = models.CharField('okpo', max_length=255, blank=True)
    bank_details = models.CharField('bank_details', max_length=255, blank=True)
    mailing_address = models.CharField('mailing_address', max_length=255, blank=True)
    fax = models.CharField('fax', max_length=255, blank=True)
    mail = models.CharField('mail', max_length=255, blank=True)
    # Questions about fixed assets
    buildings = MultiSelectField('buildings', choices=BUILDINGS, max_length=1000, blank=True)
    constructions = MultiSelectField('constructions', choices=CONSTRUCTIONS, max_length=1000, blank=True)
    premises = MultiSelectField('premises', choices=PREMISES, max_length=1000, blank=True)
    means_transport = MultiSelectField('means_transport', choices=MEANS_TRANSPORT,  max_length=1000, blank=True)
    external_engineering_networks = MultiSelectField('external_networks', choices=EEN, max_length=1000, blank=True)
    internal_engineering_networks = MultiSelectField('internal_networks', choices=IEN, max_length=1000, blank=True)
    equipment = MultiSelectField('equipment', choices=EQUIPMENT, max_length=1000, blank=True)

    def get_absolute_url(self):
        return reverse('company:generic_info_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return '{}'.format(self.company)


class TelephoneNumber(models.Model):
    company = models.ForeignKey(GenericInfoCompany)
    telephone = models.CharField('telephone', max_length=255, blank=True)


class PlaceRegistration(models.Model):
    region = models.CharField(max_length=255, blank=True)
    title = models.CharField(max_length=255)

    def __str__(self):
        return '{}'.format(self.title)


class FactorsProductionEnvironment(models.Model):

    factor = models.CharField(max_length=800)
    rating = models.CharField(max_length=5)
    scan_frequency = models.IntegerField()
    number_attachment = models.CharField(max_length=10)
    paragraph_attachment = models.CharField(max_length=10)

    def __str__(self):
        if self.rating:
            return '{} класс {} {}'.format(self.paragraph_attachment, self.rating, self.factor)
        else:
            return '{} {}'.format(self.paragraph_attachment, self.factor)