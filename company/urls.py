# coding: utf8

from django.conf.urls import url
from django.views.generic import TemplateView
from .views import CompanyList, CompanyCreate, CompanyUpdate, CompanyLeadersUpdateView, CompanyUpdateEKCDaEKTC, \
    GenericInfoDetail, GenericInfoUpdate, CompanyDelete, login_view, logout_view, DownloadStaffSubdivisionView, \
    QuestionView, StaffPositionProfessionView, StaffView, UpdateCasesView, AddPositionView, AddProfessionView, \
    EmployeePositionView, EmployeeProfessionView, StaffCreate, StaffUpdate, StaffDelete, ActivityDetail, \
    get_categories_and_list_position, get_ranks_and_list_work_professions, StaffSurnameUpdateView, AllCasesFormView, \
    StaffPosStaffProfUpdateView, FactorProductionView, StaffUpdateView, InstructionWorkerView, CompanySubdivisionUpdateView

urlpatterns = [
    url(r'^$', CompanyList.as_view(), name='list'),
    url(r'^add/$', CompanyCreate.as_view(), name='add'),
    url(r'^update/(?P<pk>[0-9]+)/$', CompanyUpdate.as_view(), name='update'),
    url(r'^update_leaders/(?P<pk>[0-9]+)/(?P<section>[\w-]+)/$', CompanyLeadersUpdateView.as_view(), name='update_leaders'),
    url(r'^update_ekcd_ektc/(?P<pk>[0-9]+)/$', CompanyUpdateEKCDaEKTC.as_view(), name='update_ekcd_ektc'),
    url(r'^generic_info/detail/(?P<pk>[0-9]+)/$', GenericInfoDetail.as_view(), name='generic_info_detail'),
    url(r'^generic_info/update/(?P<pk>[0-9]+)/$', GenericInfoUpdate.as_view(), name='generic_info_update'),
    url(r'^delete/(?P<pk>[0-9]+)/$', CompanyDelete.as_view(), name='delete'),
    url(r'^login/$', login_view, name='login'),
    url(r'^logout/$', logout_view, name='logout'),
    url(r'^download_staff/(?P<pk>[0-9]+)/$', DownloadStaffSubdivisionView.as_view(), name='staff_subdivision_download'),
    url(r'^question/(?P<division>[\w-]+)/(?P<pk>[0-9]+)/$', QuestionView.as_view(), name='question'),
    url(r'^position_profession/(?P<pk>[0-9]+)/$', StaffPositionProfessionView.as_view(), name='position_profession'),
    url(r'^staff_detail/(?P<pk>[0-9]+)/$', StaffView.as_view(), name='staff_detail'),
    url(r'^change_cases/(?P<pk>[0-9]+)/$', UpdateCasesView.as_view(), name='change_cases'),
    url(r'^add_position/(?P<pk>[0-9]+)/$', AddPositionView.as_view(), name='add_position'),
    url(r'^add_profession/(?P<pk>[0-9]+)/$', AddProfessionView.as_view(), name='add_profession'),
    url(r'^employee_position/(?P<pk>[0-9]+)/$', EmployeePositionView.as_view(), name='employee_position'),
    url(r'^employee_profession/(?P<pk>[0-9]+)/$', EmployeeProfessionView.as_view(), name='employee_profession'),
    url(r'^employee/create/$', StaffCreate.as_view(), name='employee_add'),
    url(r'^employee/(?P<pk>[0-9]+)/$', StaffUpdate.as_view(), name='employee_update'),
    url(r'^employee/(?P<pk>[0-9]+)/delete/$', StaffDelete.as_view(), name='employee_delete'),
    url(r'^dynamic_category_select/$', get_categories_and_list_position, name='dynamic_category_select'),
    url(r'^dynamic_rank_select/$', get_ranks_and_list_work_professions, name='dynamic_rank_select'),
    url(r'^activity/(?P<pk>[0-9]+)/$', ActivityDetail.as_view(), name='activity'),
    url(r'^staff_surname/(?P<pk>[0-9]+)/$', StaffSurnameUpdateView.as_view(), name='staff_surname'),
    url(r'^staff_pos_prof/$', StaffPosStaffProfUpdateView.as_view(), name='staff_pos_prof'),
    url(r'^all_cases/(?P<section>[\w-]+)/$', AllCasesFormView.as_view(), name='all_cases'),
    url(r'^check_pos_prof/$', TemplateView.as_view(template_name='company/check_okrb.html'), name='check_pos_prof'),
    url(r'^factors_production/$', FactorProductionView.as_view(), name='factors_production'),
    url(r'^staff/update/$', StaffUpdateView.as_view(), name='staff_update'),
    url(r'^staff/instructions/$', InstructionWorkerView.as_view(), name='worker_instructions'),
    url(r'^subdivision/(?P<pk>[0-9]+)/$', CompanySubdivisionUpdateView.as_view(), name='subdivision_update')
]
