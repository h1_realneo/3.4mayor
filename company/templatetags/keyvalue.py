# coding: utf8
from django import template

register = template.Library()


@register.filter(name='kv')
def get_value_dict_by_key(dictionary, value):
    if isinstance(dictionary, dict):
        return dictionary.get(value, '')
    return ''
