# coding: utf8
from django import template

register = template.Library()


@register.filter(name='pr')
def pretext(word, pretexts):
    if not word or not pretexts:
        return ''
    pretexts = pretexts.split(',')
    if len(pretexts) == 2:
        if 'обществ' in word.lower():
            return '{} {}'.format(pretexts[0], "Обществе")
        else:
            return '{} {}'.format(pretexts[1], "Предприятии")
    else:
        return ''
