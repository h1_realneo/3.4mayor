# coding: utf8
import os
import sys
import re


def get_dict():
    path_to_folder = 'D:/projects/normalize_siz/txt'
    rus_alphabet = [chr(x) for x in range(256, 10000) if 'а' <= chr(x) <= 'я' or 'А' <= chr(x) <= 'Я']
    final = []

    def get_files_in_path(path):
        massive_files = []
        for file_name in os.listdir(path):
            massive_files.append(open('{}/{}'.format(path, file_name), 'r'))
        return massive_files

    def delete_extra_space(line):
        extra_spaces = re.findall(r'\s{2,}', line)
        for extra_space in extra_spaces:
            line = line.replace(extra_space, ' ')
        return line.strip()

    def delete_all_spaces(line):
        spaces = re.findall(r'\s+', line)
        for extra_space in spaces:
            line = line.replace(extra_space, '')
        return line

    def count_rus_letter_in_line(line):
        count = 0
        for letter in line:
            if letter in rus_alphabet:
                count += 1
        return count

    def count_title_letter(line):
        count = 0
        for letter in line:
            if letter.istitle():
                count += 1
        return count

    def read_file(file):
        print(file)
        text = file.read()
        lines = text.split('\n')
        clean_lines = []
        title_document = '{} {}'.format(lines[0], lines[1])
        for line in lines:
            coincidence = None
            for letter in line:
                if letter in rus_alphabet:
                    coincidence = True
            if coincidence:
                clean_lines.append(line)
        data = {}
        code = ''
        b = 0
        section = ''
        chapter = True
        work = ''
        prev_code = 0
        first_code = 0
        for num, clean_line in enumerate(clean_lines):
            code_in_line, siz, marking, time_wearing = '', '', '', ''
            record = clean_line.split('¦')
            if len(record) != 8:
                if '+' in clean_line:
                    continue
                text_in_line = ''.join(record)
                if 'постановлен' in text_in_line:
                    continue
                if ('РАЗДЕЛ' or 'Раздел') in text_in_line:
                    section = True
                    chapter = False
                    continue
                if ('ГЛАВА' or 'Глава') in text_in_line:
                    section = False
                    chapter = True
                    continue
                count_letter = count_rus_letter_in_line(text_in_line)
                if count_letter > 7:
                    if section:
                        prev_line = clean_lines[num-1]
                        large_section = True if count_title_letter(prev_line) > 7 and ('РАЗДЕЛ' or 'Раздел') not in prev_line else False
                        if large_section:
                            section += ' {}'.format(delete_extra_space(text_in_line))
                        else:
                            section = delete_extra_space(text_in_line)
                        # print('SECTION {}'.format(section))
                    elif chapter:
                        prev_line = clean_lines[num-1]
                        large_chapter = True if count_title_letter(prev_line) > 7 and ('ГЛАВА' or 'Глава') not in prev_line else False
                        if large_chapter:
                            chapter += ' {}'.format(delete_extra_space(text_in_line))
                        else:
                            chapter = delete_extra_space(text_in_line)
                        # print('CHAPTER {}'.format(chapter))
                continue

            if record[2].strip():
                try:
                    if first_code and prev_code:
                        prev_code = code
                    code = int(record[2].replace('  ', '').replace(' ', '').strip())
                    b += 1
                    code_in_line = code
                    if first_code and not prev_code:
                        prev_code = first_code
                    elif not prev_code and not first_code:
                        first_code = code_in_line
                except Exception:
                    final.append('c={} file={}'.format(code, file))
                    break
                    # print(' Код {} итерация {}'.format(code, b))

            if record[4]:
                siz = record[4].strip()
            if record[5].strip():
                marking = record[5].strip()
            if record[6].strip():
                time_wearing = record[6].strip()

            if code_in_line and time_wearing:
                if prev_code:

                    data = search_keys_by_code(prev_code, work, data)
                    work = ''
                data.update({(code, section, chapter, title_document): [[siz, marking, time_wearing]]})
            elif code_in_line:
                if prev_code:

                    data = search_keys_by_code(prev_code, work, data)
                    work = ''
                data.update({(code, section, chapter, title_document): [[siz, marking, time_wearing]]})
            elif siz and time_wearing and (time_wearing[0].istitle() or time_wearing.isdigit()) or siz and siz[0].istitle() and not siz[1].istitle():
                value = data.get((code, section, chapter, title_document))
                value.append([siz, marking, time_wearing])
                data.update({(code, section, chapter, title_document): value})
            elif siz and not siz[0].istitle():
                value = data.get((code, section, chapter, title_document))
                prev_value_siz = value[-1][0]
                new_value_siz = '{} {}'.format(prev_value_siz, siz)
                new_value_siz = delete_extra_space(new_value_siz)
                value[-1][0] = new_value_siz
                if time_wearing and (time_wearing.isdigit() or not time_wearing[0].istitle()):
                    prev_value_time_wearing = value[-1][-1]
                    new_value_time_wearing = '{} {}'.format(prev_value_time_wearing, delete_extra_space(time_wearing))
                    value[-1][-1] = new_value_time_wearing
                data.update({(code, section, chapter, title_document): value})
            elif not siz and marking:
                value = data.get((code, section, chapter, title_document))
                prev_value_marking = value[-1][1]
                new_value_marking = '{}{}'.format(prev_value_marking, marking)
                value[-1][1] = new_value_marking
                data.update({(code, section, chapter, title_document): value})
            elif not siz and time_wearing:
                value = data.get((code, section, chapter, title_document))
                prev_value_time_wearing = value[-1][-1]
                new_value_time_wearing = '{} {}'.format(prev_value_time_wearing, delete_extra_space(time_wearing))
                value[-1][-1] = new_value_time_wearing
                data.update({(code, section, chapter, title_document): value})
            work += record[3].strip()
            if num+1 == len(clean_lines):
                value = data.pop((code, section, chapter, title_document))
                data.update({(code, section, chapter, title_document) + (work,): value})
        return data
    files = get_files_in_path(path_to_folder)
    all_dict = {}
    for i, f in enumerate(files):
        dictionary = read_file(f)
        all_dict.update(dictionary)

    return all_dict


def search_keys_by_code(code, work, data):
    update_data = {}
    for key in data.keys():
        if key[0] == code:
            item = data.get(key, None)
            if len(key) == 4:
                update_data.update({key + (work,): item})
            else:
                new_key = key[:4]
                update_data.update({new_key + (work,): item})
        else:
            update_data.update({key: data.get(key)})
    return update_data