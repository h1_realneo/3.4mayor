# coding: utf8
import xlrd
import sys
import os
from company.models import KindActivity
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = 'download kind activity'

    def handle(self, *args, **options):
        document = xlrd.open_workbook(file_contents=open(
            os.path.abspath(os.path.join(sys.path[0], os.pardir)) + '/виды деятельности.xlsx', 'rb').read())
        sheet = document.sheet_by_index(0)
        ENG_ALPHABET = [chr(x) for x in range(256) if 'A' <= chr(x) <= 'Z']
        end_list = []
        subsection = ''
        subsection_url = ''
        x = -1
        y = -1
        section_url = ''
        for rownum in range(sheet.nrows):
            row = sheet.row_values(rownum)
            if row[0]:
                x += 1
                section_url = ENG_ALPHABET[x]
                section = row[0]
                subsection = ''
                subsection_url = ''
                y = -1
            if row[1]:
                y += 1
                subsection = row[1]
                subsection_url = section_url + ENG_ALPHABET[y]
            code_group = row[2] if isinstance(row[2], str) else str(int(row[2]))
            name_group = row[3]
            production = '{0}'.format(row[5]).strip()
            production = True if production else False
            if code_group and name_group and section:
                end_list.append([code_group, name_group, section, subsection, section_url, subsection_url, production])
        for x in end_list:
            try:
                KindActivity.objects.create(
                    code_group=x[0], name_group=x[1], section=x[2], subsection=x[3], section_url=x[4],
                    subsection_url=x[5], production=x[6])
            except:
                print(x)