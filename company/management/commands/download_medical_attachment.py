# coding: utf8
import re
from django.core.management.base import BaseCommand
from company.models import WorkProfession, EmployeePosition, FactorsProductionEnvironment
import xlrd

path_file_attachment = 'C:/Users/skull/Desktop/attachment/attachment3.xlsx'


def del_spaces(text):
    if not isinstance(text, str):
        return text
    spaces = re.findall(r'\s{2,}', text)
    for space in spaces:
        text = text.replace(space, ' ')
    return text


class Command(BaseCommand):

    def handle(self, *args, **options):
        result = []
        document = xlrd.open_workbook(path_file_attachment)
        sheet = document.sheet_by_index(0)

        for row_num in range(2, sheet.nrows):
            row = sheet.row_values(row_num)
            if row[-1]:
                scan_frequency = int(row[-1])
                number_attachment = '3'
                paragraph_attachment = str(row[0]).strip()
                factor = row[1]
                factor = del_spaces(factor).strip()
                if scan_frequency == 4:
                    for rating, scan_frequency in {'3.3': 1, '3.4': 1, '4': 1, '3.1': 2, '3.2': 2, '2': 3}.items():
                        result.append([number_attachment, paragraph_attachment, factor, scan_frequency, rating])
                else:
                    result.append([number_attachment, paragraph_attachment, factor, scan_frequency, ''])
        for x in result:
            f = FactorsProductionEnvironment.objects.filter(
                number_attachment=x[0], paragraph_attachment=x[1], factor=x[2], scan_frequency=x[3], rating=x[4]
            )
            if not f:
                print(x)
                FactorsProductionEnvironment.objects.create(
                    number_attachment=x[0], paragraph_attachment=x[1], factor=x[2], scan_frequency=x[3], rating=x[4]
                )
