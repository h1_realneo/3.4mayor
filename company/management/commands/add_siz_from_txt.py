# coding: utf8
from work_safety.models import *
from django.core.management.base import BaseCommand, CommandError
from ._read_normalize_txt_siz import get_dict
from company._utility import search_in_professions, get_distance


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        work = Siz
        dict_prof_pos = get_dict()
        mas = []
        [mas.append(x[0]) for x in dict_prof_pos if x[0] not in mas]
        for x in dict_prof_pos.keys():
            all_prof = WorkProfession.objects.all()
            all_pos = EmployeePosition.objects.all()
            now_work = x[4].replace(' ', '').replace('-', '').lower()
            work = WorkProfession.objects.filter(profession_code=x[0]).first()
            prof = EmployeePosition.objects.filter(position_code=x[0]).first()
            work_by_code = work if work else prof
            if not work_by_code:
                print(' нету такого кода {} __ {}'.format(x[0], x[-1]))
                continue
            if isinstance(work_by_code, EmployeePosition):
                work_by_code = work_by_code.name_post_office_worker.replace(' ', '').replace('-', '').lower()
            elif isinstance(work_by_code, WorkProfession):
                work_by_code = work_by_code.name_working_career.replace(' ', '').replace('-', '').lower()
            else:
                continue
            di = get_distance(work_by_code, now_work)
            if di > 2:
                new_d = 100
                for code in range(x[0]-20, x[0]+20):
                    pos = all_pos.filter(position_code=code).first()
                    prof = all_prof.filter(profession_code=code).first()
                    if pos:
                        new_d = get_distance(pos.name_post_office_worker.replace(' ', '').replace('-', '').lower(), now_work)
                    elif prof:
                        new_d = get_distance(prof.name_working_career.replace(' ', '').replace('-', '').lower(), now_work)
                    if new_d < 2:
                        print(' good={}     {} __ {}'.format(new_d, x[0], x[-1]))
                        break
                if new_d < 2:
                    continue
                print(' ливеншейн={}     {} __ {}'.format(di, x[0], x[-1]))

        for key in dict_prof_pos.keys():
            condition = ''
            code = key[0]
            section = key[1]
            chapter = key[2]
            title_document = key[3]
            sizs_in = dict_prof_pos.get(key)
            prof, pos = None, None
            if code:
                prof = WorkProfession.objects.filter(profession_code=code).first()
                pos = EmployeePosition.objects.filter(position_code=code).first()
                work = prof if prof else pos

            for siz in sizs_in:
                name_thing = siz[0]
                marking = siz[1]
                time_wearing = siz[2]
                if ':' in name_thing:
                    condition = name_thing
                    continue
                if work:
                    siz = Siz.objects.create(
                        title_document=title_document,  date_published=None, condition=condition, section=section,
                        chapter=chapter, name_thing=name_thing, marking=marking, time_wearing=time_wearing,
                        profession=prof, position=pos)
                    work.sizs.add(siz)
