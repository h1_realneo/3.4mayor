from django.core.management.base import BaseCommand
import xlrd
import sys
from company.models import PlaceRegistration


class Command(BaseCommand):

    def handle(self, *args, **options):
        document = xlrd.open_workbook(file_contents=open(sys.path[3] + '/Регистрирующие органы в РБ.xlsx', 'rb').read())
        sheets = document.sheets()
        for sheet in sheets:
            for rownum in range(1, sheet.nrows):
                row = sheet.row_values(rownum)
                PlaceRegistration.objects.create(region=sheet.name, title=row[1].replace('\xa0', ''))