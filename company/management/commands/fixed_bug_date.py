from django.core.management.base import BaseCommand
from company.models import Company, Staff, StaffPosition, StaffProfession
from datetime import datetime


class Command(BaseCommand):
    def handle(self, *args, **options):
        last_company = Company.objects.last()
        staff = Staff.objects.filter(company=last_company)
        pos = StaffPosition.objects.filter(staff__in=staff)
        prof = StaffProfession.objects.filter(staff__in=staff)
        for person in pos:
            if person.date_recruitment:
                date = person.date_recruitment.strftime('%d.%m.%Y')
                fixed_date = date.replace('00', '20')
                fixed_date = datetime.strptime(fixed_date, '%d.%m.%Y')
                person.date_recruitment = fixed_date
                person._add_staff_position = False
                person.save()
        for person in prof:
            if person.date_recruitment:
                date = person.date_recruitment.strftime('%d.%m.%Y')
                fixed_date = date.replace('00', '20')
                fixed_date = datetime.strptime(fixed_date, '%d.%m.%Y')
                person.date_recruitment = fixed_date
                person._add_staff_profession = False
                person.save()