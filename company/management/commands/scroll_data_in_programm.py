# coding: utf8

from django.core.management.base import BaseCommand
import os
import xlwt
from company.additional_info_for_choices import BUILDINGS, PREMISES, EQUIPMENT, MEANS_TRANSPORT, CONSTRUCTIONS
from company.additional_info_for_choices import INTERNAL_ENGINEERING_NETWORKS as IEN
from company.additional_info_for_choices import EXTERNAL_ENGINEERING_NETWORKS as EEN
from work_safety._factors import WORKS_OF_INCREASED_DANGER
ALL_SCROLL = [['Здания', BUILDINGS], ['Помещения', PREMISES], ['Оборудование', EQUIPMENT], ['Транспорт', MEANS_TRANSPORT],
              ['Сооружения', CONSTRUCTIONS], ['Внутренние инженерные сети', IEN], ['Наружные инженерные сети', EEN],
              ['Работы повышенной опасности', WORKS_OF_INCREASED_DANGER]]


class Command(BaseCommand):
    i = 0
    wb = xlwt.Workbook()
    ws = wb.add_sheet('Test')
    font0 = xlwt.Font()
    font0.name = 'Times New Roman'
    font0.bold = True
    style0 = xlwt.XFStyle()
    style0.font = font0
    font0.colour_index = 2
    count_document = 0

    def handle(self, *args, **options):
        count = 0

        for scroll in ALL_SCROLL:
            title_scroll = scroll[0]
            self.ws.write(count, 0, '{}'.format(title_scroll), self.style0)
            count += 1
            y = 1
            for data in scroll[1]:
                if isinstance(data[1], str):
                    self.ws.write(count, 0, '{}'.format(y))
                    self.ws.write(count, 1, '{}'.format(data[1]))
                    y += 1
                    count += 1
                elif isinstance(data[1], tuple):
                    for pack_data in data[1]:
                        self.ws.write(count, 0, '{}'.format(y))
                        self.ws.write(count, 1, '{}'.format(pack_data[1]))
                        y += 1
                        count += 1
        self.wb.save('Нумерация данных загруженных в программу.xls')
