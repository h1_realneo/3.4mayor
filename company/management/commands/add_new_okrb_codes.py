# coding: utf8
import re
from django.core.management.base import BaseCommand
from company.models import WorkProfession, EmployeePosition
import xlrd

path_file_decree33 = 'D:/projects/decree33.xlsx'


class Command(BaseCommand):

    def handle(self, *args, **options):

        document = xlrd.open_workbook(path_file_decree33)
        sheet = document.sheet_by_index(0)
        count_errors = 0
        count_create = 0
        pos = [''.join(re.findall('\w', x[0])).lower() for x in EmployeePosition.objects.values_list('name_post_office_worker')]
        prof = [''.join(re.findall('\w', x[0])).lower() for x in WorkProfession.objects.values_list('name_working_career')]
        pos_id = [(''.join(re.findall('\w', x[1])).lower(), x[0]) for x in EmployeePosition.objects.values_list('id', 'name_post_office_worker')]
        prof_id = [(''.join(re.findall('\w', x[1])).lower(), x[0]) for x in WorkProfession.objects.values_list('id', 'name_working_career')]
        all_codes = []
        not_codes = []
        for row_num in range(2, sheet.nrows):
            row = sheet.row_values(row_num)
            code_group = str(row[0]).strip()
            code = str(row[1]).strip()
            title_norm = str(row[2]).strip()
            title = ''.join(re.findall('\w', str(row[2]))).lower()
            old_code_okrb = str(row[3]).strip()
            ekcd = str(row[4]).strip()
            etks = str(row[7]).strip()
            x = 0
            if row_num > 2 and not etks and not ekcd:
                while not etks and not ekcd:
                    x += 1
                    row_num1 = row_num - x
                    row1 = sheet.row_values(row_num1)
                    ekcd = str(row1[4]).strip()
                    etks = str(row1[7]).strip()

            if code_group and code:
                if title in pos:
                    for posit, id_pos in pos_id:
                        if posit == title:
                            position = EmployeePosition.objects.get(id=id_pos)
                            if code not in position.new_position_code:
                                position.new_position_code.append(code)
                                position.save()
                    all_codes.append(code)
                elif title in prof:
                    for profes, id_prof in prof_id:
                        if profes == title:
                            profession = WorkProfession.objects.get(id=id_prof)
                            if code not in profession.new_profession_code:
                                profession.new_profession_code.append(code)
                                profession.save()
                    all_codes.append(code)
                elif title not in prof and title not in pos:
                    not_codes.append([code, title, title_norm, old_code_okrb, ekcd, etks])
        new_list = {}
        for x, y, z, o, ekcd, etks in not_codes:
            # if x not in all_codes and 'АЭС' not in z and 'судебный эксперт' not in z and 'атомной электростанции' not in z and not o:
            # if x not in all_codes and 'АЭС' in z or 'атомной электростанции' in z and not o:

            # if x not in all_codes and 'производств' in z and not o:
            old_code_okrb = ''
            if x in all_codes:
                old_code_okrb = EmployeePosition.objects.filter(new_position_code__icontains=x).first() or WorkProfession.objects.filter(new_profession_code__icontains=x).first()
                old_code_okrb = getattr(old_code_okrb, 'position_code', None) or getattr(old_code_okrb, 'profession_code', None)
            elif not o:
                old_code_okrb = o

            ekcd = int(float(ekcd)) if ekcd else ''
            etks = int(float(etks)) if etks else ''
            if new_list.get((z, old_code_okrb, ekcd, etks), None):
                list_codes = new_list.get((z, old_code_okrb, ekcd, etks))
                list_codes.append(x)
                new_list.update({(z, old_code_okrb, ekcd, etks): list_codes})

            else:
                new_list.update({(z, old_code_okrb, ekcd, etks): [x]})

        for key, value in new_list.items():
            ekcd = key[2]
            etks = key[3]
            code_okrb = key[1] if key[1] else None
            if not EmployeePosition.objects.filter(name_post_office_worker=key[0]) and not WorkProfession.objects.filter(name_working_career=key[0]):
                if ekcd:
                    EmployeePosition.objects.create(name_post_office_worker=key[0], position_code=code_okrb, ekcd=ekcd)
                else:
                    WorkProfession.objects.create(name_working_career=key[0], profession_code=code_okrb, etkc=etks)
                count_create += 1
            count_errors += 1
            print(key, value)
        print('{} {}'.format(count_errors, count_create))


        # i = 0
        # k = []
        # codes = []
        # for position in EmployeePosition.objects.all():
        #     title = position.name_post_office_worker
        #
        #     if EmployeePosition.objects.filter(name_post_office_worker=title).count() > 1:
        #         if title not in k:
        #             codes.append(position.position_code)
        #             k.append(title)
        #
        #             for pos in EmployeePosition.objects.filter(name_post_office_worker=title):
        #                 if pos.position_code not in codes:
        #                     codes.append(pos.position_code)
        #                     print(title)
        #                     print(pos.new_position_code)

