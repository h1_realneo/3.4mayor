# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2017-04-15 18:49
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0068_genericinfocompany_subordination'),
    ]

    operations = [
        migrations.AlterField(
            model_name='genericinfocompany',
            name='place_registrations',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='company.PlaceRegistration'),
        ),
    ]
