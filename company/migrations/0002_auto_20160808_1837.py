# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-08-08 15:37
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('fire_safety', '0001_initial'),
        ('company', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='documents_answer_no',
            field=models.ManyToManyField(blank=True, related_name='question_no', to='fire_safety.FireDocument'),
        ),
        migrations.AddField(
            model_name='question',
            name='documents_answer_yes',
            field=models.ManyToManyField(blank=True, related_name='question_yes', to='fire_safety.FireDocument'),
        ),
        migrations.AddField(
            model_name='companyquestionanswer',
            name='company',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='question', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='companyquestionanswer',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='company.Question'),
        ),
        migrations.AddField(
            model_name='additionalinfo',
            name='company',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='additional_info', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='companyquestionanswer',
            unique_together=set([('company', 'question')]),
        ),
    ]
