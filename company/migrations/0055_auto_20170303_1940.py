# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2017-03-03 16:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0054_auto_20170303_1840'),
    ]

    operations = [
        migrations.AlterField(
            model_name='newanswer',
            name='next_question',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='next_question', to='company.NewQuestions'),
        ),
    ]
