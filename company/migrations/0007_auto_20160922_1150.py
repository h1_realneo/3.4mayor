# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-09-22 08:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0006_auto_20160908_1253'),
    ]

    operations = [
        migrations.AlterField(
            model_name='companyquestionanswer',
            name='answer',
            field=models.CharField(blank=True, choices=[(None, 'Без ответа'), (True, 'Да'), (False, 'Нет')], max_length=400, null=True),
        ),
    ]
