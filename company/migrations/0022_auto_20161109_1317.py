# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-11-09 10:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0021_auto_20161109_1300'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employeeposition',
            name='categories',
            field=models.CharField(blank=True, max_length=30, verbose_name='категории'),
        ),
        migrations.AlterField(
            model_name='employeeposition',
            name='position_code',
            field=models.IntegerField(blank=True, null=True, verbose_name='код должности'),
        ),
    ]
