# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-11-16 11:16
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0026_auto_20161112_0157'),
    ]

    operations = [
        migrations.AlterField(
            model_name='case',
            name='ablt',
            field=models.CharField(blank=True, max_length=400, verbose_name='Творительный падеж'),
        ),
        migrations.AlterField(
            model_name='case',
            name='accs',
            field=models.CharField(blank=True, max_length=400, verbose_name='Винительный падеж'),
        ),
        migrations.AlterField(
            model_name='case',
            name='datv',
            field=models.CharField(blank=True, max_length=400, verbose_name='Дательный падеж'),
        ),
        migrations.AlterField(
            model_name='case',
            name='gent',
            field=models.CharField(blank=True, max_length=400, verbose_name='Родительный падеж'),
        ),
        migrations.AlterField(
            model_name='case',
            name='loct',
            field=models.CharField(blank=True, max_length=400, verbose_name='Предложный падеж'),
        ),
        migrations.AlterField(
            model_name='case',
            name='nomn',
            field=models.CharField(blank=True, max_length=400, verbose_name='Именительный падеж'),
        ),
        migrations.AlterField(
            model_name='staff',
            name='case',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='company.Case'),
        ),
        migrations.AlterField(
            model_name='staffposition',
            name='case',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='company.Case'),
        ),
        migrations.AlterField(
            model_name='staffposition',
            name='employee_position',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='company.EmployeePosition', verbose_name='Должность'),
        ),
        migrations.AlterField(
            model_name='staffprofession',
            name='case',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='company.Case'),
        ),
        migrations.AlterField(
            model_name='staffprofession',
            name='work_profession',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='company.WorkProfession', verbose_name='Профессия'),
        ),
    ]
