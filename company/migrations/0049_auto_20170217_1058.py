# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2017-02-17 07:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0048_question_division'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='division',
            field=models.CharField(choices=[('fire_safety', 'пожарная безопасноть'), ('work_safety', 'охрана труда')], default='fire_safety', max_length=255),
        ),
    ]
