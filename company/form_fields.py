from django.forms.models import ModelChoiceIterator
from django.forms import ModelChoiceField


class WithoutRepeatQueryModelChoiceIterator(ModelChoiceIterator):

    def __iter__(self):
        if self.field.empty_label is not None:
            yield ("", self.field.empty_label)
        for obj in self.queryset:
            yield self.choice(obj)


class WithoutRepeatQueryModelChoiceField(ModelChoiceField):

    def _get_choices(self):
        if hasattr(self, '_choices'):
            return self._choices
        return WithoutRepeatQueryModelChoiceIterator(self)

    choices = property(_get_choices, ModelChoiceField._set_choices)
