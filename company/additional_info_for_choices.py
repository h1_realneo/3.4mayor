# coding: utf8

BUILDINGS = (
    ('aq', 'Здание'),
    ('aw', 'Здание жилое'),
    ('ae', 'Здание жилое общего назначения'),
    ('ar', 'Здание многоквартирного жилого дома'),
    ('at', 'Здание блокированного жилого дома'),
    ('ay', 'Здание одноквартирного жилого дома'),
    ('au', 'Здание жилое специального назначения'),
    ('ai', 'Здание общежития'),
    ('ao', 'Здание спального корпуса школ-интернатов, детских домов'),
    ('ap', 'Здание дома для престарелых и инвалидов'),
    ('aa', 'Памятник исторический, идентифицированный в основном как жилой дом'),
    ('as', 'Здание нежилое'),
    ('ad', 'Здание специализированное сельскохозяйственного  назначения'),
    ('af', 'Здание специализированное растениеводства'),
    ('ag', 'Здание специализированное животноводства'),
    ('ah', 'Здание специализированное лесохозяйственного назначения'),
    ('aj', 'Здание специализированное для рыбоводства'),
    ('ak', 'Здание специализированное горнодобывающей промышленности'),
    ('al', 'Здание специализированное для добычи и переработки торфа'),
    ('az', 'Здание специализированное для добычи нефти и газа'),
    ('ax', 'Здание горнодобывающей промышленности иного назначения'),
    ('ac', 'Здание специализированное обрабатывающей промышленности'),
    ('av', 'Здание специализированное для производства продуктов питания, включая напитки, и табака'),
    ('ab', 'Здание специализированное для производства текстильных, швейных и кожаных изделий'),
    ('an', 'Здание специализированное для обработки древесины и производства изделий из дерева, включая мебель'),
    ('am', 'Здание специализированное для переработки нефти и газа, производства химических веществ, резиновых и пластмассовых изделий, прочих неметаллических минеральных продуктов'),
    ('bq', 'Здание специализированное для производства строительных материалов'),
    ('bw', 'Здание специализированное для металлургического производства и металлообработки'),
    ('be', 'Здание специализированное для производства машин и оборудования (машиностроения)'),
    ('br', 'Здание специализированное для производства электротехнического, электронного и оптического оборудования'),
    ('bt', 'Здание специализированное для производства транспортных средств и оборудования'),
    ('by', 'Здание обрабатывающей промышленности иного назначения'),
    ('bu', 'Здание специализированное транспорта'),
    ('bi', 'Здание специализированное железнодорожного транспорта'),
    ('bo', 'Здание специализированное автомобильного транспорта'),
    ('bp', 'Здание специализированное городского электрического транспорта'),
    ('ba', 'Здание специализированное трубопроводного транспорта'),
    ('bs', 'Здание специализированное внутреннего водного транспорта'),
    ('bd', 'Здание специализированное воздушного транспорта'),
    ('bf', 'Здание специализированное связи'),
    ('bg', 'Здание специализированное энергетики'),
    ('bh', 'Здание специализированное иного назначения'),
    ('bj', 'Здание специализированное для ремонта и технического обслуживания автомобилей (в том числе автомобильные заправочные и газонаполнительные станции)'),
    ('bk', 'Здание специализированное организаций оптовой торговли, материально-технического снабжения и сбыта продукции'),
    ('bl', 'Здание специализированное розничной торговли'),
    ('bz', 'Здание гостиниц, мотелей, кемпингов'),
    ('bx', 'Здание специализированное здравоохранения и предоставления социальных услуг'),
    ('bc', 'Здание специализированное лечебно-санитарного назначения (морг)'),
    ('bv', 'Здание специализированное для общественного питания'),
    ('bb', 'Здание специализированное складов, торговых баз, баз материально-технического снабжения, хранилищ'),
    ('bn', 'Здание специализированное научно-исследовательских, проектных, конструкторских организаций, информационных центров, общественных организаций и государственных архивов'),
    ('bm', 'Здание административно-хозяйственное'),
    ('cq', 'Здание специализированное для органов государственного управления, обороны, государственной безопасности,внутренних дел'),
    ('cw', 'Здание специализированное для размещения  представительств иностранных государств'),
    ('ce', 'Здание специализированное финансового назначения'),
    ('cr', 'Здание специализированное для образования и воспитания'),
    ('ct', 'Здание специализированное для лечебно-профилактических и санаторно-курортных целей'),
    ('cy', 'Здание специализированное коммунального хозяйства'),
    ('cu', 'Здание специализированное культурно-просветительного и зрелищного назначения'),
    ('ci', 'Здание специализированное религиозного (культового) назначения'),
    ('co', 'Здание специализированное для ритуально-похоронного обслуживания'),
    ('cp', 'Здание специализированное физкультурно-оздоровительного и спортивного назначения'),
    ('ca', 'Здание специализированное для бытового обслуживания населения'),
    ('cs', 'Здание специализированное для оказания ветеринарных услуг'),
    ('cd', 'Здание специализированное для размещения подразделений по чрезвычайным ситуациям'),
    ('cf', 'Здание специализированное для организации азартных игр'),
    ('cg', 'Здание многофункциональное'),
    ('ch', 'Памятник исторический, идентифицированный в основном как нежилой дом'),
    ('cj', 'Садовый, дачный домик (дача)'),
    ('ck', 'Здание неустановленного назначения'),
)
CONSTRUCTIONS = (
    ('q', 'Сооружение специализированное сельскохозяйственного назначения'),
    ('w', 'Сооружение специализированное лесохозяйственного назначения'),
    ('e', 'Сооружение специализированное для рыбоводства'),
    ('r', 'Сооружение специализированное горнодобывающей промышленности'),
    ('t', 'Сооружение специализированное обрабатывающей промышленности'),
    ('y', 'Сооружение специализированное транспорта'),
    ('u', 'Сооружение специализированное железнодорожного транспорта'),
    ('i', 'Сооружение специализированное автомобильного транспорта и автодорожного хозяйства'),
    ('o', 'Сооружение специализированное городского электрического транспорта'),
    ('p', 'Сооружение специализированное трубопроводного транспорта'),
    ('a', 'Сооружение специализированное внутреннего водного транспорта'),
    ('s', 'Сооружение специализированное воздушного транспорта'),
    ('d', 'Сооружение специализированное связи'),
    ('f', 'Сооружение специализированное энергетики'),
    ('g', 'Сооружение специализированное водохозяйственного  назначения'),
    ('h', 'Сооружение специализированное коммунального хозяйства'),
    ('j', 'Сооружение специализированное складов, хранилищ'),
    ('k', 'Сооружение специализированное природоохранного назначения'),
    ('l', 'Сооружение специализированное оздоровительного назначения'),
    ('z', 'Сооружение специализированное рекреационного назначения'),
    ('x', 'Сооружение специализированное историко-культурного назначения'),
    ('c', 'Сооружение специализированное для культурно-просветительного и зрелищного назначения'),
    ('v', 'Сооружение специализированное религиозного (культового) назначения'),
    ('b', 'Сооружение специализированное физкультурно-оздоровительного и спортивного назначения'),
    ('n', 'Сооружение специализированное научного назначения'),
    ('m', 'Сооружение специализированное геодезического назначения'),
    ('aa', 'Сооружение специализированное для органов государственного управления, обороны, государственной безопасности, внутренних дел'),
    ('ss', 'Сооружение специализированное для размещения подразделений по чрезвычайным ситуациям'),
    ('dd', 'Сооружение специализированное гражданской обороны'),
    ('ff', 'Сооружение многофункциональное'),
    ('gg', 'Сооружение неустановленного назначения'),
)

PREMISES = (
    ('q', 'Складское помещение'),
    ('w', 'Помещение образовательного, воспитательного и научного назначения'),
    ('e', 'Помещение санитарно-бытового назначения'),
    ('r', 'Помещение здравоохранения'),
    ('t', 'Помещение общественного питания'),
    ('y', 'Помещение коммунального хозяйства'),
    ('u', 'Помещение для оказания ветеринарных услуг'),
    ('i', 'Помещение культурно-просветительного и зрелищного  назначения'),
    ('o', 'Помещение физкультурно-оздоровительного и спортивного назначения'),
    ('p', 'Помещение транспортного назначения'),
    ('a', 'Вспомогательное (подсобное) помещение, не относящееся к жилищному фонду'),
    ('s', 'Помещение связи'),
    ('d', 'Помещение для размещения представительств иностранных государств'),
    ('f', 'Помещение финансового назначения'),
    ('g', 'Помещение специализированное для органов   государственного управления, обороны, безопасности, внутренних делгосударственной'),
    ('h', 'Помещение для ритуально-похоронного обслуживания'),
    ('j', 'Помещение бытового обслуживания населения'),
    ('k', 'Помещение для размещения подразделений по чрезвычайным ситуациям'),
    ('l', 'Помещение гражданской обороны'),
    ('z', 'Помещение для организации азартных игр'),
    ('x', 'Помещение многофункциональное'),
    ('c', 'Машино-место'),
    ('v', 'Помещение неустановленного назначения'),
)

MEANS_TRANSPORT = (
    ('a', 'Легковой автомобиль'),
    ('s', 'Грузовой автомобиль'),
    ('d', 'Автобусы, микроавтобусы'),
    ('f', 'Автокраны'),
)

EXTERNAL_ENGINEERING_NETWORKS = (
    ('q', 'Наружные сети электроснабжения'),
    ('w', 'Молниезащитные и заземляющие устройства'),
    ('e', 'Наружные сети теплоснабжения'),
    ('r', 'Котельная'),
    ('t', 'Наружные сети водоснабжение'),
    ('y', 'Наружные сети газоснабжения'),
    ('u', 'Водоотвод и канализация'),
    ('i', 'Наружные сети связи'),
)

INTERNAL_ENGINEERING_NETWORKS = (
    ('q', 'Внутренние сети электроснабжения'),
    ('w', 'Внутренние сети теплоснабжения(отопление)'),
    ('e', 'Внутренние сети водоснабжения и канализации'),
    ('r', 'Внутренние сети газоснабжения'),
    ('t', 'Внутренние сети телефонии'),
    ('y', 'Вытяжная вентиляция'),
    ('u', 'Приточная вентиляция'),
    ('i', 'Приточно-вытяжная вентиляция'),
    ('o', 'Система кондиционирования воздуха'),
    ('p', 'Система водяного пожаротушения'),
    ('a', 'Пожарная сигнализация и оповещения о пожаре'),
    ('s', 'Система дымоудаления'),
    ('d', 'Система видеонаблюдения'),
    ('f', 'Система контроля доступа'),
    ('g', 'Компьютерные сети'),
)
EQUIPMENT = (
    ('q', 'Технологическое оборудование'), #1
    ('w', 'Теплогенерирующие аппараты'), #2
    ('e', 'Лестницы-стремянки, приставные лестницы'), #3
    ('r', 'Разгрузочно-погрузочное оборудование'), #4
    ('t', 'Электроинструмент'),#5
    ('y', 'Слесарно-монтажный инструмент'), #6
    ('u', 'Сосуды работающие под давлением'), #7
    ('i', 'Грузоподъемные механизмы и оборудование'), #8
    ('o', 'Строительные леса и подмости'), #9
    ('p', 'Краны'), #10
    ('a', 'Ручные грузовые тележки'), #11
    ('s', 'Пневмоинструмент'),#12
)