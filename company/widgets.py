from django.forms import Select, SelectMultiple


class StaffSelect(Select):

    def __init__(self, *args, **kwargs):
        self.staff_work = kwargs.pop('staff_work', {})
        super(StaffSelect, self).__init__(*args, **kwargs)

    def render_option(self, selected_choices, option_value, option_label):
        if option_value:
            comment = self.staff_work.get(option_value, '')
            option_label = '{} {}'.format(option_label, comment)
        return super(StaffSelect, self).render_option(selected_choices, option_value, option_label)


class StaffPositions(SelectMultiple):

    def __init__(self, *args, **kwargs):
        self.staff_position = kwargs.pop('staff_position', {})
        self.positions = kwargs.pop('positions', {})
        super(StaffPositions, self).__init__(*args, **kwargs)

    def render_option(self, selected_choices, option_value, option_label):
        staff_position = self.staff_position.get(option_value)
        category, derivative, pos_id = staff_position['category'], staff_position['derivative'], staff_position['pos_id']
        position = self.positions[pos_id]
        comment = '{} {} {}'.format(derivative, position, category)
        option_label = '{}'.format(comment)
        return super(StaffPositions, self).render_option(selected_choices, option_value, option_label)


class StaffProfessions(SelectMultiple):

    def __init__(self, *args, **kwargs):
        self.staff_profession = kwargs.pop('staff_profession', {})
        self.professions = kwargs.pop('professions', {})
        super(StaffProfessions, self).__init__(*args, **kwargs)

    def render_option(self, selected_choices, option_value, option_label):
        staff_profession = self.staff_profession.get(option_value)
        rank, prof_id = staff_profession['rank'], staff_profession['prof_id']
        profession = self.professions[prof_id]
        comment = '{} {}'.format(profession, rank)
        option_label = '{}'.format(comment)
        return super(StaffProfessions, self).render_option(selected_choices, option_value, option_label)


class StaffPositionWidget(Select):

    def __init__(self, *args, **kwargs):
        self.staff_position = kwargs.pop('staff_position', {})
        self.positions = kwargs.pop('positions', {})
        super(StaffPositionWidget, self).__init__(*args, **kwargs)

    def render_option(self, selected_choices, option_value, option_label):
        if option_value:
            staff_position = self.staff_position.get(option_value)
            category, derivative, pos_id = staff_position['category'], staff_position['derivative'], staff_position['pos_id']
            position = self.positions[pos_id]
            comment = '{} {} {}'.format(derivative, position, category)
            option_label = '{}'.format(comment)
        return super(StaffPositionWidget, self).render_option(selected_choices, option_value, option_label)


class StaffProfessionWidget(Select):

    def __init__(self, *args, **kwargs):
        self.staff_profession = kwargs.pop('staff_profession', {})
        self.professions = kwargs.pop('professions', {})
        super(StaffProfessionWidget, self).__init__(*args, **kwargs)

    def render_option(self, selected_choices, option_value, option_label):
        if option_value:
            staff_profession = self.staff_profession.get(option_value)
            rank, prof_id = staff_profession['rank'], staff_profession['prof_id']
            profession = self.professions[prof_id]
            comment = '{} {}'.format(profession, rank)
            option_label = '{}'.format(comment)
        return super(StaffProfessionWidget, self).render_option(selected_choices, option_value, option_label)


class StaffPositionPersonWorkWidget(Select):

    def __init__(self, *args, **kwargs):
        self.staff_position = kwargs.pop('staff_position', {})
        self.staff = kwargs.pop('staff', {})
        self.positions = kwargs.pop('positions', {})
        super(StaffPositionPersonWorkWidget, self).__init__(*args, **kwargs)

    def render_option(self, selected_choices, option_value, option_label):
        if option_value:
            staff_position = self.staff_position.get(option_value, {})
            if staff_position:
                category, derivative, pos_id = staff_position['category'], staff_position['derivative'], staff_position['pos_id']
                staff_id = staff_position['staff_id']
                person = self.staff.get(staff_id, {})
                position = self.positions.get(pos_id, '')
                comment = '{} {} - {} {} {}'.format(person.get('surname', ''), person.get('initials', ''), derivative, position, category)
                option_label = '{}'.format(comment)
        return super(StaffPositionPersonWorkWidget, self).render_option(selected_choices, option_value, option_label)
