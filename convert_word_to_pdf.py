import win32com.client as client
import sys
import os
import pythoncom
import threading


def _coInitialize():
    if threading.current_thread().getName() != 'MainThread':
        pythoncom.CoInitialize()

def _coUninitialize():
    if threading.current_thread().getName() != 'MainThread':
        pythoncom.CoUninitialize()

def convert_word_to_pdf():
    _coInitialize()
    word = client.Dispatch('Word.Application')
    doc = word.Documents.Open('C:/Users/skull/Desktop/u/10.docx')
    doc.SaveAs(os.path.abspath('C:/Users/skull/Desktop/u/15.pdf'), FileFormat=17)
    doc.Close()
    word.Quit()
    _coUninitialize()

if __name__ == "__main__":
    convert_word_to_pdf()
