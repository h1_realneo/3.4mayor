'---------------------------------------------------------------------------------
' The sample scripts are not supported under any Microsoft standard support
' program or service. The sample scripts are provided AS IS without warranty
' of any kind. Microsoft further disclaims all implied warranties including,
' without limitation, any implied warranties of merchantability or of fitness for
' a particular purpose. The entire risk arising out of the use or performance of
' the sample scripts and documentation remains with you. In no event shall
' Microsoft, its authors, or anyone else involved in the creation, production, or
' delivery of the scripts be liable for any damages whatsoever (including,
' without limitation, damages for loss of business profits, business interruption,
' loss of business information, or other pecuniary loss) arising out of the use
' of or inability to use the sample scripts or documentation, even if Microsoft
' has been advised of the possibility of such damages.
'---------------------------------------------------------------------------------
Option Explicit 
'################################################
'This script is to convert Excel documents to PDF files
'################################################
Sub main()
Dim ArgCount
ArgCount = WScript.Arguments.Count
Select Case ArgCount 
	Case 1	
		Dim DocPath, objshell, xlObj, xlWB, thisFileName
		DocPath = WScript.Arguments(0)
		Set objshell = CreateObject("scripting.filesystemobject")
		Set xlObj = CreateObject("Excel.Application")
		DocPath = WScript.Arguments(0)
		set xlWB = xlObj.Workbooks.Open(DocPath)
		thisFileName =Left(xlWB.FullName , InStrRev(xlWB.FullName , ".") - 1)
		xlWB.Sheets.Select
		xlWB.ActiveSheet.ExportAsFixedFormat 0, thisFileName & ".pdf", 0, 1, 0,,,0
		xlWB.close False
		xlObj.quit
	Case  Else 
	 	DocPath = WScript.Arguments(0)
End Select 
End Sub 
Call main 