'---------------------------------------------------------------------------------
' The sample scripts are not supported under any Microsoft standard support
' program or service. The sample scripts are provided AS IS without warranty
' of any kind. Microsoft further disclaims all implied warranties including,
' without limitation, any implied warranties of merchantability or of fitness for
' a particular purpose. The entire risk arising out of the use or performance of
' the sample scripts and documentation remains with you. In no event shall
' Microsoft, its authors, or anyone else involved in the creation, production, or
' delivery of the scripts be liable for any damages whatsoever (including,
' without limitation, damages for loss of business profits, business interruption,
' loss of business information, or other pecuniary loss) arising out of the use
' of or inability to use the sample scripts or documentation, even if Microsoft
' has been advised of the possibility of such damages.
'---------------------------------------------------------------------------------

Const xlHtml = 44
Const xlHtml2 = 45

' Create an instance of Excel and open the workbook...
Dim DocPath
Dim objshell,ParentFolder,BaseName,wordapp,doc,HTMLPath, mHTMLPath
Set objshell= CreateObject("scripting.filesystemobject")
DocPath = WScript.Arguments(0)
ParentFolder = objshell.GetParentFolderName(DocPath) 'Get the current folder path
BaseName = objshell.GetBaseName(DocPath) 'Get the document name
HTMLPath = parentFolder & "\" & BaseName & ".html" 
mHTMLPath = parentFolder & "\" & BaseName & ".mhtml" 

Set objExcel = CreateObject("Excel.Application")
objExcel.Workbooks.Open DocPath

' Save the workbook as an HTML or MHTML page...
objExcel.ActiveWorkbook.SaveAs HTMLPath,  xlHtml
' -or-
objExcel.ActiveWorkbook.SaveAs mHTMLPath,  xlHtml2
' Close Excel...
objExcel.Quit