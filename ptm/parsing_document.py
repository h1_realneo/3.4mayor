# coding: utf8
import shutil
import os
import datetime
from calendar import isleap
from django.template import Template, Context
from django.core.exceptions import ObjectDoesNotExist
from company.models import StaffPosition, StaffProfession
from .models import *
from zipfile import ZipFile
from mayor.settings import FINISHED_DOCUMENT, FINISHED_OLD_PATH
from company.models import Staff
from math import fmod
APP = 'ПТМ'
ALPHABET = [chr(x) for x in range(256, 10000) if 'а' <= chr(x) <= 'я' or 'А' <= chr(x) <= 'Я']
# fields and forms for this field

TODAY = datetime.datetime.today()


def parsing(document, company, section='overall', preview=False, date_pd=TODAY, date_dpd=TODAY, date_fire=TODAY):
    context = {}
    answers = company.question.all()
    [context.update({'q_{}'.format(x.question.code): x.answer}) for x in answers]
    try:
        context.update({
            'i': 1,
            'legal_address': company.genericinfocompany.address,
            'address': company.address,
            'dev_scroll': company.safety_health,
            'subordination': company.genericinfocompany.subordination,
            'characteristic': company.genericinfocompany.characteristic,
            'date_pd': date_pd.strftime('%d.%m.%Y'),
            'date_dpd': date_dpd.strftime('%d.%m.%Y'),
            'date_fire': date_fire.strftime('%d.%m.%Y'),
            'date_pd_add': add_years(date_pd, 3).strftime('%d.%m.%Y'),
            'date_dpd_add': add_years(date_dpd, 3).strftime('%d.%m.%Y'),
            'date_fire_add': add_years(date_fire, 1).strftime('%d.%m.%Y'),
            'company_name': '{} {}'.format(company.genericinfocompany.short_ownership,
                                           company.genericinfocompany.short_title),
            'staff': Staff.objects.filter(company=company).order_by('surname'),
            'staff_profession': StaffProfession.objects.filter(staff__in=Staff.objects.filter(company=company)),
            'staff_position': StaffPosition.objects.filter(staff__in=Staff.objects.filter(company=company)),
            'unique_profession': StaffProfession.objects.filter(
                staff__in=Staff.objects.filter(company=company)).distinct('work_profession', 'rank'),
            'unique_position': StaffPosition.objects.filter(
                staff__in=Staff.objects.filter(company=company)).distinct('derivative_position', 'employee_position', 'category'),
            'company_title': company.genericinfocompany.full_title,
            'company_ownership': company.genericinfocompany.ownership,
            'full_name': company.genericinfocompany.full_title,
            'director': company.boss,
            'person_safety_health': company.safety_health,

            'person_development_program': company.development_program,
            'person_development_provision': company.development_provision,
            'pb': get_list_for_ptm(company, 'pb'),
            'dpd': get_list_for_ptm(company, 'dpd'),
            'fire': get_list_for_ptm(company, 'fire')
        })
    except ObjectDoesNotExist:
        pass

    name_document = '{}.docx'.format(document.__str__())
    company_folder_name = ''
    for letter in company.genericinfocompany.short_title:
        if letter in ALPHABET:
            company_folder_name += letter

    folder_for_finished_document = '{0}{1}/{2}'.format(FINISHED_DOCUMENT, company_folder_name, APP)
    if not os.path.exists(folder_for_finished_document):
        os.makedirs(folder_for_finished_document)
    document_word = os.path.abspath('{0}{1}/{2}/{3}'.format(
        FINISHED_DOCUMENT, company_folder_name, APP, name_document)
    )

    if document.path:
        with ZipFile(document.path, 'r') as template_word:
            with ZipFile(document_word, 'w') as new_document_word:
                for item in template_word.infolist():
                    buffer = template_word.read(item.filename)
                    if item.filename == 'word/document.xml' or item.filename == 'word/header1.xml':
                        with template_word.open(item) as render_file:
                            content = str(render_file.read(), 'utf-8')
                            template = Template(content)
                            new_document_word.writestr(item, template.render(Context(context)).encode())
                    else:
                        new_document_word.writestr(item, buffer)

    document_word_old_path = '{0}{1}/{2}/'.format(FINISHED_OLD_PATH, company_folder_name, APP)
    # добавление файла в старые папки
    if not os.path.exists(document_word_old_path):
        os.makedirs(document_word_old_path)
    if document.path and os.path.exists(document_word):
        shutil.copy(document_word, document_word_old_path)
    return 1


def get_list_for_ptm(company, ptm):
    staff_pb = []
    [staff_pb.append(x) for x in StaffProfession.objects.filter(staff__company=company).filter(ptm=ptm)]
    [staff_pb.append(x) for x in StaffPosition.objects.filter(staff__company=company).filter(ptm=ptm)]
    result = []
    one_page = []
    for i, person in enumerate(staff_pb, 1):
        one_page.append(person)
        if fmod(i, 4) == 0:
            result.append(one_page)
            one_page = []
    return result


def add_years(d, years):
    new_year = d.year + years
    try:
        return d.replace(year=new_year)
    except ValueError:
        if (d.month == 2 and d.day == 29 and  # leap day
                isleap(d.year) and not isleap(new_year)):
            return d.replace(year=new_year, day=28)
        raise
