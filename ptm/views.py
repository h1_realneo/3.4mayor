import datetime
from django.views.generic import FormView, TemplateView, UpdateView
from .forms import StaffPositionPTMFormset, StaffProfessionPTMFormset, StaffProfessionPTMForm, PTMGenericForm, \
    PTMBossForm, DataFilingForm
from django.http.response import HttpResponseRedirect
from django.core.urlresolvers import reverse
from company.models import Staff, StaffProfession, StaffPosition, GenericInfoCompany, Company
from .models import PTMDocument
from .parsing_document import parsing


class KindPTM(FormView):
    form_class = StaffProfessionPTMForm
    template_name = 'ptm/kind_ptm.html'

    def get_context_data(self, **kwargs):
        context = super(KindPTM, self).get_context_data(**kwargs)
        company = self.request.user
        staff = [x.id for x in Staff.objects.filter(company=company)]
        prof_formset = StaffProfessionPTMFormset(
            queryset=StaffProfession.objects.filter(staff__id__in=staff), prefix='prof')
        pos_formset = StaffPositionPTMFormset(
            queryset=StaffPosition.objects.filter(staff__id__in=staff), prefix='pos')
        if not context.get('prof_formset') and not context.get('pos_formset'):
            context['prof_formset'] = prof_formset
            context['pos_formset'] = pos_formset
        return context

    def post(self, request, *args, **kwargs):
        company = request.user
        staff = [x.id for x in Staff.objects.filter(company=company)]
        prof_formset = StaffProfessionPTMFormset(
            request.POST, queryset=StaffProfession.objects.filter(staff__id__in=staff), prefix='prof')
        pos_formset = StaffPositionPTMFormset(
            request.POST, queryset=StaffPosition.objects.filter(staff__id__in=staff), prefix='pos')
        if prof_formset.is_valid() and pos_formset.is_valid():
            prof_formset.save()
            pos_formset.save()
            return HttpResponseRedirect(self.get_success_url())
        else:
            return self.render_to_response(
                self.get_context_data(
                    prof_formset=prof_formset, pos_formset=pos_formset
                )
            )

    def get_success_url(self):
        return reverse('ptm:create_ptm')


class InstructionsView(TemplateView):
    template_name = 'ptm/instructions_ptm.html'


class CompanyInfoUpdateView(UpdateView):
    model = GenericInfoCompany
    form_class = PTMGenericForm
    template_name = 'ptm/generic_company.html'

    def get_success_url(self):
        return reverse('ptm:boss_ptm', args=(self.request.user.pk,))


class BossPTMView(UpdateView):
    model = Company
    form_class = PTMBossForm
    template_name = 'ptm/boss_ptm.html'

    def get_success_url(self):
        return reverse('ptm:kind_ptm')


class CreatePTMView(FormView):
    form_class = DataFilingForm
    template_name = 'ptm/create_documents.html'

    def form_valid(self, form):
        today = datetime.datetime.today()
        date_pd = form.cleaned_data['date_pd']
        date_dpd = form.cleaned_data['date_dpd']
        date_fire = form.cleaned_data['date_fire']
        date_pd = datetime.datetime.strptime(date_pd, '%Y-%m-%d') if date_pd else today
        date_dpd = datetime.datetime.strptime(date_dpd, '%Y-%m-%d') if date_dpd else today
        date_fire = datetime.datetime.strptime(date_fire, '%Y-%m-%d') if date_fire else today
        for document in PTMDocument.objects.all():
            parsing(document, company=self.request.user, date_pd=date_pd, date_dpd=date_dpd, date_fire=date_fire)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('ptm:instruction')

