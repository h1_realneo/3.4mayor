# coding: utf8
from django.forms import ModelForm, Form, TextInput, CharField
from django.forms import RadioSelect
from company.models import StaffProfession, StaffPosition, GenericInfoCompany, Company
from django.forms import modelformset_factory
from company.forms import customize_style


class StaffProfessionPTMForm(ModelForm):
    class Meta:
        fields = ['ptm']
        model = StaffProfession
        widgets = {
            'ptm': RadioSelect
        }

    def __init__(self, *args, **kwargs):
        super(StaffProfessionPTMForm, self).__init__(*args, **kwargs)
        if self.initial:
            self['ptm'].help_text = self.instance.staff


class StaffPositionPTMForm(ModelForm):
    class Meta:
        fields = ['ptm']
        model = StaffPosition
        widgets = {
            'ptm': RadioSelect
        }

    def __init__(self, *args, **kwargs):
        super(StaffPositionPTMForm, self).__init__(*args, **kwargs)
        if self.initial:
            self['ptm'].help_text = self.instance.staff


StaffProfessionPTMFormset = modelformset_factory(StaffProfession, fields=('ptm',), extra=0, can_delete=False,
                                                 form=StaffProfessionPTMForm)
StaffPositionPTMFormset = modelformset_factory(StaffPosition, fields=('ptm',), extra=0, can_delete=False,
                                               form=StaffPositionPTMForm)


class PTMGenericForm(ModelForm):
    class Meta:
        model = GenericInfoCompany
        fields = ['ownership', 'full_title', 'short_title']

    def __init__(self, *args, **kwargs):
        super(PTMGenericForm, self).__init__(*args, **kwargs)
        customize_style(self)


class PTMBossForm(ModelForm):
    class Meta:
        model = Company
        fields = ['boss']

    def __init__(self, *args, **kwargs):
        super(PTMBossForm, self).__init__(*args, **kwargs)
        customize_style(self)


class DataFilingForm(Form):
    date_pd = CharField(required=False,
                        label='дата талона для ответственных за ПБ  и инструктаж',
                        widget=TextInput(attrs={'class': 'datepicker_generic_info'}))
    date_dpd = CharField(required=False,
                         label='дата талона для членов ДПД и ПТК',
                         widget=TextInput(attrs={'class': 'datepicker_generic_info'}))
    date_fire = CharField(required=False,
                          label='дата талона по огневым работам',
                          widget=TextInput(attrs={'class': 'datepicker_generic_info'}))
