# coding: utf8
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView
from .views import *

urlpatterns = [
    url(r'^staff/$', KindPTM.as_view(), name='kind_ptm'),
    url(r'^instruction/$', InstructionsView.as_view(), name='instruction'),
    url(r'^generic_ptm/(?P<pk>[0-9]+)/$', CompanyInfoUpdateView.as_view(), name='generic_ptm'),
    url(r'^boss_ptm/(?P<pk>[0-9]+)/$', BossPTMView.as_view(), name='boss_ptm'),
    url(r'^create_ptm/$', CreatePTMView.as_view(), name='create_ptm'),

]
