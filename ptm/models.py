# coding: utf8
from __future__ import unicode_literals
from django.conf import settings
from django.db import models
from company.models import PTM


class PTMDocument(models.Model):
    document_code = models.IntegerField(unique=True, null=True)
    title = models.CharField(max_length=300, default='первый')
    path = models.FilePathField(blank=True, null=True, max_length=300, path=settings.FILE_PATH_PTM,
                                recursive=True)
    section = models.CharField(max_length=30, choices=PTM, blank=True)

    def __str__(self):
        return "{}".format(self.title)

    class Meta:
        verbose_name = 'Документ ПТМ'
        verbose_name_plural = 'Документы ПТМ'
