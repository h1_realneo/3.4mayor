# coding: utf8
from django.forms import ModelForm, ModelChoiceField, ModelMultipleChoiceField, modelformset_factory, BaseModelFormSet, \
    Form, BooleanField
from django.forms.widgets import HiddenInput
from .models import Question, FieldEC, Answer
from fire_safety.forms import customize_style
from django.forms.models import ModelChoiceIterator
from company.models import Staff, StaffProfession, StaffPosition
from django import forms
from company.widgets import StaffSelect
from company.form_fields import WithoutRepeatQueryModelChoiceField


class ClearAnswerForm(Form):
    clear = BooleanField()


class QuestionForm(ModelForm):

    class Meta:
        model = Question
        fields = ('text',)
        widgets = {
            'text': HiddenInput
        }

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company', None)
        super(QuestionForm, self).__init__(*args, **kwargs)
        answers = self.instance.answer.all()
        if self.instance.multi_choice:
            self.fields['answer'] = GroupModelMultipleChoiceField(queryset=answers)
            self.fields['answer'].widget.attrs.update({'class': 'selectpicker_question'})
        else:
            self.fields['answer'] = GroupedModelChoiceField(queryset=answers, empty_label=None)
            self.fields['answer'].widget.attrs.update({'class': 'selectpicker'})
        self.fields['answer'].required = False if self.instance.blank else True
        customize_style(self)


class GroupedModelChoiceField(ModelChoiceField):

    def _get_choices(self):
        if hasattr(self, '_choices'):
            return self._choices
        return GroupedModelChoiceIterator(self)
    choices = property(_get_choices, ModelChoiceField._set_choices)


class GroupedModelChoiceIterator(ModelChoiceIterator):
    def __iter__(self):
        if self.field.empty_label is not None:
            yield (u"", self.field.empty_label)
        queryset = self.queryset.all().order_by('division', '-answer')
        value = [x for x in queryset]
        divisions = []
        for obj in value:
            if obj.division:
                if obj.division not in divisions:
                    divisions.append(obj.division)
                    yield (obj.division, [self.choice(x) for x in value if obj.division == x.division])
            else:
                yield self.choice(obj)


class GroupModelMultipleChoiceField(GroupedModelChoiceField, ModelMultipleChoiceField):
    pass


class CustomQuestionModelFormSet(BaseModelFormSet):

    def __init__(self, *args, **kwargs):
        self.company = kwargs.pop('company', None)
        super(CustomQuestionModelFormSet, self).__init__(*args, **kwargs)

    def clean(self):
        super(CustomQuestionModelFormSet, self).clean()
        answers_company = self.company.answer_set.all().values_list('id')
        answers = [x[0] for x in answers_company]
        for form in self.forms:
            answer = form.cleaned_data['answer']
            if hasattr(answer, 'id'):
                answers.append(answer.id)
            else:
                for answer in form.cleaned_data['answer']:
                    answers.append(answer.id)
        self.company.answer_set = Answer.objects.filter(id__in=answers)


QuestionFormset = modelformset_factory(Question, form=QuestionForm, formset=CustomQuestionModelFormSet, extra=0)


class Base(ModelForm):

    def __init__(self, *args, **kwargs):
        kwargs.pop('queryset', None)
        form_kwargs = kwargs.pop('form_kwargs', None)
        self.label = form_kwargs.get('label', None)
        self.company = form_kwargs.get('company', None)
        self.position_number = form_kwargs.get('position_number', None)
        self.document = form_kwargs.get('document', None)
        self.field_name = form_kwargs.get('field_name', None)
        about_staff = form_kwargs.get('about_staff', None)
        staff_queryset = form_kwargs.get('staff_queryset', None)
        some_pos = form_kwargs.get('some_pos', None)
        some_prof = form_kwargs.get('some_prof', None)
        about_staff.add_mayor()
        staff_work = {}
        staff_profession, staff_position = about_staff.get_staff_professions, about_staff.get_staff_positions
        positions, professions = about_staff.get_positions, about_staff.get_professions
        [staff_work.update({x: about_staff.get_works_read_by_staff(x)}) for x in about_staff.staff_id]
        super(Base, self).__init__(*args, **kwargs)
        if self.fields.get('employee'):
            self.fields['employee'].queryset = staff_queryset
            self.fields['employee'].widget.staff_work = staff_work
            css_class = self.fields['employee'].widget.attrs.get('class')
            css_class = 'employee {}'.format(css_class) if css_class else 'employee'
            self.fields['employee'].widget.attrs.update({'class': css_class})
        if self.fields.get('staff_profession'):
            self.fields['staff_profession'].queryset = some_prof
            self.fields['staff_profession'].widget.attrs.update({'class': 'professions'})
        if self.fields.get('staff_position'):
            self.fields['staff_position'].queryset = some_pos
            self.fields['staff_position'].widget.attrs.update({'class': 'positions'})
        if self.fields.get('positions'):
            self.fields['positions'].queryset = StaffPosition.objects.filter(id__in=about_staff.get_staff_unique_staff_position())
            self.fields['positions'].widget.staff_position = staff_position
            self.fields['positions'].widget.positions = positions
        if self.fields.get('professions'):
            self.fields['professions'].queryset = StaffProfession.objects.filter(id__in=about_staff.get_staff_unique_staff_profession())
            self.fields['professions'].widget.staff_profession = staff_profession
            self.fields['professions'].widget.professions = professions
        if self.fields.get('position'):
            self.fields['position'].queryset = StaffPosition.objects.filter(id__in=about_staff.get_staff_unique_staff_position())
            self.fields['position'].widget.staff_position = staff_position
            self.fields['position'].widget.positions = positions
        if self.fields.get('profession'):
            self.fields['profession'].queryset = StaffProfession.objects.filter(id__in=about_staff.get_staff_unique_staff_profession())
            self.fields['profession'].widget.staff_profession = staff_profession
            self.fields['profession'].widget.professions = professions
        customize_style(self)

    def save(self, commit=True):
        instance = super(Base, self).save(commit)
        if instance:
            if instance.employee:
                positions = instance.employee.staffposition_set.all()
                professions = instance.employee.staffprofession_set.all()
                if instance.staff_profession not in professions and instance.staff_position not in positions:
                    if instance.employee.employee_positions.all():
                        instance.staff_position = instance.employee.staffposition_set.all().first()
                    else:
                        instance.staff_profession = instance.employee.staffprofession_set.all().first()
        return instance


class BaseForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.label = kwargs.pop('label', None)
        self.company = kwargs.pop('company', None)
        self.position_number = kwargs.pop('position_number', None)
        self.document = kwargs.pop('document', None)
        self.field_name = kwargs.pop('field_name', None)
        about_staff = kwargs.pop('about_staff', None)
        staff_queryset = kwargs.pop('staff_queryset', None)
        some_pos = kwargs.pop('some_pos', None)
        some_prof = kwargs.pop('some_prof', None)
        about_staff.add_mayor()
        staff_work = {}
        staff_profession, staff_position = about_staff.get_staff_professions, about_staff.get_staff_positions
        positions, professions = about_staff.get_positions, about_staff.get_professions
        [staff_work.update({x: about_staff.get_works_read_by_staff(x)}) for x in about_staff.staff_id]
        super(BaseForm, self).__init__(*args, **kwargs)
        if self.fields.get('employee'):
            self.fields['employee'].queryset = staff_queryset
            css_class = self.fields['employee'].widget.attrs.get('class')
            css_class = 'employee {}'.format(css_class) if css_class else 'employee'
            self.fields['employee'].widget.attrs.update({'class': css_class})
            self.fields['employee'].widget.staff_work = staff_work
        if self.fields.get('staff_profession'):
            self.fields['staff_profession'].queryset = some_prof
            self.fields['staff_profession'].widget.attrs.update({'class': 'professions'})
        if self.fields.get('staff_position'):
            self.fields['staff_position'].queryset = some_pos
            self.fields['staff_position'].widget.attrs.update({'class': 'positions'})
        if self.fields.get('positions'):
            self.fields['positions'].queryset = StaffPosition.objects.filter(id__in=about_staff.get_staff_unique_staff_position())
            self.fields['positions'].widget.staff_position = staff_position
            self.fields['positions'].widget.positions = positions
        if self.fields.get('professions'):
            self.fields['professions'].queryset = StaffProfession.objects.filter(id__in=about_staff.get_staff_unique_staff_profession())
            self.fields['professions'].widget.staff_profession = staff_profession
            self.fields['professions'].widget.professions = professions
        if self.fields.get('position'):
            self.fields['position'].queryset = StaffPosition.objects.filter(id__in=about_staff.get_staff_unique_staff_position())
            self.fields['position'].widget.staff_position = staff_position
            self.fields['position'].widget.positions = positions
        if self.fields.get('profession'):
            self.fields['profession'].queryset = StaffProfession.objects.filter(id__in=about_staff.get_staff_unique_staff_profession())
            self.fields['profession'].widget.staff_profession = staff_profession
            self.fields['profession'].widget.professions = professions
        customize_style(self)

    def save(self, commit=True):
        instance = super(BaseForm, self).save(commit)
        if instance:
            if instance.employee:
                positions = instance.employee.staffposition_set.all()
                professions = instance.employee.staffprofession_set.all()
                if instance.staff_profession not in professions and instance.staff_position not in positions:
                    if instance.employee.employee_positions.all():
                        instance.staff_position = instance.employee.staffposition_set.all().first()
                    else:
                        instance.staff_profession = instance.employee.staffprofession_set.all().first()
        return instance


class BaseFormSet(BaseModelFormSet):
    def __init__(self, *args, **kwargs):
        self.label = kwargs.get('form_kwargs').get('label')
        self.company = kwargs.get('form_kwargs').get('company')
        self.position_number = kwargs.get('form_kwargs').get('position_number')
        self.document = kwargs.get('form_kwargs').get('document')
        self.field_name = kwargs.get('form_kwargs').get('field_name')
        super(BaseFormSet, self).__init__(*args, **kwargs)
        self.queryset = self.model.objects.filter(
            company=self.company, document=self.document, position_number=self.position_number, field_name=self.field_name
        )
        self.extra = 0 if self.queryset else 1

    def clean(self):
        super(BaseFormSet, self).clean()
        if self.field_name == 'F21':
            week = []
            for form in self.forms:
                days = form.cleaned_data.get('days')
                if days:
                    for day in days:
                        if day in week:
                            form.add_error('days', 'Вы уже указали')
                            break
                        week.append(day)


class F3Form(Base):
    class Meta:
        model = FieldEC
        fields = ['paragraph']
        widgets = {
            "paragraph": forms.Textarea(attrs={'rows': 2}),
            }


class F4Form(Base):
    employee = WithoutRepeatQueryModelChoiceField(
        label='Сотрудник',
        queryset=Staff.objects.none(),
        required=True,
        widget=StaffSelect(attrs={'class': 'select_employee'}),
        error_messages={'required': "Укажите сотрудника"}
    )

    class Meta:
        model = FieldEC
        fields = ['employee', 'staff_profession', 'staff_position']
        field_classes = {
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }


class F41Form(Base):
    class Meta:
        model = FieldEC
        fields = ['employee', 'staff_profession', 'staff_position']
        widgets = {
            'employee': StaffSelect(attrs={'class': 'select_employee'}),
        }
        field_classes = {
            'employee': WithoutRepeatQueryModelChoiceField,
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }


class F7Form(Base):
    class Meta:
        model = FieldEC
        fields = ['date']
        widgets = {
            "date": forms.TextInput(attrs={'class': 'date'}),
            }


class F8Form(Base):
    class Meta:
        model = FieldEC
        fields = ['document_number']


class F11Form(Base):
    class Meta:
        model = FieldEC
        fields = ['trigger']


# Forms for Formsets
class F6Form(BaseForm):

    class Meta:
        model = FieldEC
        fields = ['employee', 'staff_profession', 'staff_position']
        widgets = {
            'employee': StaffSelect(attrs={'class': 'select_employee'}),
        }
        field_classes = {
            'employee': WithoutRepeatQueryModelChoiceField,
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }


class F9Form(BaseForm):
    employee = WithoutRepeatQueryModelChoiceField(
        label='Сотрудник',
        queryset=Staff.objects.none(),
        required=True,
        widget=StaffSelect(attrs={'class': 'select_employee'}),
        error_messages={'required': "Укажите сотрудника"}
    )

    class Meta:
        model = FieldEC
        fields = ['employee', 'staff_profession', 'staff_position']
        field_classes = {
            'staff_profession': WithoutRepeatQueryModelChoiceField,
            'staff_position': WithoutRepeatQueryModelChoiceField,
        }


F6Formset = modelformset_factory(FieldEC, form=F6Form, formset=BaseFormSet, extra=0, can_delete=True)
F9Formset = modelformset_factory(FieldEC, form=F9Form, formset=BaseFormSet, extra=0, can_delete=True)

FORMSETS_PREFIX = ['formset_fio%s' % x for x in range(1, 10)] + \
                  ['agreement1', 'dev_instruction', 'time_briefing_pos', 'time_briefing_prof', 'view_instructions',
                   'subdivisions', 'days', 'subdivision1']


def all_forms():
    return dict(
        F3=F3Form,
        F4=F4Form,
        F41=F41Form,
        F7=F7Form,
        F8=F8Form,
        F11=F11Form,
        # formsets
        F6=F6Formset,
        F9=F9Formset,
    )

