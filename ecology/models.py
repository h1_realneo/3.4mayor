# coding: utf8
from django.db import models
from company.models import Company, StaffPosition, StaffProfession
from django.conf import settings
from django.core.files.storage import FileSystemStorage
import os


class Question(models.Model):
    text = models.CharField(max_length=255)
    blank = models.BooleanField(default=False)
    multi_choice = models.BooleanField(default=False)

    def __str__(self):
        return '{}'.format(self.text)


class Answer(models.Model):
    answer_number = models.IntegerField(null=True)
    answer = models.CharField(max_length=255)
    company = models.ManyToManyField(Company, blank=True)
    question = models.ForeignKey(Question, related_name='answer', blank=True, null=True)
    division = models.CharField(max_length=255, blank=True)
    related_fields = models.ManyToManyField('FieldInDocument', blank=True)
    related_questions = models.ManyToManyField(Question, related_name='related_q', blank=True)

    def __str__(self):
        return '{}'.format(self.answer)


class ECDocument(models.Model):

    NOTHING = 'else'
    OT_SCROLL = 'scroll'
    OT_ORDER = 'order'
    OT_INSTRUCTION = 'instruction'
    OT_PROGRAM = 'program'
    OT_PROVISION = 'provision'
    OT_PROTOCOL = 'protocol'
    SECTIONS_DOC = (
        (NOTHING, 'Документы'),
        (OT_SCROLL, 'Перечень'),
        (OT_ORDER, 'Приказ'),
        (OT_INSTRUCTION, 'Инструкция'),
        (OT_PROGRAM, 'Программа'),
        (OT_PROVISION, 'Положение'),
        (OT_PROTOCOL, 'Протокол'),
    )

    document_code = models.IntegerField(unique=True, null=True)
    related_fields = models.ManyToManyField('FieldInDocument', blank=True, related_name='related_field')
    title = models.CharField(max_length=300, default='первый')
    path = models.FilePathField(blank=True, null=True, max_length=300, path=settings.FILE_PATH_ECOLOGY,
                                recursive=True)
    section_document = models.CharField(max_length=100, choices=SECTIONS_DOC, default=NOTHING, blank=True)
    default_document = models.BooleanField('Стандартно необходимый документ', default=False)

    def __str__(self):
        return "{}".format(self.title)

    class Meta:
        verbose_name = 'Документы экологии'
        verbose_name_plural = 'Документы экологии'


class FieldInDocument(models.Model):
    F3 = 'F3'
    F4 = 'F4'
    F41 = 'F41'
    F6 = 'F6'
    F7 = 'F7'
    F8 = 'F8'
    F9 = 'F9'
    F11 = 'F11'
    FIELDS = (
        (F3, 'F3 дополнительный параграф'),
        (F4, 'F4 фио'),
        (F41, 'F41 фио или ничего'),
        (F6, 'F6 согласовавшие'),
        (F7, 'F7 дата документа'),
        (F8, 'F8 номер документа'),
        (F9, 'F9 фио формсет'),
        (F11, 'F11 да нет'),
    )
    document = models.ForeignKey(ECDocument, related_name='fields')
    label = models.TextField(max_length=300, blank=True)
    field_name = models.CharField(max_length=100, choices=FIELDS, default=F41)
    prefix = models.CharField(max_length=100, blank=True)
    position_number = models.IntegerField(blank=True, null=True)
    index_number = models.IntegerField('Порядковый номер поля на форме', blank=True, null=True)
    required_field = models.BooleanField('Необходимое поле?', blank=True, default=False)
    element_chain = models.BooleanField('Поле для цепочки вопросов', blank=True, default=False)

    def __str__(self):
        return '{}, {}, {}, {}'.format(self.document.title, self.field_name, self.prefix, self.label)

    class Meta:
        verbose_name = 'Поля в документах'
        verbose_name_plural = 'Поля в документах'


class FieldEC(models.Model):

    field_name = models.CharField(max_length=255, choices=FieldInDocument.FIELDS, default=FieldInDocument.FIELDS[0])
    company = models.ForeignKey('company.Company', blank=True, null=True)
    document = models.ForeignKey(ECDocument, blank=True, null=True)
    position_number = models.IntegerField(blank=True, null=True)
    label = models.CharField(max_length=255, blank=True)
    # field's name
    employee = models.ForeignKey('company.Staff', blank=True, null=True, verbose_name='сотрудник')
    date = models.CharField('дата подписи', max_length=255, blank=True)
    document_number = models.CharField('номер документа', max_length=255, blank=True)
    paragraph = models.TextField('дополнительный пункт', max_length=800, blank=True)
    trigger = models.BooleanField('Да Нет', max_length=255, blank=True, default=False)
    # if employee have some professions in this field we save one of them that use in this field
    staff_profession = models.ForeignKey(StaffProfession, blank=True, null=True, verbose_name='профессия')
    staff_position = models.ForeignKey(StaffPosition, blank=True, null=True, verbose_name='должность')

    class Meta:
        verbose_name = 'Поле'
        verbose_name_plural = 'Поля'

    def __str__(self):
        return '{} {} {} {}'.format(self.company, self.document, self.field_name, self.position_number)


class ECStorage(FileSystemStorage):
    def get_available_name(self, name, max_length=None):
        name = name.replace('_', ' ')
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name


fs = ECStorage(location=settings.MEDIA_ROOT)


def ec_document_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/company_<id>/ec_<section>/document_<id>/<filename>
    return 'ec/{}/{}/{}/{}'.format(
        instance.company.id, instance.ec_document.section_document, instance.ec_document.id, filename
    )


class ReadyECDocument(models.Model):
    company = models.ForeignKey(Company)
    ec_document = models.ForeignKey(ECDocument)
    pdf_file = models.FileField(upload_to=ec_document_directory_path, storage=fs, max_length=350)
    word_file = models.FileField(upload_to=ec_document_directory_path, blank=True)

    def __str__(self):
        return '{} {}'.format(self.company, self.ec_document)
