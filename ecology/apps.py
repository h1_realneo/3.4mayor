from django.apps import AppConfig


class EcologyConfig(AppConfig):
    name = 'ecology'
