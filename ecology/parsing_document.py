# coding: utf8
import shutil
import re

from subprocess import Popen
from django.template import Template, Context
from django.core.exceptions import ObjectDoesNotExist

from .models import *
from zipfile import ZipFile
from mayor.settings import WINWORD_PATH, FINISHED_DOCUMENT, FINISHED_OLD_PATH
from company.models import Staff
from django.core.files import File
from company.additional_info_for_choices import CONSTRUCTIONS, BUILDINGS, PREMISES, EQUIPMENT, MEANS_TRANSPORT

APP = 'экология'
ALPHABET = [chr(x) for x in range(256, 10000) if 'а' <= chr(x) <= 'я' or 'А' <= chr(x) <= 'Я']
# fields and forms for this field
FIELDS_FORMS = {
    'F1': ['F1'], 'F2': ['F2'], 'F3': ['F3'], 'F4': ['F4', 'F41'], 'F5': ['F5'], 'F6': ['F6'], 'F7': ['F7'],
    'F8': ['F8'], 'F9': ['F9'], 'F10': ['F10'], 'F11': ['F11'], 'F12': ['F12'], 'F13': ['F13'], 'F14': ['F14'],
    'F15': ['F15'], 'F16': ['F16'], 'F17': ['F17'], 'F18': ['F18'], 'F19': ['F19'], 'F20': ['F20'], 'F21': ['F21'],
    'F22': ['F22'], 'F23': ['F23']
}


def parsing(document, company, section='overall', preview=False):
    context = {}
    if isinstance(document, ECDocument):
        for name_model, forms in FIELDS_FORMS.items():
            context.update({name_model.lower(): FieldEC.objects.filter(
                document=document, company=company, field_name__in=forms)
            })
    print(document)
    related_context = get_related_context(document, company)
    context.update(related_context)

    type_ownership = get_type_ownership(company)
    safety_health = get_person_safety_health(company)
    authorized = get_authorized(company)
    try:
        context.update({
            'i': 1,
            'legal_address': company.genericinfocompany.address,
            'address': company.address,
            'transport': company.genericinfocompany.means_transport,
            'provisions': company.work_documents.all().filter(section_document='provision'),
            'ownership': type_ownership,
            'first_authorized': authorized[0],
            'second_authorized': authorized[1],
            'number_protocol': authorized[2],
            'person_authorized': authorized[3],
            'safety_health': safety_health[0],
            'company_ot': safety_health[1],
            'company_id': company.id,
            'dev_scroll': company.safety_health,
            'subordination': company.genericinfocompany.subordination,
            'characteristic': company.genericinfocompany.characteristic,

            'short_ownership': '{}'.format(company.genericinfocompany.short_ownership),
            'company_name': '{} {}'.format(company.genericinfocompany.short_ownership,
                                           company.genericinfocompany.short_title),
            'company_short_case': company.genericinfocompany.short_title_case,
            'staff': Staff.objects.filter(company=company).order_by('surname'),
            'staff_profession': StaffProfession.objects.filter(staff__in=Staff.objects.filter(company=company)),
            'staff_position': StaffPosition.objects.filter(staff__in=Staff.objects.filter(company=company)),
            'unique_profession': StaffProfession.objects.filter(
                staff__in=Staff.objects.filter(company=company)).distinct('work_profession', 'rank'),
            'unique_position': StaffPosition.objects.filter(
                staff__in=Staff.objects.filter(company=company)).distinct('derivative_position', 'employee_position', 'category'),
            'company_title': company.genericinfocompany.full_title,
            'company_ownership': company.genericinfocompany.ownership,
            'full_name': company.genericinfocompany.full_title,
            'director': company.boss,
            'person_safety_health': company.safety_health,
            'person_development_program': company.development_program,
            'person_development_provision': company.development_provision,
        })
        answers = {}
        [answers.update({'a_{}'.format(a.answer_number): a}) for a in Answer.objects.filter(company=company)]
        context.update(answers)
    except ObjectDoesNotExist:
        pass

    name_document = '{}.docx'.format(document.__str__())
    company_folder_name = ''
    for letter in company.genericinfocompany.short_title:
        if letter in ALPHABET:
            company_folder_name += letter

    folder_for_finished_document = '{0}{1}/{2}'.format(FINISHED_DOCUMENT, company_folder_name, section)
    if not os.path.exists(folder_for_finished_document):
        os.makedirs(folder_for_finished_document)
    document_word = os.path.abspath('{0}{1}/{2}/{3}'.format(
        FINISHED_DOCUMENT, company_folder_name, section, name_document)
    )
    with ZipFile(document.path, 'r') as template_word:
        with ZipFile(document_word, 'w') as new_document_word:
            for item in template_word.infolist():
                buffer = template_word.read(item.filename)
                if item.filename == 'word/document.xml' or item.filename == 'word/header1.xml':
                    with template_word.open(item) as render_file:
                        content = str(render_file.read(), 'utf-8')
                        template = Template(content)
                        new_document_word.writestr(item, template.render(Context(context)).encode())
                else:
                    new_document_word.writestr(item, buffer)

    document_pdf = os.path.abspath(document_word.replace('docx', 'pdf'))
    if os.path.exists(document_pdf):
        os.remove(document_pdf)
    if os.path.exists(document_word):
        import requests
        import requests.exceptions as exc
        with open(document_word, 'rb') as docx:
            try:
                res = requests.post(
                    url='http://127.0.0.2:9016/v1/00000000-0000-0000-0000-000000000000/convert',
                    data=docx,
                    headers={'Content-Type': 'application/octet-stream'}
                )
                f = open(document_pdf, 'wb')
                f.write(res.content)
                print('Документ сформирован по новому -- {}'.format(document))
            except exc.ConnectionError:
                process = Popen('"{}" "{}" /mExportToPDFext /q'.format(WINWORD_PATH, document_word), shell=True)
                process.wait()
                print('Документ сформирован по старому -- {}'.format(document))
    else:
        return

    if os.path.exists(document_pdf):
        with open(document_pdf, 'rb') as f:
            if isinstance(document, ECDocument):
                ready = ReadyECDocument.objects.filter(company=company, ec_document=document).first()
                if ready:
                    ready.pdf_file.delete()
                else:
                    ready = ReadyECDocument.objects.create(company=company, ec_document=document)
                ready.pdf_file.save('{}.pdf'.format(document.__str__()), File(f))
    rus_section = [x[1] for x in ECDocument.SECTIONS_DOC if x[0] == section]
    rus_section = rus_section[0].replace(':', '/')
    document_word_old_path = '{0}{1}/{2}/{3}'.format(FINISHED_OLD_PATH, company_folder_name, APP, rus_section)
    # добавление файла в старые папки
    if not os.path.exists(document_word_old_path):
        os.makedirs(document_word_old_path)
    shutil.copy(document_word, document_word_old_path)
    return 1


def get_related_context(document, company):
    related_context = {}
    fields = document.related_fields.all()
    if fields:
        for field in fields:
            document = field.document
            field_name = field.field_name
            position_number = field.position_number
            related_data = FieldEC.objects.filter(
                company=company, document=document, position_number=position_number, field_name=field_name)
            related_context.update(
                {'{}_{}_{}'.format(document.document_code, field_name, position_number).lower(): related_data}
            )
        return related_context
    return related_context


def normalize_title(title):
    short_title = ''
    for letter in title:
        if letter in ALPHABET:
            short_title += letter
    return short_title


def get_type_ownership(company):
    not_society = ['чуп']
    for x in not_society:
        if re.findall(r'\W{0}\W'.format(x), company.full_title.lower()):
            return 'Предприятие'
    return 'Общество'


def get_person_safety_health(company):
    company = company.safety_health.staff.company
    company_name = '{} {}'.format(company.genericinfocompany.short_ownership, company.genericinfocompany.short_title)
    return [company.safety_health, company_name]


def get_authorized(company):
    person_authorized = company.person_authorized
    if person_authorized:
        person_authorized = person_authorized.staffposition_set.first() or person_authorized.staffprofession_set.first()
    if person_authorized and company.personauthorizedcompany.chairperson:
        return ['Председатель ППО',  company.genericinfocompany.short_title_case.gent, '', person_authorized]
    elif person_authorized and company.personauthorizedcompany.public_inspector:
        return ['Общественный инспектор по охране',  'труда профсоюзной организации', '', person_authorized]
    elif person_authorized and company.personauthorizedcompany.protocol_number:
        return ['Протокол заседания',  'Профсоюзного комитета', '№ {}'.format(company.personauthorizedcompany.protocol_number), '']
    elif person_authorized:
        return ['Уполномоченное лицо работников',  company.genericinfocompany.short_title_case.gent, '', person_authorized]
    else:
        return ['',  '', '', '']
