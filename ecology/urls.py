# coding: utf8
from django.conf.urls import url
from .views import QuestionView, ClearAnswerView, ECInstructionView, DocumentCreate, DocumentList


urlpatterns = [
    url(r'^question/$', QuestionView.as_view(), name='question'),
    url(r'^clear_answer/$', ClearAnswerView.as_view(), name='clear_answer'),
    url(r'^instruction/$', ECInstructionView.as_view(), name='instruction'),
    url(r'^list/$', DocumentList.as_view(), name='list',),
    url(r'^create_document/(?P<pk>[0-9]+)/$', DocumentCreate.as_view(), name='create_document',),

]
