from django.shortcuts import HttpResponseRedirect
from django.views.generic import FormView, TemplateView, CreateView, ListView
from .forms import QuestionFormset, ClearAnswerForm, all_forms, FORMSETS_PREFIX
from .models import Question, Answer, FieldEC, ECDocument, ReadyECDocument, FieldInDocument
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from company._utility import AllAboutStaff
from company.models import Staff, StaffPosition, StaffProfession
from .parsing_document import parsing
from django.http import Http404
from django.db.models import Q


def get_question(answer, answered_question, answer_related_question):
    related_question = answer_related_question.get(answer.id, [])
    if not related_question:
        return related_question
    else:
        all_question_id = []
        for question in related_question:
            if question not in answered_question:
                all_question_id.append(question)
        return all_question_id


class QuestionView(FormView):
    question_queryset = Question.objects.none()
    form_class = QuestionFormset
    template_name = 'ecology/question.html'

    def get_form_kwargs(self):
        kwargs = super(QuestionView, self).get_form_kwargs()
        answer_company = Answer.objects.filter(company=self.request.user)
        all_questions = Question.objects.all()
        answer_related_question = {}
        answered_question = [answer.question_id for answer in answer_company]
        answer_rel_question = [x for x in Answer.related_questions.through.objects.only('question_id', 'answer_id')]
        for answer_question in answer_rel_question:
            answer = answer_question.answer_id
            questions = answer_related_question.get(answer, [])
            questions.append(answer_question.question_id)
            answer_related_question.update({answer: questions})
        related_questions = [x.question_id for x in answer_rel_question]
        questions_queryset = []
        if not answer_company:
            for question in all_questions:
                if question.id not in related_questions:
                    questions_queryset.append(question.id)
        else:
            for answer in answer_company:
                questions_queryset += get_question(answer, answered_question, answer_related_question)
        queryset = Question.objects.filter(id__in=questions_queryset)
        self.question_queryset = queryset
        kwargs['queryset'] = queryset
        kwargs['company'] = self.request.user
        kwargs['form_kwargs'] = {'company': self.request.user}
        return kwargs

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()
        if not self.question_queryset:
            return HttpResponseRedirect(reverse('ecology:clear_answer'))
        return self.render_to_response(context)

    def get_success_url(self):
        return reverse('ecology:question')


class ClearAnswerView(FormView):
    form_class = ClearAnswerForm
    template_name = 'ecology/clear_answer.html'

    def form_valid(self, form):
        if form.cleaned_data['clear']:
            self.request.user.answer_set.clear()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('ecology:question')


@method_decorator(login_required, name='dispatch')
class ECInstructionView(TemplateView):
    template_name = 'ecology/instruction.html'


@method_decorator(login_required, name='dispatch')
class DocumentCreate(CreateView):
    template_name = 'ecology/create_document.html'
    all_forms = all_forms()
    form_kwargs = None
    forms_instances = None
    object = None
    prefix = None
    document = None
    about_staff = None
    formsets_prefix = FORMSETS_PREFIX
    staff = None
    some_pos = None
    some_prof = None

    def get_context_data(self, **kwargs):
        context = super(DocumentCreate, self).get_context_data(**kwargs)
        context['document'] = self.document
        context['staff'] = Staff.objects.filter(company=self.request.user)
        context['staff_work'] = self.about_staff.get_staff_works_read()
        context['formset'] = self.formsets_prefix
        context['method'] = self.request.method
        context['ready_document'] = ReadyECDocument.objects.filter(
            company=self.request.user, ec_document=context['document']).first()
        return context

    def get_forms(self, render_fields=None):
        render_forms = []
        if render_fields is None:
            return
        exclude_fields = []
        if self.document == ECDocument.objects.first():
            field_related_answer = FieldInDocument.objects.filter(answer__isnull=False).values_list('id')
            exclude_fields = [x[0] for x in field_related_answer]
            field_company = Answer.objects.filter(company=self.request.user).filter(related_fields__isnull=False).values('id', 'related_fields')
            [exclude_fields.remove(x['related_fields']) for x in field_company if x['related_fields'] in exclude_fields]
            if self.request.method == "GET":
                self.delete_data_exclude_fields(FieldInDocument.objects.filter(id__in=exclude_fields))
        fields = [(x.field_name, x.position_number, x.prefix, x.label) for x in render_fields if x.id not in exclude_fields]
        [self.forms_instances.update({(x.field_name, x.position_number): x}) for x in
         FieldEC.objects.filter(company=self.request.user, document=self.document)]
        for field in fields:
            form_name, position, prefix, label, name = field[0], field[1], field[2], field[3], field[0]
            self.prefix = prefix
            self.form_kwargs = dict(
                document=self.document,
                company=self.request.user,
                field_name=name,
                position_number=position,
                label=label,
                about_staff=self.about_staff,
                staff_queryset=self.staff,
                some_pos=self.some_pos,
                some_prof=self.some_prof
            )
            form = self.get_form(form_name)
            render_forms.append(form)
        return render_forms

    def get_form(self, form_name=None):
        if form_name:
            form_class = self.all_forms.get(form_name)
            return form_class(**self.get_form_kwargs())
        return

    def delete_data_exclude_fields(self, exclude_fields):
        fields = exclude_fields.values_list('document_id', 'field_name', 'position_number')
        if fields:
            company = self.request.user
            queries = [Q(company=company, document=f[0], field_name=f[1], position_number=f[2]) for f in fields]
            # Take one Q object from the list
            query = queries.pop()
            # Or the Q object with the ones remaining in the list
            for item in queries:
                query |= item
            FieldEC.objects.filter(query).delete()

    def get_form_kwargs(self):
        form_name = self.form_kwargs.get('field_name')
        form = self.all_forms.get(form_name)
        kwargs = dict(prefix=self.prefix, form_kwargs=self.form_kwargs)
        if form and self.request.method == 'POST':
            kwargs.update({'data': self.request.POST})
        if not hasattr(form, 'management_form'):
            instance = self.forms_instances.get(
                (self.form_kwargs['field_name'], self.form_kwargs['position_number']), None)
            kwargs.update({'instance': instance})
        return kwargs

    def get(self, request, *args, **kwargs):
        self.forms_instances = {}
        self.about_staff = AllAboutStaff(self.request.user).add_mayor()
        self.staff = Staff.objects.filter(id__in=self.about_staff.staff_id).order_by('surname')
        self.some_prof = StaffProfession.objects.filter(staff_id__in=self.about_staff.get_staff_some_work())
        self.some_pos = StaffPosition.objects.filter(staff_id__in=self.about_staff.get_staff_some_work())
        document = ECDocument.objects.filter(id=int(kwargs.get('pk', -1))).first()
        fields = document.fields.all().order_by('index_number') if document else kwargs.pop('fields', None)
        if not fields and not document:
            raise Http404
        self.document = fields.first().document if fields else document
        render_forms = self.get_forms(render_fields=fields)
        return self.render_to_response(
            self.get_context_data(form=render_forms)
        )

    def post(self, request, *args, **kwargs):
        self.forms_instances = {}
        self.about_staff = AllAboutStaff(self.request.user).add_mayor()
        self.staff = Staff.objects.filter(id__in=self.about_staff.staff_id).order_by('surname')
        self.some_prof = StaffProfession.objects.filter(staff_id__in=self.about_staff.get_staff_some_work())
        self.some_pos = StaffPosition.objects.filter(staff_id__in=self.about_staff.get_staff_some_work())
        errors_in_forms = False
        company = self.request.user
        document = ECDocument.objects.filter(id=int(kwargs.get('pk', -1))).first()
        fields = document.fields.all().order_by('index_number') if document else kwargs.pop('fields', None)
        if not fields and not document:
            raise Http404
        self.document = fields.first().document if fields else document
        if document and not fields:
            response = parsing(
                document=document, company=self.request.user, section=document.section_document, preview=True
            )
            if response:
                return HttpResponseRedirect(reverse('ecology:create_document', kwargs={'pk': document.pk}))
        render_forms = self.get_forms(render_fields=fields)
        if render_forms:
            for form in render_forms:
                if not form.is_valid():
                    errors_in_forms = True
            if not errors_in_forms:
                for form in render_forms:
                    if not hasattr(form, 'management_form'):
                        instance = form.save(commit=False)
                        instance.company = company
                        instance.document = self.document
                        instance.position_number = form.position_number
                        instance.field_name = form.field_name
                        instance.label = form.label
                        instance.save()
                        form.save_m2m()
                    else:
                        instances = form.save(commit=False)
                        [obj.delete() for obj in form.deleted_objects]
                        for cls in instances:
                            cls.company = company
                            cls.document = self.document
                            cls.position_number = form.position_number
                            cls.field_name = form.field_name
                            cls.label = form.label
                            cls.save()
                        form.save_m2m()
            else:
                return self.form_invalid(render_forms)
            if not document:
                success_url = kwargs.get('success_url')
                return HttpResponseRedirect(success_url)
            response = parsing(document=document, company=self.request.user, section=document.section_document, preview=True)
            if response:
                return HttpResponseRedirect(reverse('ecology:create_document', kwargs={'pk': document.pk}))
        return HttpResponseRedirect(reverse('ecology:create_document', kwargs={'pk': document.pk}))


class DocumentList(ListView):
    model = ECDocument
    template_name = 'ecology/documents_list.html'

    def get_context_data(self, **kwargs):
        context = super(DocumentList, self).get_context_data(**kwargs)
        document_ready = []
        documents = ECDocument.objects.all()
        for document in documents:
            if ReadyECDocument.objects.filter(ec_document=document, company=self.request.user):
                document_ready.append([document, True])
            else:
                document_ready.append([document, False])
        context['documents'] = document_ready
        return context
