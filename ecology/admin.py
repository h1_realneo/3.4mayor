from django.contrib import admin
from .models import *
from django import forms
admin.site.register(FieldEC)
admin.site.register(Question)


@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = ('answer', 'answer_number', 'question')


@admin.register(ECDocument)
class ECDocumentAdmin(admin.ModelAdmin):
    filter_horizontal = ('related_fields',)
    ordering = ['section_document', 'title']
    list_display = ('document_code', 'title', 'section_document', 'default_document')
    actions = ['make_default_document_true']

    class Media:
        css = {
            'all': ('work_safety/resize-widget.css',),
        }

    def make_default_document_true(self, request, queryset):
        for field in queryset:
            field.default_document = True
            field.save()
    make_default_document_true.short_description = "Make default doc True"


@admin.register(FieldInDocument)
class FieldInDocumentAdmin(admin.ModelAdmin):
    ordering = ['document', 'field_name']
    list_display = ('document', 'field_name', 'prefix', 'label', 'position_number', 'element_chain')
    actions = ['make_element_chain_true']

    def make_element_chain_true(self, request, queryset):
        for field in queryset:
            field.element_chain = True
            field.save()
    make_element_chain_true.short_description = "Make element chain field True"
