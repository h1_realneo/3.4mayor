"""mayor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import View, TemplateView, DetailView
from django.http import HttpRequest, HttpResponse
from fire_safety.models import FireDocument, ReadyDocument
from company.models import Company
from django.shortcuts import get_object_or_404
from django.contrib.auth.mixins import UserPassesTestMixin
from work_safety.models import OTDocument, ReadyOTDocument
from electrical_safety.models import ElSafetyDocument, ReadyElSafetyDocument
from ecology.models import ReadyECDocument, ECDocument
import debug_toolbar


class DocumentView(UserPassesTestMixin, DetailView):
    content_type = 'application/pdf'
    model = Company

    def get(self, request, *args, **kwargs):
        obj = self.get_object()
        document = get_object_or_404(FireDocument, id=int(kwargs.get('document_id', 1000)))
        ready_document = get_object_or_404(ReadyDocument, company=obj, fire_document=document)
        return HttpResponse(ready_document.pdf_file.file, content_type="application/pdf")

    def test_func(self):
        return self.request.user.pk == int(self.kwargs.get('pk', None))


class DocumentOTView(UserPassesTestMixin, DetailView):
    content_type = 'application/pdf'
    model = Company

    def get(self, request, *args, **kwargs):
        obj = self.get_object()
        document = get_object_or_404(OTDocument, id=int(kwargs.get('document_id', 1000)))
        ready_document = get_object_or_404(ReadyOTDocument, company=obj, ot_document=document)
        return HttpResponse(ready_document.pdf_file.file, content_type="application/pdf")

    def test_func(self):
        return self.request.user.pk == int(self.kwargs.get('pk', None))


class DocumentELView(UserPassesTestMixin, DetailView):
    content_type = 'application/pdf'
    model = Company

    def get(self, request, *args, **kwargs):
        obj = self.get_object()
        document = get_object_or_404(ElSafetyDocument, id=int(kwargs.get('document_id', 1000)))
        ready_document = get_object_or_404(ReadyElSafetyDocument, company=obj, el_document=document)
        return HttpResponse(ready_document.pdf_file.file, content_type="application/pdf")

    def test_func(self):
        return self.request.user.pk == int(self.kwargs.get('pk', None))


class DocumentECView(UserPassesTestMixin, DetailView):
    content_type = 'application/pdf'
    model = Company

    def get(self, request, *args, **kwargs):
        obj = self.get_object()
        document = get_object_or_404(ECDocument, id=int(kwargs.get('document_id', 1000)))
        ready_document = get_object_or_404(ReadyECDocument, company=obj, ec_document=document)
        return HttpResponse(ready_document.pdf_file.file, content_type="application/pdf")

    def test_func(self):
        return self.request.user.pk == int(self.kwargs.get('pk', 1000))

urlpatterns = [
    url(r'^__debug__/', include(debug_toolbar.urls)),
    url(r'^$',  TemplateView.as_view(template_name="base.html"), name='home'),
    url(r'^company/', include('company.urls', namespace='company')),
    url(r'^fire/', include('fire_safety.urls', namespace='fire')),
    url(r'^ot/', include('work_safety.urls', namespace='work-safety')),
    url(r'^electrical/', include('electrical_safety.urls', namespace='electrical_safety')),
    url(r'^ecology/', include('ecology.urls', namespace='ecology')),
    url(r'^ptm/', include('ptm.urls', namespace='ptm')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^media/fire/(?P<pk>[0-9]+)/(?P<section>[\w-]+)/(?P<document_id>[\w-]+)/', DocumentView.as_view(), name='doc'),
    url(r'^media/ot/(?P<pk>[0-9]+)/(?P<section>[\w-]+)/(?P<document_id>[\w-]+)/', DocumentOTView.as_view(), name='doc_ot'),
    url(r'^media/electrical/(?P<pk>[0-9]+)/(?P<section>[\w-]+)/(?P<document_id>[\w-]+)/', DocumentELView.as_view(), name='doc_el'),
    url(r'^media/ec/(?P<pk>[0-9]+)/(?P<section>[\w-]+)/(?P<document_id>[\w-]+)/', DocumentECView.as_view(), name='doc_ec'),
]


