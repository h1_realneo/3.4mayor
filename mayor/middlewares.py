from django.shortcuts import redirect
from django.core.urlresolvers import reverse

ALLOWED_URLs = [
    reverse('company:list'),
    reverse('company:login'),
    reverse('company:add'),
]


class RedirectAnonymousUser:

    def process_request(self, request):
        if not request.user.is_authenticated() and request.path not in ALLOWED_URLs:
            return redirect(reverse('company:login'))